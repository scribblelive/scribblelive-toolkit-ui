var ScribbleToolkit =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _es6Promise = __webpack_require__(22);

	var _es6Promise2 = _interopRequireDefault(_es6Promise);

	var _render = __webpack_require__(24);

	var _render2 = _interopRequireDefault(_render);

	var _renderingOptions = __webpack_require__(29);

	var _renderingOptions2 = _interopRequireDefault(_renderingOptions);

	var _coreWrapper = __webpack_require__(89);

	var _coreWrapper2 = _interopRequireDefault(_coreWrapper);

	var _postFunctions = __webpack_require__(183);

	var _uiConfigService = __webpack_require__(182);

	var _uiConfigService2 = _interopRequireDefault(_uiConfigService);

	var _consentForm = __webpack_require__(185);

	var _consentForm2 = _interopRequireDefault(_consentForm);

	var _postStore = __webpack_require__(28);

	var _postStore2 = _interopRequireDefault(_postStore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	if (typeof window !== 'undefined' && !window.Promise) {
	  window.Promise = _es6Promise2.default;
	}

	var ScribbleToolkitUi = function () {
	  function ScribbleToolkitUi(options) {
	    (0, _classCallCheck3.default)(this, ScribbleToolkitUi);

	    _renderingOptions2.default.init(options);
	    this.Render = _render2.default;
	    var token = options.token;
	    (0, _coreWrapper.initialize)({ token: token, options: options });
	    this.isNonTracking = options.isNonTracking;
	    _uiConfigService2.default.init(options.uiOptions || {});
	    _uiConfigService2.default.pageSize = options.paging ? options.paging.pageSize : 10;
	    _uiConfigService2.default.newestAtBottom = options.newestAtBottom;
	    var baseUrl = options.baseUrl ? options.baseUrl : 'https://api.scribblelive.com/v1';
	    _consentForm2.default.init({ token: token, baseUrl: baseUrl });
	  }

	  (0, _createClass3.default)(ScribbleToolkitUi, [{
	    key: 'init',
	    value: function init(streamId) {
	      var _this = this;

	      _consentForm2.default.getClientData().then(function () {
	        renderPosts();
	      }).catch(function (e) {
	        console.log(e);
	        renderPosts();
	      });

	      var renderPosts = function renderPosts() {
	        if (_renderingOptions2.default.posts && _renderingOptions2.default.posts.length > 0) {
	          _this.Render.list(_renderingOptions2.default.posts);
	          return;
	        }
	        _coreWrapper2.default.instance.Stream.loadWithOptions({ streamId: streamId,
	          pageSize: _uiConfigService2.default.pageSize,
	          newestAtBottom: _uiConfigService2.default.newestAtBottom }, postCallback);

	        window.addEventListener('message', function (message) {
	          if (message.origin.indexOf('scribblelive.com') === -1) return;
	          if (!message.data.action) return;
	          if (!message.data.iframeName) return;

	          if (message.data.action === 'REMOVE_TRACKING_POST_SLIDESHOW') {
	            var child = document.querySelector('iframe[name="' + message.data.iframeName + '"').closest('.SL-HTML');
	            if (child) {
	              child.parentNode.removeChild(child);
	            }
	          }
	          if (message.data.action === 'GET_FILTERTRACKING_COOKIE') {
	            var payload = {
	              action: 'POST_FILTERTRACKING_COOKIE',
	              value: _postStore2.default.consentFormData.filterPost ? true : false
	            };
	            var frm = window.frames[message.data.iframeName];
	            frm.postMessage(payload, '*');
	          }
	        });
	      };

	      var postCallback = function postCallback(err, data) {
	        if (err) {
	          console.log(err);
	          return;
	        }

	        if (data.adData) {

	          _this.Render.adData(data.adData);
	        }

	        var postsWithStreamIds = (0, _postFunctions.addStreamIds)(data.posts, streamId);

	        if (data.pagination && _uiConfigService2.default.currentPage <= data.pagination.TotalPages - 1) {

	          _uiConfigService2.default.incrementCurrentPage();

	          _coreWrapper2.default.instance.Stream.loadWithOptions({ streamId: streamId,
	            pageNum: _uiConfigService2.default.currentPage,
	            pageSize: _uiConfigService2.default.pageSize,
	            newestAtBottom: _uiConfigService2.default.newestAtBottom }, postCallback);
	        } else if (data.pagination && _uiConfigService2.default.currentPage > data.pagination.TotalPages - 1) {
	          _uiConfigService2.default.incrementCurrentPage(); //increment so we don't come into this func again
	          _coreWrapper2.default.instance.Stream.poll(streamId, postCallback); //start polling when everything is done
	        }

	        _this.Render.list(postsWithStreamIds);

	        if (!_this.isNonTracking) {
	          FB.XFBML.parse();
	          if (twttr && twttr.widgets && twttr.widgets) {
	            twttr.widgets.load();
	          }
	        }

	        SCRBBL.post.go();
	      };
	    }
	  }]);
	  return ScribbleToolkitUi;
	}();

	module.exports = ScribbleToolkitUi;

/***/ }),
/* 1 */
/***/ (function(module, exports) {

	"use strict";

	exports.__esModule = true;

	exports.default = function (instance, Constructor) {
	  if (!(instance instanceof Constructor)) {
	    throw new TypeError("Cannot call a class as a function");
	  }
	};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	exports.__esModule = true;

	var _defineProperty = __webpack_require__(3);

	var _defineProperty2 = _interopRequireDefault(_defineProperty);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = function () {
	  function defineProperties(target, props) {
	    for (var i = 0; i < props.length; i++) {
	      var descriptor = props[i];
	      descriptor.enumerable = descriptor.enumerable || false;
	      descriptor.configurable = true;
	      if ("value" in descriptor) descriptor.writable = true;
	      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
	    }
	  }

	  return function (Constructor, protoProps, staticProps) {
	    if (protoProps) defineProperties(Constructor.prototype, protoProps);
	    if (staticProps) defineProperties(Constructor, staticProps);
	    return Constructor;
	  };
	}();

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(4), __esModule: true };

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(5);
	var $Object = __webpack_require__(8).Object;
	module.exports = function defineProperty(it, key, desc) {
	  return $Object.defineProperty(it, key, desc);
	};


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

	var $export = __webpack_require__(6);
	// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
	$export($export.S + $export.F * !__webpack_require__(16), 'Object', { defineProperty: __webpack_require__(12).f });


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	var global = __webpack_require__(7);
	var core = __webpack_require__(8);
	var ctx = __webpack_require__(9);
	var hide = __webpack_require__(11);
	var has = __webpack_require__(21);
	var PROTOTYPE = 'prototype';

	var $export = function (type, name, source) {
	  var IS_FORCED = type & $export.F;
	  var IS_GLOBAL = type & $export.G;
	  var IS_STATIC = type & $export.S;
	  var IS_PROTO = type & $export.P;
	  var IS_BIND = type & $export.B;
	  var IS_WRAP = type & $export.W;
	  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
	  var expProto = exports[PROTOTYPE];
	  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
	  var key, own, out;
	  if (IS_GLOBAL) source = name;
	  for (key in source) {
	    // contains in native
	    own = !IS_FORCED && target && target[key] !== undefined;
	    if (own && has(exports, key)) continue;
	    // export native or passed
	    out = own ? target[key] : source[key];
	    // prevent global pollution for namespaces
	    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
	    // bind timers to global for call from export context
	    : IS_BIND && own ? ctx(out, global)
	    // wrap global constructors for prevent change them in library
	    : IS_WRAP && target[key] == out ? (function (C) {
	      var F = function (a, b, c) {
	        if (this instanceof C) {
	          switch (arguments.length) {
	            case 0: return new C();
	            case 1: return new C(a);
	            case 2: return new C(a, b);
	          } return new C(a, b, c);
	        } return C.apply(this, arguments);
	      };
	      F[PROTOTYPE] = C[PROTOTYPE];
	      return F;
	    // make static versions for prototype methods
	    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
	    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
	    if (IS_PROTO) {
	      (exports.virtual || (exports.virtual = {}))[key] = out;
	      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
	      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
	    }
	  }
	};
	// type bitmap
	$export.F = 1;   // forced
	$export.G = 2;   // global
	$export.S = 4;   // static
	$export.P = 8;   // proto
	$export.B = 16;  // bind
	$export.W = 32;  // wrap
	$export.U = 64;  // safe
	$export.R = 128; // real proto method for `library`
	module.exports = $export;


/***/ }),
/* 7 */
/***/ (function(module, exports) {

	// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
	var global = module.exports = typeof window != 'undefined' && window.Math == Math
	  ? window : typeof self != 'undefined' && self.Math == Math ? self
	  // eslint-disable-next-line no-new-func
	  : Function('return this')();
	if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),
/* 8 */
/***/ (function(module, exports) {

	var core = module.exports = { version: '2.6.9' };
	if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

	// optional / simple context binding
	var aFunction = __webpack_require__(10);
	module.exports = function (fn, that, length) {
	  aFunction(fn);
	  if (that === undefined) return fn;
	  switch (length) {
	    case 1: return function (a) {
	      return fn.call(that, a);
	    };
	    case 2: return function (a, b) {
	      return fn.call(that, a, b);
	    };
	    case 3: return function (a, b, c) {
	      return fn.call(that, a, b, c);
	    };
	  }
	  return function (/* ...args */) {
	    return fn.apply(that, arguments);
	  };
	};


/***/ }),
/* 10 */
/***/ (function(module, exports) {

	module.exports = function (it) {
	  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
	  return it;
	};


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

	var dP = __webpack_require__(12);
	var createDesc = __webpack_require__(20);
	module.exports = __webpack_require__(16) ? function (object, key, value) {
	  return dP.f(object, key, createDesc(1, value));
	} : function (object, key, value) {
	  object[key] = value;
	  return object;
	};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

	var anObject = __webpack_require__(13);
	var IE8_DOM_DEFINE = __webpack_require__(15);
	var toPrimitive = __webpack_require__(19);
	var dP = Object.defineProperty;

	exports.f = __webpack_require__(16) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
	  anObject(O);
	  P = toPrimitive(P, true);
	  anObject(Attributes);
	  if (IE8_DOM_DEFINE) try {
	    return dP(O, P, Attributes);
	  } catch (e) { /* empty */ }
	  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
	  if ('value' in Attributes) O[P] = Attributes.value;
	  return O;
	};


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(14);
	module.exports = function (it) {
	  if (!isObject(it)) throw TypeError(it + ' is not an object!');
	  return it;
	};


/***/ }),
/* 14 */
/***/ (function(module, exports) {

	module.exports = function (it) {
	  return typeof it === 'object' ? it !== null : typeof it === 'function';
	};


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = !__webpack_require__(16) && !__webpack_require__(17)(function () {
	  return Object.defineProperty(__webpack_require__(18)('div'), 'a', { get: function () { return 7; } }).a != 7;
	});


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

	// Thank's IE8 for his funny defineProperty
	module.exports = !__webpack_require__(17)(function () {
	  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
	});


/***/ }),
/* 17 */
/***/ (function(module, exports) {

	module.exports = function (exec) {
	  try {
	    return !!exec();
	  } catch (e) {
	    return true;
	  }
	};


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(14);
	var document = __webpack_require__(7).document;
	// typeof document.createElement is 'object' in old IE
	var is = isObject(document) && isObject(document.createElement);
	module.exports = function (it) {
	  return is ? document.createElement(it) : {};
	};


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

	// 7.1.1 ToPrimitive(input [, PreferredType])
	var isObject = __webpack_require__(14);
	// instead of the ES6 spec version, we didn't implement @@toPrimitive case
	// and the second argument - flag - preferred type is a string
	module.exports = function (it, S) {
	  if (!isObject(it)) return it;
	  var fn, val;
	  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
	  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
	  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
	  throw TypeError("Can't convert object to primitive value");
	};


/***/ }),
/* 20 */
/***/ (function(module, exports) {

	module.exports = function (bitmap, value) {
	  return {
	    enumerable: !(bitmap & 1),
	    configurable: !(bitmap & 2),
	    writable: !(bitmap & 4),
	    value: value
	  };
	};


/***/ }),
/* 21 */
/***/ (function(module, exports) {

	var hasOwnProperty = {}.hasOwnProperty;
	module.exports = function (it, key) {
	  return hasOwnProperty.call(it, key);
	};


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process, global) {/*!
	 * @overview es6-promise - a tiny implementation of Promises/A+.
	 * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
	 * @license   Licensed under MIT license
	 *            See https://raw.githubusercontent.com/stefanpenner/es6-promise/master/LICENSE
	 * @version   v4.2.8+1e68dce6
	 */

	(function (global, factory) {
		 true ? module.exports = factory() :
		typeof define === 'function' && define.amd ? define(factory) :
		(global.ES6Promise = factory());
	}(this, (function () { 'use strict';

	function objectOrFunction(x) {
	  var type = typeof x;
	  return x !== null && (type === 'object' || type === 'function');
	}

	function isFunction(x) {
	  return typeof x === 'function';
	}



	var _isArray = void 0;
	if (Array.isArray) {
	  _isArray = Array.isArray;
	} else {
	  _isArray = function (x) {
	    return Object.prototype.toString.call(x) === '[object Array]';
	  };
	}

	var isArray = _isArray;

	var len = 0;
	var vertxNext = void 0;
	var customSchedulerFn = void 0;

	var asap = function asap(callback, arg) {
	  queue[len] = callback;
	  queue[len + 1] = arg;
	  len += 2;
	  if (len === 2) {
	    // If len is 2, that means that we need to schedule an async flush.
	    // If additional callbacks are queued before the queue is flushed, they
	    // will be processed by this flush that we are scheduling.
	    if (customSchedulerFn) {
	      customSchedulerFn(flush);
	    } else {
	      scheduleFlush();
	    }
	  }
	};

	function setScheduler(scheduleFn) {
	  customSchedulerFn = scheduleFn;
	}

	function setAsap(asapFn) {
	  asap = asapFn;
	}

	var browserWindow = typeof window !== 'undefined' ? window : undefined;
	var browserGlobal = browserWindow || {};
	var BrowserMutationObserver = browserGlobal.MutationObserver || browserGlobal.WebKitMutationObserver;
	var isNode = typeof self === 'undefined' && typeof process !== 'undefined' && {}.toString.call(process) === '[object process]';

	// test for web worker but not in IE10
	var isWorker = typeof Uint8ClampedArray !== 'undefined' && typeof importScripts !== 'undefined' && typeof MessageChannel !== 'undefined';

	// node
	function useNextTick() {
	  // node version 0.10.x displays a deprecation warning when nextTick is used recursively
	  // see https://github.com/cujojs/when/issues/410 for details
	  return function () {
	    return process.nextTick(flush);
	  };
	}

	// vertx
	function useVertxTimer() {
	  if (typeof vertxNext !== 'undefined') {
	    return function () {
	      vertxNext(flush);
	    };
	  }

	  return useSetTimeout();
	}

	function useMutationObserver() {
	  var iterations = 0;
	  var observer = new BrowserMutationObserver(flush);
	  var node = document.createTextNode('');
	  observer.observe(node, { characterData: true });

	  return function () {
	    node.data = iterations = ++iterations % 2;
	  };
	}

	// web worker
	function useMessageChannel() {
	  var channel = new MessageChannel();
	  channel.port1.onmessage = flush;
	  return function () {
	    return channel.port2.postMessage(0);
	  };
	}

	function useSetTimeout() {
	  // Store setTimeout reference so es6-promise will be unaffected by
	  // other code modifying setTimeout (like sinon.useFakeTimers())
	  var globalSetTimeout = setTimeout;
	  return function () {
	    return globalSetTimeout(flush, 1);
	  };
	}

	var queue = new Array(1000);
	function flush() {
	  for (var i = 0; i < len; i += 2) {
	    var callback = queue[i];
	    var arg = queue[i + 1];

	    callback(arg);

	    queue[i] = undefined;
	    queue[i + 1] = undefined;
	  }

	  len = 0;
	}

	function attemptVertx() {
	  try {
	    var vertx = Function('return this')().require('vertx');
	    vertxNext = vertx.runOnLoop || vertx.runOnContext;
	    return useVertxTimer();
	  } catch (e) {
	    return useSetTimeout();
	  }
	}

	var scheduleFlush = void 0;
	// Decide what async method to use to triggering processing of queued callbacks:
	if (isNode) {
	  scheduleFlush = useNextTick();
	} else if (BrowserMutationObserver) {
	  scheduleFlush = useMutationObserver();
	} else if (isWorker) {
	  scheduleFlush = useMessageChannel();
	} else if (browserWindow === undefined && "function" === 'function') {
	  scheduleFlush = attemptVertx();
	} else {
	  scheduleFlush = useSetTimeout();
	}

	function then(onFulfillment, onRejection) {
	  var parent = this;

	  var child = new this.constructor(noop);

	  if (child[PROMISE_ID] === undefined) {
	    makePromise(child);
	  }

	  var _state = parent._state;


	  if (_state) {
	    var callback = arguments[_state - 1];
	    asap(function () {
	      return invokeCallback(_state, child, callback, parent._result);
	    });
	  } else {
	    subscribe(parent, child, onFulfillment, onRejection);
	  }

	  return child;
	}

	/**
	  `Promise.resolve` returns a promise that will become resolved with the
	  passed `value`. It is shorthand for the following:

	  ```javascript
	  let promise = new Promise(function(resolve, reject){
	    resolve(1);
	  });

	  promise.then(function(value){
	    // value === 1
	  });
	  ```

	  Instead of writing the above, your code now simply becomes the following:

	  ```javascript
	  let promise = Promise.resolve(1);

	  promise.then(function(value){
	    // value === 1
	  });
	  ```

	  @method resolve
	  @static
	  @param {Any} value value that the returned promise will be resolved with
	  Useful for tooling.
	  @return {Promise} a promise that will become fulfilled with the given
	  `value`
	*/
	function resolve$1(object) {
	  /*jshint validthis:true */
	  var Constructor = this;

	  if (object && typeof object === 'object' && object.constructor === Constructor) {
	    return object;
	  }

	  var promise = new Constructor(noop);
	  resolve(promise, object);
	  return promise;
	}

	var PROMISE_ID = Math.random().toString(36).substring(2);

	function noop() {}

	var PENDING = void 0;
	var FULFILLED = 1;
	var REJECTED = 2;

	function selfFulfillment() {
	  return new TypeError("You cannot resolve a promise with itself");
	}

	function cannotReturnOwn() {
	  return new TypeError('A promises callback cannot return that same promise.');
	}

	function tryThen(then$$1, value, fulfillmentHandler, rejectionHandler) {
	  try {
	    then$$1.call(value, fulfillmentHandler, rejectionHandler);
	  } catch (e) {
	    return e;
	  }
	}

	function handleForeignThenable(promise, thenable, then$$1) {
	  asap(function (promise) {
	    var sealed = false;
	    var error = tryThen(then$$1, thenable, function (value) {
	      if (sealed) {
	        return;
	      }
	      sealed = true;
	      if (thenable !== value) {
	        resolve(promise, value);
	      } else {
	        fulfill(promise, value);
	      }
	    }, function (reason) {
	      if (sealed) {
	        return;
	      }
	      sealed = true;

	      reject(promise, reason);
	    }, 'Settle: ' + (promise._label || ' unknown promise'));

	    if (!sealed && error) {
	      sealed = true;
	      reject(promise, error);
	    }
	  }, promise);
	}

	function handleOwnThenable(promise, thenable) {
	  if (thenable._state === FULFILLED) {
	    fulfill(promise, thenable._result);
	  } else if (thenable._state === REJECTED) {
	    reject(promise, thenable._result);
	  } else {
	    subscribe(thenable, undefined, function (value) {
	      return resolve(promise, value);
	    }, function (reason) {
	      return reject(promise, reason);
	    });
	  }
	}

	function handleMaybeThenable(promise, maybeThenable, then$$1) {
	  if (maybeThenable.constructor === promise.constructor && then$$1 === then && maybeThenable.constructor.resolve === resolve$1) {
	    handleOwnThenable(promise, maybeThenable);
	  } else {
	    if (then$$1 === undefined) {
	      fulfill(promise, maybeThenable);
	    } else if (isFunction(then$$1)) {
	      handleForeignThenable(promise, maybeThenable, then$$1);
	    } else {
	      fulfill(promise, maybeThenable);
	    }
	  }
	}

	function resolve(promise, value) {
	  if (promise === value) {
	    reject(promise, selfFulfillment());
	  } else if (objectOrFunction(value)) {
	    var then$$1 = void 0;
	    try {
	      then$$1 = value.then;
	    } catch (error) {
	      reject(promise, error);
	      return;
	    }
	    handleMaybeThenable(promise, value, then$$1);
	  } else {
	    fulfill(promise, value);
	  }
	}

	function publishRejection(promise) {
	  if (promise._onerror) {
	    promise._onerror(promise._result);
	  }

	  publish(promise);
	}

	function fulfill(promise, value) {
	  if (promise._state !== PENDING) {
	    return;
	  }

	  promise._result = value;
	  promise._state = FULFILLED;

	  if (promise._subscribers.length !== 0) {
	    asap(publish, promise);
	  }
	}

	function reject(promise, reason) {
	  if (promise._state !== PENDING) {
	    return;
	  }
	  promise._state = REJECTED;
	  promise._result = reason;

	  asap(publishRejection, promise);
	}

	function subscribe(parent, child, onFulfillment, onRejection) {
	  var _subscribers = parent._subscribers;
	  var length = _subscribers.length;


	  parent._onerror = null;

	  _subscribers[length] = child;
	  _subscribers[length + FULFILLED] = onFulfillment;
	  _subscribers[length + REJECTED] = onRejection;

	  if (length === 0 && parent._state) {
	    asap(publish, parent);
	  }
	}

	function publish(promise) {
	  var subscribers = promise._subscribers;
	  var settled = promise._state;

	  if (subscribers.length === 0) {
	    return;
	  }

	  var child = void 0,
	      callback = void 0,
	      detail = promise._result;

	  for (var i = 0; i < subscribers.length; i += 3) {
	    child = subscribers[i];
	    callback = subscribers[i + settled];

	    if (child) {
	      invokeCallback(settled, child, callback, detail);
	    } else {
	      callback(detail);
	    }
	  }

	  promise._subscribers.length = 0;
	}

	function invokeCallback(settled, promise, callback, detail) {
	  var hasCallback = isFunction(callback),
	      value = void 0,
	      error = void 0,
	      succeeded = true;

	  if (hasCallback) {
	    try {
	      value = callback(detail);
	    } catch (e) {
	      succeeded = false;
	      error = e;
	    }

	    if (promise === value) {
	      reject(promise, cannotReturnOwn());
	      return;
	    }
	  } else {
	    value = detail;
	  }

	  if (promise._state !== PENDING) {
	    // noop
	  } else if (hasCallback && succeeded) {
	    resolve(promise, value);
	  } else if (succeeded === false) {
	    reject(promise, error);
	  } else if (settled === FULFILLED) {
	    fulfill(promise, value);
	  } else if (settled === REJECTED) {
	    reject(promise, value);
	  }
	}

	function initializePromise(promise, resolver) {
	  try {
	    resolver(function resolvePromise(value) {
	      resolve(promise, value);
	    }, function rejectPromise(reason) {
	      reject(promise, reason);
	    });
	  } catch (e) {
	    reject(promise, e);
	  }
	}

	var id = 0;
	function nextId() {
	  return id++;
	}

	function makePromise(promise) {
	  promise[PROMISE_ID] = id++;
	  promise._state = undefined;
	  promise._result = undefined;
	  promise._subscribers = [];
	}

	function validationError() {
	  return new Error('Array Methods must be provided an Array');
	}

	var Enumerator = function () {
	  function Enumerator(Constructor, input) {
	    this._instanceConstructor = Constructor;
	    this.promise = new Constructor(noop);

	    if (!this.promise[PROMISE_ID]) {
	      makePromise(this.promise);
	    }

	    if (isArray(input)) {
	      this.length = input.length;
	      this._remaining = input.length;

	      this._result = new Array(this.length);

	      if (this.length === 0) {
	        fulfill(this.promise, this._result);
	      } else {
	        this.length = this.length || 0;
	        this._enumerate(input);
	        if (this._remaining === 0) {
	          fulfill(this.promise, this._result);
	        }
	      }
	    } else {
	      reject(this.promise, validationError());
	    }
	  }

	  Enumerator.prototype._enumerate = function _enumerate(input) {
	    for (var i = 0; this._state === PENDING && i < input.length; i++) {
	      this._eachEntry(input[i], i);
	    }
	  };

	  Enumerator.prototype._eachEntry = function _eachEntry(entry, i) {
	    var c = this._instanceConstructor;
	    var resolve$$1 = c.resolve;


	    if (resolve$$1 === resolve$1) {
	      var _then = void 0;
	      var error = void 0;
	      var didError = false;
	      try {
	        _then = entry.then;
	      } catch (e) {
	        didError = true;
	        error = e;
	      }

	      if (_then === then && entry._state !== PENDING) {
	        this._settledAt(entry._state, i, entry._result);
	      } else if (typeof _then !== 'function') {
	        this._remaining--;
	        this._result[i] = entry;
	      } else if (c === Promise$1) {
	        var promise = new c(noop);
	        if (didError) {
	          reject(promise, error);
	        } else {
	          handleMaybeThenable(promise, entry, _then);
	        }
	        this._willSettleAt(promise, i);
	      } else {
	        this._willSettleAt(new c(function (resolve$$1) {
	          return resolve$$1(entry);
	        }), i);
	      }
	    } else {
	      this._willSettleAt(resolve$$1(entry), i);
	    }
	  };

	  Enumerator.prototype._settledAt = function _settledAt(state, i, value) {
	    var promise = this.promise;


	    if (promise._state === PENDING) {
	      this._remaining--;

	      if (state === REJECTED) {
	        reject(promise, value);
	      } else {
	        this._result[i] = value;
	      }
	    }

	    if (this._remaining === 0) {
	      fulfill(promise, this._result);
	    }
	  };

	  Enumerator.prototype._willSettleAt = function _willSettleAt(promise, i) {
	    var enumerator = this;

	    subscribe(promise, undefined, function (value) {
	      return enumerator._settledAt(FULFILLED, i, value);
	    }, function (reason) {
	      return enumerator._settledAt(REJECTED, i, reason);
	    });
	  };

	  return Enumerator;
	}();

	/**
	  `Promise.all` accepts an array of promises, and returns a new promise which
	  is fulfilled with an array of fulfillment values for the passed promises, or
	  rejected with the reason of the first passed promise to be rejected. It casts all
	  elements of the passed iterable to promises as it runs this algorithm.

	  Example:

	  ```javascript
	  let promise1 = resolve(1);
	  let promise2 = resolve(2);
	  let promise3 = resolve(3);
	  let promises = [ promise1, promise2, promise3 ];

	  Promise.all(promises).then(function(array){
	    // The array here would be [ 1, 2, 3 ];
	  });
	  ```

	  If any of the `promises` given to `all` are rejected, the first promise
	  that is rejected will be given as an argument to the returned promises's
	  rejection handler. For example:

	  Example:

	  ```javascript
	  let promise1 = resolve(1);
	  let promise2 = reject(new Error("2"));
	  let promise3 = reject(new Error("3"));
	  let promises = [ promise1, promise2, promise3 ];

	  Promise.all(promises).then(function(array){
	    // Code here never runs because there are rejected promises!
	  }, function(error) {
	    // error.message === "2"
	  });
	  ```

	  @method all
	  @static
	  @param {Array} entries array of promises
	  @param {String} label optional string for labeling the promise.
	  Useful for tooling.
	  @return {Promise} promise that is fulfilled when all `promises` have been
	  fulfilled, or rejected if any of them become rejected.
	  @static
	*/
	function all(entries) {
	  return new Enumerator(this, entries).promise;
	}

	/**
	  `Promise.race` returns a new promise which is settled in the same way as the
	  first passed promise to settle.

	  Example:

	  ```javascript
	  let promise1 = new Promise(function(resolve, reject){
	    setTimeout(function(){
	      resolve('promise 1');
	    }, 200);
	  });

	  let promise2 = new Promise(function(resolve, reject){
	    setTimeout(function(){
	      resolve('promise 2');
	    }, 100);
	  });

	  Promise.race([promise1, promise2]).then(function(result){
	    // result === 'promise 2' because it was resolved before promise1
	    // was resolved.
	  });
	  ```

	  `Promise.race` is deterministic in that only the state of the first
	  settled promise matters. For example, even if other promises given to the
	  `promises` array argument are resolved, but the first settled promise has
	  become rejected before the other promises became fulfilled, the returned
	  promise will become rejected:

	  ```javascript
	  let promise1 = new Promise(function(resolve, reject){
	    setTimeout(function(){
	      resolve('promise 1');
	    }, 200);
	  });

	  let promise2 = new Promise(function(resolve, reject){
	    setTimeout(function(){
	      reject(new Error('promise 2'));
	    }, 100);
	  });

	  Promise.race([promise1, promise2]).then(function(result){
	    // Code here never runs
	  }, function(reason){
	    // reason.message === 'promise 2' because promise 2 became rejected before
	    // promise 1 became fulfilled
	  });
	  ```

	  An example real-world use case is implementing timeouts:

	  ```javascript
	  Promise.race([ajax('foo.json'), timeout(5000)])
	  ```

	  @method race
	  @static
	  @param {Array} promises array of promises to observe
	  Useful for tooling.
	  @return {Promise} a promise which settles in the same way as the first passed
	  promise to settle.
	*/
	function race(entries) {
	  /*jshint validthis:true */
	  var Constructor = this;

	  if (!isArray(entries)) {
	    return new Constructor(function (_, reject) {
	      return reject(new TypeError('You must pass an array to race.'));
	    });
	  } else {
	    return new Constructor(function (resolve, reject) {
	      var length = entries.length;
	      for (var i = 0; i < length; i++) {
	        Constructor.resolve(entries[i]).then(resolve, reject);
	      }
	    });
	  }
	}

	/**
	  `Promise.reject` returns a promise rejected with the passed `reason`.
	  It is shorthand for the following:

	  ```javascript
	  let promise = new Promise(function(resolve, reject){
	    reject(new Error('WHOOPS'));
	  });

	  promise.then(function(value){
	    // Code here doesn't run because the promise is rejected!
	  }, function(reason){
	    // reason.message === 'WHOOPS'
	  });
	  ```

	  Instead of writing the above, your code now simply becomes the following:

	  ```javascript
	  let promise = Promise.reject(new Error('WHOOPS'));

	  promise.then(function(value){
	    // Code here doesn't run because the promise is rejected!
	  }, function(reason){
	    // reason.message === 'WHOOPS'
	  });
	  ```

	  @method reject
	  @static
	  @param {Any} reason value that the returned promise will be rejected with.
	  Useful for tooling.
	  @return {Promise} a promise rejected with the given `reason`.
	*/
	function reject$1(reason) {
	  /*jshint validthis:true */
	  var Constructor = this;
	  var promise = new Constructor(noop);
	  reject(promise, reason);
	  return promise;
	}

	function needsResolver() {
	  throw new TypeError('You must pass a resolver function as the first argument to the promise constructor');
	}

	function needsNew() {
	  throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
	}

	/**
	  Promise objects represent the eventual result of an asynchronous operation. The
	  primary way of interacting with a promise is through its `then` method, which
	  registers callbacks to receive either a promise's eventual value or the reason
	  why the promise cannot be fulfilled.

	  Terminology
	  -----------

	  - `promise` is an object or function with a `then` method whose behavior conforms to this specification.
	  - `thenable` is an object or function that defines a `then` method.
	  - `value` is any legal JavaScript value (including undefined, a thenable, or a promise).
	  - `exception` is a value that is thrown using the throw statement.
	  - `reason` is a value that indicates why a promise was rejected.
	  - `settled` the final resting state of a promise, fulfilled or rejected.

	  A promise can be in one of three states: pending, fulfilled, or rejected.

	  Promises that are fulfilled have a fulfillment value and are in the fulfilled
	  state.  Promises that are rejected have a rejection reason and are in the
	  rejected state.  A fulfillment value is never a thenable.

	  Promises can also be said to *resolve* a value.  If this value is also a
	  promise, then the original promise's settled state will match the value's
	  settled state.  So a promise that *resolves* a promise that rejects will
	  itself reject, and a promise that *resolves* a promise that fulfills will
	  itself fulfill.


	  Basic Usage:
	  ------------

	  ```js
	  let promise = new Promise(function(resolve, reject) {
	    // on success
	    resolve(value);

	    // on failure
	    reject(reason);
	  });

	  promise.then(function(value) {
	    // on fulfillment
	  }, function(reason) {
	    // on rejection
	  });
	  ```

	  Advanced Usage:
	  ---------------

	  Promises shine when abstracting away asynchronous interactions such as
	  `XMLHttpRequest`s.

	  ```js
	  function getJSON(url) {
	    return new Promise(function(resolve, reject){
	      let xhr = new XMLHttpRequest();

	      xhr.open('GET', url);
	      xhr.onreadystatechange = handler;
	      xhr.responseType = 'json';
	      xhr.setRequestHeader('Accept', 'application/json');
	      xhr.send();

	      function handler() {
	        if (this.readyState === this.DONE) {
	          if (this.status === 200) {
	            resolve(this.response);
	          } else {
	            reject(new Error('getJSON: `' + url + '` failed with status: [' + this.status + ']'));
	          }
	        }
	      };
	    });
	  }

	  getJSON('/posts.json').then(function(json) {
	    // on fulfillment
	  }, function(reason) {
	    // on rejection
	  });
	  ```

	  Unlike callbacks, promises are great composable primitives.

	  ```js
	  Promise.all([
	    getJSON('/posts'),
	    getJSON('/comments')
	  ]).then(function(values){
	    values[0] // => postsJSON
	    values[1] // => commentsJSON

	    return values;
	  });
	  ```

	  @class Promise
	  @param {Function} resolver
	  Useful for tooling.
	  @constructor
	*/

	var Promise$1 = function () {
	  function Promise(resolver) {
	    this[PROMISE_ID] = nextId();
	    this._result = this._state = undefined;
	    this._subscribers = [];

	    if (noop !== resolver) {
	      typeof resolver !== 'function' && needsResolver();
	      this instanceof Promise ? initializePromise(this, resolver) : needsNew();
	    }
	  }

	  /**
	  The primary way of interacting with a promise is through its `then` method,
	  which registers callbacks to receive either a promise's eventual value or the
	  reason why the promise cannot be fulfilled.
	   ```js
	  findUser().then(function(user){
	    // user is available
	  }, function(reason){
	    // user is unavailable, and you are given the reason why
	  });
	  ```
	   Chaining
	  --------
	   The return value of `then` is itself a promise.  This second, 'downstream'
	  promise is resolved with the return value of the first promise's fulfillment
	  or rejection handler, or rejected if the handler throws an exception.
	   ```js
	  findUser().then(function (user) {
	    return user.name;
	  }, function (reason) {
	    return 'default name';
	  }).then(function (userName) {
	    // If `findUser` fulfilled, `userName` will be the user's name, otherwise it
	    // will be `'default name'`
	  });
	   findUser().then(function (user) {
	    throw new Error('Found user, but still unhappy');
	  }, function (reason) {
	    throw new Error('`findUser` rejected and we're unhappy');
	  }).then(function (value) {
	    // never reached
	  }, function (reason) {
	    // if `findUser` fulfilled, `reason` will be 'Found user, but still unhappy'.
	    // If `findUser` rejected, `reason` will be '`findUser` rejected and we're unhappy'.
	  });
	  ```
	  If the downstream promise does not specify a rejection handler, rejection reasons will be propagated further downstream.
	   ```js
	  findUser().then(function (user) {
	    throw new PedagogicalException('Upstream error');
	  }).then(function (value) {
	    // never reached
	  }).then(function (value) {
	    // never reached
	  }, function (reason) {
	    // The `PedgagocialException` is propagated all the way down to here
	  });
	  ```
	   Assimilation
	  ------------
	   Sometimes the value you want to propagate to a downstream promise can only be
	  retrieved asynchronously. This can be achieved by returning a promise in the
	  fulfillment or rejection handler. The downstream promise will then be pending
	  until the returned promise is settled. This is called *assimilation*.
	   ```js
	  findUser().then(function (user) {
	    return findCommentsByAuthor(user);
	  }).then(function (comments) {
	    // The user's comments are now available
	  });
	  ```
	   If the assimliated promise rejects, then the downstream promise will also reject.
	   ```js
	  findUser().then(function (user) {
	    return findCommentsByAuthor(user);
	  }).then(function (comments) {
	    // If `findCommentsByAuthor` fulfills, we'll have the value here
	  }, function (reason) {
	    // If `findCommentsByAuthor` rejects, we'll have the reason here
	  });
	  ```
	   Simple Example
	  --------------
	   Synchronous Example
	   ```javascript
	  let result;
	   try {
	    result = findResult();
	    // success
	  } catch(reason) {
	    // failure
	  }
	  ```
	   Errback Example
	   ```js
	  findResult(function(result, err){
	    if (err) {
	      // failure
	    } else {
	      // success
	    }
	  });
	  ```
	   Promise Example;
	   ```javascript
	  findResult().then(function(result){
	    // success
	  }, function(reason){
	    // failure
	  });
	  ```
	   Advanced Example
	  --------------
	   Synchronous Example
	   ```javascript
	  let author, books;
	   try {
	    author = findAuthor();
	    books  = findBooksByAuthor(author);
	    // success
	  } catch(reason) {
	    // failure
	  }
	  ```
	   Errback Example
	   ```js
	   function foundBooks(books) {
	   }
	   function failure(reason) {
	   }
	   findAuthor(function(author, err){
	    if (err) {
	      failure(err);
	      // failure
	    } else {
	      try {
	        findBoooksByAuthor(author, function(books, err) {
	          if (err) {
	            failure(err);
	          } else {
	            try {
	              foundBooks(books);
	            } catch(reason) {
	              failure(reason);
	            }
	          }
	        });
	      } catch(error) {
	        failure(err);
	      }
	      // success
	    }
	  });
	  ```
	   Promise Example;
	   ```javascript
	  findAuthor().
	    then(findBooksByAuthor).
	    then(function(books){
	      // found books
	  }).catch(function(reason){
	    // something went wrong
	  });
	  ```
	   @method then
	  @param {Function} onFulfilled
	  @param {Function} onRejected
	  Useful for tooling.
	  @return {Promise}
	  */

	  /**
	  `catch` is simply sugar for `then(undefined, onRejection)` which makes it the same
	  as the catch block of a try/catch statement.
	  ```js
	  function findAuthor(){
	  throw new Error('couldn't find that author');
	  }
	  // synchronous
	  try {
	  findAuthor();
	  } catch(reason) {
	  // something went wrong
	  }
	  // async with promises
	  findAuthor().catch(function(reason){
	  // something went wrong
	  });
	  ```
	  @method catch
	  @param {Function} onRejection
	  Useful for tooling.
	  @return {Promise}
	  */


	  Promise.prototype.catch = function _catch(onRejection) {
	    return this.then(null, onRejection);
	  };

	  /**
	    `finally` will be invoked regardless of the promise's fate just as native
	    try/catch/finally behaves
	  
	    Synchronous example:
	  
	    ```js
	    findAuthor() {
	      if (Math.random() > 0.5) {
	        throw new Error();
	      }
	      return new Author();
	    }
	  
	    try {
	      return findAuthor(); // succeed or fail
	    } catch(error) {
	      return findOtherAuther();
	    } finally {
	      // always runs
	      // doesn't affect the return value
	    }
	    ```
	  
	    Asynchronous example:
	  
	    ```js
	    findAuthor().catch(function(reason){
	      return findOtherAuther();
	    }).finally(function(){
	      // author was either found, or not
	    });
	    ```
	  
	    @method finally
	    @param {Function} callback
	    @return {Promise}
	  */


	  Promise.prototype.finally = function _finally(callback) {
	    var promise = this;
	    var constructor = promise.constructor;

	    if (isFunction(callback)) {
	      return promise.then(function (value) {
	        return constructor.resolve(callback()).then(function () {
	          return value;
	        });
	      }, function (reason) {
	        return constructor.resolve(callback()).then(function () {
	          throw reason;
	        });
	      });
	    }

	    return promise.then(callback, callback);
	  };

	  return Promise;
	}();

	Promise$1.prototype.then = then;
	Promise$1.all = all;
	Promise$1.race = race;
	Promise$1.resolve = resolve$1;
	Promise$1.reject = reject$1;
	Promise$1._setScheduler = setScheduler;
	Promise$1._setAsap = setAsap;
	Promise$1._asap = asap;

	/*global self*/
	function polyfill() {
	  var local = void 0;

	  if (typeof global !== 'undefined') {
	    local = global;
	  } else if (typeof self !== 'undefined') {
	    local = self;
	  } else {
	    try {
	      local = Function('return this')();
	    } catch (e) {
	      throw new Error('polyfill failed because global object is unavailable in this environment');
	    }
	  }

	  var P = local.Promise;

	  if (P) {
	    var promiseToString = null;
	    try {
	      promiseToString = Object.prototype.toString.call(P.resolve());
	    } catch (e) {
	      // silently ignored
	    }

	    if (promiseToString === '[object Promise]' && !P.cast) {
	      return;
	    }
	  }

	  local.Promise = Promise$1;
	}

	// Strange compat..
	Promise$1.polyfill = polyfill;
	Promise$1.Promise = Promise$1;

	return Promise$1;

	})));



	//# sourceMappingURL=es6-promise.map

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(23), (function() { return this; }())))

/***/ }),
/* 23 */
/***/ (function(module, exports) {

	// shim for using process in browser
	var process = module.exports = {};

	// cached from whatever global is present so that test runners that stub it
	// don't break things.  But we need to wrap it in a try catch in case it is
	// wrapped in strict mode code which doesn't define any globals.  It's inside a
	// function because try/catches deoptimize in certain engines.

	var cachedSetTimeout;
	var cachedClearTimeout;

	function defaultSetTimout() {
	    throw new Error('setTimeout has not been defined');
	}
	function defaultClearTimeout () {
	    throw new Error('clearTimeout has not been defined');
	}
	(function () {
	    try {
	        if (typeof setTimeout === 'function') {
	            cachedSetTimeout = setTimeout;
	        } else {
	            cachedSetTimeout = defaultSetTimout;
	        }
	    } catch (e) {
	        cachedSetTimeout = defaultSetTimout;
	    }
	    try {
	        if (typeof clearTimeout === 'function') {
	            cachedClearTimeout = clearTimeout;
	        } else {
	            cachedClearTimeout = defaultClearTimeout;
	        }
	    } catch (e) {
	        cachedClearTimeout = defaultClearTimeout;
	    }
	} ())
	function runTimeout(fun) {
	    if (cachedSetTimeout === setTimeout) {
	        //normal enviroments in sane situations
	        return setTimeout(fun, 0);
	    }
	    // if setTimeout wasn't available but was latter defined
	    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
	        cachedSetTimeout = setTimeout;
	        return setTimeout(fun, 0);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedSetTimeout(fun, 0);
	    } catch(e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
	            return cachedSetTimeout.call(null, fun, 0);
	        } catch(e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
	            return cachedSetTimeout.call(this, fun, 0);
	        }
	    }


	}
	function runClearTimeout(marker) {
	    if (cachedClearTimeout === clearTimeout) {
	        //normal enviroments in sane situations
	        return clearTimeout(marker);
	    }
	    // if clearTimeout wasn't available but was latter defined
	    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
	        cachedClearTimeout = clearTimeout;
	        return clearTimeout(marker);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedClearTimeout(marker);
	    } catch (e){
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
	            return cachedClearTimeout.call(null, marker);
	        } catch (e){
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
	            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
	            return cachedClearTimeout.call(this, marker);
	        }
	    }



	}
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;

	function cleanUpNextTick() {
	    if (!draining || !currentQueue) {
	        return;
	    }
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}

	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = runTimeout(cleanUpNextTick);
	    draining = true;

	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    runClearTimeout(timeout);
	}

	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        runTimeout(drainQueue);
	    }
	};

	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};

	function noop() {}

	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;
	process.prependListener = noop;
	process.prependOnceListener = noop;

	process.listeners = function (name) { return [] }

	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};

	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _postHelper = __webpack_require__(25);

	var _postHelper2 = _interopRequireDefault(_postHelper);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	/**
	 * RenderService contains wrapper functions to call PostHelper.
	 */
	var RenderService = function () {
	  function RenderService() {
	    (0, _classCallCheck3.default)(this, RenderService);
	  }

	  (0, _createClass3.default)(RenderService, null, [{
	    key: 'list',
	    value: function list(posts, options) {
	      posts.forEach(_postHelper2.default.update);
	      _postHelper2.default.sort();
	      _postHelper2.default.posts.forEach(function (post) {
	        return post.HasUpdated ? _postHelper2.default.sync(post) : '';
	      });
	      _postHelper2.default.organizeTypes();
	      _postHelper2.default.renderPosts();
	    }
	  }, {
	    key: 'adData',
	    value: function adData(data) {
	      _postHelper2.default.setAdData(data);
	    }
	  }]);
	  return RenderService;
	}();

	exports.default = RenderService;

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _stringify = __webpack_require__(26);

	var _stringify2 = _interopRequireDefault(_stringify);

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _postStore = __webpack_require__(28);

	var _postStore2 = _interopRequireDefault(_postStore);

	var _renderingOptions = __webpack_require__(29);

	var _renderingOptions2 = _interopRequireDefault(_renderingOptions);

	var _list = __webpack_require__(166);

	var _list2 = _interopRequireDefault(_list);

	var _footer = __webpack_require__(181);

	var _footer2 = _interopRequireDefault(_footer);

	var _helperFunctions = __webpack_require__(60);

	var _uiConfigService = __webpack_require__(182);

	var _uiConfigService2 = _interopRequireDefault(_uiConfigService);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	/**
	 * PostHelper controls the updating and placement of posts on the page.
	 */
	var PostHelper = function () {
	  function PostHelper() {
	    (0, _classCallCheck3.default)(this, PostHelper);
	  }

	  (0, _createClass3.default)(PostHelper, null, [{
	    key: 'getAdData',
	    value: function getAdData() {
	      return _postStore2.default.adData;
	    }
	  }, {
	    key: 'getAdScript',
	    value: function getAdScript() {
	      return _postStore2.default.adScriptEnabled;
	    }

	    /*
	        SETTERS
	     */

	  }, {
	    key: 'setAdScript',
	    value: function setAdScript(val) {
	      _postStore2.default.adScriptEnabled = val;
	    }

	    /*
	        STATIC FUNCTIONS
	     */

	    /**
	     * This function will mark, add, update and/or remove any new posts.
	     * @param post
	     */

	  }, {
	    key: 'update',
	    value: function update(post) {

	      post.HasUpdated = true;

	      if (post.IsDeleted === 1) {
	        // Delete post from DOM and STORE.
	        if (getPostIndex(post) !== -1) {
	          _postStore2.default.posts.splice(getPostIndex(post), 1);
	          return removePost(post);
	        }
	        return;
	      }

	      var postIndex = getPostIndex(post);
	      if (postIndex === -1) {
	        _postStore2.default.posts.push(post);
	        return;
	      }

	      var postDate = null;
	      var oldDate = null;
	      try {
	        postDate = post.LastModifiedDate ? new Date(post.LastModifiedDate) : new Date(post.Date + ' GMT');
	        oldDate = _postStore2.default.posts[postIndex].LastModifiedDate ? new Date(_postStore2.default.posts[postIndex].LastModifiedDate) : new Date(_postStore2.default.posts[postIndex].Date + ' GMT');
	      } catch (e) {
	        return;
	      }
	      if (postDate > oldDate) {
	        _postStore2.default.posts[postIndex] = post;
	      }
	    }

	    /**
	     * This function will sort posts by creationDate by default although allow
	     * a user to pass in a custom sorting method.
	     * @param sortFunc
	     */

	  }, {
	    key: 'sort',
	    value: function sort() {
	      var sortFunc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : sortPostsByCreationDate;

	      _postStore2.default.posts.sort(sortFunc);
	    }
	  }, {
	    key: 'organizeTypes',
	    value: function organizeTypes() {
	      var target = 'IMAGE';
	      var lastFound = -2;
	      // Copying posts to renderList

	      if (!_uiConfigService2.default.newestAtBottom) {
	        _postStore2.default.renderList = _postStore2.default.posts.reverse().map(function (post) {
	          return post;
	        });
	      } else {
	        _postStore2.default.renderList = _postStore2.default.posts.map(function (post) {
	          return post;
	        });
	      }

	      // Loop over renderList and find connected posts.
	      for (var i = 0; i < _postStore2.default.renderList.length; i++) {
	        if (_postStore2.default.renderList[i].Type === target) {
	          if (lastFound === i - 1) {
	            _postStore2.default.renderList[i - 1].imageClose = true;
	            _postStore2.default.renderList[i].imageClose = true;
	            lastFound = i;
	          } else {
	            lastFound = i;
	          }
	        }
	      }
	    }

	    /**
	     * This function will remove the HasUpdated flag, remove the post from the page
	     * and redraw the post on the page.
	     * @param post
	     */

	  }, {
	    key: 'sync',
	    value: function sync(post) {
	      _postStore2.default.posts[getPostIndex(post)].HasUpdated = false;
	      removePost(post);
	    }
	  }, {
	    key: 'isFilterNeeded',
	    value: function isFilterNeeded() {
	      return _postStore2.default.consentFormData.filterPost;
	    }
	  }, {
	    key: 'filterPosts',
	    value: function filterPosts(posts) {
	      return posts.filter(function (post) {
	        if (post.PostMeta && post.PostMeta.Type && (post.PostMeta.Type.indexOf('facebook:post') > -1 || post.PostMeta.Type.indexOf('instagram:post') > -1)) {
	          return false;
	        } else if (post.PostMeta && post.PostMeta.Data && ((0, _stringify2.default)(post.PostMeta.Data).indexOf('\"type\":\"FACEBOOK\"') > -1 || (0, _stringify2.default)(post.PostMeta.Data).indexOf('\"type\":\"INSTAGRAM\"') > -1)) {
	          return false;
	        } else if (post.Content && (post.Content.indexOf('data-url=\"https://www.instagram.com/') > -1 || post.Content.indexOf('href=\\"https://www.facebook.com/') > -1 || post.Content.indexOf(encodeURIComponent('data-url=\"https://www.instagram.com/')) > -1 || post.Content.indexOf(encodeURIComponent('href=\\"https://www.facebook.com/')) > -1 || post.Content.indexOf('data-url%3D%22https%3A//www.instagram.com/') > -1 || post.Content.indexOf('href%3D%22https%3A//www.facebook.com/') > -1)) {
	          return false;
	        }
	        return true;
	      });
	    }
	  }, {
	    key: 'renderPosts',
	    value: function renderPosts() {
	      if (_postStore2.default.consentFormData.showModal || _postStore2.default.consentFormData.loading) return;
	      _postStore2.default.renderList = this.isFilterNeeded() ? this.filterPosts(_postStore2.default.renderList) : _postStore2.default.renderList;
	      for (var i = 0; i < _postStore2.default.renderList.length; i++) {
	        var newPost = _postStore2.default.renderList[i];
	        putPost(newPost, i);
	      }
	      if (!_postStore2.default.footerRendered) {
	        this.renderFooter();
	      }
	    }
	  }, {
	    key: 'renderFooter',
	    value: function renderFooter() {
	      _renderingOptions2.default.rootEl.appendChild((0, _footer2.default)());
	      _postStore2.default.footerRendered = true;
	    }
	  }, {
	    key: 'setAdData',
	    value: function setAdData(data) {
	      _postStore2.default.adData = data;
	    }
	  }, {
	    key: 'posts',


	    /*
	        GETTERS
	     */

	    /**
	     * This is a getter which returns the posts in the STORE.
	     * @returns {Array}
	     */
	    get: function get() {
	      return _postStore2.default.posts;
	    }
	  }]);
	  return PostHelper;
	}();

	/**
	 * This function will find the passed in post within the store and return it's index.
	 * @param post
	 * @returns {number}
	 */


	exports.default = PostHelper;
	function getPostIndex(post) {
	  for (var i = 0; i < _postStore2.default.posts.length; i++) {
	    if (_postStore2.default.posts[i].Id === post.Id) {
	      return i;
	    }
	  }
	  return -1;
	}

	/**
	 * This function will check if a post is currently on the page.
	 * @param  {[type]} post [description]
	 * @return {[type]}      [description]
	 */
	function currentlyExistsOnPage(post) {
	  var target = document.getElementById('SL-' + post.Id);
	  return target !== undefined && target !== null;
	}

	/**
	 * This function will put a post on the page in the order it is listed.
	 * @param post
	 */
	function putPost(post, index) {
	  if (!post.HasUpdated && currentlyExistsOnPage(post)) return;
	  var htmlEle = null;
	  if (post instanceof _list2.default) {
	    htmlEle = post.render();
	  } else {
	    var renderFunc = _renderingOptions2.default.getRenderer(post.Type, post);
	    if (renderFunc === null || typeof renderFunc !== 'function') return;
	    htmlEle = renderFunc(post, index);
	  }

	  //check the current page number. then based on it, either prepend or append
	  //if newestAtBottom, append
	  //if newestNotAtBottom, prepend

	  if (_uiConfigService2.default.currentPage == 1) {
	    if (index === 0) {
	      _renderingOptions2.default.rootEl.insertBefore(htmlEle, _renderingOptions2.default.rootEl.childNodes[0]);
	      _uiConfigService2.default.incrementLastRenderedIndex();
	      return;
	    } else if (currentlyExistsOnPage(post)) {
	      var target = document.getElementById('SL-' + post.Id);
	      target = htmlEle;
	      return;
	    }
	    var targetEle = document.getElementById('SL-' + _postStore2.default.renderList[index - 1].Id);

	    insertAfter(targetEle, htmlEle);
	    _uiConfigService2.default.incrementLastRenderedIndex();
	    return;
	  }

	  var targetEl = _renderingOptions2.default.rootEl.childNodes[_uiConfigService2.default.lastRenderedIndex];
	  insertAfter(targetEl, htmlEle);
	  //targetEl.appendChild(htmlEle);
	  _uiConfigService2.default.incrementLastRenderedIndex();
	  return;
	}

	/**
	 * This function will add a post after a reference to another post.
	 * @param referenceNode
	 * @param newNode
	 */
	function insertAfter(referenceNode, newNode) {
	  if (referenceNode && referenceNode.parentNode) {
	    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
	  }
	}

	/**
	 * This function will remove a post from the page.
	 * @param post
	 * @returns {boolean} based on whether the post was removed or not.
	 */
	function removePost(post) {
	  var target = document.getElementById('SL-' + post.Id);
	  if (!target) return false;
	  var parent = target.parentNode;
	  parent.removeChild(target);
	  return true;
	}

	/**
	 * This is the default sorting function which sorts by a posts CreationDate.
	 * @param a
	 * @param b
	 * @returns {number}
	 */
	function sortPostsByCreationDate(a, b) {
	  if (new Date(a.CreationDate) < new Date(b.CreationDate)) return _renderingOptions2.default.newestAtBottom(1);
	  if (new Date(a.CreationDate) > new Date(b.CreationDate)) return _renderingOptions2.default.newestAtBottom(-1);
	  return 0;
	}

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(27), __esModule: true };

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

	var core = __webpack_require__(8);
	var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
	module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
	  return $JSON.stringify.apply($JSON, arguments);
	};


/***/ }),
/* 28 */
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * Store containing static information regarding post information.
	 * @type {{posts: Array}}
	 */
	exports.default = {
	  posts: [],
	  renderList: [],
	  footerRendered: false,
	  videoScrollCreated: false,
	  adData: {},
	  adScriptEnabled: false,
	  expandedContainer: null,
	  clientData: null,
	  consentFormData: {
	    showModal: false,
	    filterPost: false,
	    loading: false
	  }
	};

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _keys = __webpack_require__(30);

	var _keys2 = _interopRequireDefault(_keys);

	var _assign = __webpack_require__(50);

	var _assign2 = _interopRequireDefault(_assign);

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _image = __webpack_require__(56);

	var _image2 = _interopRequireDefault(_image);

	var _text = __webpack_require__(61);

	var _text2 = _interopRequireDefault(_text);

	var _html = __webpack_require__(62);

	var _html2 = _interopRequireDefault(_html);

	var _instagram = __webpack_require__(63);

	var _instagram2 = _interopRequireDefault(_instagram);

	var _facebook = __webpack_require__(64);

	var _facebook2 = _interopRequireDefault(_facebook);

	var _twitter = __webpack_require__(65);

	var _twitter2 = _interopRequireDefault(_twitter);

	var _embed = __webpack_require__(66);

	var _embed2 = _interopRequireDefault(_embed);

	var _poll = __webpack_require__(67);

	var _poll2 = _interopRequireDefault(_poll);

	var _video = __webpack_require__(164);

	var _video2 = _interopRequireDefault(_video);

	var _advertisement = __webpack_require__(165);

	var _advertisement2 = _interopRequireDefault(_advertisement);

	var _list = __webpack_require__(166);

	var _list2 = _interopRequireDefault(_list);

	var _audio = __webpack_require__(167);

	var _audio2 = _interopRequireDefault(_audio);

	var _untrackedSocial = __webpack_require__(168);

	var _untrackedSocial2 = _interopRequireDefault(_untrackedSocial);

	var _helperFunctions = __webpack_require__(60);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var DEFAULT_OPTIONS = {
	  isNonTracking: null,
	  renderingTypes: {
	    IMAGE: null,
	    TEXT: null,
	    HTML: null,
	    INSTAGRAM: null,
	    FACEBOOK: null,
	    TWITTER: null
	  },
	  rootElement: null,
	  posts: []
	};

	var RenderingOptions = function () {
	  function RenderingOptions() {
	    (0, _classCallCheck3.default)(this, RenderingOptions);
	  }

	  (0, _createClass3.default)(RenderingOptions, null, [{
	    key: 'init',
	    value: function init(options) {
	      this._options = (0, _assign2.default)({}, DEFAULT_OPTIONS, options);
	      validate(this._options);
	    }
	  }, {
	    key: 'getRenderer',
	    value: function getRenderer(postType, post) {
	      var content = post.Content;

	      switch (postType) {
	        case 'IMAGE':
	          return this.image;
	        case 'TEXT':
	          return this.text;
	        case 'TWEET':
	          return this.twitter;
	        case 'FACEBOOK':
	          return this.facebook;
	        case 'INSTAGRAM':
	          return this.instagram;
	        case 'HTML':
	          if ((0, _helperFunctions.checkIfHtmlIsFacebook)(post)) {
	            return this.facebook;
	          }
	          return this.html;
	        case 'EMBED':
	          return this.embed;
	        case 'POLL':
	          return this.poll;
	        case 'VIDEO':
	          return this.video;
	        case 'ADVERTISEMENT':
	          return this.advertisement;
	        case 'AUDIO':
	          return this.audio;
	        case 'SLIDESHOW':
	          return this.html;
	        case 'YOUTUBE':
	          return this.html;
	        default:
	          return null;
	      }
	    }
	  }, {
	    key: 'newestAtBottom',
	    value: function newestAtBottom(before) {
	      var newestAtBottom = true;
	      if (this._options && this._options.newestAtBottom) {
	        try {
	          newestAtBottom = JSON.parse(this._options.newestAtBottom);
	          if (typeof newestAtBottom !== "boolean") {
	            newestAtBottom = true;
	          }
	        } catch (e) {
	          newestAtBottom = false;
	        }
	      }

	      if (before === 1 && newestAtBottom) {
	        if (newestAtBottom) {
	          return -1;
	        }
	        return 1;
	      } else if (before === -1) {
	        if (newestAtBottom) {
	          return 1;
	        }
	        return -1;
	      }
	      return before;
	    }
	  }, {
	    key: 'getList',
	    value: function getList() {
	      return _list2.default;
	    }
	  }, {
	    key: 'rootEl',
	    get: function get() {
	      var rootEl = null;
	      var rootClassName = "scrbbl-storytelling";

	      if (this._options && this._options.rootElement) {
	        if (typeof this._options.rootElement === 'string') {
	          // iframe
	          rootEl = document.querySelector(this._options.rootElement);

	          if (!document.body.classList.contains(rootClassName)) {
	            document.body.classList.add(rootClassName);
	          }

	          return rootEl;
	        } else {
	          // script embed

	          var existingContainer = document.getElementsByClassName(rootClassName);

	          if (existingContainer.length > 0) {
	            rootEl = existingContainer[0];
	          } else {
	            rootEl = document.createElement("div");
	            this._options.rootElement.appendChild(rootEl);
	          }
	        }
	      }

	      if (!rootEl) {
	        rootEl = document.body;
	      }

	      if (rootEl && !rootEl.classList.contains(rootClassName)) {
	        rootEl.classList.add(rootClassName);
	      }

	      return rootEl;
	    }
	  }, {
	    key: 'isNonTracking',
	    get: function get() {
	      if (this._options.isNonTracking) return this._options.isNonTracking;
	      return false;
	    }
	  }, {
	    key: 'image',
	    get: function get() {
	      if (this._options.renderingTypes.IMAGE) return this._options.renderingTypes.IMAGE;
	      return _image2.default;
	    }
	  }, {
	    key: 'text',
	    get: function get() {
	      if (this._options.renderingTypes.TEXT) return this._options.renderingTypes.TEXT;
	      return _text2.default;
	    }
	  }, {
	    key: 'html',
	    get: function get() {
	      if (this._options.renderingTypes.HTML) return this._options.renderingTypes.HTML;
	      return this.isNonTracking ? _untrackedSocial2.default : _html2.default;
	    }
	  }, {
	    key: 'instagram',
	    get: function get() {
	      if (this._options.renderingTypes.INSTAGRAM) return this._options.renderingTypes.INSTAGRAM;
	      return this.isNonTracking ? _untrackedSocial2.default : _instagram2.default;
	    }
	  }, {
	    key: 'facebook',
	    get: function get() {
	      if (this._options.renderingTypes.FACEBOOK) return this._options.renderingTypes.FACEBOOK;
	      return this.isNonTracking ? _untrackedSocial2.default : _facebook2.default;
	    }
	  }, {
	    key: 'twitter',
	    get: function get() {
	      if (this._options.renderingTypes.TWITTER) return this._options.renderingTypes.TWITTER;
	      return this.isNonTracking ? _untrackedSocial2.default : _twitter2.default;
	    }
	  }, {
	    key: 'poll',
	    get: function get() {
	      if (this._options.renderingTypes.POLL) return this._options.renderingTypes.POLL;
	      return _poll2.default;
	    }
	  }, {
	    key: 'embed',
	    get: function get() {
	      if (this._options.renderingTypes.EMBED) return this._options.renderingTypes.EMBED;
	      return _embed2.default;
	    }
	  }, {
	    key: 'video',
	    get: function get() {
	      if (this._options.renderingTypes.VIDEO) return this._options.renderingTypes.VIDEO;
	      return _video2.default;
	    }
	  }, {
	    key: 'advertisement',
	    get: function get() {
	      if (this._options.renderingTypes.ADVERTISEMENT) return this._options.renderingTypes.ADVERTISEMENT;
	      return _advertisement2.default;
	    }
	  }, {
	    key: 'audio',
	    get: function get() {
	      if (this._options.renderingTypes.AUDIO) return this._options.renderingTypes.AUDIO;
	      return _audio2.default;
	    }
	  }, {
	    key: 'posts',
	    get: function get() {
	      return this._options.posts;
	    }
	  }]);
	  return RenderingOptions;
	}();

	exports.default = RenderingOptions;


	function validate(options) {
	  var keys = (0, _keys2.default)(options.renderingTypes);
	  for (var i = 0; i < keys.length; i++) {
	    var key = keys[i];
	    if (typeof options[key] !== 'function') {
	      delete options[key];
	      keys.splice(i, 1);
	      i--;
	    }
	  }
	  if (!options.rootElement) {
	    throw 'No root element specified.';
	  }
	}

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(31), __esModule: true };

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(32);
	module.exports = __webpack_require__(8).Object.keys;


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

	// 19.1.2.14 Object.keys(O)
	var toObject = __webpack_require__(33);
	var $keys = __webpack_require__(35);

	__webpack_require__(49)('keys', function () {
	  return function keys(it) {
	    return $keys(toObject(it));
	  };
	});


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

	// 7.1.13 ToObject(argument)
	var defined = __webpack_require__(34);
	module.exports = function (it) {
	  return Object(defined(it));
	};


/***/ }),
/* 34 */
/***/ (function(module, exports) {

	// 7.2.1 RequireObjectCoercible(argument)
	module.exports = function (it) {
	  if (it == undefined) throw TypeError("Can't call method on  " + it);
	  return it;
	};


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

	// 19.1.2.14 / 15.2.3.14 Object.keys(O)
	var $keys = __webpack_require__(36);
	var enumBugKeys = __webpack_require__(48);

	module.exports = Object.keys || function keys(O) {
	  return $keys(O, enumBugKeys);
	};


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

	var has = __webpack_require__(21);
	var toIObject = __webpack_require__(37);
	var arrayIndexOf = __webpack_require__(40)(false);
	var IE_PROTO = __webpack_require__(44)('IE_PROTO');

	module.exports = function (object, names) {
	  var O = toIObject(object);
	  var i = 0;
	  var result = [];
	  var key;
	  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
	  // Don't enum bug & hidden keys
	  while (names.length > i) if (has(O, key = names[i++])) {
	    ~arrayIndexOf(result, key) || result.push(key);
	  }
	  return result;
	};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

	// to indexed object, toObject with fallback for non-array-like ES3 strings
	var IObject = __webpack_require__(38);
	var defined = __webpack_require__(34);
	module.exports = function (it) {
	  return IObject(defined(it));
	};


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

	// fallback for non-array-like ES3 and non-enumerable old V8 strings
	var cof = __webpack_require__(39);
	// eslint-disable-next-line no-prototype-builtins
	module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
	  return cof(it) == 'String' ? it.split('') : Object(it);
	};


/***/ }),
/* 39 */
/***/ (function(module, exports) {

	var toString = {}.toString;

	module.exports = function (it) {
	  return toString.call(it).slice(8, -1);
	};


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

	// false -> Array#indexOf
	// true  -> Array#includes
	var toIObject = __webpack_require__(37);
	var toLength = __webpack_require__(41);
	var toAbsoluteIndex = __webpack_require__(43);
	module.exports = function (IS_INCLUDES) {
	  return function ($this, el, fromIndex) {
	    var O = toIObject($this);
	    var length = toLength(O.length);
	    var index = toAbsoluteIndex(fromIndex, length);
	    var value;
	    // Array#includes uses SameValueZero equality algorithm
	    // eslint-disable-next-line no-self-compare
	    if (IS_INCLUDES && el != el) while (length > index) {
	      value = O[index++];
	      // eslint-disable-next-line no-self-compare
	      if (value != value) return true;
	    // Array#indexOf ignores holes, Array#includes - not
	    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
	      if (O[index] === el) return IS_INCLUDES || index || 0;
	    } return !IS_INCLUDES && -1;
	  };
	};


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

	// 7.1.15 ToLength
	var toInteger = __webpack_require__(42);
	var min = Math.min;
	module.exports = function (it) {
	  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
	};


/***/ }),
/* 42 */
/***/ (function(module, exports) {

	// 7.1.4 ToInteger
	var ceil = Math.ceil;
	var floor = Math.floor;
	module.exports = function (it) {
	  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
	};


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

	var toInteger = __webpack_require__(42);
	var max = Math.max;
	var min = Math.min;
	module.exports = function (index, length) {
	  index = toInteger(index);
	  return index < 0 ? max(index + length, 0) : min(index, length);
	};


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

	var shared = __webpack_require__(45)('keys');
	var uid = __webpack_require__(47);
	module.exports = function (key) {
	  return shared[key] || (shared[key] = uid(key));
	};


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

	var core = __webpack_require__(8);
	var global = __webpack_require__(7);
	var SHARED = '__core-js_shared__';
	var store = global[SHARED] || (global[SHARED] = {});

	(module.exports = function (key, value) {
	  return store[key] || (store[key] = value !== undefined ? value : {});
	})('versions', []).push({
	  version: core.version,
	  mode: __webpack_require__(46) ? 'pure' : 'global',
	  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
	});


/***/ }),
/* 46 */
/***/ (function(module, exports) {

	module.exports = true;


/***/ }),
/* 47 */
/***/ (function(module, exports) {

	var id = 0;
	var px = Math.random();
	module.exports = function (key) {
	  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
	};


/***/ }),
/* 48 */
/***/ (function(module, exports) {

	// IE 8- don't enum bug keys
	module.exports = (
	  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
	).split(',');


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

	// most Object methods by ES6 should accept primitives
	var $export = __webpack_require__(6);
	var core = __webpack_require__(8);
	var fails = __webpack_require__(17);
	module.exports = function (KEY, exec) {
	  var fn = (core.Object || {})[KEY] || Object[KEY];
	  var exp = {};
	  exp[KEY] = exec(fn);
	  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
	};


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(51), __esModule: true };

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(52);
	module.exports = __webpack_require__(8).Object.assign;


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

	// 19.1.3.1 Object.assign(target, source)
	var $export = __webpack_require__(6);

	$export($export.S + $export.F, 'Object', { assign: __webpack_require__(53) });


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	// 19.1.2.1 Object.assign(target, source, ...)
	var DESCRIPTORS = __webpack_require__(16);
	var getKeys = __webpack_require__(35);
	var gOPS = __webpack_require__(54);
	var pIE = __webpack_require__(55);
	var toObject = __webpack_require__(33);
	var IObject = __webpack_require__(38);
	var $assign = Object.assign;

	// should work with symbols and should have deterministic property order (V8 bug)
	module.exports = !$assign || __webpack_require__(17)(function () {
	  var A = {};
	  var B = {};
	  // eslint-disable-next-line no-undef
	  var S = Symbol();
	  var K = 'abcdefghijklmnopqrst';
	  A[S] = 7;
	  K.split('').forEach(function (k) { B[k] = k; });
	  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
	}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
	  var T = toObject(target);
	  var aLen = arguments.length;
	  var index = 1;
	  var getSymbols = gOPS.f;
	  var isEnum = pIE.f;
	  while (aLen > index) {
	    var S = IObject(arguments[index++]);
	    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
	    var length = keys.length;
	    var j = 0;
	    var key;
	    while (length > j) {
	      key = keys[j++];
	      if (!DESCRIPTORS || isEnum.call(S, key)) T[key] = S[key];
	    }
	  } return T;
	} : $assign;


/***/ }),
/* 54 */
/***/ (function(module, exports) {

	exports.f = Object.getOwnPropertySymbols;


/***/ }),
/* 55 */
/***/ (function(module, exports) {

	exports.f = {}.propertyIsEnumerable;


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _keys = __webpack_require__(30);

	var _keys2 = _interopRequireDefault(_keys);

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	var _expandFunctions = __webpack_require__(58);

	var _helperFunctions = __webpack_require__(60);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function getAppropriateImageSize(media) {
	  if (!media.Sizes) return media.Url;
	  var width = document.documentElement.clientWidth;
	  var validUrl = media.Url;
	  for (var size in media.Sizes) {
	    if (size > width) {
	      return validUrl;
	    }
	    validUrl = media.Sizes[size];
	  }
	  return validUrl;
	}

	function getSizes(media) {
	  if (!media.Sizes) return media.Url;
	  var list = [];
	  var keys = (0, _keys2.default)(media.Sizes);
	  for (var i = 0; i < keys.length; i++) {
	    if (keys[i] === 'orig') {
	      list.push(media.Sizes[keys[i]] + ' ' + (parseInt(keys[i - 1], 10) + 1) + 'w');
	    } else {
	      list.push(media.Sizes[keys[i]] + ' ' + keys[i] + 'w');
	    }
	  }
	  return list.join(', ');
	}

	function mobileCheck() {
	  return typeof window.orientation !== "undefined" || navigator.userAgent.indexOf('IEMobile') !== -1;
	}

	function isIE() {
	  var ua = window.navigator.userAgent;
	  if (ua.indexOf('MSIE ') > -1 || ua.indexOf('Trident') > -1) {
	    return true;
	  }
	  return false;
	}

	function inIframe() {
	  try {
	    return window.self !== window.top;
	  } catch (e) {
	    return true;
	  }
	}

	function getImageSet(post) {
	  if (isIE()) {
	    var src = post.Media.Sizes && post.Media.Sizes['orig'] ? post.Media.Sizes['orig'] : post.Media.Url;
	    return '<picture><img src="' + src + '" size="300w" /></picture>';
	  }
	  return '<picture class="section-background-picture">\n      <source srcset="' + getSizes(post.Media) + '">\n      <img src="' + post.Media.Url + '" size="300w;"">\n  </picture>';
	}

	var image = function image(post, index) {
	  var caption = '<div class="caption-container"></div>';
	  var hero = '';
	  var arrow = '';
	  var transitionImage = !post.imageClose ? null : ' TRANSITION';

	  if (post.PostMeta && post.PostMeta.Tags && post.PostMeta.Tags.indexOf('hero') > -1) {
	    hero = ' hero-image';

	    if (!(inIframe() && mobileCheck())) {
	      hero = hero + ' desktop';
	    }
	  }
	  if (hero && index !== undefined && index === 0) {
	    arrow = '<div class="hero-arrow-button">\n        <div class="arrow"></div>\n      </div>';
	  }
	  if (post.Caption && post.Caption.length > 0) {
	    if (hero.length > 0) {
	      caption = '<div class="caption-container"><div>' + post.Caption + '</div></div>';
	    } else {
	      caption = '<div class="caption-container"><p>' + post.Caption + '</p></div>';
	    }
	  }
	  var image = null;
	  if (hero.length > 0) {
	    image = _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-IMAGE' + hero + '" style="background-image:url(\'' + getAppropriateImageSize(post.Media) + '\')">\n            ' + caption + '\n            ' + arrow + '\n        </div>');
	  } else if (!transitionImage) {
	    image = _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-IMAGE">\n        <img src="' + getAppropriateImageSize(post.Media) + '">\n            ' + caption + '\n        </div>');
	  } else {
	    image = _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-IMAGE' + transitionImage + '" >\n          ' + caption + '\n          <div class="section-background fixed">\n              ' + getImageSet(post) + '\n          </div>\n      </div>');
	  }

	  if (post.Media && post.Media.Sizes && post.Media.Sizes['800'] && mobileCheck()) {
	    var largerImage = _htmlGenerator2.default.generate('<img style="display:none; width:0; height:0;" src="' + post.Media.Sizes['800'] + '" />');
	    largerImage.onload = function (_ref) {
	      var target = _ref.target;

	      if (image.style.backgroundImage !== '') {
	        image.style.backgroundImage = 'url(' + target.src + ')';
	      } else {
	        image.getElementsByTagName('img')[0].src = target.src;
	        image.getElementsByTagName('img')[0].size = '800w';
	      }
	    };
	  }

	  function scrollToY() {
	    var scrollTargetY = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
	    var speed = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2000;
	    var easing = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'easeOutSine';

	    // scrollTargetY: the target scrollY property of the window
	    // speed: time in pixels per second
	    // easing: easing equation to use

	    var scrollY = window.scrollY,
	        currentTime = 0;

	    // min time .1, max time .8 seconds
	    var time = Math.max(0.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, 0.8));

	    // easing equations from https://github.com/danro/easing-js/blob/master/easing.js
	    var PI_D2 = Math.PI / 2,
	        easingEquations = {
	      easeOutSine: function easeOutSine(pos) {
	        return Math.sin(pos * (Math.PI / 2));
	      },
	      easeInOutSine: function easeInOutSine(pos) {
	        return -0.5 * (Math.cos(Math.PI * pos) - 1);
	      },
	      easeInOutQuint: function easeInOutQuint(pos) {
	        if ((pos /= 0.5) < 1) {
	          return 0.5 * Math.pow(pos, 5);
	        }
	        return 0.5 * (Math.pow(pos - 2, 5) + 2);
	      }
	    };

	    // add animation loop
	    function tick() {
	      currentTime += 1 / 60;

	      var p = currentTime / time;
	      var t = easingEquations[easing](p);

	      if (p < 1) {
	        requestAnimFrame(tick);
	        window.scrollTo(0, scrollY + (scrollTargetY - scrollY) * t);
	      } else {
	        window.scrollTo(0, scrollTargetY);
	      }
	    }

	    // call it once to get started
	    tick();
	  }

	  if (arrow.length > 0) {
	    window.requestAnimFrame = function () {
	      return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (callback) {
	        window.setTimeout(callback, 1000 / 60);
	      };
	    }();

	    var _arrow = image.querySelector('.hero-arrow-button .arrow');
	    if (_arrow) {
	      _arrow.onclick = function (_ref2) {
	        var target = _ref2.target;

	        if (!target) return;
	        var imageBottom = window.scrollY + target.parentNode.getBoundingClientRect().bottom;
	        scrollToY(imageBottom, 3000);
	      };
	    }
	  }

	  if ((0, _helperFunctions.isMobile)()) {
	    if (arrow.length > 0) {
	      image.querySelector('.caption-container').onclick = function () {
	        return (0, _expandFunctions.expandImage)(post);
	      };
	    } else {
	      image.onclick = function () {
	        return (0, _expandFunctions.expandImage)(post);
	      };
	    }
	  }

	  return image;
	};

	exports.default = image;

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var HtmlGenerator = function () {
	  function HtmlGenerator() {
	    (0, _classCallCheck3.default)(this, HtmlGenerator);
	  }

	  (0, _createClass3.default)(HtmlGenerator, null, [{
	    key: 'generate',
	    value: function generate(str) {
	      return postProcess(this.getHtml(str));
	    }
	  }, {
	    key: 'getHtml',
	    value: function getHtml(str) {
	      var div = document.createElement('div');
	      div.innerHTML = str;
	      return div.firstChild;
	    }
	  }]);
	  return HtmlGenerator;
	}();

	exports.default = HtmlGenerator;


	function postProcess(ele) {
	  addTargetBlank(ele);
	  return ele;
	}

	function addTargetBlank(ele) {
	  var as = ele.getElementsByTagName('a');
	  for (var i = 0; i < as.length; i++) {
	    as[i].setAttribute('target', '_blank');
	  }
	}

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.expandImage = expandImage;
	exports.closeExpandedImage = closeExpandedImage;

	var _postStore = __webpack_require__(28);

	var _postStore2 = _interopRequireDefault(_postStore);

	var _renderingOptions = __webpack_require__(29);

	var _renderingOptions2 = _interopRequireDefault(_renderingOptions);

	var _expandedContainer = __webpack_require__(59);

	var _expandedContainer2 = _interopRequireDefault(_expandedContainer);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function expandImage(post) {
	  if (!_postStore2.default.expandedContainer) {
	    _postStore2.default.expandedContainer = _renderingOptions2.default.rootEl.appendChild((0, _expandedContainer2.default)(post));
	    document.body.classList.add('expanded');
	  }
	}

	function closeExpandedImage() {
	  _renderingOptions2.default.rootEl.removeChild(_postStore2.default.expandedContainer);
	  _postStore2.default.expandedContainer = null;
	  document.body.classList.remove('expanded');
	}

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	var _expandFunctions = __webpack_require__(58);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var expandedContainer = function expandedContainer(post) {
	  var container = _htmlGenerator2.default.generate('<div class="SL-EXPANDED">\n      <div class="fa fa-times SL-expanded-close"></div>\n      <div class="SL-expanded-inner">\n        <img class="SL-expanded-image" src="' + post.Media.Url + '"/>\n        ' + (post.Caption ? '<div class="SL-expanded-caption">' + post.Caption + '</div>' : '') + '\n      </div>\n    </div>');

	  container.getElementsByClassName('SL-expanded-close')[0].onclick = _expandFunctions.closeExpandedImage;

	  return container;
	};

	exports.default = expandedContainer;

/***/ }),
/* 60 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.isMobile = isMobile;
	exports.checkIfHtmlIsFacebook = checkIfHtmlIsFacebook;
	function isMobile() {
	  var check = false;
	  (function (a) {
	    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
	  })(navigator.userAgent || navigator.vendor || window.opera);
	  return check;
	};

	function checkIfHtmlIsFacebook(post) {
	  return post.Content.substring(0, 20) === '<div class="fb-post"';
	}

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var text = function text(post) {
	    return _htmlGenerator2.default.generate("<div id=\"SL-" + post.Id + "\" class=\"SL-TEXT\">\n                            <p>" + post.Content + "</p>\n                        </div>");
	};

	exports.default = text;

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function renderHTMLScriptElements(post) {
	  try {
	    var postHTML = unescape(post.Content);

	    var isScript = false;
	    if (postHTML.indexOf('&lt;script') > -1 || postHTML.indexOf('<script') > -1) {
	      isScript = true;
	    }

	    if (isScript) {
	      // will render the scribble embed for this post id
	      var scriptEmbed = '<div class="scrbbl-post-embed" data-post-id="' + post.Id + '"></div>';
	      return scriptEmbed;
	    }
	  } catch (err) {}

	  return post.Content;
	}

	function imageOnRight(html) {
	  var cols = html.getElementsByClassName('col-md-6');
	  if (!cols || cols.length !== 2) return false;
	  // There is an image on the right, but not on the left.
	  if (cols[1].getElementsByTagName('img').length && cols[0].getElementsByTagName('img').length === 0) {
	    return true;
	  }
	  return false;
	}

	var html = function html(post) {
	  var htmlPost = _htmlGenerator2.default.getHtml(post.Content);
	  var imgOnRight = imageOnRight(htmlPost);
	  // console.log(htmlPost);
	  return _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-HTML' + (imgOnRight ? ' img-right' : '') + '">\n                                    ' + renderHTMLScriptElements(post) + '\n                                    </div>');
	};

	exports.default = html;

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var instagram = function instagram(post) {
	    return _htmlGenerator2.default.generate("<div id=\"SL-" + post.Id + "\" class=\"SL-INSTAGRAM\">\n                                        " + post.Content + "\n                                    </div>");
	};

	exports.default = instagram;

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var facebook = function facebook(post) {
	    return _htmlGenerator2.default.generate("<div id=\"SL-" + post.Id + "\" class=\"SL-FACEBOOK\">\n                                " + post.Content + "\n                            </div>");
	};

	exports.default = facebook;

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var twitter = function twitter(post) {
	    return _htmlGenerator2.default.generate("<div id=\"SL-" + post.Id + "\" class=\"SL-TWITTER\">\n                                        " + post.Content + "\n                                    </div>");
	};

	exports.default = twitter;

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var embed = function embed(post) {
	    return _htmlGenerator2.default.generate("<div id=\"SL-" + post.Id + "\" class=\"SL-EMBED\">\n                                " + post.Content + "\n                            </div>");
	};

	exports.default = embed;

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _getIterator2 = __webpack_require__(68);

	var _getIterator3 = _interopRequireDefault(_getIterator2);

	var _assign = __webpack_require__(50);

	var _assign2 = _interopRequireDefault(_assign);

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	var _coreWrapper = __webpack_require__(89);

	var _coreWrapper2 = _interopRequireDefault(_coreWrapper);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	// holds references to answer elements
	var answerElements = {};

	var calculatePercentage = function calculatePercentage(answerVotes, totalVotes) {
	  if (answerVotes === 0 || totalVotes === 0) {
	    return 0;
	  }
	  return Math.floor(100 * (answerVotes / totalVotes));
	};

	var generateWrapper = function generateWrapper(post, pollClass) {
	  return '<div id="SL-' + post.Id + '" class="SL-POLL ' + pollClass + '">\n            <p class="poll-question">' + post.Content + '</p>\n          </div>';
	};

	var generateVoteText = function generateVoteText(votes) {
	  return '(' + votes + ')';
	};

	/**
	 * Sets the reference data for later use
	 * @param answer
	 * @param element
	 */
	var setAnswerElements = function setAnswerElements(_ref) {
	  var answer = _ref.answer,
	      element = _ref.element;

	  if (!answer || !answer.Id || !element) return;
	  answerElements[answer.Id] = (0, _assign2.default)({}, answerElements[answer.Id], {
	    element: element,
	    data: answer
	  });
	};

	var pollingCallback = function pollingCallback(err, data) {
	  var _iteratorNormalCompletion = true;
	  var _didIteratorError = false;
	  var _iteratorError = undefined;

	  try {
	    var _loop = function _loop() {
	      var answer = _step.value;

	      var prevAnswer = answerElements[answer.Id];

	      if (!prevAnswer) {
	        return 'continue';
	      }

	      var element = prevAnswer.element;

	      var percentageElement = element.querySelector('.poll-result-item-percentage');
	      var percentage = calculatePercentage(answer.Votes, data.TotalVotes);
	      var prevPercentage = parseInt(percentageElement.innerHTML, 10);

	      element.querySelector('.poll-result-item-description__votes').innerHTML = generateVoteText(answer.Votes);
	      element.querySelector('.poll-result-item-bar__color-bar').style.width = percentage + '%';

	      var start = prevPercentage;
	      var interval = setInterval(function () {
	        if (percentage > prevPercentage) {
	          start++;
	        } else if (percentage < prevPercentage) {
	          start--;
	        }

	        percentageElement.innerHTML = start + '%';
	        if (start === percentage) {
	          clearInterval(interval);
	          setAnswerElements({ answer: answer, element: element });
	        }
	      }, 100);
	    };

	    for (var _iterator = (0, _getIterator3.default)(data.Answers), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	      var _ret = _loop();

	      if (_ret === 'continue') continue;
	    }
	  } catch (err) {
	    _didIteratorError = true;
	    _iteratorError = err;
	  } finally {
	    try {
	      if (!_iteratorNormalCompletion && _iterator.return) {
	        _iterator.return();
	      }
	    } finally {
	      if (_didIteratorError) {
	        throw _iteratorError;
	      }
	    }
	  }
	};

	var renderResultView = function renderResultView(post) {
	  var _post$Entities = post.Entities,
	      answers = _post$Entities.Answers,
	      totalVotes = _post$Entities.TotalVotes,
	      pollId = _post$Entities.Id;

	  var answerContainer = _htmlGenerator2.default.generate('<div class="poll-results"></div>');

	  answers.forEach(function (answer) {
	    var percentage = calculatePercentage(answer.Votes, totalVotes);
	    var voteText = generateVoteText(answer.Votes);
	    var element = _htmlGenerator2.default.generate('<div class="poll-result-item">\n                                                    <div class="poll-result-item-row">\n                                                      <div class="poll-result-item-description">\n                                                          <span class="poll-result-item-description__text">' + answer.Text + '</span> \n                                                          <span class="poll-result-item-description__votes">' + voteText + '</span>\n                                                      </div>\n                                                      <div class="poll-result-item-percentage">\n                                                          ' + percentage + '%\n                                                      </div>\n                                                    </div>\n                                                    <div class="poll-result-item-bar">\n                                                        <span class="poll-result-item-bar__color-bar" \n                                                                style="width:' + percentage + '%"></span>\n                                                    </div>\n                                                </div>');
	    answerContainer.appendChild(element);
	    setAnswerElements({ answer: answer, element: element });
	  });

	  _coreWrapper2.default.instance.Poll.listen({ pollId: pollId, cb: pollingCallback });
	  return answerContainer;
	};

	var renderVoteView = function renderVoteView(post) {
	  var _post$Entities2 = post.Entities,
	      answers = _post$Entities2.Answers,
	      pollId = _post$Entities2.Id;

	  var streamId = post.StreamId;
	  var answerContainer = _htmlGenerator2.default.generate('<div class="poll-answers"></div>');
	  var wrapper = _htmlGenerator2.default.generate(generateWrapper(post, 'voting'));

	  answers.forEach(function (answer, index) {
	    var answerDom = _htmlGenerator2.default.generate('<div class="poll-answer">' + answer.Text + '</div>');
	    var hoverClass = 'poll-answer--hover';
	    var vote = function vote() {
	      answer.Votes++;
	      post.Entities.TotalVotes++;
	      _coreWrapper2.default.instance.Poll.vote({
	        streamId: streamId,
	        pollId: pollId,
	        selectionId: answer.Id
	      }).catch(function () {});

	      answerContainer.classList.add('poll-answers--hidden');
	      answerContainer.addEventListener('transitionend', function () {
	        var resultsDom = renderResultView(post);
	        answerContainer.parentNode.removeChild(answerContainer);
	        resultsDom.classList.add('poll-results--hidden');
	        wrapper.appendChild(resultsDom);
	        resultsDom.classList.remove('poll-results--hidden');
	      });
	    };

	    var hover = function hover(e) {
	      e.target.classList.add(hoverClass);
	    };

	    var mouseout = function mouseout(e) {
	      e.target.classList.remove(hoverClass);
	    };

	    if (index === answers.length - 1) {
	      answerDom.classList.add('poll-answer--last');
	    }

	    answerDom.addEventListener('click', vote);
	    answerDom.addEventListener('mouseover', hover);
	    answerDom.addEventListener('mouseout', mouseout);
	    answerContainer.appendChild(answerDom);
	  });

	  wrapper.appendChild(answerContainer);
	  return wrapper;
	};

	var poll = function poll(post) {
	  var streamId = post.StreamId;
	  var pollId = post.Entities.Id;

	  if (_coreWrapper2.default.instance.Poll.hasVoted({ streamId: streamId, pollId: pollId })) {
	    var wrapper = _htmlGenerator2.default.generate(generateWrapper(post, 'result'));
	    var pollContent = renderResultView(post);
	    wrapper.appendChild(pollContent);
	    return wrapper;
	  }
	  return renderVoteView(post);
	};

	exports.default = poll;

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(69), __esModule: true };

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(70);
	__webpack_require__(84);
	module.exports = __webpack_require__(86);


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(71);
	var global = __webpack_require__(7);
	var hide = __webpack_require__(11);
	var Iterators = __webpack_require__(74);
	var TO_STRING_TAG = __webpack_require__(82)('toStringTag');

	var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
	  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
	  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
	  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
	  'TextTrackList,TouchList').split(',');

	for (var i = 0; i < DOMIterables.length; i++) {
	  var NAME = DOMIterables[i];
	  var Collection = global[NAME];
	  var proto = Collection && Collection.prototype;
	  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
	  Iterators[NAME] = Iterators.Array;
	}


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	var addToUnscopables = __webpack_require__(72);
	var step = __webpack_require__(73);
	var Iterators = __webpack_require__(74);
	var toIObject = __webpack_require__(37);

	// 22.1.3.4 Array.prototype.entries()
	// 22.1.3.13 Array.prototype.keys()
	// 22.1.3.29 Array.prototype.values()
	// 22.1.3.30 Array.prototype[@@iterator]()
	module.exports = __webpack_require__(75)(Array, 'Array', function (iterated, kind) {
	  this._t = toIObject(iterated); // target
	  this._i = 0;                   // next index
	  this._k = kind;                // kind
	// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
	}, function () {
	  var O = this._t;
	  var kind = this._k;
	  var index = this._i++;
	  if (!O || index >= O.length) {
	    this._t = undefined;
	    return step(1);
	  }
	  if (kind == 'keys') return step(0, index);
	  if (kind == 'values') return step(0, O[index]);
	  return step(0, [index, O[index]]);
	}, 'values');

	// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
	Iterators.Arguments = Iterators.Array;

	addToUnscopables('keys');
	addToUnscopables('values');
	addToUnscopables('entries');


/***/ }),
/* 72 */
/***/ (function(module, exports) {

	module.exports = function () { /* empty */ };


/***/ }),
/* 73 */
/***/ (function(module, exports) {

	module.exports = function (done, value) {
	  return { value: value, done: !!done };
	};


/***/ }),
/* 74 */
/***/ (function(module, exports) {

	module.exports = {};


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	var LIBRARY = __webpack_require__(46);
	var $export = __webpack_require__(6);
	var redefine = __webpack_require__(76);
	var hide = __webpack_require__(11);
	var Iterators = __webpack_require__(74);
	var $iterCreate = __webpack_require__(77);
	var setToStringTag = __webpack_require__(81);
	var getPrototypeOf = __webpack_require__(83);
	var ITERATOR = __webpack_require__(82)('iterator');
	var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
	var FF_ITERATOR = '@@iterator';
	var KEYS = 'keys';
	var VALUES = 'values';

	var returnThis = function () { return this; };

	module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
	  $iterCreate(Constructor, NAME, next);
	  var getMethod = function (kind) {
	    if (!BUGGY && kind in proto) return proto[kind];
	    switch (kind) {
	      case KEYS: return function keys() { return new Constructor(this, kind); };
	      case VALUES: return function values() { return new Constructor(this, kind); };
	    } return function entries() { return new Constructor(this, kind); };
	  };
	  var TAG = NAME + ' Iterator';
	  var DEF_VALUES = DEFAULT == VALUES;
	  var VALUES_BUG = false;
	  var proto = Base.prototype;
	  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
	  var $default = $native || getMethod(DEFAULT);
	  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
	  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
	  var methods, key, IteratorPrototype;
	  // Fix native
	  if ($anyNative) {
	    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
	    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
	      // Set @@toStringTag to native iterators
	      setToStringTag(IteratorPrototype, TAG, true);
	      // fix for some old engines
	      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
	    }
	  }
	  // fix Array#{values, @@iterator}.name in V8 / FF
	  if (DEF_VALUES && $native && $native.name !== VALUES) {
	    VALUES_BUG = true;
	    $default = function values() { return $native.call(this); };
	  }
	  // Define iterator
	  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
	    hide(proto, ITERATOR, $default);
	  }
	  // Plug for library
	  Iterators[NAME] = $default;
	  Iterators[TAG] = returnThis;
	  if (DEFAULT) {
	    methods = {
	      values: DEF_VALUES ? $default : getMethod(VALUES),
	      keys: IS_SET ? $default : getMethod(KEYS),
	      entries: $entries
	    };
	    if (FORCED) for (key in methods) {
	      if (!(key in proto)) redefine(proto, key, methods[key]);
	    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
	  }
	  return methods;
	};


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(11);


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	var create = __webpack_require__(78);
	var descriptor = __webpack_require__(20);
	var setToStringTag = __webpack_require__(81);
	var IteratorPrototype = {};

	// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
	__webpack_require__(11)(IteratorPrototype, __webpack_require__(82)('iterator'), function () { return this; });

	module.exports = function (Constructor, NAME, next) {
	  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
	  setToStringTag(Constructor, NAME + ' Iterator');
	};


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

	// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
	var anObject = __webpack_require__(13);
	var dPs = __webpack_require__(79);
	var enumBugKeys = __webpack_require__(48);
	var IE_PROTO = __webpack_require__(44)('IE_PROTO');
	var Empty = function () { /* empty */ };
	var PROTOTYPE = 'prototype';

	// Create object with fake `null` prototype: use iframe Object with cleared prototype
	var createDict = function () {
	  // Thrash, waste and sodomy: IE GC bug
	  var iframe = __webpack_require__(18)('iframe');
	  var i = enumBugKeys.length;
	  var lt = '<';
	  var gt = '>';
	  var iframeDocument;
	  iframe.style.display = 'none';
	  __webpack_require__(80).appendChild(iframe);
	  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
	  // createDict = iframe.contentWindow.Object;
	  // html.removeChild(iframe);
	  iframeDocument = iframe.contentWindow.document;
	  iframeDocument.open();
	  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
	  iframeDocument.close();
	  createDict = iframeDocument.F;
	  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
	  return createDict();
	};

	module.exports = Object.create || function create(O, Properties) {
	  var result;
	  if (O !== null) {
	    Empty[PROTOTYPE] = anObject(O);
	    result = new Empty();
	    Empty[PROTOTYPE] = null;
	    // add "__proto__" for Object.getPrototypeOf polyfill
	    result[IE_PROTO] = O;
	  } else result = createDict();
	  return Properties === undefined ? result : dPs(result, Properties);
	};


/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

	var dP = __webpack_require__(12);
	var anObject = __webpack_require__(13);
	var getKeys = __webpack_require__(35);

	module.exports = __webpack_require__(16) ? Object.defineProperties : function defineProperties(O, Properties) {
	  anObject(O);
	  var keys = getKeys(Properties);
	  var length = keys.length;
	  var i = 0;
	  var P;
	  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
	  return O;
	};


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

	var document = __webpack_require__(7).document;
	module.exports = document && document.documentElement;


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

	var def = __webpack_require__(12).f;
	var has = __webpack_require__(21);
	var TAG = __webpack_require__(82)('toStringTag');

	module.exports = function (it, tag, stat) {
	  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
	};


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

	var store = __webpack_require__(45)('wks');
	var uid = __webpack_require__(47);
	var Symbol = __webpack_require__(7).Symbol;
	var USE_SYMBOL = typeof Symbol == 'function';

	var $exports = module.exports = function (name) {
	  return store[name] || (store[name] =
	    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
	};

	$exports.store = store;


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

	// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
	var has = __webpack_require__(21);
	var toObject = __webpack_require__(33);
	var IE_PROTO = __webpack_require__(44)('IE_PROTO');
	var ObjectProto = Object.prototype;

	module.exports = Object.getPrototypeOf || function (O) {
	  O = toObject(O);
	  if (has(O, IE_PROTO)) return O[IE_PROTO];
	  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
	    return O.constructor.prototype;
	  } return O instanceof Object ? ObjectProto : null;
	};


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	var $at = __webpack_require__(85)(true);

	// 21.1.3.27 String.prototype[@@iterator]()
	__webpack_require__(75)(String, 'String', function (iterated) {
	  this._t = String(iterated); // target
	  this._i = 0;                // next index
	// 21.1.5.2.1 %StringIteratorPrototype%.next()
	}, function () {
	  var O = this._t;
	  var index = this._i;
	  var point;
	  if (index >= O.length) return { value: undefined, done: true };
	  point = $at(O, index);
	  this._i += point.length;
	  return { value: point, done: false };
	});


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

	var toInteger = __webpack_require__(42);
	var defined = __webpack_require__(34);
	// true  -> String#at
	// false -> String#codePointAt
	module.exports = function (TO_STRING) {
	  return function (that, pos) {
	    var s = String(defined(that));
	    var i = toInteger(pos);
	    var l = s.length;
	    var a, b;
	    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
	    a = s.charCodeAt(i);
	    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
	      ? TO_STRING ? s.charAt(i) : a
	      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
	  };
	};


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

	var anObject = __webpack_require__(13);
	var get = __webpack_require__(87);
	module.exports = __webpack_require__(8).getIterator = function (it) {
	  var iterFn = get(it);
	  if (typeof iterFn != 'function') throw TypeError(it + ' is not iterable!');
	  return anObject(iterFn.call(it));
	};


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

	var classof = __webpack_require__(88);
	var ITERATOR = __webpack_require__(82)('iterator');
	var Iterators = __webpack_require__(74);
	module.exports = __webpack_require__(8).getIteratorMethod = function (it) {
	  if (it != undefined) return it[ITERATOR]
	    || it['@@iterator']
	    || Iterators[classof(it)];
	};


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

	// getting tag from 19.1.3.6 Object.prototype.toString()
	var cof = __webpack_require__(39);
	var TAG = __webpack_require__(82)('toStringTag');
	// ES3 wrong here
	var ARG = cof(function () { return arguments; }()) == 'Arguments';

	// fallback for IE11 Script Access Denied error
	var tryGet = function (it, key) {
	  try {
	    return it[key];
	  } catch (e) { /* empty */ }
	};

	module.exports = function (it) {
	  var O, T, B;
	  return it === undefined ? 'Undefined' : it === null ? 'Null'
	    // @@toStringTag case
	    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
	    // builtinTag case
	    : ARG ? cof(O)
	    // ES3 arguments fallback
	    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
	};


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.initialize = undefined;

	var _scribbleliveToolkitCore = __webpack_require__(90);

	var _scribbleliveToolkitCore2 = _interopRequireDefault(_scribbleliveToolkitCore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var _core = null;

	var initialize = exports.initialize = function initialize(_ref) {
	  var token = _ref.token,
	      options = _ref.options;

	  _core = new _scribbleliveToolkitCore2.default({ token: token, options: options });
	  return _core;
	};

	exports.default = {
	  get instance() {
	    if (_core === null) {
	      throw new Error('Core has not been initialized');
	    }

	    return _core;
	  }
	};

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _es6Promise = __webpack_require__(22);

	var _es6Promise2 = _interopRequireDefault(_es6Promise);

	var _auth = __webpack_require__(91);

	var _auth2 = _interopRequireDefault(_auth);

	var _stream = __webpack_require__(92);

	var _stream2 = _interopRequireDefault(_stream);

	var _config = __webpack_require__(121);

	var _config2 = _interopRequireDefault(_config);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	if (typeof window !== 'undefined' && !window.Promise) {
	  window.Promise = _es6Promise2.default;
	}

	var ScribbleToolkitCore = function () {
	  function ScribbleToolkitCore(options) {
	    (0, _classCallCheck3.default)(this, ScribbleToolkitCore);

	    _auth2.default.init(options.auth, options.token);
	    _config2.default.init(options.options);

	    this._likesModule = null;
	    this._authModule = null;
	    this._pollModule = null;
	    this._collectionModule = null;

	    this.Stream = _stream2.default;
	  }

	  (0, _createClass3.default)(ScribbleToolkitCore, [{
	    key: 'Likes',
	    get: function get() {
	      if (this._likesModule !== null) return this._likesModule;

	      try {
	        this._likesModule = __webpack_require__(129);

	        return this._likesModule;
	      } catch (e) {
	        console.warn('scribblelive-toolkit-likes module has not been installed.');
	        return null;
	      }
	    }
	  }, {
	    key: 'Auth',
	    get: function get() {
	      if (this._authModule !== null) return this._authModule;

	      try {
	        this._authModule = __webpack_require__(134);

	        return this._authModule;
	      } catch (e) {
	        console.warn('scribblelive-toolkit-auth module has not been installed.');
	        return null;
	      }
	    }
	  }, {
	    key: 'Poll',
	    get: function get() {
	      if (this._pollModule !== null) return this._pollModule;

	      try {
	        var PollClass = __webpack_require__(140);
	        this._pollModule = new PollClass(_config2.default.pollSettings);
	        return this._pollModule;
	      } catch (e) {
	        console.warn('scribblelive-toolkit-auth module has not been installed.');
	      }
	    }
	  }, {
	    key: 'Collection',
	    get: function get() {
	      if (this._collectionModule !== null) return this._collectionModule;

	      try {
	        var CollectionClass = __webpack_require__(159);
	        var CollectionSettings = _config2.default.collectionSettings;
	        CollectionSettings.token = _auth2.default.token;
	        this._collectionModule = new CollectionClass(CollectionSettings);
	        return this._collectionModule;
	      } catch (e) {
	        console.warn('scribblelive-toolkit-collection module has not been installed.');
	      }
	    }
	  }]);
	  return ScribbleToolkitCore;
	}();

	ScribbleToolkitCore.TYPES = {
	  IMAGE: 'IMAGE',
	  VIDEO: 'VIDEO',
	  AUDIO: 'AUDIO',
	  EMBED: 'EMBED',
	  HTML: 'HTML',
	  YOUTUBE: 'YOUTUBE',
	  FACEBOOK: 'FACEBOOK',
	  TWITTER: 'TWITTER',
	  INSTAGRAM: 'INSTAGRAM',
	  SCRIBBLE: 'SCRIBBLE'
	};

	module.exports = ScribbleToolkitCore;

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var AUTH = {
	  auth: null,
	  token: null
	};

	var AuthService = function () {
	  function AuthService() {
	    (0, _classCallCheck3.default)(this, AuthService);
	  }

	  (0, _createClass3.default)(AuthService, null, [{
	    key: 'init',
	    value: function init(auth, token) {
	      if (!token) {
	        throw Error('Token is required');
	      }

	      AUTH.auth = auth;
	      AUTH.token = token;
	    }
	  }, {
	    key: 'auth',
	    get: function get() {
	      return AUTH.auth;
	    }
	  }, {
	    key: 'token',
	    get: function get() {
	      return AUTH.token;
	    }
	  }]);
	  return AuthService;
	}();

	exports.default = AuthService;

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _promise = __webpack_require__(93);

	var _promise2 = _interopRequireDefault(_promise);

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _superagent = __webpack_require__(114);

	var _superagent2 = _interopRequireDefault(_superagent);

	var _guid = __webpack_require__(119);

	var _guid2 = _interopRequireDefault(_guid);

	var _auth = __webpack_require__(91);

	var _auth2 = _interopRequireDefault(_auth);

	var _utils = __webpack_require__(120);

	var _utils2 = _interopRequireDefault(_utils);

	var _config = __webpack_require__(121);

	var _config2 = _interopRequireDefault(_config);

	var _pollingService = __webpack_require__(123);

	var _pollingService2 = _interopRequireDefault(_pollingService);

	var _postNormalizer = __webpack_require__(124);

	var _postNormalizer2 = _interopRequireDefault(_postNormalizer);

	var _tracking = __webpack_require__(125);

	var _tracking2 = _interopRequireDefault(_tracking);

	var _loggingService = __webpack_require__(128);

	var _loggingService2 = _interopRequireDefault(_loggingService);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var tracking = new _tracking2.default();

	var StreamService = function () {
	  function StreamService() {
	    (0, _classCallCheck3.default)(this, StreamService);
	  }

	  (0, _createClass3.default)(StreamService, null, [{
	    key: 'load',


	    /**
	     * This will load the specified stream and return posts (default 10, max 99).
	     * @param streamId
	     * @param callback
	     * @returns {Promise}
	     */

	    value: function load(streamId) {
	      var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _utils2.default.noop;

	      return this.loadWithOptions({
	        streamId: streamId,
	        pageSize: _config2.default.pageSize,
	        pageNum: 0
	      }, callback);
	    }

	    /**
	       * This will load the specified stream and return posts (default 10, max 99) with support for page number.
	       * @param options
	       * @returns {Promise}
	       */

	  }, {
	    key: 'loadWithOptions',
	    value: function loadWithOptions() {
	      var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { streamId: streamId, pageNum: pageNum, pageSize: pageSize, newestAtBottom: newestAtBottom, includeComments: includeComments };
	      var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _utils2.default.noop;
	      return function () {

	        if (!opts) {
	          throw Error("options not passed");
	        }

	        var streamId = opts.streamId;
	        if (!opts.streamId) {
	          throw Error('streamId is required');
	        }

	        if (!callback || typeof callback != 'function') {
	          throw Error("callback is required");
	        }

	        var callBackFunc = callback;

	        var pageNum = opts.pageNum ? Math.max(0, opts.pageNum) : 0;
	        var pageSize = opts.pageSize ? Math.max(0, opts.pageSize) : 10;
	        var newestAtBottom = opts.newestAtBottom ? 'true' : 'false'; //in case the property is missing, default to false
	        var includeComments = opts.includeComments ? 'true' : 'false';

	        var startTime = new Date();

	        tracking.track(_auth2.default.token, streamId, _config2.default.metricSourceType);

	        var url = _config2.default.baseUrl + '/stream/' + streamId + '/posts?PageNumber=' + pageNum + '&PageSize=' + pageSize + '&NewestAtBottom=' + newestAtBottom + '&IncludeComments=' + includeComments + '&token=' + _auth2.default.token;
	        return new _promise2.default(function (resolve, reject) {
	          _superagent2.default.get(url).then(function (res) {
	            try {
	              var result = JSON.parse(res.text);
	              var atoms = result.posts.map(_postNormalizer2.default.normalize);
	              _loggingService2.default.info('Time to load stream: ' + (new Date() - startTime) + 'ms');
	              result.posts = atoms;
	              callBackFunc(null, result);
	              resolve(result);
	            } catch (err) {
	              throw err;
	            }
	          }).catch(function (err) {
	            callBackFunc(err, null);
	            reject(err);
	          });
	        });
	      }();
	    }

	    /**
	     * This will poll the specified stream and return any new posts.
	     * @param streamId
	     * @param callback
	     * @returns {number}
	     */

	  }, {
	    key: 'poll',
	    value: function poll(streamId, callback) {
	      if (!streamId) {
	        throw Error('streamId is required');
	      }

	      tracking.track(_auth2.default.token, streamId, _config2.default.metricSourceType);

	      var guid = _guid2.default.raw();
	      var id = setInterval(function () {
	        return _pollingService2.default.poll(streamId, guid, callback);
	      }, _config2.default.pollingInterval);

	      var pollObj = {
	        lastChanged: 0,
	        lastModifiedPostTime: Math.ceil(Date.now() / 1000),
	        isOpen: null,
	        cancelFn: function cancelFn() {
	          clearInterval(id);
	        }
	      };
	      _pollingService2.default.setPollObject(guid, pollObj);

	      return guid;
	    }

	    /**
	     * This will get a specified streams posts for a page,
	     * given a page size (default 10,max 99) and page number (default 0).
	     * @param streamId
	     * @param pageSize
	     * @param pageNumber
	     * @param callback
	     * @returns {Promise}
	     */

	  }, {
	    key: 'page',
	    value: function page(streamId) {
	      var pageSize = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 10;
	      var pageNumber = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
	      var callback = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : _utils2.default.noop;

	      if (!streamId) {
	        throw Error('streamId is required');
	      }

	      var startTime = new Date();

	      tracking.track(_auth2.default.token, streamId, _config2.default.metricSourceType);

	      var url = _config2.default.baseUrl + '/stream/' + streamId + '/posts?token=' + _auth2.default.token + '&PageSize=' + pageSize + '&PageNumber=' + pageNumber;
	      return new _promise2.default(function (resolve, reject) {
	        _superagent2.default.get(url).end(function (err, res) {
	          if (err) {
	            callback(err, null);
	            reject(err);
	            return;
	          }
	          var result = JSON.parse(res.text);
	          var atoms = result.posts.map(_postNormalizer2.default.normalize);
	          _loggingService2.default.info('Time to page for stream: ' + (new Date() - startTime) + 'ms');
	          result.posts = atoms;
	          callback(null, result);
	          resolve(result);
	        });
	      }).catch(_utils2.default.noop);
	    }

	    /**
	     * This will clear a poll interval and clear it's information from the latestPostInfo.
	     * @param pollId
	     * @returns {boolean}
	     */

	  }, {
	    key: 'killPoll',
	    value: function killPoll(pollId) {
	      if (!pollId || typeof pollId !== 'number') {
	        return false;
	      }
	      _pollingService2.default.killPoll(pollId);
	      return true;
	    }

	    /**
	     * This will clear all polls and clear the latestPostInfo object.
	     * @returns {boolean}
	     */

	  }, {
	    key: 'killAllPolls',
	    value: function killAllPolls() {
	      return _pollingService2.default.killAllPolls();
	    }

	    /**
	     * This will return all current poll ids.
	     * @returns {Array}
	     */

	  }, {
	    key: 'getAllPolls',
	    value: function getAllPolls() {
	      return _pollingService2.default.getPollIds();
	    }
	  }]);
	  return StreamService;
	}();

	exports.default = StreamService;

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(94), __esModule: true };

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(95);
	__webpack_require__(84);
	__webpack_require__(70);
	__webpack_require__(96);
	__webpack_require__(112);
	__webpack_require__(113);
	module.exports = __webpack_require__(8).Promise;


/***/ }),
/* 95 */
/***/ (function(module, exports) {

	

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	var LIBRARY = __webpack_require__(46);
	var global = __webpack_require__(7);
	var ctx = __webpack_require__(9);
	var classof = __webpack_require__(88);
	var $export = __webpack_require__(6);
	var isObject = __webpack_require__(14);
	var aFunction = __webpack_require__(10);
	var anInstance = __webpack_require__(97);
	var forOf = __webpack_require__(98);
	var speciesConstructor = __webpack_require__(101);
	var task = __webpack_require__(102).set;
	var microtask = __webpack_require__(104)();
	var newPromiseCapabilityModule = __webpack_require__(105);
	var perform = __webpack_require__(106);
	var userAgent = __webpack_require__(107);
	var promiseResolve = __webpack_require__(108);
	var PROMISE = 'Promise';
	var TypeError = global.TypeError;
	var process = global.process;
	var versions = process && process.versions;
	var v8 = versions && versions.v8 || '';
	var $Promise = global[PROMISE];
	var isNode = classof(process) == 'process';
	var empty = function () { /* empty */ };
	var Internal, newGenericPromiseCapability, OwnPromiseCapability, Wrapper;
	var newPromiseCapability = newGenericPromiseCapability = newPromiseCapabilityModule.f;

	var USE_NATIVE = !!function () {
	  try {
	    // correct subclassing with @@species support
	    var promise = $Promise.resolve(1);
	    var FakePromise = (promise.constructor = {})[__webpack_require__(82)('species')] = function (exec) {
	      exec(empty, empty);
	    };
	    // unhandled rejections tracking support, NodeJS Promise without it fails @@species test
	    return (isNode || typeof PromiseRejectionEvent == 'function')
	      && promise.then(empty) instanceof FakePromise
	      // v8 6.6 (Node 10 and Chrome 66) have a bug with resolving custom thenables
	      // https://bugs.chromium.org/p/chromium/issues/detail?id=830565
	      // we can't detect it synchronously, so just check versions
	      && v8.indexOf('6.6') !== 0
	      && userAgent.indexOf('Chrome/66') === -1;
	  } catch (e) { /* empty */ }
	}();

	// helpers
	var isThenable = function (it) {
	  var then;
	  return isObject(it) && typeof (then = it.then) == 'function' ? then : false;
	};
	var notify = function (promise, isReject) {
	  if (promise._n) return;
	  promise._n = true;
	  var chain = promise._c;
	  microtask(function () {
	    var value = promise._v;
	    var ok = promise._s == 1;
	    var i = 0;
	    var run = function (reaction) {
	      var handler = ok ? reaction.ok : reaction.fail;
	      var resolve = reaction.resolve;
	      var reject = reaction.reject;
	      var domain = reaction.domain;
	      var result, then, exited;
	      try {
	        if (handler) {
	          if (!ok) {
	            if (promise._h == 2) onHandleUnhandled(promise);
	            promise._h = 1;
	          }
	          if (handler === true) result = value;
	          else {
	            if (domain) domain.enter();
	            result = handler(value); // may throw
	            if (domain) {
	              domain.exit();
	              exited = true;
	            }
	          }
	          if (result === reaction.promise) {
	            reject(TypeError('Promise-chain cycle'));
	          } else if (then = isThenable(result)) {
	            then.call(result, resolve, reject);
	          } else resolve(result);
	        } else reject(value);
	      } catch (e) {
	        if (domain && !exited) domain.exit();
	        reject(e);
	      }
	    };
	    while (chain.length > i) run(chain[i++]); // variable length - can't use forEach
	    promise._c = [];
	    promise._n = false;
	    if (isReject && !promise._h) onUnhandled(promise);
	  });
	};
	var onUnhandled = function (promise) {
	  task.call(global, function () {
	    var value = promise._v;
	    var unhandled = isUnhandled(promise);
	    var result, handler, console;
	    if (unhandled) {
	      result = perform(function () {
	        if (isNode) {
	          process.emit('unhandledRejection', value, promise);
	        } else if (handler = global.onunhandledrejection) {
	          handler({ promise: promise, reason: value });
	        } else if ((console = global.console) && console.error) {
	          console.error('Unhandled promise rejection', value);
	        }
	      });
	      // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should
	      promise._h = isNode || isUnhandled(promise) ? 2 : 1;
	    } promise._a = undefined;
	    if (unhandled && result.e) throw result.v;
	  });
	};
	var isUnhandled = function (promise) {
	  return promise._h !== 1 && (promise._a || promise._c).length === 0;
	};
	var onHandleUnhandled = function (promise) {
	  task.call(global, function () {
	    var handler;
	    if (isNode) {
	      process.emit('rejectionHandled', promise);
	    } else if (handler = global.onrejectionhandled) {
	      handler({ promise: promise, reason: promise._v });
	    }
	  });
	};
	var $reject = function (value) {
	  var promise = this;
	  if (promise._d) return;
	  promise._d = true;
	  promise = promise._w || promise; // unwrap
	  promise._v = value;
	  promise._s = 2;
	  if (!promise._a) promise._a = promise._c.slice();
	  notify(promise, true);
	};
	var $resolve = function (value) {
	  var promise = this;
	  var then;
	  if (promise._d) return;
	  promise._d = true;
	  promise = promise._w || promise; // unwrap
	  try {
	    if (promise === value) throw TypeError("Promise can't be resolved itself");
	    if (then = isThenable(value)) {
	      microtask(function () {
	        var wrapper = { _w: promise, _d: false }; // wrap
	        try {
	          then.call(value, ctx($resolve, wrapper, 1), ctx($reject, wrapper, 1));
	        } catch (e) {
	          $reject.call(wrapper, e);
	        }
	      });
	    } else {
	      promise._v = value;
	      promise._s = 1;
	      notify(promise, false);
	    }
	  } catch (e) {
	    $reject.call({ _w: promise, _d: false }, e); // wrap
	  }
	};

	// constructor polyfill
	if (!USE_NATIVE) {
	  // 25.4.3.1 Promise(executor)
	  $Promise = function Promise(executor) {
	    anInstance(this, $Promise, PROMISE, '_h');
	    aFunction(executor);
	    Internal.call(this);
	    try {
	      executor(ctx($resolve, this, 1), ctx($reject, this, 1));
	    } catch (err) {
	      $reject.call(this, err);
	    }
	  };
	  // eslint-disable-next-line no-unused-vars
	  Internal = function Promise(executor) {
	    this._c = [];             // <- awaiting reactions
	    this._a = undefined;      // <- checked in isUnhandled reactions
	    this._s = 0;              // <- state
	    this._d = false;          // <- done
	    this._v = undefined;      // <- value
	    this._h = 0;              // <- rejection state, 0 - default, 1 - handled, 2 - unhandled
	    this._n = false;          // <- notify
	  };
	  Internal.prototype = __webpack_require__(109)($Promise.prototype, {
	    // 25.4.5.3 Promise.prototype.then(onFulfilled, onRejected)
	    then: function then(onFulfilled, onRejected) {
	      var reaction = newPromiseCapability(speciesConstructor(this, $Promise));
	      reaction.ok = typeof onFulfilled == 'function' ? onFulfilled : true;
	      reaction.fail = typeof onRejected == 'function' && onRejected;
	      reaction.domain = isNode ? process.domain : undefined;
	      this._c.push(reaction);
	      if (this._a) this._a.push(reaction);
	      if (this._s) notify(this, false);
	      return reaction.promise;
	    },
	    // 25.4.5.1 Promise.prototype.catch(onRejected)
	    'catch': function (onRejected) {
	      return this.then(undefined, onRejected);
	    }
	  });
	  OwnPromiseCapability = function () {
	    var promise = new Internal();
	    this.promise = promise;
	    this.resolve = ctx($resolve, promise, 1);
	    this.reject = ctx($reject, promise, 1);
	  };
	  newPromiseCapabilityModule.f = newPromiseCapability = function (C) {
	    return C === $Promise || C === Wrapper
	      ? new OwnPromiseCapability(C)
	      : newGenericPromiseCapability(C);
	  };
	}

	$export($export.G + $export.W + $export.F * !USE_NATIVE, { Promise: $Promise });
	__webpack_require__(81)($Promise, PROMISE);
	__webpack_require__(110)(PROMISE);
	Wrapper = __webpack_require__(8)[PROMISE];

	// statics
	$export($export.S + $export.F * !USE_NATIVE, PROMISE, {
	  // 25.4.4.5 Promise.reject(r)
	  reject: function reject(r) {
	    var capability = newPromiseCapability(this);
	    var $$reject = capability.reject;
	    $$reject(r);
	    return capability.promise;
	  }
	});
	$export($export.S + $export.F * (LIBRARY || !USE_NATIVE), PROMISE, {
	  // 25.4.4.6 Promise.resolve(x)
	  resolve: function resolve(x) {
	    return promiseResolve(LIBRARY && this === Wrapper ? $Promise : this, x);
	  }
	});
	$export($export.S + $export.F * !(USE_NATIVE && __webpack_require__(111)(function (iter) {
	  $Promise.all(iter)['catch'](empty);
	})), PROMISE, {
	  // 25.4.4.1 Promise.all(iterable)
	  all: function all(iterable) {
	    var C = this;
	    var capability = newPromiseCapability(C);
	    var resolve = capability.resolve;
	    var reject = capability.reject;
	    var result = perform(function () {
	      var values = [];
	      var index = 0;
	      var remaining = 1;
	      forOf(iterable, false, function (promise) {
	        var $index = index++;
	        var alreadyCalled = false;
	        values.push(undefined);
	        remaining++;
	        C.resolve(promise).then(function (value) {
	          if (alreadyCalled) return;
	          alreadyCalled = true;
	          values[$index] = value;
	          --remaining || resolve(values);
	        }, reject);
	      });
	      --remaining || resolve(values);
	    });
	    if (result.e) reject(result.v);
	    return capability.promise;
	  },
	  // 25.4.4.4 Promise.race(iterable)
	  race: function race(iterable) {
	    var C = this;
	    var capability = newPromiseCapability(C);
	    var reject = capability.reject;
	    var result = perform(function () {
	      forOf(iterable, false, function (promise) {
	        C.resolve(promise).then(capability.resolve, reject);
	      });
	    });
	    if (result.e) reject(result.v);
	    return capability.promise;
	  }
	});


/***/ }),
/* 97 */
/***/ (function(module, exports) {

	module.exports = function (it, Constructor, name, forbiddenField) {
	  if (!(it instanceof Constructor) || (forbiddenField !== undefined && forbiddenField in it)) {
	    throw TypeError(name + ': incorrect invocation!');
	  } return it;
	};


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

	var ctx = __webpack_require__(9);
	var call = __webpack_require__(99);
	var isArrayIter = __webpack_require__(100);
	var anObject = __webpack_require__(13);
	var toLength = __webpack_require__(41);
	var getIterFn = __webpack_require__(87);
	var BREAK = {};
	var RETURN = {};
	var exports = module.exports = function (iterable, entries, fn, that, ITERATOR) {
	  var iterFn = ITERATOR ? function () { return iterable; } : getIterFn(iterable);
	  var f = ctx(fn, that, entries ? 2 : 1);
	  var index = 0;
	  var length, step, iterator, result;
	  if (typeof iterFn != 'function') throw TypeError(iterable + ' is not iterable!');
	  // fast case for arrays with default iterator
	  if (isArrayIter(iterFn)) for (length = toLength(iterable.length); length > index; index++) {
	    result = entries ? f(anObject(step = iterable[index])[0], step[1]) : f(iterable[index]);
	    if (result === BREAK || result === RETURN) return result;
	  } else for (iterator = iterFn.call(iterable); !(step = iterator.next()).done;) {
	    result = call(iterator, f, step.value, entries);
	    if (result === BREAK || result === RETURN) return result;
	  }
	};
	exports.BREAK = BREAK;
	exports.RETURN = RETURN;


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

	// call something on iterator step with safe closing on error
	var anObject = __webpack_require__(13);
	module.exports = function (iterator, fn, value, entries) {
	  try {
	    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
	  // 7.4.6 IteratorClose(iterator, completion)
	  } catch (e) {
	    var ret = iterator['return'];
	    if (ret !== undefined) anObject(ret.call(iterator));
	    throw e;
	  }
	};


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

	// check on default Array iterator
	var Iterators = __webpack_require__(74);
	var ITERATOR = __webpack_require__(82)('iterator');
	var ArrayProto = Array.prototype;

	module.exports = function (it) {
	  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
	};


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

	// 7.3.20 SpeciesConstructor(O, defaultConstructor)
	var anObject = __webpack_require__(13);
	var aFunction = __webpack_require__(10);
	var SPECIES = __webpack_require__(82)('species');
	module.exports = function (O, D) {
	  var C = anObject(O).constructor;
	  var S;
	  return C === undefined || (S = anObject(C)[SPECIES]) == undefined ? D : aFunction(S);
	};


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

	var ctx = __webpack_require__(9);
	var invoke = __webpack_require__(103);
	var html = __webpack_require__(80);
	var cel = __webpack_require__(18);
	var global = __webpack_require__(7);
	var process = global.process;
	var setTask = global.setImmediate;
	var clearTask = global.clearImmediate;
	var MessageChannel = global.MessageChannel;
	var Dispatch = global.Dispatch;
	var counter = 0;
	var queue = {};
	var ONREADYSTATECHANGE = 'onreadystatechange';
	var defer, channel, port;
	var run = function () {
	  var id = +this;
	  // eslint-disable-next-line no-prototype-builtins
	  if (queue.hasOwnProperty(id)) {
	    var fn = queue[id];
	    delete queue[id];
	    fn();
	  }
	};
	var listener = function (event) {
	  run.call(event.data);
	};
	// Node.js 0.9+ & IE10+ has setImmediate, otherwise:
	if (!setTask || !clearTask) {
	  setTask = function setImmediate(fn) {
	    var args = [];
	    var i = 1;
	    while (arguments.length > i) args.push(arguments[i++]);
	    queue[++counter] = function () {
	      // eslint-disable-next-line no-new-func
	      invoke(typeof fn == 'function' ? fn : Function(fn), args);
	    };
	    defer(counter);
	    return counter;
	  };
	  clearTask = function clearImmediate(id) {
	    delete queue[id];
	  };
	  // Node.js 0.8-
	  if (__webpack_require__(39)(process) == 'process') {
	    defer = function (id) {
	      process.nextTick(ctx(run, id, 1));
	    };
	  // Sphere (JS game engine) Dispatch API
	  } else if (Dispatch && Dispatch.now) {
	    defer = function (id) {
	      Dispatch.now(ctx(run, id, 1));
	    };
	  // Browsers with MessageChannel, includes WebWorkers
	  } else if (MessageChannel) {
	    channel = new MessageChannel();
	    port = channel.port2;
	    channel.port1.onmessage = listener;
	    defer = ctx(port.postMessage, port, 1);
	  // Browsers with postMessage, skip WebWorkers
	  // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
	  } else if (global.addEventListener && typeof postMessage == 'function' && !global.importScripts) {
	    defer = function (id) {
	      global.postMessage(id + '', '*');
	    };
	    global.addEventListener('message', listener, false);
	  // IE8-
	  } else if (ONREADYSTATECHANGE in cel('script')) {
	    defer = function (id) {
	      html.appendChild(cel('script'))[ONREADYSTATECHANGE] = function () {
	        html.removeChild(this);
	        run.call(id);
	      };
	    };
	  // Rest old browsers
	  } else {
	    defer = function (id) {
	      setTimeout(ctx(run, id, 1), 0);
	    };
	  }
	}
	module.exports = {
	  set: setTask,
	  clear: clearTask
	};


/***/ }),
/* 103 */
/***/ (function(module, exports) {

	// fast apply, http://jsperf.lnkit.com/fast-apply/5
	module.exports = function (fn, args, that) {
	  var un = that === undefined;
	  switch (args.length) {
	    case 0: return un ? fn()
	                      : fn.call(that);
	    case 1: return un ? fn(args[0])
	                      : fn.call(that, args[0]);
	    case 2: return un ? fn(args[0], args[1])
	                      : fn.call(that, args[0], args[1]);
	    case 3: return un ? fn(args[0], args[1], args[2])
	                      : fn.call(that, args[0], args[1], args[2]);
	    case 4: return un ? fn(args[0], args[1], args[2], args[3])
	                      : fn.call(that, args[0], args[1], args[2], args[3]);
	  } return fn.apply(that, args);
	};


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

	var global = __webpack_require__(7);
	var macrotask = __webpack_require__(102).set;
	var Observer = global.MutationObserver || global.WebKitMutationObserver;
	var process = global.process;
	var Promise = global.Promise;
	var isNode = __webpack_require__(39)(process) == 'process';

	module.exports = function () {
	  var head, last, notify;

	  var flush = function () {
	    var parent, fn;
	    if (isNode && (parent = process.domain)) parent.exit();
	    while (head) {
	      fn = head.fn;
	      head = head.next;
	      try {
	        fn();
	      } catch (e) {
	        if (head) notify();
	        else last = undefined;
	        throw e;
	      }
	    } last = undefined;
	    if (parent) parent.enter();
	  };

	  // Node.js
	  if (isNode) {
	    notify = function () {
	      process.nextTick(flush);
	    };
	  // browsers with MutationObserver, except iOS Safari - https://github.com/zloirock/core-js/issues/339
	  } else if (Observer && !(global.navigator && global.navigator.standalone)) {
	    var toggle = true;
	    var node = document.createTextNode('');
	    new Observer(flush).observe(node, { characterData: true }); // eslint-disable-line no-new
	    notify = function () {
	      node.data = toggle = !toggle;
	    };
	  // environments with maybe non-completely correct, but existent Promise
	  } else if (Promise && Promise.resolve) {
	    // Promise.resolve without an argument throws an error in LG WebOS 2
	    var promise = Promise.resolve(undefined);
	    notify = function () {
	      promise.then(flush);
	    };
	  // for other environments - macrotask based on:
	  // - setImmediate
	  // - MessageChannel
	  // - window.postMessag
	  // - onreadystatechange
	  // - setTimeout
	  } else {
	    notify = function () {
	      // strange IE + webpack dev server bug - use .call(global)
	      macrotask.call(global, flush);
	    };
	  }

	  return function (fn) {
	    var task = { fn: fn, next: undefined };
	    if (last) last.next = task;
	    if (!head) {
	      head = task;
	      notify();
	    } last = task;
	  };
	};


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	// 25.4.1.5 NewPromiseCapability(C)
	var aFunction = __webpack_require__(10);

	function PromiseCapability(C) {
	  var resolve, reject;
	  this.promise = new C(function ($$resolve, $$reject) {
	    if (resolve !== undefined || reject !== undefined) throw TypeError('Bad Promise constructor');
	    resolve = $$resolve;
	    reject = $$reject;
	  });
	  this.resolve = aFunction(resolve);
	  this.reject = aFunction(reject);
	}

	module.exports.f = function (C) {
	  return new PromiseCapability(C);
	};


/***/ }),
/* 106 */
/***/ (function(module, exports) {

	module.exports = function (exec) {
	  try {
	    return { e: false, v: exec() };
	  } catch (e) {
	    return { e: true, v: e };
	  }
	};


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

	var global = __webpack_require__(7);
	var navigator = global.navigator;

	module.exports = navigator && navigator.userAgent || '';


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

	var anObject = __webpack_require__(13);
	var isObject = __webpack_require__(14);
	var newPromiseCapability = __webpack_require__(105);

	module.exports = function (C, x) {
	  anObject(C);
	  if (isObject(x) && x.constructor === C) return x;
	  var promiseCapability = newPromiseCapability.f(C);
	  var resolve = promiseCapability.resolve;
	  resolve(x);
	  return promiseCapability.promise;
	};


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

	var hide = __webpack_require__(11);
	module.exports = function (target, src, safe) {
	  for (var key in src) {
	    if (safe && target[key]) target[key] = src[key];
	    else hide(target, key, src[key]);
	  } return target;
	};


/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	var global = __webpack_require__(7);
	var core = __webpack_require__(8);
	var dP = __webpack_require__(12);
	var DESCRIPTORS = __webpack_require__(16);
	var SPECIES = __webpack_require__(82)('species');

	module.exports = function (KEY) {
	  var C = typeof core[KEY] == 'function' ? core[KEY] : global[KEY];
	  if (DESCRIPTORS && C && !C[SPECIES]) dP.f(C, SPECIES, {
	    configurable: true,
	    get: function () { return this; }
	  });
	};


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

	var ITERATOR = __webpack_require__(82)('iterator');
	var SAFE_CLOSING = false;

	try {
	  var riter = [7][ITERATOR]();
	  riter['return'] = function () { SAFE_CLOSING = true; };
	  // eslint-disable-next-line no-throw-literal
	  Array.from(riter, function () { throw 2; });
	} catch (e) { /* empty */ }

	module.exports = function (exec, skipClosing) {
	  if (!skipClosing && !SAFE_CLOSING) return false;
	  var safe = false;
	  try {
	    var arr = [7];
	    var iter = arr[ITERATOR]();
	    iter.next = function () { return { done: safe = true }; };
	    arr[ITERATOR] = function () { return iter; };
	    exec(arr);
	  } catch (e) { /* empty */ }
	  return safe;
	};


/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

	// https://github.com/tc39/proposal-promise-finally
	'use strict';
	var $export = __webpack_require__(6);
	var core = __webpack_require__(8);
	var global = __webpack_require__(7);
	var speciesConstructor = __webpack_require__(101);
	var promiseResolve = __webpack_require__(108);

	$export($export.P + $export.R, 'Promise', { 'finally': function (onFinally) {
	  var C = speciesConstructor(this, core.Promise || global.Promise);
	  var isFunction = typeof onFinally == 'function';
	  return this.then(
	    isFunction ? function (x) {
	      return promiseResolve(C, onFinally()).then(function () { return x; });
	    } : onFinally,
	    isFunction ? function (e) {
	      return promiseResolve(C, onFinally()).then(function () { throw e; });
	    } : onFinally
	  );
	} });


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	// https://github.com/tc39/proposal-promise-try
	var $export = __webpack_require__(6);
	var newPromiseCapability = __webpack_require__(105);
	var perform = __webpack_require__(106);

	$export($export.S, 'Promise', { 'try': function (callbackfn) {
	  var promiseCapability = newPromiseCapability.f(this);
	  var result = perform(callbackfn);
	  (result.e ? promiseCapability.reject : promiseCapability.resolve)(result.v);
	  return promiseCapability.promise;
	} });


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

	/**
	 * Root reference for iframes.
	 */

	var root;
	if (typeof window !== 'undefined') { // Browser window
	  root = window;
	} else if (typeof self !== 'undefined') { // Web Worker
	  root = self;
	} else { // Other environments
	  console.warn("Using browser-only version of superagent in non-browser environment");
	  root = this;
	}

	var Emitter = __webpack_require__(115);
	var requestBase = __webpack_require__(116);
	var isObject = __webpack_require__(117);

	/**
	 * Noop.
	 */

	function noop(){};

	/**
	 * Expose `request`.
	 */

	var request = module.exports = __webpack_require__(118).bind(null, Request);

	/**
	 * Determine XHR.
	 */

	request.getXHR = function () {
	  if (root.XMLHttpRequest
	      && (!root.location || 'file:' != root.location.protocol
	          || !root.ActiveXObject)) {
	    return new XMLHttpRequest;
	  } else {
	    try { return new ActiveXObject('Microsoft.XMLHTTP'); } catch(e) {}
	    try { return new ActiveXObject('Msxml2.XMLHTTP.6.0'); } catch(e) {}
	    try { return new ActiveXObject('Msxml2.XMLHTTP.3.0'); } catch(e) {}
	    try { return new ActiveXObject('Msxml2.XMLHTTP'); } catch(e) {}
	  }
	  throw Error("Browser-only verison of superagent could not find XHR");
	};

	/**
	 * Removes leading and trailing whitespace, added to support IE.
	 *
	 * @param {String} s
	 * @return {String}
	 * @api private
	 */

	var trim = ''.trim
	  ? function(s) { return s.trim(); }
	  : function(s) { return s.replace(/(^\s*|\s*$)/g, ''); };

	/**
	 * Serialize the given `obj`.
	 *
	 * @param {Object} obj
	 * @return {String}
	 * @api private
	 */

	function serialize(obj) {
	  if (!isObject(obj)) return obj;
	  var pairs = [];
	  for (var key in obj) {
	    pushEncodedKeyValuePair(pairs, key, obj[key]);
	  }
	  return pairs.join('&');
	}

	/**
	 * Helps 'serialize' with serializing arrays.
	 * Mutates the pairs array.
	 *
	 * @param {Array} pairs
	 * @param {String} key
	 * @param {Mixed} val
	 */

	function pushEncodedKeyValuePair(pairs, key, val) {
	  if (val != null) {
	    if (Array.isArray(val)) {
	      val.forEach(function(v) {
	        pushEncodedKeyValuePair(pairs, key, v);
	      });
	    } else if (isObject(val)) {
	      for(var subkey in val) {
	        pushEncodedKeyValuePair(pairs, key + '[' + subkey + ']', val[subkey]);
	      }
	    } else {
	      pairs.push(encodeURIComponent(key)
	        + '=' + encodeURIComponent(val));
	    }
	  } else if (val === null) {
	    pairs.push(encodeURIComponent(key));
	  }
	}

	/**
	 * Expose serialization method.
	 */

	 request.serializeObject = serialize;

	 /**
	  * Parse the given x-www-form-urlencoded `str`.
	  *
	  * @param {String} str
	  * @return {Object}
	  * @api private
	  */

	function parseString(str) {
	  var obj = {};
	  var pairs = str.split('&');
	  var pair;
	  var pos;

	  for (var i = 0, len = pairs.length; i < len; ++i) {
	    pair = pairs[i];
	    pos = pair.indexOf('=');
	    if (pos == -1) {
	      obj[decodeURIComponent(pair)] = '';
	    } else {
	      obj[decodeURIComponent(pair.slice(0, pos))] =
	        decodeURIComponent(pair.slice(pos + 1));
	    }
	  }

	  return obj;
	}

	/**
	 * Expose parser.
	 */

	request.parseString = parseString;

	/**
	 * Default MIME type map.
	 *
	 *     superagent.types.xml = 'application/xml';
	 *
	 */

	request.types = {
	  html: 'text/html',
	  json: 'application/json',
	  xml: 'application/xml',
	  urlencoded: 'application/x-www-form-urlencoded',
	  'form': 'application/x-www-form-urlencoded',
	  'form-data': 'application/x-www-form-urlencoded'
	};

	/**
	 * Default serialization map.
	 *
	 *     superagent.serialize['application/xml'] = function(obj){
	 *       return 'generated xml here';
	 *     };
	 *
	 */

	 request.serialize = {
	   'application/x-www-form-urlencoded': serialize,
	   'application/json': JSON.stringify
	 };

	 /**
	  * Default parsers.
	  *
	  *     superagent.parse['application/xml'] = function(str){
	  *       return { object parsed from str };
	  *     };
	  *
	  */

	request.parse = {
	  'application/x-www-form-urlencoded': parseString,
	  'application/json': JSON.parse
	};

	/**
	 * Parse the given header `str` into
	 * an object containing the mapped fields.
	 *
	 * @param {String} str
	 * @return {Object}
	 * @api private
	 */

	function parseHeader(str) {
	  var lines = str.split(/\r?\n/);
	  var fields = {};
	  var index;
	  var line;
	  var field;
	  var val;

	  lines.pop(); // trailing CRLF

	  for (var i = 0, len = lines.length; i < len; ++i) {
	    line = lines[i];
	    index = line.indexOf(':');
	    field = line.slice(0, index).toLowerCase();
	    val = trim(line.slice(index + 1));
	    fields[field] = val;
	  }

	  return fields;
	}

	/**
	 * Check if `mime` is json or has +json structured syntax suffix.
	 *
	 * @param {String} mime
	 * @return {Boolean}
	 * @api private
	 */

	function isJSON(mime) {
	  return /[\/+]json\b/.test(mime);
	}

	/**
	 * Return the mime type for the given `str`.
	 *
	 * @param {String} str
	 * @return {String}
	 * @api private
	 */

	function type(str){
	  return str.split(/ *; */).shift();
	};

	/**
	 * Return header field parameters.
	 *
	 * @param {String} str
	 * @return {Object}
	 * @api private
	 */

	function params(str){
	  return str.split(/ *; */).reduce(function(obj, str){
	    var parts = str.split(/ *= */),
	        key = parts.shift(),
	        val = parts.shift();

	    if (key && val) obj[key] = val;
	    return obj;
	  }, {});
	};

	/**
	 * Initialize a new `Response` with the given `xhr`.
	 *
	 *  - set flags (.ok, .error, etc)
	 *  - parse header
	 *
	 * Examples:
	 *
	 *  Aliasing `superagent` as `request` is nice:
	 *
	 *      request = superagent;
	 *
	 *  We can use the promise-like API, or pass callbacks:
	 *
	 *      request.get('/').end(function(res){});
	 *      request.get('/', function(res){});
	 *
	 *  Sending data can be chained:
	 *
	 *      request
	 *        .post('/user')
	 *        .send({ name: 'tj' })
	 *        .end(function(res){});
	 *
	 *  Or passed to `.send()`:
	 *
	 *      request
	 *        .post('/user')
	 *        .send({ name: 'tj' }, function(res){});
	 *
	 *  Or passed to `.post()`:
	 *
	 *      request
	 *        .post('/user', { name: 'tj' })
	 *        .end(function(res){});
	 *
	 * Or further reduced to a single call for simple cases:
	 *
	 *      request
	 *        .post('/user', { name: 'tj' }, function(res){});
	 *
	 * @param {XMLHTTPRequest} xhr
	 * @param {Object} options
	 * @api private
	 */

	function Response(req, options) {
	  options = options || {};
	  this.req = req;
	  this.xhr = this.req.xhr;
	  // responseText is accessible only if responseType is '' or 'text' and on older browsers
	  this.text = ((this.req.method !='HEAD' && (this.xhr.responseType === '' || this.xhr.responseType === 'text')) || typeof this.xhr.responseType === 'undefined')
	     ? this.xhr.responseText
	     : null;
	  this.statusText = this.req.xhr.statusText;
	  this._setStatusProperties(this.xhr.status);
	  this.header = this.headers = parseHeader(this.xhr.getAllResponseHeaders());
	  // getAllResponseHeaders sometimes falsely returns "" for CORS requests, but
	  // getResponseHeader still works. so we get content-type even if getting
	  // other headers fails.
	  this.header['content-type'] = this.xhr.getResponseHeader('content-type');
	  this._setHeaderProperties(this.header);
	  this.body = this.req.method != 'HEAD'
	    ? this._parseBody(this.text ? this.text : this.xhr.response)
	    : null;
	}

	/**
	 * Get case-insensitive `field` value.
	 *
	 * @param {String} field
	 * @return {String}
	 * @api public
	 */

	Response.prototype.get = function(field){
	  return this.header[field.toLowerCase()];
	};

	/**
	 * Set header related properties:
	 *
	 *   - `.type` the content type without params
	 *
	 * A response of "Content-Type: text/plain; charset=utf-8"
	 * will provide you with a `.type` of "text/plain".
	 *
	 * @param {Object} header
	 * @api private
	 */

	Response.prototype._setHeaderProperties = function(header){
	  // content-type
	  var ct = this.header['content-type'] || '';
	  this.type = type(ct);

	  // params
	  var obj = params(ct);
	  for (var key in obj) this[key] = obj[key];
	};

	/**
	 * Parse the given body `str`.
	 *
	 * Used for auto-parsing of bodies. Parsers
	 * are defined on the `superagent.parse` object.
	 *
	 * @param {String} str
	 * @return {Mixed}
	 * @api private
	 */

	Response.prototype._parseBody = function(str){
	  var parse = request.parse[this.type];
	  if (!parse && isJSON(this.type)) {
	    parse = request.parse['application/json'];
	  }
	  return parse && str && (str.length || str instanceof Object)
	    ? parse(str)
	    : null;
	};

	/**
	 * Set flags such as `.ok` based on `status`.
	 *
	 * For example a 2xx response will give you a `.ok` of __true__
	 * whereas 5xx will be __false__ and `.error` will be __true__. The
	 * `.clientError` and `.serverError` are also available to be more
	 * specific, and `.statusType` is the class of error ranging from 1..5
	 * sometimes useful for mapping respond colors etc.
	 *
	 * "sugar" properties are also defined for common cases. Currently providing:
	 *
	 *   - .noContent
	 *   - .badRequest
	 *   - .unauthorized
	 *   - .notAcceptable
	 *   - .notFound
	 *
	 * @param {Number} status
	 * @api private
	 */

	Response.prototype._setStatusProperties = function(status){
	  // handle IE9 bug: http://stackoverflow.com/questions/10046972/msie-returns-status-code-of-1223-for-ajax-request
	  if (status === 1223) {
	    status = 204;
	  }

	  var type = status / 100 | 0;

	  // status / class
	  this.status = this.statusCode = status;
	  this.statusType = type;

	  // basics
	  this.info = 1 == type;
	  this.ok = 2 == type;
	  this.clientError = 4 == type;
	  this.serverError = 5 == type;
	  this.error = (4 == type || 5 == type)
	    ? this.toError()
	    : false;

	  // sugar
	  this.accepted = 202 == status;
	  this.noContent = 204 == status;
	  this.badRequest = 400 == status;
	  this.unauthorized = 401 == status;
	  this.notAcceptable = 406 == status;
	  this.notFound = 404 == status;
	  this.forbidden = 403 == status;
	};

	/**
	 * Return an `Error` representative of this response.
	 *
	 * @return {Error}
	 * @api public
	 */

	Response.prototype.toError = function(){
	  var req = this.req;
	  var method = req.method;
	  var url = req.url;

	  var msg = 'cannot ' + method + ' ' + url + ' (' + this.status + ')';
	  var err = new Error(msg);
	  err.status = this.status;
	  err.method = method;
	  err.url = url;

	  return err;
	};

	/**
	 * Expose `Response`.
	 */

	request.Response = Response;

	/**
	 * Initialize a new `Request` with the given `method` and `url`.
	 *
	 * @param {String} method
	 * @param {String} url
	 * @api public
	 */

	function Request(method, url) {
	  var self = this;
	  this._query = this._query || [];
	  this.method = method;
	  this.url = url;
	  this.header = {}; // preserves header name case
	  this._header = {}; // coerces header names to lowercase
	  this.on('end', function(){
	    var err = null;
	    var res = null;

	    try {
	      res = new Response(self);
	    } catch(e) {
	      err = new Error('Parser is unable to parse the response');
	      err.parse = true;
	      err.original = e;
	      // issue #675: return the raw response if the response parsing fails
	      err.rawResponse = self.xhr && self.xhr.responseText ? self.xhr.responseText : null;
	      // issue #876: return the http status code if the response parsing fails
	      err.statusCode = self.xhr && self.xhr.status ? self.xhr.status : null;
	      return self.callback(err);
	    }

	    self.emit('response', res);

	    var new_err;
	    try {
	      if (res.status < 200 || res.status >= 300) {
	        new_err = new Error(res.statusText || 'Unsuccessful HTTP response');
	        new_err.original = err;
	        new_err.response = res;
	        new_err.status = res.status;
	      }
	    } catch(e) {
	      new_err = e; // #985 touching res may cause INVALID_STATE_ERR on old Android
	    }

	    // #1000 don't catch errors from the callback to avoid double calling it
	    if (new_err) {
	      self.callback(new_err, res);
	    } else {
	      self.callback(null, res);
	    }
	  });
	}

	/**
	 * Mixin `Emitter` and `requestBase`.
	 */

	Emitter(Request.prototype);
	for (var key in requestBase) {
	  Request.prototype[key] = requestBase[key];
	}

	/**
	 * Set Content-Type to `type`, mapping values from `request.types`.
	 *
	 * Examples:
	 *
	 *      superagent.types.xml = 'application/xml';
	 *
	 *      request.post('/')
	 *        .type('xml')
	 *        .send(xmlstring)
	 *        .end(callback);
	 *
	 *      request.post('/')
	 *        .type('application/xml')
	 *        .send(xmlstring)
	 *        .end(callback);
	 *
	 * @param {String} type
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.type = function(type){
	  this.set('Content-Type', request.types[type] || type);
	  return this;
	};

	/**
	 * Set responseType to `val`. Presently valid responseTypes are 'blob' and
	 * 'arraybuffer'.
	 *
	 * Examples:
	 *
	 *      req.get('/')
	 *        .responseType('blob')
	 *        .end(callback);
	 *
	 * @param {String} val
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.responseType = function(val){
	  this._responseType = val;
	  return this;
	};

	/**
	 * Set Accept to `type`, mapping values from `request.types`.
	 *
	 * Examples:
	 *
	 *      superagent.types.json = 'application/json';
	 *
	 *      request.get('/agent')
	 *        .accept('json')
	 *        .end(callback);
	 *
	 *      request.get('/agent')
	 *        .accept('application/json')
	 *        .end(callback);
	 *
	 * @param {String} accept
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.accept = function(type){
	  this.set('Accept', request.types[type] || type);
	  return this;
	};

	/**
	 * Set Authorization field value with `user` and `pass`.
	 *
	 * @param {String} user
	 * @param {String} pass
	 * @param {Object} options with 'type' property 'auto' or 'basic' (default 'basic')
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.auth = function(user, pass, options){
	  if (!options) {
	    options = {
	      type: 'basic'
	    }
	  }

	  switch (options.type) {
	    case 'basic':
	      var str = btoa(user + ':' + pass);
	      this.set('Authorization', 'Basic ' + str);
	    break;

	    case 'auto':
	      this.username = user;
	      this.password = pass;
	    break;
	  }
	  return this;
	};

	/**
	* Add query-string `val`.
	*
	* Examples:
	*
	*   request.get('/shoes')
	*     .query('size=10')
	*     .query({ color: 'blue' })
	*
	* @param {Object|String} val
	* @return {Request} for chaining
	* @api public
	*/

	Request.prototype.query = function(val){
	  if ('string' != typeof val) val = serialize(val);
	  if (val) this._query.push(val);
	  return this;
	};

	/**
	 * Queue the given `file` as an attachment to the specified `field`,
	 * with optional `filename`.
	 *
	 * ``` js
	 * request.post('/upload')
	 *   .attach('content', new Blob(['<a id="a"><b id="b">hey!</b></a>'], { type: "text/html"}))
	 *   .end(callback);
	 * ```
	 *
	 * @param {String} field
	 * @param {Blob|File} file
	 * @param {String} filename
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.attach = function(field, file, filename){
	  this._getFormData().append(field, file, filename || file.name);
	  return this;
	};

	Request.prototype._getFormData = function(){
	  if (!this._formData) {
	    this._formData = new root.FormData();
	  }
	  return this._formData;
	};

	/**
	 * Invoke the callback with `err` and `res`
	 * and handle arity check.
	 *
	 * @param {Error} err
	 * @param {Response} res
	 * @api private
	 */

	Request.prototype.callback = function(err, res){
	  var fn = this._callback;
	  this.clearTimeout();
	  fn(err, res);
	};

	/**
	 * Invoke callback with x-domain error.
	 *
	 * @api private
	 */

	Request.prototype.crossDomainError = function(){
	  var err = new Error('Request has been terminated\nPossible causes: the network is offline, Origin is not allowed by Access-Control-Allow-Origin, the page is being unloaded, etc.');
	  err.crossDomain = true;

	  err.status = this.status;
	  err.method = this.method;
	  err.url = this.url;

	  this.callback(err);
	};

	/**
	 * Invoke callback with timeout error.
	 *
	 * @api private
	 */

	Request.prototype._timeoutError = function(){
	  var timeout = this._timeout;
	  var err = new Error('timeout of ' + timeout + 'ms exceeded');
	  err.timeout = timeout;
	  this.callback(err);
	};

	/**
	 * Compose querystring to append to req.url
	 *
	 * @api private
	 */

	Request.prototype._appendQueryString = function(){
	  var query = this._query.join('&');
	  if (query) {
	    this.url += ~this.url.indexOf('?')
	      ? '&' + query
	      : '?' + query;
	  }
	};

	/**
	 * Initiate request, invoking callback `fn(res)`
	 * with an instanceof `Response`.
	 *
	 * @param {Function} fn
	 * @return {Request} for chaining
	 * @api public
	 */

	Request.prototype.end = function(fn){
	  var self = this;
	  var xhr = this.xhr = request.getXHR();
	  var timeout = this._timeout;
	  var data = this._formData || this._data;

	  // store callback
	  this._callback = fn || noop;

	  // state change
	  xhr.onreadystatechange = function(){
	    if (4 != xhr.readyState) return;

	    // In IE9, reads to any property (e.g. status) off of an aborted XHR will
	    // result in the error "Could not complete the operation due to error c00c023f"
	    var status;
	    try { status = xhr.status } catch(e) { status = 0; }

	    if (0 == status) {
	      if (self.timedout) return self._timeoutError();
	      if (self._aborted) return;
	      return self.crossDomainError();
	    }
	    self.emit('end');
	  };

	  // progress
	  var handleProgress = function(direction, e) {
	    if (e.total > 0) {
	      e.percent = e.loaded / e.total * 100;
	    }
	    e.direction = direction;
	    self.emit('progress', e);
	  }
	  if (this.hasListeners('progress')) {
	    try {
	      xhr.onprogress = handleProgress.bind(null, 'download');
	      if (xhr.upload) {
	        xhr.upload.onprogress = handleProgress.bind(null, 'upload');
	      }
	    } catch(e) {
	      // Accessing xhr.upload fails in IE from a web worker, so just pretend it doesn't exist.
	      // Reported here:
	      // https://connect.microsoft.com/IE/feedback/details/837245/xmlhttprequest-upload-throws-invalid-argument-when-used-from-web-worker-context
	    }
	  }

	  // timeout
	  if (timeout && !this._timer) {
	    this._timer = setTimeout(function(){
	      self.timedout = true;
	      self.abort();
	    }, timeout);
	  }

	  // querystring
	  this._appendQueryString();

	  // initiate request
	  if (this.username && this.password) {
	    xhr.open(this.method, this.url, true, this.username, this.password);
	  } else {
	    xhr.open(this.method, this.url, true);
	  }

	  // CORS
	  if (this._withCredentials) xhr.withCredentials = true;

	  // body
	  if ('GET' != this.method && 'HEAD' != this.method && 'string' != typeof data && !this._isHost(data)) {
	    // serialize stuff
	    var contentType = this._header['content-type'];
	    var serialize = this._serializer || request.serialize[contentType ? contentType.split(';')[0] : ''];
	    if (!serialize && isJSON(contentType)) serialize = request.serialize['application/json'];
	    if (serialize) data = serialize(data);
	  }

	  // set header fields
	  for (var field in this.header) {
	    if (null == this.header[field]) continue;
	    xhr.setRequestHeader(field, this.header[field]);
	  }

	  if (this._responseType) {
	    xhr.responseType = this._responseType;
	  }

	  // send stuff
	  this.emit('request', this);

	  // IE11 xhr.send(undefined) sends 'undefined' string as POST payload (instead of nothing)
	  // We need null here if data is undefined
	  xhr.send(typeof data !== 'undefined' ? data : null);
	  return this;
	};


	/**
	 * Expose `Request`.
	 */

	request.Request = Request;

	/**
	 * GET `url` with optional callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Mixed|Function} [data] or fn
	 * @param {Function} [fn]
	 * @return {Request}
	 * @api public
	 */

	request.get = function(url, data, fn){
	  var req = request('GET', url);
	  if ('function' == typeof data) fn = data, data = null;
	  if (data) req.query(data);
	  if (fn) req.end(fn);
	  return req;
	};

	/**
	 * HEAD `url` with optional callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Mixed|Function} [data] or fn
	 * @param {Function} [fn]
	 * @return {Request}
	 * @api public
	 */

	request.head = function(url, data, fn){
	  var req = request('HEAD', url);
	  if ('function' == typeof data) fn = data, data = null;
	  if (data) req.send(data);
	  if (fn) req.end(fn);
	  return req;
	};

	/**
	 * OPTIONS query to `url` with optional callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Mixed|Function} [data] or fn
	 * @param {Function} [fn]
	 * @return {Request}
	 * @api public
	 */

	request.options = function(url, data, fn){
	  var req = request('OPTIONS', url);
	  if ('function' == typeof data) fn = data, data = null;
	  if (data) req.send(data);
	  if (fn) req.end(fn);
	  return req;
	};

	/**
	 * DELETE `url` with optional callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Function} [fn]
	 * @return {Request}
	 * @api public
	 */

	function del(url, fn){
	  var req = request('DELETE', url);
	  if (fn) req.end(fn);
	  return req;
	};

	request['del'] = del;
	request['delete'] = del;

	/**
	 * PATCH `url` with optional `data` and callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Mixed} [data]
	 * @param {Function} [fn]
	 * @return {Request}
	 * @api public
	 */

	request.patch = function(url, data, fn){
	  var req = request('PATCH', url);
	  if ('function' == typeof data) fn = data, data = null;
	  if (data) req.send(data);
	  if (fn) req.end(fn);
	  return req;
	};

	/**
	 * POST `url` with optional `data` and callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Mixed} [data]
	 * @param {Function} [fn]
	 * @return {Request}
	 * @api public
	 */

	request.post = function(url, data, fn){
	  var req = request('POST', url);
	  if ('function' == typeof data) fn = data, data = null;
	  if (data) req.send(data);
	  if (fn) req.end(fn);
	  return req;
	};

	/**
	 * PUT `url` with optional `data` and callback `fn(res)`.
	 *
	 * @param {String} url
	 * @param {Mixed|Function} [data] or fn
	 * @param {Function} [fn]
	 * @return {Request}
	 * @api public
	 */

	request.put = function(url, data, fn){
	  var req = request('PUT', url);
	  if ('function' == typeof data) fn = data, data = null;
	  if (data) req.send(data);
	  if (fn) req.end(fn);
	  return req;
	};


/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

	
	/**
	 * Expose `Emitter`.
	 */

	if (true) {
	  module.exports = Emitter;
	}

	/**
	 * Initialize a new `Emitter`.
	 *
	 * @api public
	 */

	function Emitter(obj) {
	  if (obj) return mixin(obj);
	};

	/**
	 * Mixin the emitter properties.
	 *
	 * @param {Object} obj
	 * @return {Object}
	 * @api private
	 */

	function mixin(obj) {
	  for (var key in Emitter.prototype) {
	    obj[key] = Emitter.prototype[key];
	  }
	  return obj;
	}

	/**
	 * Listen on the given `event` with `fn`.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.on =
	Emitter.prototype.addEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};
	  (this._callbacks['$' + event] = this._callbacks['$' + event] || [])
	    .push(fn);
	  return this;
	};

	/**
	 * Adds an `event` listener that will be invoked a single
	 * time then automatically removed.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.once = function(event, fn){
	  function on() {
	    this.off(event, on);
	    fn.apply(this, arguments);
	  }

	  on.fn = fn;
	  this.on(event, on);
	  return this;
	};

	/**
	 * Remove the given callback for `event` or all
	 * registered callbacks.
	 *
	 * @param {String} event
	 * @param {Function} fn
	 * @return {Emitter}
	 * @api public
	 */

	Emitter.prototype.off =
	Emitter.prototype.removeListener =
	Emitter.prototype.removeAllListeners =
	Emitter.prototype.removeEventListener = function(event, fn){
	  this._callbacks = this._callbacks || {};

	  // all
	  if (0 == arguments.length) {
	    this._callbacks = {};
	    return this;
	  }

	  // specific event
	  var callbacks = this._callbacks['$' + event];
	  if (!callbacks) return this;

	  // remove all handlers
	  if (1 == arguments.length) {
	    delete this._callbacks['$' + event];
	    return this;
	  }

	  // remove specific handler
	  var cb;
	  for (var i = 0; i < callbacks.length; i++) {
	    cb = callbacks[i];
	    if (cb === fn || cb.fn === fn) {
	      callbacks.splice(i, 1);
	      break;
	    }
	  }

	  // Remove event specific arrays for event types that no
	  // one is subscribed for to avoid memory leak.
	  if (callbacks.length === 0) {
	    delete this._callbacks['$' + event];
	  }

	  return this;
	};

	/**
	 * Emit `event` with the given args.
	 *
	 * @param {String} event
	 * @param {Mixed} ...
	 * @return {Emitter}
	 */

	Emitter.prototype.emit = function(event){
	  this._callbacks = this._callbacks || {};

	  var args = new Array(arguments.length - 1)
	    , callbacks = this._callbacks['$' + event];

	  for (var i = 1; i < arguments.length; i++) {
	    args[i - 1] = arguments[i];
	  }

	  if (callbacks) {
	    callbacks = callbacks.slice(0);
	    for (var i = 0, len = callbacks.length; i < len; ++i) {
	      callbacks[i].apply(this, args);
	    }
	  }

	  return this;
	};

	/**
	 * Return array of callbacks for `event`.
	 *
	 * @param {String} event
	 * @return {Array}
	 * @api public
	 */

	Emitter.prototype.listeners = function(event){
	  this._callbacks = this._callbacks || {};
	  return this._callbacks['$' + event] || [];
	};

	/**
	 * Check if this emitter has `event` handlers.
	 *
	 * @param {String} event
	 * @return {Boolean}
	 * @api public
	 */

	Emitter.prototype.hasListeners = function(event){
	  return !! this.listeners(event).length;
	};


/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

	/**
	 * Module of mixed-in functions shared between node and client code
	 */
	var isObject = __webpack_require__(117);

	/**
	 * Clear previous timeout.
	 *
	 * @return {Request} for chaining
	 * @api public
	 */

	exports.clearTimeout = function _clearTimeout(){
	  this._timeout = 0;
	  clearTimeout(this._timer);
	  return this;
	};

	/**
	 * Override default response body parser
	 *
	 * This function will be called to convert incoming data into request.body
	 *
	 * @param {Function}
	 * @api public
	 */

	exports.parse = function parse(fn){
	  this._parser = fn;
	  return this;
	};

	/**
	 * Override default request body serializer
	 *
	 * This function will be called to convert data set via .send or .attach into payload to send
	 *
	 * @param {Function}
	 * @api public
	 */

	exports.serialize = function serialize(fn){
	  this._serializer = fn;
	  return this;
	};

	/**
	 * Set timeout to `ms`.
	 *
	 * @param {Number} ms
	 * @return {Request} for chaining
	 * @api public
	 */

	exports.timeout = function timeout(ms){
	  this._timeout = ms;
	  return this;
	};

	/**
	 * Promise support
	 *
	 * @param {Function} resolve
	 * @param {Function} reject
	 * @return {Request}
	 */

	exports.then = function then(resolve, reject) {
	  if (!this._fullfilledPromise) {
	    var self = this;
	    this._fullfilledPromise = new Promise(function(innerResolve, innerReject){
	      self.end(function(err, res){
	        if (err) innerReject(err); else innerResolve(res);
	      });
	    });
	  }
	  return this._fullfilledPromise.then(resolve, reject);
	}

	exports.catch = function(cb) {
	  return this.then(undefined, cb);
	};

	/**
	 * Allow for extension
	 */

	exports.use = function use(fn) {
	  fn(this);
	  return this;
	}


	/**
	 * Get request header `field`.
	 * Case-insensitive.
	 *
	 * @param {String} field
	 * @return {String}
	 * @api public
	 */

	exports.get = function(field){
	  return this._header[field.toLowerCase()];
	};

	/**
	 * Get case-insensitive header `field` value.
	 * This is a deprecated internal API. Use `.get(field)` instead.
	 *
	 * (getHeader is no longer used internally by the superagent code base)
	 *
	 * @param {String} field
	 * @return {String}
	 * @api private
	 * @deprecated
	 */

	exports.getHeader = exports.get;

	/**
	 * Set header `field` to `val`, or multiple fields with one object.
	 * Case-insensitive.
	 *
	 * Examples:
	 *
	 *      req.get('/')
	 *        .set('Accept', 'application/json')
	 *        .set('X-API-Key', 'foobar')
	 *        .end(callback);
	 *
	 *      req.get('/')
	 *        .set({ Accept: 'application/json', 'X-API-Key': 'foobar' })
	 *        .end(callback);
	 *
	 * @param {String|Object} field
	 * @param {String} val
	 * @return {Request} for chaining
	 * @api public
	 */

	exports.set = function(field, val){
	  if (isObject(field)) {
	    for (var key in field) {
	      this.set(key, field[key]);
	    }
	    return this;
	  }
	  this._header[field.toLowerCase()] = val;
	  this.header[field] = val;
	  return this;
	};

	/**
	 * Remove header `field`.
	 * Case-insensitive.
	 *
	 * Example:
	 *
	 *      req.get('/')
	 *        .unset('User-Agent')
	 *        .end(callback);
	 *
	 * @param {String} field
	 */
	exports.unset = function(field){
	  delete this._header[field.toLowerCase()];
	  delete this.header[field];
	  return this;
	};

	/**
	 * Write the field `name` and `val`, or multiple fields with one object
	 * for "multipart/form-data" request bodies.
	 *
	 * ``` js
	 * request.post('/upload')
	 *   .field('foo', 'bar')
	 *   .end(callback);
	 *
	 * request.post('/upload')
	 *   .field({ foo: 'bar', baz: 'qux' })
	 *   .end(callback);
	 * ```
	 *
	 * @param {String|Object} name
	 * @param {String|Blob|File|Buffer|fs.ReadStream} val
	 * @return {Request} for chaining
	 * @api public
	 */
	exports.field = function(name, val) {

	  // name should be either a string or an object.
	  if (null === name ||  undefined === name) {
	    throw new Error('.field(name, val) name can not be empty');
	  }

	  if (isObject(name)) {
	    for (var key in name) {
	      this.field(key, name[key]);
	    }
	    return this;
	  }

	  // val should be defined now
	  if (null === val || undefined === val) {
	    throw new Error('.field(name, val) val can not be empty');
	  }
	  this._getFormData().append(name, val);
	  return this;
	};

	/**
	 * Abort the request, and clear potential timeout.
	 *
	 * @return {Request}
	 * @api public
	 */
	exports.abort = function(){
	  if (this._aborted) {
	    return this;
	  }
	  this._aborted = true;
	  this.xhr && this.xhr.abort(); // browser
	  this.req && this.req.abort(); // node
	  this.clearTimeout();
	  this.emit('abort');
	  return this;
	};

	/**
	 * Enable transmission of cookies with x-domain requests.
	 *
	 * Note that for this to work the origin must not be
	 * using "Access-Control-Allow-Origin" with a wildcard,
	 * and also must set "Access-Control-Allow-Credentials"
	 * to "true".
	 *
	 * @api public
	 */

	exports.withCredentials = function(){
	  // This is browser-only functionality. Node side is no-op.
	  this._withCredentials = true;
	  return this;
	};

	/**
	 * Set the max redirects to `n`. Does noting in browser XHR implementation.
	 *
	 * @param {Number} n
	 * @return {Request} for chaining
	 * @api public
	 */

	exports.redirects = function(n){
	  this._maxRedirects = n;
	  return this;
	};

	/**
	 * Convert to a plain javascript object (not JSON string) of scalar properties.
	 * Note as this method is designed to return a useful non-this value,
	 * it cannot be chained.
	 *
	 * @return {Object} describing method, url, and data of this request
	 * @api public
	 */

	exports.toJSON = function(){
	  return {
	    method: this.method,
	    url: this.url,
	    data: this._data,
	    headers: this._header
	  };
	};

	/**
	 * Check if `obj` is a host object,
	 * we don't want to serialize these :)
	 *
	 * TODO: future proof, move to compoent land
	 *
	 * @param {Object} obj
	 * @return {Boolean}
	 * @api private
	 */

	exports._isHost = function _isHost(obj) {
	  var str = {}.toString.call(obj);

	  switch (str) {
	    case '[object File]':
	    case '[object Blob]':
	    case '[object FormData]':
	      return true;
	    default:
	      return false;
	  }
	}

	/**
	 * Send `data` as the request body, defaulting the `.type()` to "json" when
	 * an object is given.
	 *
	 * Examples:
	 *
	 *       // manual json
	 *       request.post('/user')
	 *         .type('json')
	 *         .send('{"name":"tj"}')
	 *         .end(callback)
	 *
	 *       // auto json
	 *       request.post('/user')
	 *         .send({ name: 'tj' })
	 *         .end(callback)
	 *
	 *       // manual x-www-form-urlencoded
	 *       request.post('/user')
	 *         .type('form')
	 *         .send('name=tj')
	 *         .end(callback)
	 *
	 *       // auto x-www-form-urlencoded
	 *       request.post('/user')
	 *         .type('form')
	 *         .send({ name: 'tj' })
	 *         .end(callback)
	 *
	 *       // defaults to x-www-form-urlencoded
	 *      request.post('/user')
	 *        .send('name=tobi')
	 *        .send('species=ferret')
	 *        .end(callback)
	 *
	 * @param {String|Object} data
	 * @return {Request} for chaining
	 * @api public
	 */

	exports.send = function(data){
	  var obj = isObject(data);
	  var type = this._header['content-type'];

	  // merge
	  if (obj && isObject(this._data)) {
	    for (var key in data) {
	      this._data[key] = data[key];
	    }
	  } else if ('string' == typeof data) {
	    // default to x-www-form-urlencoded
	    if (!type) this.type('form');
	    type = this._header['content-type'];
	    if ('application/x-www-form-urlencoded' == type) {
	      this._data = this._data
	        ? this._data + '&' + data
	        : data;
	    } else {
	      this._data = (this._data || '') + data;
	    }
	  } else {
	    this._data = data;
	  }

	  if (!obj || this._isHost(data)) return this;

	  // default to json
	  if (!type) this.type('json');
	  return this;
	};


/***/ }),
/* 117 */
/***/ (function(module, exports) {

	/**
	 * Check if `obj` is an object.
	 *
	 * @param {Object} obj
	 * @return {Boolean}
	 * @api private
	 */

	function isObject(obj) {
	  return null !== obj && 'object' === typeof obj;
	}

	module.exports = isObject;


/***/ }),
/* 118 */
/***/ (function(module, exports) {

	// The node and browser modules expose versions of this with the
	// appropriate constructor function bound as first argument
	/**
	 * Issue a request:
	 *
	 * Examples:
	 *
	 *    request('GET', '/users').end(callback)
	 *    request('/users').end(callback)
	 *    request('/users', callback)
	 *
	 * @param {String} method
	 * @param {String|Function} url or callback
	 * @return {Request}
	 * @api public
	 */

	function request(RequestConstructor, method, url) {
	  // callback
	  if ('function' == typeof url) {
	    return new RequestConstructor('GET', method).end(url);
	  }

	  // url first
	  if (2 == arguments.length) {
	    return new RequestConstructor('GET', method);
	  }

	  return new RequestConstructor(method, url);
	}

	module.exports = request;


/***/ }),
/* 119 */
/***/ (function(module, exports) {

	(function () {
	  var validator = new RegExp("^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$", "i");

	  function gen(count) {
	    var out = "";
	    for (var i=0; i<count; i++) {
	      out += (((1+Math.random())*0x10000)|0).toString(16).substring(1);
	    }
	    return out;
	  }

	  function Guid(guid) {
	    if (!guid) throw new TypeError("Invalid argument; `value` has no value.");
	      
	    this.value = Guid.EMPTY;
	    
	    if (guid && guid instanceof Guid) {
	      this.value = guid.toString();

	    } else if (guid && Object.prototype.toString.call(guid) === "[object String]" && Guid.isGuid(guid)) {
	      this.value = guid;
	    }
	    
	    this.equals = function(other) {
	      // Comparing string `value` against provided `guid` will auto-call
	      // toString on `guid` for comparison
	      return Guid.isGuid(other) && this.value == other;
	    };

	    this.isEmpty = function() {
	      return this.value === Guid.EMPTY;
	    };
	    
	    this.toString = function() {
	      return this.value;
	    };
	    
	    this.toJSON = function() {
	      return this.value;
	    };
	  };

	  Guid.EMPTY = "00000000-0000-0000-0000-000000000000";

	  Guid.isGuid = function(value) {
	    return value && (value instanceof Guid || validator.test(value.toString()));
	  };

	  Guid.create = function() {
	    return new Guid([gen(2), gen(1), gen(1), gen(1), gen(3)].join("-"));
	  };

	  Guid.raw = function() {
	    return [gen(2), gen(1), gen(1), gen(1), gen(3)].join("-");
	  };

	  if(typeof module != 'undefined' && module.exports) {
	    module.exports = Guid;
	  }
	  else if (typeof window != 'undefined') {
	    window.Guid = Guid;
	  }
	})();


/***/ }),
/* 120 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = {
	    noop: function noop() {},
	    getCookie: function getCookie(name) {

	        if (!document || !document.cookie) {
	            return null;
	        }

	        var start = document.cookie.indexOf(name + "="),
	            len = start + name.length + 1,
	            end = document.cookie.indexOf(';', len);

	        if (!start && name !== document.cookie.substring(0, name.length)) {
	            return null;
	        }

	        if (start === -1) {
	            return null;
	        }

	        if (end === -1) {
	            end = document.cookie.length;
	        }
	        return unescape(document.cookie.substring(len, end));
	    },
	    setCookie: function setCookie(name, value, expires, path, domain, secure) {

	        if (!document || !document.cookie) {
	            return null;
	        }

	        var today = new Date(),
	            expires_date;

	        today.setTime(today.getTime());
	        if (expires) {
	            expires = expires * 1000 * 60 * 60 * 24;
	        }

	        expires_date = new Date(today.getTime() + expires);

	        document.cookie = name + '=' + escape(value) + (expires ? ';expires=' + expires_date.toGMTString() : '') + ( //expires.toGMTString()
	        path ? ';path=' + path : '') + (domain ? ';domain=' + domain : '') + (secure ? ';secure' : '');
	    },
	    hashCode: function hashCode(str) {
	        var hash = 0,
	            char2,
	            i;

	        if (str.length === 0) {
	            return hash;
	        }

	        for (i = 0; i < str.length; i++) {
	            char2 = str.charCodeAt(i);
	            hash = (hash << 5) - hash + char2;
	            hash = hash & hash; // Convert to 32bit integer
	        }

	        return hash;
	    }
	};

/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _config = __webpack_require__(122);

	var _config2 = _interopRequireDefault(_config);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var ConfigService = function () {
	  function ConfigService() {
	    (0, _classCallCheck3.default)(this, ConfigService);
	  }

	  (0, _createClass3.default)(ConfigService, null, [{
	    key: 'init',
	    value: function init(options) {
	      this._options = options;
	    }
	  }, {
	    key: 'tracking',
	    value: function tracking() {
	      var enabledByDefault = true; // on by default
	      var tr = { enabled: enabledByDefault }; // base object
	      // specified as false
	      if (this._options && this._options.tracking === false) {
	        // simply off, no specs provided
	        tr.enabled = false;
	      } else if (this._options && this._options.tracking === true) {
	        // simply on, no specs provided
	        tr.enabled = true;
	      } else if (this._options && typeof this._options.tracking !== 'undefined') {
	        // custom object provided
	        tr = this._options.tracking;
	        // on or off depending on what user specified. If not specified, on by default.
	        tr.enabled = typeof this._options.tracking.enabled !== 'undefined' ? this._options.tracking.enabled : enabledByDefault;
	      }
	      return tr;
	    }
	  }, {
	    key: 'baseUrl',
	    get: function get() {
	      if (this._options && this._options.baseUrl) return this._options.baseUrl;
	      return _config2.default.BASE_URL;
	    }
	  }, {
	    key: 'pollingInterval',
	    get: function get() {
	      if (this._options && this._options.polling && this._options.polling.pollInterval) return this._options.polling.pollInterval;
	      return _config2.default.POLLING.POLL_INTERVAL;
	    }
	  }, {
	    key: 'metricSourceType',
	    get: function get() {
	      if (this._options && this._options.metrics && this._options.metrics.source) return this._options.metrics.source;
	      return _config2.default.METRICS.SOURCE;
	    }
	  }, {
	    key: 'logging',
	    get: function get() {
	      var defaultLogLevel = -1;
	      var logging = { logLevel: defaultLogLevel, isSpecified: false };
	      if (this._options && this._options.logging) {
	        logging = this._options.logging;
	        logging.logLevel = typeof this._options.logging.logLevel !== 'undefined' ? this._options.logging.logLevel : defaultLogLevel;
	        logging.isSpecified = true;
	      }

	      return logging;
	    }
	  }, {
	    key: 'metricEndpoint',
	    get: function get() {
	      if (this._options && this._options.metrics && this._options.metrics.endpoint) return this._options.metrics.endpoint;
	      return _config2.default.METRICS.ENDPOINT;
	    }
	  }, {
	    key: 'metricPollInterval',
	    get: function get() {
	      return _config2.default.METRICS.POLL_INTERVAL;
	    }
	  }, {
	    key: 'clientEndpoint',
	    get: function get() {
	      if (this._options && this._options.client && this._options.client.endpoint) return this._options.client.endpoint;
	      return _config2.default.CLIENT.ENDPOINT;
	    }
	  }, {
	    key: 'pageSize',
	    get: function get() {
	      if (this._options && this._options.paging && this._options.paging.pageSize) {
	        var size = parseInt(this._options.paging.pageSize);
	        if (isNaN(size) || size > 100) {
	          return 100;
	        }
	        return size;
	      }
	      return _config2.default.PAGING.PAGE_SIZE;
	    }
	  }, {
	    key: 'pollSettings',
	    get: function get() {
	      return this._options.pollSettings || {};
	    }
	  }, {
	    key: 'collectionSettings',
	    get: function get() {
	      return this._options.collectionSettings || {};
	    }
	  }]);
	  return ConfigService;
	}();

	exports.default = ConfigService;

/***/ }),
/* 122 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  BASE_URL: 'https://api.scribblelive.com/v1',
	  POLLING: {
	    POLL_INTERVAL: 5000
	  },
	  METRICS: {
	    ENDPOINT: 'https://counter.scribblelive.com',
	    SOURCE: 3,
	    POLL_INTERVAL: 60000
	  },
	  CLIENT: {
	    ENDPOINT: 'https://api.scribblelive.com/v1/client'
	  },
	  PAGING: {
	    PAGE_SIZE: 99
	  }
	};

/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _keys = __webpack_require__(30);

	var _keys2 = _interopRequireDefault(_keys);

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _promise = __webpack_require__(93);

	var _promise2 = _interopRequireDefault(_promise);

	var _superagent = __webpack_require__(114);

	var _superagent2 = _interopRequireDefault(_superagent);

	var _config = __webpack_require__(121);

	var _config2 = _interopRequireDefault(_config);

	var _postNormalizer = __webpack_require__(124);

	var _postNormalizer2 = _interopRequireDefault(_postNormalizer);

	var _auth = __webpack_require__(91);

	var _auth2 = _interopRequireDefault(_auth);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	/**
	 * Constant store which holds information regarding polls.
	 * @type {{pollId: {}}}
	 */
	var latestPostInfo = {
	  pollId: {}
	};

	/**
	 * This function will check if the current poll is valid.
	 * @param id
	 * @returns {boolean}
	 */
	var shouldPoll = function shouldPoll(id) {
	  if (latestPostInfo.pollId[id] && latestPostInfo.pollId[id].disabled) {
	    latestPostInfo.pollId[id] = {
	      lastChanged: Math.floor(Date.now() / 1000),
	      isOpen: null
	    };

	    return false;
	  }
	  return true;
	};

	/**
	 * This function will get the most recent posts for the stream.
	 * @param streamId
	 * @param id
	 * @returns {Promise}
	 */
	var recentPosts = function recentPosts(streamId, id) {
	  var recentUrl = _config2.default.baseUrl + "/stream/" + streamId + "/posts/recent?token=" + _auth2.default.token + "&Timestamp=" + latestPostInfo.pollId[id].lastChanged;
	  return new _promise2.default(function (resolve, reject) {
	    _superagent2.default.get(recentUrl).then(function (res) {
	      // Get the body.
	      var response = res.body;
	      // Set the isOpen status of the room.
	      latestPostInfo.pollId[id].isOpen = response.isOpen;
	      // Check if there are posts.
	      if (response.posts.length) {
	        // We get the most recent posts Creation Date and set it to GMT.
	        var lastPost = response.posts[response.posts.length - 1];
	        var time = new Date();
	        if (lastPost.Date) {
	          time = response.posts[response.posts.length - 1].Date + " GMT";
	        } else if (lastPost.LastModifiedDate || lastPost.CreationDate) {
	          time = lastPost.LastModifiedDate || lastPost.CreationDate;
	        }
	        // Store that timestamp within the lastChanged to only get most recent posts.
	        latestPostInfo.pollId[id].lastChanged = Math.floor(new Date(time).getTime() / 1000);
	      }
	      resolve(response);
	    }, function (err) {
	      reject(err);
	    });
	  });
	};

	/**
	 * PollingService has helper functions allowing users to control
	 * polls for specified streams.
	 */

	var PollingService = function () {
	  function PollingService() {
	    (0, _classCallCheck3.default)(this, PollingService);
	  }

	  (0, _createClass3.default)(PollingService, null, [{
	    key: "poll",


	    /**
	     * This function will start the polling for a specific stream.
	     * @param streamId
	     * @param id
	     * @param cb
	     */
	    value: function poll(streamId, id, cb) {
	      var _this = this;

	      if (!shouldPoll(id)) return;
	      var url = _config2.default.baseUrl + "/stream/" + streamId + "/lastmodified?token=" + _auth2.default.token;
	      return new _promise2.default(function (resolve, reject) {
	        _this.lastModified(url, id).then(function () {
	          return recentPosts(streamId, id);
	        }).then(function (data) {
	          var postData = data;
	          postData.posts = postData.posts.map(_postNormalizer2.default.normalize);
	          cb(null, postData);
	          resolve(postData);
	        }).catch(function (err) {
	          if (err) {
	            cb(err, null);
	            reject(err);
	            return;
	          }

	          cb(null, { isOpen: latestPostInfo.pollId[id].isOpen, posts: [] });
	          resolve({ isOpen: latestPostInfo.pollId[id].isOpen, posts: [] });
	        });
	      });
	    }

	    /**
	     * This function will get and set the last modified time for the stream.
	     * @param url
	     * @param id
	     * @returns {Promise}
	     */

	  }, {
	    key: "lastModified",
	    value: function lastModified(url, id) {
	      return new _promise2.default(function (resolve, reject) {
	        _superagent2.default.get(url).then(function (res) {
	          if (!res.body) {
	            reject("Nothing returned.");
	            return;
	          }
	          var response = res.body;
	          // Setup pollId for interval if it doesn't exist.
	          if (latestPostInfo.pollId[id].lastChanged === 0) {
	            latestPostInfo.pollId[id].lastChanged = parseInt(response.Time);
	            resolve();
	            return;
	          }

	          if (latestPostInfo.pollId[id].lastChanged < parseInt(response.Time)) {
	            resolve();
	            return;
	          }
	          reject();
	        }).catch(function (error) {});
	      });
	    }

	    /**
	     * This function sets an object with it's guid and poll information.
	     * @param {[type]} guid [description]
	     * @param {[type]} obj  [description]
	     */

	  }, {
	    key: "setPollObject",
	    value: function setPollObject(guid, obj) {
	      latestPostInfo.pollId[guid] = obj;
	    }

	    /**
	     * This will clear a poll interval and clear it's information from the latestPostInfo.
	     * @param pollId
	     * @returns {boolean}
	     */

	  }, {
	    key: "killPoll",
	    value: function killPoll(id) {
	      latestPostInfo.pollId[id].cancelFn();
	      latestPostInfo.pollId[id].disabled = true;
	    }

	    /**
	     * This will clear all polls and clear the latestPostInfo object.
	     * @returns {boolean}
	     */

	  }, {
	    key: "killAllPolls",
	    value: function killAllPolls() {
	      var keys = (0, _keys2.default)(latestPostInfo.pollId);
	      for (var i = 0; i < keys.length; i++) {
	        clearInterval(keys[i]);
	        this.killPoll(keys[i]);
	      }
	      return true;
	    }

	    /**
	     * Clears a reference to all polls.
	     */

	  }, {
	    key: "clearPolls",
	    value: function clearPolls() {
	      latestPostInfo.pollId = {};
	    }

	    /**
	     * This will return all current poll ids.
	     * @returns {Array}
	     */

	  }, {
	    key: "getPollIds",
	    value: function getPollIds() {
	      return (0, _keys2.default)(latestPostInfo.pollId);
	    }
	  }]);
	  return PollingService;
	}();

	exports.default = PollingService;

/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var TYPES = {
	  IMAGE: "IMAGE",
	  TEXT: "TEXT",
	  HTML: "HTML",
	  VIDEO: "VIDEO",
	  INSTAGRAM: "INSTAGRAM",
	  FACEBOOK: "FACEBOOK",
	  TWEET: "TWEET",
	  SLIDESHOW: "SLIDESHOW",
	  YOUTUBE: "YOUTUBE"
	};

	var PostNormalizer = function () {
	  function PostNormalizer() {
	    (0, _classCallCheck3.default)(this, PostNormalizer);
	  }

	  (0, _createClass3.default)(PostNormalizer, null, [{
	    key: "normalize",
	    value: function normalize(post) {
	      if (typeof post.PostMeta === "undefined" || post.PostMeta === null) {
	        post.PostMeta = {};
	      }

	      switch (post.Type) {
	        case TYPES.HTML:
	          switch (post.PostMeta.Type || post.PostMeta.source || post.PostMeta.PostType) {
	            case "INSTAGRAM":
	            case "instagram:post":
	              post.Type = TYPES.INSTAGRAM;
	              break;
	            case "facebook:post":
	            case "facebook":
	              post.Type = TYPES.FACEBOOK;
	              break;
	            case "twitter:tweet":
	              post.Type = TYPES.TWEET;
	              break;
	            case "youtube:post":
	              post.Type = TYPES.YOUTUBE;
	              break;
	            case "SLIDESHOW":
	              post.Type = TYPES.SLIDESHOW;
	              break;
	            default:
	              break;
	          }

	          break;
	        case TYPES.VIDEO:
	          if (!post.Media) {
	            post.Media = {};
	            post.Media.Url = post.Content;
	            post.Media.Caption = post.Caption;
	          }
	          break;
	        case TYPES.IMAGE:
	          if (post.Media && post.Media.Url) {
	            var url = post.Media.Url;
	            var index = /[.]/.exec(url) ? /[^.]+$/.exec(url) : undefined;

	            if (!index) break;

	            post.Media.Sizes = {
	              1000: [url.slice(0, index.index - 1), "_1000", url.slice(index.index - 1)].join(""),
	              300: [url.slice(0, index.index - 1), "_300", url.slice(index.index - 1)].join(""),
	              500: [url.slice(0, index.index - 1), "_500", url.slice(index.index - 1)].join(""),
	              800: [url.slice(0, index.index - 1), "_800", url.slice(index.index - 1)].join(""),
	              orig: [url.slice(0, index.index - 1), "_orig", url.slice(index.index - 1)].join("")
	            };
	          }
	          break;
	        default:
	          break;
	      }

	      if (!post.CreationDate) {
	        post.CreationDate = post.ReceivedDate + " GMT";
	      }

	      return post;
	    }
	  }]);
	  return PostNormalizer;
	}();

	exports.default = PostNormalizer;

/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _assign = __webpack_require__(50);

	var _assign2 = _interopRequireDefault(_assign);

	var _promise = __webpack_require__(93);

	var _promise2 = _interopRequireDefault(_promise);

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _superagent = __webpack_require__(114);

	var _superagent2 = _interopRequireDefault(_superagent);

	var _config = __webpack_require__(121);

	var _config2 = _interopRequireDefault(_config);

	var _resources = __webpack_require__(126);

	var _utils = __webpack_require__(120);

	var _utils2 = _interopRequireDefault(_utils);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var trackingImage = null;
	var STOP_TRACKING_KEY = "scrbbl-preview-mode";

	function generateUrl(base, params) {
	  var url = base;
	  for (var param in params) {
	    if (url.indexOf("?") === -1) {
	      url += "?" + param + "=" + encodeURIComponent(params[param]);
	    } else {
	      url += "&" + param + "=" + encodeURIComponent(params[param]);
	    }
	  }
	  return url;
	}

	function sendTracking(options, isPageView) {
	  delete options.pageview;
	  delete options.first;

	  var UniquesList = null;
	  var CookieUniquesList = null;
	  var StatsUserId = null;
	  var CookiesEnabled = true;
	  var IsFirstView = isPageView;
	  var ItemKey = options.page;
	  var MAXCOOKIELENGTH = 3500;
	  var MAXVARLENGTH = 20000;

	  if (isPageView) {
	    options.pageview = 1;

	    if (StatsUserId === null) {
	      StatsUserId = _utils2.default.getCookie("SLStatUid");

	      if (StatsUserId === null) {
	        // Generate a unique ID for the user
	        var domain = document && typeof document.domain !== 'undefined' ? document.domain : 'nodomain';

	        StatsUserId = _utils2.default.hashCode(+domain) + "_" + new Date().getTime() + "_" + Math.floor(Math.random() * 10000000 + 1);
	        _utils2.default.setCookie("SLStatUid", StatsUserId);

	        CookiesEnabled = _utils2.default.getCookie("SLStatUid") !== null;
	      }
	    }

	    if (UniquesList === null) {
	      UniquesList = _utils2.default.getCookie("SLStatHist");
	    }

	    if (UniquesList === null) {
	      UniquesList = "";
	    }

	    // If we've already seen that item
	    if (UniquesList.match(new RegExp("(^|\\|)" + ItemKey))) {
	      IsFirstView = false;
	    } else {
	      IsFirstView = true;
	      UniquesList += "|" + ItemKey;

	      if (UniquesList.length > MAXVARLENGTH) {
	        UniquesList = UniquesList.substring(UniquesList.length - MAXVARLENGTH);
	        UniquesList = UniquesList.substring(UniquesList.indexOf("|"));
	      }

	      CookieUniquesList = UniquesList;

	      if (CookieUniquesList.length > MAXCOOKIELENGTH) {
	        CookieUniquesList = CookieUniquesList.substring(CookieUniquesList.length - MAXCOOKIELENGTH);
	        CookieUniquesList = CookieUniquesList.substring(CookieUniquesList.indexOf("|"));
	      }

	      _utils2.default.setCookie("SLStatHist", CookieUniquesList);
	    }
	  }

	  if (IsFirstView) {
	    options.first = 1;
	  }

	  if (StatsUserId) {
	    options.uid = StatsUserId;
	  }

	  try {
	    options.rand = Math.round(100000000 * Math.random());
	    trackingImage = new Image();
	    trackingImage.src = generateUrl(_config2.default.metricEndpoint, options);
	    return trackingImage.src;
	  } catch (e) {}
	}

	function getSource() {

	  if (!_config2.default.tracking().enabled) return '';
	  return _config2.default.tracking().source || document.location.href;
	}

	function getHash() {
	  return document.location.hash;
	}

	function attachWarning() {
	  if (document.getElementById("scrbbl-preview-mode")) return;
	  var element = document.createElement("div");
	  element.id = "scrbbl-preview-mode";
	  element.style.cssText = "transition:top 0.5s;position:fixed;left:0;right:0;top:0;padding:15px;background:rgba(255,200,61,0.8);text-align:center;z-index:10000";
	  element.innerHTML = (0, _resources.getResource)("metricsTrackingAlert");
	  element.addEventListener("click", function (_ref) {
	    var target = _ref.target;

	    var rect = target.getBoundingClientRect();
	    target.style.top = -1 * rect.height + "px";
	    target.addEventListener("transitionend", function (_ref2) {
	      var target = _ref2.target;
	      return target.parentNode.removeChild(target);
	    });
	  });
	  element.addEventListener("mouseover", function () {
	    element.style.cursor = "pointer";
	  });
	  document.body.appendChild(element);
	}

	var TrackingService = function () {
	  function TrackingService() {
	    (0, _classCallCheck3.default)(this, TrackingService);
	  }

	  (0, _createClass3.default)(TrackingService, [{
	    key: "track",
	    value: function track(token, streamId) {
	      var _this = this;

	      var sourceType = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : _config2.default.metricSourceType;

	      if (token === undefined || streamId === undefined) {
	        throw Error("Metrics tracking error.");
	      }

	      if (this[streamId + "_Interval"] || !_config2.default.tracking().enabled) return;

	      return new _promise2.default(function (resolve, reject) {
	        // stop tracking if in preview mode
	        if (getHash().indexOf(STOP_TRACKING_KEY) > -1) {
	          attachWarning();
	          reject("Preview mode loaded");
	          return;
	        }

	        _superagent2.default.get(_config2.default.clientEndpoint + "?Token=" + token).then(function (response) {
	          var body = response.body;
	          if (!body.Id) {
	            reject("Cannot determine clientId from token.");
	            return;
	          }

	          var options = {
	            page: streamId,
	            Source: getSource(),
	            SourceType: sourceType,
	            Client: body.Id
	          };

	          resolve(options);
	        }).catch(function (error) {});
	      }).then(function (options) {
	        return _this.startTracking(options);
	      });
	    }
	  }, {
	    key: "startTracking",
	    value: function startTracking(options) {
	      var _this2 = this;

	      return new _promise2.default(function (resolve) {

	        if (_this2[options.page + "_Interval"]) return;

	        var id = setInterval(function () {
	          sendTracking((0, _assign2.default)({}, options), false);
	        }, _config2.default.metricPollInterval);
	        _this2[options.page + "_Interval"] = { interval: id };

	        resolve(sendTracking(options, true));
	      });
	    }
	  }]);
	  return TrackingService;
	}();

	exports.default = TrackingService;

/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.getResource = undefined;

	var _enUs = __webpack_require__(127);

	var _enUs2 = _interopRequireDefault(_enUs);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var _resources = {
	  'en': _enUs2.default
	};

	var userLanguage = null;

	var getLanguage = function getLanguage() {
	  if (userLanguage === null) {
	    return navigator.language.split('-')[0].toLowerCase();
	  }

	  return userLanguage;
	};

	var getResource = exports.getResource = function getResource(key) {
	  var resources = _resources[getLanguage()];
	  var undefinedMessage = 'Key undefined';

	  if (!resources) {
	    return undefinedMessage;
	  }

	  return resources[key] ? resources[key] : undefinedMessage;
	};

/***/ }),
/* 127 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  metricsTrackingAlert: 'This visualization is a preview with metrics tracking disabled.  Please refrain from using this link in your commercial release.'
	};

/***/ }),
/* 128 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _config = __webpack_require__(121);

	var _config2 = _interopRequireDefault(_config);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var LOG_LEVELS = [1, 2, 3, 4];

	var isValid = function isValid(level) {
	  if (LOG_LEVELS.indexOf(level) === -1 || level > getLogLevel()) {
	    return false;
	  }
	  return true;
	};

	var log = function log(level, value) {
	  if (isValid(level)) {
	    console.log(value);
	  }
	};

	var info = function info(value) {
	  log(1, value);
	};

	function getLogLevel() {
	  if (_config2.default.logging.isSpecified) return _config2.default.logging.logLevel;
	  if (typeof window === 'undefined' || typeof window.location === 'undefined') return -1;

	  var arr = window.location.hash.match(/(debug=){1}(\d){1}/);
	  if (arr && Array.isArray(arr) && arr.length === 3) {
	    try {
	      var num = parseInt(arr[2]);
	      if (LOG_LEVELS.indexOf(num) > -1) {
	        return num;
	      }
	    } catch (e) {}
	  }
	  return -1;
	}

	var Log = {
	  info: info,
	  log: log
	};

	exports.default = Log;

	module.exports = Log;

/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _keys = __webpack_require__(30);

	var _keys2 = _interopRequireDefault(_keys);

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _es6Promise = __webpack_require__(22);

	var _es6Promise2 = _interopRequireDefault(_es6Promise);

	var _likesService = __webpack_require__(130);

	var _likesService2 = _interopRequireDefault(_likesService);

	var _config = __webpack_require__(131);

	var _config2 = _interopRequireDefault(_config);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	if (typeof window !== 'undefined' && !window.Promise) {
	    window.Promise = _es6Promise2.default;
	}

	/**
	 * pollingData is used to hold poll ids so they can be stopped.
	 * @type {{}}
	 */
	var pollingData = {};

	var ScribbleToolkitLikes = function () {
	    function ScribbleToolkitLikes() {
	        (0, _classCallCheck3.default)(this, ScribbleToolkitLikes);
	    }

	    (0, _createClass3.default)(ScribbleToolkitLikes, null, [{
	        key: 'post',


	        /**
	         * This function will like a post of a given streamId.
	         * @param streamId
	         * @param postId
	         * @param cb
	         * @returns Promise(likeData)
	         */
	        value: function post(streamId, postId, cb) {
	            return _likesService2.default.like(streamId, postId, cb);
	        }

	        /**
	         * This function will get likes for a given array of post Ids.
	         * @param streamId
	         * @param postIds
	         * @param cb
	         * @returns Promise(likeData)
	         */

	    }, {
	        key: 'getLikes',
	        value: function getLikes(streamId) {
	            var postIds = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
	            var cb = arguments[2];

	            return _likesService2.default.getLikes(streamId, postIds, cb);
	        }

	        /**
	         * This function will poll a provided array of posts.
	         * @param streamId
	         * @param postIds
	         * @param cb
	         * @returns {number}
	         */

	    }, {
	        key: 'pollLikes',
	        value: function pollLikes(streamId) {
	            var postIds = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
	            var cb = arguments[2];

	            if (!streamId) throw "streamId is required";
	            var id = setInterval(function () {
	                return _likesService2.default.getLikes(streamId, postIds, cb);
	            }, _config2.default.pollInteval);
	            pollingData[id] = true;
	            return id;
	        }

	        /**
	         * This function will kill a poll based on it's id.
	         * @param id
	         */

	    }, {
	        key: 'killPoll',
	        value: function killPoll(id) {
	            clearInterval(id);
	            delete pollingData[id];
	        }

	        /**
	         * This function will return all poll ids.
	         * @returns {Array}
	         */

	    }, {
	        key: 'getAllPolls',
	        value: function getAllPolls() {
	            var keys = (0, _keys2.default)(pollingData);
	            return keys;
	        }

	        /**
	         * This function will clear all current polls.
	         */

	    }, {
	        key: 'clearAllPolls',
	        value: function clearAllPolls() {
	            var keys = this.getAllPolls();
	            keys.forEach(this.killPoll);
	        }
	    }]);
	    return ScribbleToolkitLikes;
	}();

	module.exports = ScribbleToolkitLikes;

/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _keys = __webpack_require__(30);

	var _keys2 = _interopRequireDefault(_keys);

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _promise = __webpack_require__(93);

	var _promise2 = _interopRequireDefault(_promise);

	var _superagent = __webpack_require__(114);

	var _superagent2 = _interopRequireDefault(_superagent);

	var _config = __webpack_require__(131);

	var _config2 = _interopRequireDefault(_config);

	var _utils = __webpack_require__(133);

	var _utils2 = _interopRequireDefault(_utils);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var STREAM_LIKES = {};

	var MAX_GET_LIKES = 10;

	/**
	 * This function will get the likes for a list of post Ids.
	 * Note: Maximum of 10 postIds.
	 * @param streamId
	 * @param postIds
	 * @returns {Promise}
	 */
	function _getLikes(streamId) {
	    var postIds = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [0];

	    var ids = encodeURIComponent(postIds.join(","));
	    var url = _config2.default.baseUrl + '/' + _config2.default.getEndpoint + '/' + streamId + '?postids=' + ids;
	    return new _promise2.default(function (resolve, reject) {
	        _superagent2.default.get(url).then(function (res) {
	            try {
	                var result = JSON.parse(res.text);
	                resolve(result);
	            } catch (err) {
	                throw err;
	            }
	        }).catch(function (err) {
	            reject(err);
	        });
	    });
	}

	var LikesService = function () {
	    function LikesService() {
	        (0, _classCallCheck3.default)(this, LikesService);
	    }

	    (0, _createClass3.default)(LikesService, null, [{
	        key: 'like',

	        /**
	         * This function will like a postId within the stream.
	         * @param streamId
	         * @param postId
	         * @param cb
	         * @returns {Promise}
	         */
	        value: function like(streamId, postId) {
	            var cb = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : _utils2.default.noop;

	            if (typeof streamId !== 'number' || typeof postId !== 'number') throw "Invalid streamId or postId";
	            var url = _config2.default.baseUrl + '/' + _config2.default.likesEndpoint + '/' + streamId + '/' + postId;
	            return new _promise2.default(function (resolve, reject) {
	                _superagent2.default.get(url).then(function (res) {
	                    try {
	                        var result = JSON.parse(res.text);
	                        cb(null, result);
	                        resolve(result);
	                    } catch (err) {
	                        throw err;
	                    }
	                }).catch(function (err) {
	                    cb(err, null);
	                    reject(err);
	                });
	            });
	        }

	        /**
	         * This function will get all likes from an array of postIds from the provided stream.
	         * @param streamId
	         * @param postIds
	         * @param cb
	         * @returns {Promise.<TResult>}
	         */

	    }, {
	        key: 'getLikes',
	        value: function getLikes(streamId, postIds) {
	            var cb = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : _utils2.default.noop;

	            if (typeof streamId !== "number") throw "Invalid streamId";
	            if (!Array.isArray(postIds)) throw "Invalid postIds for getLikes.  Requires an array.";

	            if (!STREAM_LIKES[streamId]) {
	                STREAM_LIKES[streamId] = { posts: getPostGroups(postIds), total: postIds.length };
	            } else {
	                for (var i = 0; i < postIds.length; i++) {
	                    var postId = postIds[i];
	                    if (!inStreamLikes(streamId, postId)) addToStreamLikes(streamId, postId);
	                }
	            }

	            var promises = [];
	            for (var i = 0; i < STREAM_LIKES[streamId].posts.length; i++) {
	                promises.push(_getLikes(streamId, STREAM_LIKES[streamId].posts[i], cb));
	            }
	            return new _promise2.default(function (resolve, reject) {
	                return _promise2.default.all(promises).then(function (res) {
	                    var results = { posts: [] };
	                    for (var i = 0; i < res.length; i++) {
	                        var keys = (0, _keys2.default)(res[i].posts);
	                        keys.forEach(function (key) {
	                            var obj = {};
	                            obj['' + key] = res[i].posts[key];
	                            results.posts.push(obj);
	                        });
	                    }
	                    cb(results);
	                    resolve(results);
	                    return results;
	                }).catch(function (err) {
	                    if (err) {
	                        cb(err);
	                        reject(err);
	                        return err;
	                    }
	                });
	            });
	        }
	    }]);
	    return LikesService;
	}();

	/**
	 * This function will add a postId to a given block of posts to help maintain cache.
	 * @param streamId
	 * @param id
	 */


	function addToStreamLikes(streamId, id) {
	    if (STREAM_LIKES[streamId].posts[STREAM_LIKES[streamId].posts.length].length < MAX_GET_LIKES) {
	        STREAM_LIKES[streamId].posts[STREAM_LIKES[streamId].posts.length].push(id);
	        return;
	    }

	    STREAM_LIKES[streamId].posts.push([id]);
	}

	/**
	 * This function will check if we currently have the postId in our list of lists.
	 * @param streamId
	 * @param id
	 * @returns {boolean}
	 */
	function inStreamLikes(streamId, id) {
	    for (var i = 0; i < STREAM_LIKES[streamId].posts.length; i++) {
	        for (var j = 0; j < STREAM_LIKES[streamId].posts[i].length; j++) {
	            if (STREAM_LIKES[streamId].posts[i][j] === id) {
	                return true;
	            }
	        }
	    }
	    return false;
	}

	/**
	 * This function will generate an array of arrays for given posts.
	 * @param posts
	 * @returns {Array}
	 */
	function getPostGroups(posts) {
	    var arr = [];
	    for (var i = 0; i < posts.length; i += MAX_GET_LIKES) {
	        arr.push(posts.slice(i, i + MAX_GET_LIKES));
	    }
	    return arr;
	}

	module.exports = LikesService;

/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _config = __webpack_require__(132);

	var _config2 = _interopRequireDefault(_config);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var ConfigService = function () {
	    function ConfigService() {
	        (0, _classCallCheck3.default)(this, ConfigService);
	    }

	    (0, _createClass3.default)(ConfigService, null, [{
	        key: 'baseUrl',


	        /**
	         * This function will return the baseUrl being used for likes.
	         * @returns {string}
	         */
	        get: function get() {
	            return _config2.default.baseUrl;
	        }

	        /**
	         * This function will return the endpoint for liking a post.
	         * @returns {string}
	         */

	    }, {
	        key: 'likesEndpoint',
	        get: function get() {
	            return _config2.default.likesEnd;
	        }

	        /**
	         * This function will return the endpoint for getting likes.
	         * @returns {string}
	         */

	    }, {
	        key: 'getEndpoint',
	        get: function get() {
	            return _config2.default.getEnd;
	        }

	        /**
	         * This function will return the polling interval.
	         * @returns {number}
	         */

	    }, {
	        key: 'pollInteval',
	        get: function get() {
	            return _config2.default.polling.pollInterval;
	        }
	    }]);
	    return ConfigService;
	}();

	module.exports = ConfigService;

/***/ }),
/* 132 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = {
	    baseUrl: 'http://cdnlove.scribblelive.com',
	    polling: {
	        pollInterval: 5000
	    },
	    likesEnd: 'like',
	    getEnd: 'likes/event'
	};

/***/ }),
/* 133 */
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = {
	    noop: function noop() {}
	};

/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _assign = __webpack_require__(50);

	var _assign2 = _interopRequireDefault(_assign);

	var _es6Promise = __webpack_require__(22);

	var _es6Promise2 = _interopRequireDefault(_es6Promise);

	var _options = __webpack_require__(135);

	var _options2 = _interopRequireDefault(_options);

	var _socialProviders = __webpack_require__(136);

	var _socialProviders2 = _interopRequireDefault(_socialProviders);

	var _authentication = __webpack_require__(137);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	if (typeof window !== 'undefined' && !window.Promise) {
	  window.Promise = _es6Promise2.default;
	}

	var _user = {
	  id: null,
	  auth: null,
	  name: null,
	  avatar: null
	};

	var _updateUserFromApi = function _updateUserFromApi(_ref) {
	  var Id = _ref.Id,
	      Name = _ref.Name,
	      Auth = _ref.Auth,
	      Thumbnail = _ref.Thumbnail;

	  _user.name = Name;
	  _user.avatar = Thumbnail;
	  _user.auth = Auth;
	  _user.id = Id;

	  return _getUser();
	};

	var _getUser = function _getUser() {
	  // object.assign to prevent reference
	  return (0, _assign2.default)({}, _user);
	};

	var Auth = function () {
	  function Auth(options) {
	    (0, _classCallCheck3.default)(this, Auth);

	    _options2.default.initialize(options);
	  }

	  (0, _createClass3.default)(Auth, [{
	    key: 'logout',
	    value: function logout() {
	      _user.name = null;
	      _user.avatar = null;
	      _user.id = null;
	      _user.auth = null;
	    }
	  }, {
	    key: 'getUser',
	    value: function getUser() {
	      return _getUser();
	    }
	  }, {
	    key: 'isLoggedIn',
	    value: function isLoggedIn() {
	      return _user.name !== null;
	    }
	  }, {
	    key: 'anonLogin',
	    value: function anonLogin(name) {
	      var _this = this;

	      var avatar = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	      return new _es6Promise2.default(function (resolve, reject) {
	        _this.logout();
	        if (!name) {
	          throw new Error('A valid name must be provided to log in');
	        }

	        (0, _authentication.anonLogin)({ name: name, avatar: avatar }).then(function (response) {
	          return _updateUserFromApi(response);
	        }).then(resolve, reject);
	      });
	    }
	  }, {
	    key: 'socialLogin',
	    value: function socialLogin(provider) {
	      if (!_socialProviders2.default[provider]) {
	        throw 'Invalid provider supplied. ' + provider + ' is not supported';
	      }

	      return (0, _authentication.socialLogin)({ provider: provider }).then(function (response) {
	        return _updateUserFromApi(response);
	      });
	    }
	  }, {
	    key: 'PROVIDERS',
	    get: function get() {
	      return _socialProviders2.default;
	    }
	  }]);
	  return Auth;
	}();

	exports.default = Auth;

	module.exports = Auth;

/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _assign = __webpack_require__(50);

	var _assign2 = _interopRequireDefault(_assign);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var _defaultOptions = {
	  baseUrl: 'https://apigateway.scribblelive.com',
	  uploadBaseUrl: 'http://embed.scribblelive.com',
	  token: null
	};

	var _clientOptions = null;

	exports.default = {
	  initialize: function initialize(options) {
	    _clientOptions = (0, _assign2.default)({}, _defaultOptions, options);

	    if (!_clientOptions.token) {
	      throw new Error('Auth token is required');
	    }
	  },


	  get baseUrl() {
	    return _clientOptions.baseUrl;
	  },

	  get clientId() {
	    return _clientOptions.clientId;
	  },

	  get token() {
	    return _clientOptions.token;
	  },

	  get uploadBaseUrl() {
	    return _clientOptions.uploadBaseUrl;
	  }
	};

/***/ }),
/* 136 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  twitter: 'twitter',
	  facebook: 'facebook',
	  googleplus: 'googleplus',
	  yahoo: 'yahoo',
	  linkedin: 'linkedin'
	};

/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.anonLogin = exports.socialLogin = undefined;

	var _promise = __webpack_require__(93);

	var _promise2 = _interopRequireDefault(_promise);

	var _options = __webpack_require__(135);

	var _options2 = _interopRequireDefault(_options);

	var _superagent = __webpack_require__(114);

	var _superagent2 = _interopRequireDefault(_superagent);

	var _avatar = __webpack_require__(138);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var _handleGigyaResponse = function _handleGigyaResponse(res, resolve, reject) {
	  if (res.errorCode !== 0) {
	    reject(res.statusMessage);
	    return;
	  }

	  var _res$user = res.user,
	      firstName = _res$user.firstName,
	      lastName = _res$user.lastName,
	      photoURL = _res$user.photoURL;


	  _login({
	    name: firstName + ' ' + lastName,
	    avatarUrl: photoURL
	  }).then(function (res) {
	    return resolve(res);
	  }).catch(function (err) {
	    return reject(err);
	  });
	};

	var _login = function _login(_ref) {
	  var name = _ref.name,
	      _ref$avatarUrl = _ref.avatarUrl,
	      avatarUrl = _ref$avatarUrl === undefined ? null : _ref$avatarUrl;

	  return new _promise2.default(function (resolve, reject) {
	    var baseUrl = _options2.default.baseUrl,
	        clientId = _options2.default.clientId,
	        token = _options2.default.token;

	    var url = baseUrl + '/user/create/anon?token=' + token;
	    var payload = {
	      Name: name,
	      ClientId: clientId
	    };

	    if (avatarUrl) {
	      payload.Thumbnail = avatarUrl;
	    }

	    _superagent2.default.post(url).send(payload).end(function (err, res) {
	      if (err) {
	        reject(err);
	        return;
	      }

	      resolve(res.body);
	    });
	  });
	};

	var socialLogin = exports.socialLogin = function socialLogin(_ref2) {
	  var provider = _ref2.provider;

	  if (!gigya) {
	    throw 'gigya is undefined';
	  }

	  return new _promise2.default(function (resolve, reject) {
	    gigya.socialize.login({
	      provider: provider,
	      callback: function callback(res) {
	        return _handleGigyaResponse(res, resolve, reject);
	      }
	    });
	  });
	};

	var anonLogin = exports.anonLogin = function anonLogin(_ref3) {
	  var name = _ref3.name,
	      avatar = _ref3.avatar;

	  if (avatar && avatar.indexOf && avatar.indexOf("http") === 0) {
	    var avatarUrl = avatar;
	    return _login({ name: name, avatarUrl: avatarUrl });
	  } else if (avatar) {
	    return (0, _avatar.uploadAvatar)(avatar).then(function (avatarUrl) {
	      return _login({ name: name, avatarUrl: avatarUrl });
	    });
	  }

	  return _login({ name: name });
	};

/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.uploadAvatar = undefined;

	var _promise = __webpack_require__(93);

	var _promise2 = _interopRequireDefault(_promise);

	var _options = __webpack_require__(135);

	var _options2 = _interopRequireDefault(_options);

	var _superagent = __webpack_require__(114);

	var _superagent2 = _interopRequireDefault(_superagent);

	var _scribbleliveToolkitFileUploader = __webpack_require__(139);

	var _scribbleliveToolkitFileUploader2 = _interopRequireDefault(_scribbleliveToolkitFileUploader);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var _moveAndResizeImage = function _moveAndResizeImage(bucket, key) {
	  var baseUrl = _options2.default.baseUrl,
	      token = _options2.default.token;

	  return _superagent2.default.post(baseUrl + '/user/create/avatar?token=' + token).send({ UploadBucket: bucket, UploadItem: key });
	};

	var uploadAvatar = exports.uploadAvatar = function uploadAvatar(avatar) {
	  return new _promise2.default(function (resolve, reject) {
	    if (avatar && !avatar instanceof window.File) {
	      reject('avatar must be an instance of File.');
	      return;
	    }

	    var token = _options2.default.token,
	        gatewayUrl = _options2.default.baseUrl;

	    var fileUploader = new _scribbleliveToolkitFileUploader2.default({ token: token, gatewayUrl: gatewayUrl });
	    var uploadData = null;
	    fileUploader.uploadFile(avatar).then(function (res) {
	      uploadData = res;
	      return _superagent2.default.put(uploadData.uploadUrl).send(avatar);
	    }).then(function () {
	      return _moveAndResizeImage(uploadData.bucket, uploadData.key);
	    }).then(function (res) {
	      return resolve('https://' + res.body.avatarUrl);
	    }, function (err) {
	      return reject(err);
	    });
	  });
	};

/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _es6Promise = __webpack_require__(22);

	var _es6Promise2 = _interopRequireDefault(_es6Promise);

	var _superagent = __webpack_require__(114);

	var _superagent2 = _interopRequireDefault(_superagent);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	if (typeof window !== 'undefined' && !window.Promise) {
	  window.Promise = _es6Promise2.default;
	}

	var _options = {
	  gatewayUrl: 'https://apigateway.scribblelive.com/',
	  token: null
	};

	var _getUploadUrl = function _getUploadUrl(fileName, fileType) {
	  var gatewayUrl = _options.gatewayUrl,
	      token = _options.token;

	  var uploadUrl = gatewayUrl + '/file-uploader/upload-url?token=' + token;

	  return _superagent2.default.post(uploadUrl).send({ fileName: fileName, fileType: fileType });
	};

	var FileUploader = function () {
	  function FileUploader() {
	    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
	        token = _ref.token,
	        gatewayUrl = _ref.gatewayUrl;

	    (0, _classCallCheck3.default)(this, FileUploader);

	    if (!token) {
	      throw new Error('Token is required for File Uploader');
	    }

	    if (gatewayUrl) {
	      _options.gatewayUrl = gatewayUrl;
	    }

	    _options.token = token;
	  }

	  (0, _createClass3.default)(FileUploader, [{
	    key: 'uploadFile',
	    value: function uploadFile(file) {
	      return new _es6Promise2.default(function (resolve, reject) {
	        if (!file) {
	          reject('File is required');
	          return;
	        }

	        if (file && !(file instanceof window.File)) {
	          reject('file must be of type File');
	          return;
	        }

	        var responseData = null;

	        _getUploadUrl(file.name, file.type).then(function (response) {
	          responseData = response.body;
	          return _superagent2.default.put(responseData.uploadUrl).send(file);
	        }).then(function () {
	          return resolve(responseData);
	        }, function (err) {
	          return reject(err);
	        });
	      });
	    }
	  }]);
	  return FileUploader;
	}();

	exports.default = FileUploader;

	module.exports = FileUploader;

/***/ }),
/* 140 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _defineProperty2 = __webpack_require__(141);

	var _defineProperty3 = _interopRequireDefault(_defineProperty2);

	var _assign = __webpack_require__(50);

	var _assign2 = _interopRequireDefault(_assign);

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _stringify = __webpack_require__(26);

	var _stringify2 = _interopRequireDefault(_stringify);

	var _typeof2 = __webpack_require__(142);

	var _typeof3 = _interopRequireDefault(_typeof2);

	var _es6Promise = __webpack_require__(22);

	var _es6Promise2 = _interopRequireDefault(_es6Promise);

	var _superagent = __webpack_require__(114);

	var _superagent2 = _interopRequireDefault(_superagent);

	var _config = __webpack_require__(158);

	var _config2 = _interopRequireDefault(_config);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	if (typeof window !== "undefined" && !window.Promise) {
	  window.Promise = _es6Promise2.default;
	}

	var LOCAL_STORAGE_KEY = "SCRIB_POLLS";

	// holds a cache of the response from the server to compare
	// the data to determine if it should fire the call back
	var POLL_RESPONSE_DATA = {};

	var getPollKey = function getPollKey(streamId) {
	  return LOCAL_STORAGE_KEY + "_" + streamId;
	};

	var getExistingPollData = function getExistingPollData(streamId) {
	  if (typeof localStorage === "undefined") return [];
	  var pollKey = getPollKey(streamId);
	  var existingData = JSON.parse(localStorage.getItem(pollKey));
	  return existingData && (typeof existingData === "undefined" ? "undefined" : (0, _typeof3.default)(existingData)) === "object" ? existingData : {};
	};

	var setPollData = function setPollData(streamId, data) {
	  if (typeof localStorage === "undefined") return;
	  localStorage.setItem(getPollKey(streamId), (0, _stringify2.default)(data));
	};

	var setPollResponseData = function setPollResponseData(pollId, data) {
	  POLL_RESPONSE_DATA[pollId] = data;
	};

	var getPollResponseData = function getPollResponseData(pollId) {
	  return POLL_RESPONSE_DATA[pollId] ? POLL_RESPONSE_DATA[pollId] : null;
	};

	var intervalCallback = function intervalCallback(_ref) {
	  var pollId = _ref.pollId,
	      pollingPromiseFn = _ref.pollingPromiseFn,
	      cb = _ref.cb;
	  return function () {
	    pollingPromiseFn().then(function (res) {
	      var pollCache = getPollResponseData(pollId);
	      if (res.TotalVotes === pollCache.TotalVotes) return;
	      setPollResponseData(pollId, res);
	      cb(null, res);
	    }, function (err) {
	      return cb(err);
	    });
	  };
	};

	var Poll = function () {
	  function Poll() {
	    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	    (0, _classCallCheck3.default)(this, Poll);

	    this._options = (0, _assign2.default)({}, _config2.default, options);
	  }

	  (0, _createClass3.default)(Poll, [{
	    key: "vote",
	    value: function vote(_ref2) {
	      var _this = this;

	      var streamId = _ref2.streamId,
	          pollId = _ref2.pollId,
	          selectionId = _ref2.selectionId;

	      if (!streamId) {
	        throw new Error("Stream id is required");
	      }

	      if (!pollId) {
	        throw new Error("Poll id is required");
	      }

	      if (!selectionId) {
	        throw new Error("Selection id is required");
	      }
	      // make api call to vote - http://embed.dev.scribblelive.com/poll/5992/vote/21359?format=json
	      return new _es6Promise2.default(function (res, rej) {
	        var existingPollData = getExistingPollData(streamId);
	        if (existingPollData[pollId]) {
	          rej("User has already voted.");
	          return;
	        }

	        _superagent2.default.get(_this.baseUrl + "/poll/" + pollId + "/vote/" + selectionId + "?format=json").end(function (error, response) {
	          if (error) {
	            rej(error);
	            return;
	          }

	          // store vote into cookies/local storage - {"polls":[{"id":5992,"vote":21359}]}
	          var updatedPollData = (0, _assign2.default)({}, existingPollData, (0, _defineProperty3.default)({}, pollId, selectionId));
	          setPollData(streamId, updatedPollData);
	          res(response.body);
	        });
	      });
	    }
	  }, {
	    key: "getPoll",
	    value: function getPoll(pollId) {
	      var _this2 = this;

	      return new _es6Promise2.default(function (resolve, reject) {
	        _superagent2.default.get(_this2.baseUrl + "/poll/" + pollId).query({ format: "json" }).end(function (err, res) {
	          if (err) {
	            reject(err);
	            return;
	          }

	          resolve(res.body);
	        });
	      });
	    }
	  }, {
	    key: "listen",
	    value: function listen(_ref3) {
	      var _this3 = this;

	      var pollId = _ref3.pollId,
	          _ref3$interval = _ref3.interval,
	          interval = _ref3$interval === undefined ? this.pollInterval : _ref3$interval,
	          cb = _ref3.cb;

	      return new _es6Promise2.default(function (resolve) {
	        _this3.getPoll(pollId).then(function (response) {
	          setPollResponseData(response.Id, response);
	          var pollingPromiseFn = function pollingPromiseFn() {
	            return _this3.getPoll(pollId);
	          };
	          var intervalId = setInterval(intervalCallback({ pollId: pollId, pollingPromiseFn: pollingPromiseFn, cb: cb }), interval);
	          // resolve function to clear interval
	          resolve(function () {
	            return clearInterval(intervalId);
	          });
	        });
	      });
	    }
	  }, {
	    key: "hasVoted",
	    value: function hasVoted(_ref4) {
	      var streamId = _ref4.streamId,
	          pollId = _ref4.pollId;

	      var existingPollData = getExistingPollData(streamId);
	      return !!existingPollData[pollId];
	    }
	  }, {
	    key: "baseUrl",
	    get: function get() {
	      return this._options.baseUrl;
	    }
	  }, {
	    key: "pollInterval",
	    get: function get() {
	      return this._options.pollInterval;
	    }
	  }]);
	  return Poll;
	}();

	exports.default = Poll;

	module.exports = Poll;

/***/ }),
/* 141 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	exports.__esModule = true;

	var _defineProperty = __webpack_require__(3);

	var _defineProperty2 = _interopRequireDefault(_defineProperty);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = function (obj, key, value) {
	  if (key in obj) {
	    (0, _defineProperty2.default)(obj, key, {
	      value: value,
	      enumerable: true,
	      configurable: true,
	      writable: true
	    });
	  } else {
	    obj[key] = value;
	  }

	  return obj;
	};

/***/ }),
/* 142 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	exports.__esModule = true;

	var _iterator = __webpack_require__(143);

	var _iterator2 = _interopRequireDefault(_iterator);

	var _symbol = __webpack_require__(146);

	var _symbol2 = _interopRequireDefault(_symbol);

	var _typeof = typeof _symbol2.default === "function" && typeof _iterator2.default === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj; };

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
	  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
	} : function (obj) {
	  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
	};

/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(144), __esModule: true };

/***/ }),
/* 144 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(84);
	__webpack_require__(70);
	module.exports = __webpack_require__(145).f('iterator');


/***/ }),
/* 145 */
/***/ (function(module, exports, __webpack_require__) {

	exports.f = __webpack_require__(82);


/***/ }),
/* 146 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = { "default": __webpack_require__(147), __esModule: true };

/***/ }),
/* 147 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(148);
	__webpack_require__(95);
	__webpack_require__(156);
	__webpack_require__(157);
	module.exports = __webpack_require__(8).Symbol;


/***/ }),
/* 148 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	// ECMAScript 6 symbols shim
	var global = __webpack_require__(7);
	var has = __webpack_require__(21);
	var DESCRIPTORS = __webpack_require__(16);
	var $export = __webpack_require__(6);
	var redefine = __webpack_require__(76);
	var META = __webpack_require__(149).KEY;
	var $fails = __webpack_require__(17);
	var shared = __webpack_require__(45);
	var setToStringTag = __webpack_require__(81);
	var uid = __webpack_require__(47);
	var wks = __webpack_require__(82);
	var wksExt = __webpack_require__(145);
	var wksDefine = __webpack_require__(150);
	var enumKeys = __webpack_require__(151);
	var isArray = __webpack_require__(152);
	var anObject = __webpack_require__(13);
	var isObject = __webpack_require__(14);
	var toObject = __webpack_require__(33);
	var toIObject = __webpack_require__(37);
	var toPrimitive = __webpack_require__(19);
	var createDesc = __webpack_require__(20);
	var _create = __webpack_require__(78);
	var gOPNExt = __webpack_require__(153);
	var $GOPD = __webpack_require__(155);
	var $GOPS = __webpack_require__(54);
	var $DP = __webpack_require__(12);
	var $keys = __webpack_require__(35);
	var gOPD = $GOPD.f;
	var dP = $DP.f;
	var gOPN = gOPNExt.f;
	var $Symbol = global.Symbol;
	var $JSON = global.JSON;
	var _stringify = $JSON && $JSON.stringify;
	var PROTOTYPE = 'prototype';
	var HIDDEN = wks('_hidden');
	var TO_PRIMITIVE = wks('toPrimitive');
	var isEnum = {}.propertyIsEnumerable;
	var SymbolRegistry = shared('symbol-registry');
	var AllSymbols = shared('symbols');
	var OPSymbols = shared('op-symbols');
	var ObjectProto = Object[PROTOTYPE];
	var USE_NATIVE = typeof $Symbol == 'function' && !!$GOPS.f;
	var QObject = global.QObject;
	// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
	var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

	// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
	var setSymbolDesc = DESCRIPTORS && $fails(function () {
	  return _create(dP({}, 'a', {
	    get: function () { return dP(this, 'a', { value: 7 }).a; }
	  })).a != 7;
	}) ? function (it, key, D) {
	  var protoDesc = gOPD(ObjectProto, key);
	  if (protoDesc) delete ObjectProto[key];
	  dP(it, key, D);
	  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
	} : dP;

	var wrap = function (tag) {
	  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
	  sym._k = tag;
	  return sym;
	};

	var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
	  return typeof it == 'symbol';
	} : function (it) {
	  return it instanceof $Symbol;
	};

	var $defineProperty = function defineProperty(it, key, D) {
	  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
	  anObject(it);
	  key = toPrimitive(key, true);
	  anObject(D);
	  if (has(AllSymbols, key)) {
	    if (!D.enumerable) {
	      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
	      it[HIDDEN][key] = true;
	    } else {
	      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
	      D = _create(D, { enumerable: createDesc(0, false) });
	    } return setSymbolDesc(it, key, D);
	  } return dP(it, key, D);
	};
	var $defineProperties = function defineProperties(it, P) {
	  anObject(it);
	  var keys = enumKeys(P = toIObject(P));
	  var i = 0;
	  var l = keys.length;
	  var key;
	  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
	  return it;
	};
	var $create = function create(it, P) {
	  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
	};
	var $propertyIsEnumerable = function propertyIsEnumerable(key) {
	  var E = isEnum.call(this, key = toPrimitive(key, true));
	  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
	  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
	};
	var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
	  it = toIObject(it);
	  key = toPrimitive(key, true);
	  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
	  var D = gOPD(it, key);
	  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
	  return D;
	};
	var $getOwnPropertyNames = function getOwnPropertyNames(it) {
	  var names = gOPN(toIObject(it));
	  var result = [];
	  var i = 0;
	  var key;
	  while (names.length > i) {
	    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
	  } return result;
	};
	var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
	  var IS_OP = it === ObjectProto;
	  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
	  var result = [];
	  var i = 0;
	  var key;
	  while (names.length > i) {
	    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
	  } return result;
	};

	// 19.4.1.1 Symbol([description])
	if (!USE_NATIVE) {
	  $Symbol = function Symbol() {
	    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
	    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
	    var $set = function (value) {
	      if (this === ObjectProto) $set.call(OPSymbols, value);
	      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
	      setSymbolDesc(this, tag, createDesc(1, value));
	    };
	    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
	    return wrap(tag);
	  };
	  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
	    return this._k;
	  });

	  $GOPD.f = $getOwnPropertyDescriptor;
	  $DP.f = $defineProperty;
	  __webpack_require__(154).f = gOPNExt.f = $getOwnPropertyNames;
	  __webpack_require__(55).f = $propertyIsEnumerable;
	  $GOPS.f = $getOwnPropertySymbols;

	  if (DESCRIPTORS && !__webpack_require__(46)) {
	    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
	  }

	  wksExt.f = function (name) {
	    return wrap(wks(name));
	  };
	}

	$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

	for (var es6Symbols = (
	  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
	  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
	).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

	for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

	$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
	  // 19.4.2.1 Symbol.for(key)
	  'for': function (key) {
	    return has(SymbolRegistry, key += '')
	      ? SymbolRegistry[key]
	      : SymbolRegistry[key] = $Symbol(key);
	  },
	  // 19.4.2.5 Symbol.keyFor(sym)
	  keyFor: function keyFor(sym) {
	    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
	    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
	  },
	  useSetter: function () { setter = true; },
	  useSimple: function () { setter = false; }
	});

	$export($export.S + $export.F * !USE_NATIVE, 'Object', {
	  // 19.1.2.2 Object.create(O [, Properties])
	  create: $create,
	  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
	  defineProperty: $defineProperty,
	  // 19.1.2.3 Object.defineProperties(O, Properties)
	  defineProperties: $defineProperties,
	  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
	  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
	  // 19.1.2.7 Object.getOwnPropertyNames(O)
	  getOwnPropertyNames: $getOwnPropertyNames,
	  // 19.1.2.8 Object.getOwnPropertySymbols(O)
	  getOwnPropertySymbols: $getOwnPropertySymbols
	});

	// Chrome 38 and 39 `Object.getOwnPropertySymbols` fails on primitives
	// https://bugs.chromium.org/p/v8/issues/detail?id=3443
	var FAILS_ON_PRIMITIVES = $fails(function () { $GOPS.f(1); });

	$export($export.S + $export.F * FAILS_ON_PRIMITIVES, 'Object', {
	  getOwnPropertySymbols: function getOwnPropertySymbols(it) {
	    return $GOPS.f(toObject(it));
	  }
	});

	// 24.3.2 JSON.stringify(value [, replacer [, space]])
	$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
	  var S = $Symbol();
	  // MS Edge converts symbol values to JSON as {}
	  // WebKit converts symbol values to JSON as null
	  // V8 throws on boxed symbols
	  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
	})), 'JSON', {
	  stringify: function stringify(it) {
	    var args = [it];
	    var i = 1;
	    var replacer, $replacer;
	    while (arguments.length > i) args.push(arguments[i++]);
	    $replacer = replacer = args[1];
	    if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
	    if (!isArray(replacer)) replacer = function (key, value) {
	      if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
	      if (!isSymbol(value)) return value;
	    };
	    args[1] = replacer;
	    return _stringify.apply($JSON, args);
	  }
	});

	// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
	$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(11)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
	// 19.4.3.5 Symbol.prototype[@@toStringTag]
	setToStringTag($Symbol, 'Symbol');
	// 20.2.1.9 Math[@@toStringTag]
	setToStringTag(Math, 'Math', true);
	// 24.3.3 JSON[@@toStringTag]
	setToStringTag(global.JSON, 'JSON', true);


/***/ }),
/* 149 */
/***/ (function(module, exports, __webpack_require__) {

	var META = __webpack_require__(47)('meta');
	var isObject = __webpack_require__(14);
	var has = __webpack_require__(21);
	var setDesc = __webpack_require__(12).f;
	var id = 0;
	var isExtensible = Object.isExtensible || function () {
	  return true;
	};
	var FREEZE = !__webpack_require__(17)(function () {
	  return isExtensible(Object.preventExtensions({}));
	});
	var setMeta = function (it) {
	  setDesc(it, META, { value: {
	    i: 'O' + ++id, // object ID
	    w: {}          // weak collections IDs
	  } });
	};
	var fastKey = function (it, create) {
	  // return primitive with prefix
	  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
	  if (!has(it, META)) {
	    // can't set metadata to uncaught frozen object
	    if (!isExtensible(it)) return 'F';
	    // not necessary to add metadata
	    if (!create) return 'E';
	    // add missing metadata
	    setMeta(it);
	  // return object ID
	  } return it[META].i;
	};
	var getWeak = function (it, create) {
	  if (!has(it, META)) {
	    // can't set metadata to uncaught frozen object
	    if (!isExtensible(it)) return true;
	    // not necessary to add metadata
	    if (!create) return false;
	    // add missing metadata
	    setMeta(it);
	  // return hash weak collections IDs
	  } return it[META].w;
	};
	// add metadata on freeze-family methods calling
	var onFreeze = function (it) {
	  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
	  return it;
	};
	var meta = module.exports = {
	  KEY: META,
	  NEED: false,
	  fastKey: fastKey,
	  getWeak: getWeak,
	  onFreeze: onFreeze
	};


/***/ }),
/* 150 */
/***/ (function(module, exports, __webpack_require__) {

	var global = __webpack_require__(7);
	var core = __webpack_require__(8);
	var LIBRARY = __webpack_require__(46);
	var wksExt = __webpack_require__(145);
	var defineProperty = __webpack_require__(12).f;
	module.exports = function (name) {
	  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
	  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
	};


/***/ }),
/* 151 */
/***/ (function(module, exports, __webpack_require__) {

	// all enumerable object keys, includes symbols
	var getKeys = __webpack_require__(35);
	var gOPS = __webpack_require__(54);
	var pIE = __webpack_require__(55);
	module.exports = function (it) {
	  var result = getKeys(it);
	  var getSymbols = gOPS.f;
	  if (getSymbols) {
	    var symbols = getSymbols(it);
	    var isEnum = pIE.f;
	    var i = 0;
	    var key;
	    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
	  } return result;
	};


/***/ }),
/* 152 */
/***/ (function(module, exports, __webpack_require__) {

	// 7.2.2 IsArray(argument)
	var cof = __webpack_require__(39);
	module.exports = Array.isArray || function isArray(arg) {
	  return cof(arg) == 'Array';
	};


/***/ }),
/* 153 */
/***/ (function(module, exports, __webpack_require__) {

	// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
	var toIObject = __webpack_require__(37);
	var gOPN = __webpack_require__(154).f;
	var toString = {}.toString;

	var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
	  ? Object.getOwnPropertyNames(window) : [];

	var getWindowNames = function (it) {
	  try {
	    return gOPN(it);
	  } catch (e) {
	    return windowNames.slice();
	  }
	};

	module.exports.f = function getOwnPropertyNames(it) {
	  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
	};


/***/ }),
/* 154 */
/***/ (function(module, exports, __webpack_require__) {

	// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
	var $keys = __webpack_require__(36);
	var hiddenKeys = __webpack_require__(48).concat('length', 'prototype');

	exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
	  return $keys(O, hiddenKeys);
	};


/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

	var pIE = __webpack_require__(55);
	var createDesc = __webpack_require__(20);
	var toIObject = __webpack_require__(37);
	var toPrimitive = __webpack_require__(19);
	var has = __webpack_require__(21);
	var IE8_DOM_DEFINE = __webpack_require__(15);
	var gOPD = Object.getOwnPropertyDescriptor;

	exports.f = __webpack_require__(16) ? gOPD : function getOwnPropertyDescriptor(O, P) {
	  O = toIObject(O);
	  P = toPrimitive(P, true);
	  if (IE8_DOM_DEFINE) try {
	    return gOPD(O, P);
	  } catch (e) { /* empty */ }
	  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
	};


/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(150)('asyncIterator');


/***/ }),
/* 157 */
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(150)('observable');


/***/ }),
/* 158 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  baseUrl: 'https://apiv1.scribblelive.com',
	  pollInterval: 5000
	};

/***/ }),
/* 159 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _es6Promise = __webpack_require__(22);

	var _es6Promise2 = _interopRequireDefault(_es6Promise);

	var _superagent = __webpack_require__(114);

	var _superagent2 = _interopRequireDefault(_superagent);

	var _guid = __webpack_require__(119);

	var _guid2 = _interopRequireDefault(_guid);

	var _utils = __webpack_require__(160);

	var _utils2 = _interopRequireDefault(_utils);

	var _config = __webpack_require__(161);

	var _config2 = _interopRequireDefault(_config);

	var _config3 = __webpack_require__(162);

	var _config4 = _interopRequireDefault(_config3);

	var _pollingService = __webpack_require__(163);

	var _pollingService2 = _interopRequireDefault(_pollingService);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	if (typeof window !== 'undefined' && !window.Promise) {
	  window.Promise = _es6Promise2.default;
	}

	var CollectionService = function () {
	  function CollectionService() {
	    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	    (0, _classCallCheck3.default)(this, CollectionService);

	    _config4.default.init(options);
	    this.collectionMap = {};
	  }

	  /**
	   * This will load the specified collection and return posts (default 10, max 100).
	   * @param collectionId
	   * @param callback
	   * @returns {Promise}
	   */


	  (0, _createClass3.default)(CollectionService, [{
	    key: 'load',
	    value: function load(collectionId) {
	      var _this = this;

	      var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _utils2.default.noop;

	      if (!collectionId) {
	        throw Error('collectionId is required');
	      }

	      var guid = _guid2.default.raw();
	      this.collectionMap[collectionId] = guid;

	      var pollObj = {
	        items: [],
	        lasModifiedPostTime: Math.ceil(Date.now() / 1000)
	      };
	      _pollingService2.default.setPollObject(guid, pollObj);

	      var lastModifiedUrl = _config4.default.baseUrl + '/' + collectionId + '/lastmodified?token=' + _config4.default.token;

	      return new _es6Promise2.default(function (resolve, reject) {
	        _superagent2.default.get(lastModifiedUrl).then(function (res) {
	          var time = res.body.Time - 10000;
	          _pollingService2.default.setPollObjectValue(guid, 'lastChanged', time);
	          var url = _config4.default.baseUrl + '/' + collectionId + '?token=' + _config4.default.token + '&MaxItems=' + _config4.default.maxItem + '&timestamp=' + time;
	          if (_config4.default.auth) {
	            url += '&auth=' + _config4.default.auth;
	          }
	          _superagent2.default.get(url).then(function (res) {
	            try {
	              var result = JSON.parse(res.text);
	              if (_this.collectionMap.collectionId) {
	                _pollingService2.default.setInitialPollObjectItems(_this.collectionMap.collectionId, result.CollectionItems);
	              }
	              callback(null, result);
	              resolve(result);
	            } catch (err) {
	              throw err;
	            }
	          }).catch(function (err) {
	            callback(err, null);
	            reject(err);
	          });
	        });
	      });
	    }
	  }, {
	    key: 'poll',
	    value: function poll(collectionId) {
	      var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _utils2.default.noop;

	      if (!collectionId) {
	        throw Error('collectionId is required');
	      }

	      var guid = this.collectionMap[collectionId];
	      var id = setInterval(function () {
	        return _pollingService2.default.poll(collectionId, guid, callback);
	      }, _config4.default.pollInterval);

	      _pollingService2.default.setPollObjectValue(guid, 'cancelFn', function () {
	        return clearInterval(id);
	      });

	      return guid;
	    }
	  }]);
	  return CollectionService;
	}();

	exports.default = CollectionService;


	module.exports = CollectionService;

/***/ }),
/* 160 */
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  noop: function noop() {}
	};

/***/ }),
/* 161 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  baseUrl: 'https://api.scribblelive.com/v1/collection',
	  MaxItem: 50,
	  polling: {
	    pollInterval: 5000
	  }
	};

/***/ }),
/* 162 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _config = __webpack_require__(161);

	var _config2 = _interopRequireDefault(_config);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var ConfigService = function () {
	  function ConfigService() {
	    (0, _classCallCheck3.default)(this, ConfigService);
	  }

	  (0, _createClass3.default)(ConfigService, null, [{
	    key: 'init',
	    value: function init(options) {
	      if (!options.token) {
	        throw Error('Token is required');
	      }
	      this._options = options;
	    }
	  }, {
	    key: 'baseUrl',
	    get: function get() {
	      if (this._options && this._options.baseUrl) return this._options.baseUrl;
	      return _config2.default.baseUrl;
	    }
	  }, {
	    key: 'pollInterval',
	    get: function get() {
	      if (this._options && this._options.polling && this._options.polling.pollInterval) {
	        return this._options.polling.pollInterval;
	      }
	      return _config2.default.polling.pollInterval;
	    }
	  }, {
	    key: 'maxItem',
	    get: function get() {
	      if (this._options && this._options.MaxItem) return this._options.MaxItem;
	      return _config2.default.MaxItem;
	    }
	  }, {
	    key: 'token',
	    get: function get() {
	      if (this._options && this._options.token) return this._options.token;
	      throw Error('Token is required.');
	    }
	  }, {
	    key: 'auth',
	    get: function get() {
	      if (this._options && this._options.auth) return this._options.auth;
	      return null;
	    }
	  }]);
	  return ConfigService;
	}();

	exports.default = ConfigService;

/***/ }),
/* 163 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _keys = __webpack_require__(30);

	var _keys2 = _interopRequireDefault(_keys);

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _promise = __webpack_require__(93);

	var _promise2 = _interopRequireDefault(_promise);

	var _getIterator2 = __webpack_require__(68);

	var _getIterator3 = _interopRequireDefault(_getIterator2);

	var _superagent = __webpack_require__(114);

	var _superagent2 = _interopRequireDefault(_superagent);

	var _config = __webpack_require__(162);

	var _config2 = _interopRequireDefault(_config);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var latestPostInfo = {
	  pollId: {}
	};

	var shouldPoll = function shouldPoll(id) {
	  if (latestPostInfo.pollId[id] && latestPostInfo.pollId[id].disabled) {
	    return false;
	  }
	  return true;
	};

	var filterCollectionItemsByTimeFunction = function filterCollectionItemsByTimeFunction(newCollectionItem, id) {
	  if (newCollectionItem.LastModified > latestPostInfo.pollId[id].lastChanged) {
	    return true;
	  }
	  if (newCollectionItem.Timestamp > latestPostInfo.pollId[id].lastChanged) {
	    return true;
	  }
	  return false;
	};

	var filterOutDeletedItems = function filterOutDeletedItems(latestPostInfoItem, newCollectionItems, id) {
	  var _iteratorNormalCompletion = true;
	  var _didIteratorError = false;
	  var _iteratorError = undefined;

	  try {
	    for (var _iterator = (0, _getIterator3.default)(newCollectionItems), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	      var newItem = _step.value;

	      if (latestPostInfoItem.Id === newItem.Id) {
	        return false;
	      }
	    }
	  } catch (err) {
	    _didIteratorError = true;
	    _iteratorError = err;
	  } finally {
	    try {
	      if (!_iteratorNormalCompletion && _iterator.return) {
	        _iterator.return();
	      }
	    } finally {
	      if (_didIteratorError) {
	        throw _iteratorError;
	      }
	    }
	  }

	  return true;
	};

	var getDeletedItems = function getDeletedItems(newCollectionItems, id) {
	  var latestPostInfoItems = latestPostInfo.pollId[id].items;
	  var deletedItems = latestPostInfoItems.filter(function (item) {
	    return filterOutDeletedItems(item, newCollectionItems, id);
	  });
	  deletedItems.map(function (item) {
	    return item.IsDeleted = 1;
	  });
	  return deletedItems;
	};

	var getCollection = function getCollection(collectionId, id, time) {
	  var url = _config2.default.baseUrl + '/' + collectionId + '?token=' + _config2.default.token + '&orderBy=asc&timestamp=' + time;
	  return new _promise2.default(function (resolve, reject) {
	    _superagent2.default.get(url).then(function (res) {
	      var updatedItems = [];
	      var response = res.body;
	      if (response.CollectionItems.length) {
	        var newAndUpdatedItems = response.CollectionItems.filter(function (item) {
	          return filterCollectionItemsByTimeFunction(item, id);
	        });
	        var deletedItems = getDeletedItems(response.CollectionItems, id);
	        updatedItems = newAndUpdatedItems.concat(deletedItems);
	        // Store that timestamp within the lastChanged to only get most recent posts.
	        latestPostInfo.pollId[id].lastChanged = time;
	        latestPostInfo.pollId[id].items = response.CollectionItems;
	      }
	      resolve(updatedItems);
	    }, function (err) {
	      reject('Collection call failed.');
	    });
	  });
	};

	var PollingService = function () {
	  function PollingService() {
	    (0, _classCallCheck3.default)(this, PollingService);
	  }

	  (0, _createClass3.default)(PollingService, null, [{
	    key: 'poll',


	    /**
	     * This function will start the polling for a specific stream.
	     * @param collectionId
	     * @param id
	     * @param cb
	     */
	    value: function poll(collectionId, id, cb) {
	      var _this = this;

	      if (!shouldPoll(id)) return;
	      var url = _config2.default.baseUrl + '/' + collectionId + '/lastmodified?token=' + _config2.default.token;
	      return new _promise2.default(function (resolve, reject) {
	        _this.lastModified(url, id).then(function (time) {
	          return getCollection(collectionId, id, time);
	        }).then(function (collectionItems) {
	          cb(null, { CollectionItems: collectionItems });
	          resolve(collectionItems);
	          return;
	        }).catch(function (err) {
	          if (err) {
	            cb(err, null);
	            reject(err);
	            return;
	          }
	        });

	        resolve([]);
	      });
	    }

	    /**
	     * This function will get and set the last modified time for the stream.
	     * @param url
	     * @param id
	     * @returns {Promise}
	     */

	  }, {
	    key: 'lastModified',
	    value: function lastModified(url, id) {
	      return new _promise2.default(function (resolve, reject) {
	        _superagent2.default.get(url).then(function (res) {
	          if (!res.body) {
	            reject('Nothing returned.');
	            return;
	          }
	          var response = res.body;
	          if (latestPostInfo.pollId[id].lastChanged < parseInt(response.Time)) {
	            resolve(parseInt(response.Time));
	            return;
	          }
	          reject();
	        }, function (err) {
	          reject('Collection does not exist or has no lastmodified.');
	        });
	      });
	    }
	  }, {
	    key: 'setInitialPollObjectItems',
	    value: function setInitialPollObjectItems(guid, items) {
	      if (!latestPostInfo.pollId[guid].items) {
	        latestPostInfo.pollId[guid].items = items;
	      }
	    }

	    /**
	     * This function sets an object with its guid and poll information.
	     * @param {number} guid
	     * @param {Object} obj
	     */

	  }, {
	    key: 'setPollObject',
	    value: function setPollObject(guid, obj) {
	      latestPostInfo.pollId[guid] = obj;
	    }

	    /**
	     * This function sets a poll object's given value to a given value.
	     * @param {number} guid
	     * @param key
	     * @param value
	     */

	  }, {
	    key: 'setPollObjectValue',
	    value: function setPollObjectValue(guid, key, value) {
	      if (!latestPostInfo.pollId[guid]) {
	        this.setPollObject(guid, {});
	      }
	      latestPostInfo.pollId[guid][key] = value;
	    }

	    /**
	     * This will clear a poll interval and clear it's information from the latestPostInfo.
	     * @param pollId
	     * @returns {boolean}
	     */

	  }, {
	    key: 'killPoll',
	    value: function killPoll(id) {
	      latestPostInfo.pollId[id].cancelFn();
	      latestPostInfo.pollId[id].disabled = true;
	    }

	    /**
	     * This will clear all polls and clear the latestPostInfo object.
	     * @returns {boolean}
	     */

	  }, {
	    key: 'killAllPolls',
	    value: function killAllPolls() {
	      var keys = (0, _keys2.default)(latestPostInfo.pollId);
	      for (var i = 0; i < keys.length; i++) {
	        clearInterval(keys[i]);
	        this.killPoll(keys[i]);
	      }
	      return true;
	    }

	    /**
	     * Clears a reference to all polls.
	     */

	  }, {
	    key: 'clearPolls',
	    value: function clearPolls() {
	      latestPostInfo.pollId = {};
	    }

	    /**
	     * This will return all current poll ids.
	     * @returns {Array}
	     */

	  }, {
	    key: 'getPollIds',
	    value: function getPollIds() {
	      return (0, _keys2.default)(latestPostInfo.pollId);
	    }
	  }]);
	  return PollingService;
	}();

	exports.default = PollingService;

/***/ }),
/* 164 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	var _helperFunctions = __webpack_require__(60);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var video = function video(post) {
	    var _isMobile = (0, _helperFunctions.isMobile)();
	    var url = post.Media.Url;
	    var thumbnail = post.Media.Thumbnail || '';
	    var duration = post.Media.Duration || 0;
	    var isHero = false;
	    if (post.PostMeta && post.PostMeta.Tags && post.PostMeta.Tags.indexOf("hero") > -1) {
	        isHero = true;
	    }

	    var caption = '';
	    if (post.Caption !== undefined) {
	        caption = '<div class="caption-container">\n                        ' + (isHero ? '<div>' + post.Caption + '</div>' : '<p>' + post.Caption + '</p>') + '\n                    </div>';
	    }

	    var _isFF = navigator.userAgent.match(/firefox/i) != null;
	    var _isEdge = navigator.userAgent.indexOf("Edge") > -1;

	    // don't include the objectTag in FF to stop the autoplay
	    var objectTag = '<object data="' + url + '"><embed src="' + thumbnail + '"></object>';

	    var videoPost = _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-VIDEO' + (isHero ? " hero" : "") + '">\n                                        ' + (isHero && _isMobile ? '<div class="fa fa-video-camera"></div>' : '') + '\n                                        <video ' + (!isHero ? "controls" : _isMobile ? "" : "loop muted autoplay") + ' poster="' + thumbnail + '" preload="' + (duration > 15 && !isHero ? 'none' : 'auto') + '">\n                                            <source src="' + url + '" type="video/mp4">\n                                            Your browser does not support the video tag.\n                                            ' + (_isFF || _isEdge ? "" : objectTag) + '\n                                        </video>\n                                        ' + caption + '\n                                    </div>');

	    if (_isMobile) {
	        var videoElement = videoPost.querySelector('video');
	        videoPost.onclick = function () {
	            videoElement.play();
	            if (typeof videoElement.webkitEnterFullscreen !== 'undefined') {
	                // Android Stock
	                videoElement.webkitEnterFullscreen();
	            } else if (typeof videoElement.webkitRequestFullscreen !== 'undefined') {
	                // Chrome
	                videoElement.webkitRequestFullscreen();
	            }
	        };
	        videoElement.addEventListener('webkitfullscreenchange', function () {
	            if (!document.webkitFullscreenElement) {
	                videoElement.pause();
	            }
	        });
	    }

	    return videoPost;
	};

	exports.default = video;

/***/ }),
/* 165 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _getIterator2 = __webpack_require__(68);

	var _getIterator3 = _interopRequireDefault(_getIterator2);

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	var _postHelper = __webpack_require__(25);

	var _postHelper2 = _interopRequireDefault(_postHelper);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var adUrl = 'https://client-ads.scribblelive.com';

	function getAd(clientId, ad) {
	  var Id = ad.Id;
	  var Size = ad.Size || ad.EventId;
	  var clientDir = ('' + clientId).split('').join('/');
	  return adUrl + '/' + clientDir + '/' + Id + '_' + clientId + '_' + Size + '.html?calculateWidth';
	}

	function getValidAd(ads) {
	  var width = document.documentElement.clientWidth;
	  var target = null;
	  for (var i = 0; i < ads.length; i++) {
	    if (!ads[i].Id || !ads[i].Size && !ads[i].EventId) {
	      continue;
	    } else if (!target && (ads[i].Size < width || ads[i].EventId)) {
	      target = ads[i];
	      if (ads[i].EventId) break;
	    }
	  }
	  if (!target) {
	    return ads[0];
	  }
	  return target;
	}

	function sortBySize(a, b) {
	  if (a.Size < b.Size) return 1;else if (a.Size > b.Size) return -1;
	  return 0;
	}

	var advertisement = function advertisement(post) {
	  var adData = _postHelper2.default.getAdData();
	  var adScript = _postHelper2.default.getAdScript();

	  if (adData) {
	    adData.ads.sort(sortBySize);
	  }

	  var ad = getValidAd(adData.ads);
	  var url = getAd(adData.clientId, ad);
	  if (url) {
	    if (!adScript) {
	      // Set window.eventListener
	      window.addEventListener('message', function (data) {
	        try {
	          if (typeof data.data !== 'string') return;
	          var adInfo = JSON.parse(data.data.replace('scrbbl-resizableiframe: ', ''));

	          //switch to classes because all the iframes are loaded with same id
	          //when we lookup iframe for resizing, we only get one. the class
	          //allows us to resize all.
	          var targets = document.getElementsByClassName('SL-AD-' + adInfo.AdId);

	          //the width of the advertisement can be null if the ad code was placed
	          //during creation of the story. So we'll  check for null if no element
	          //with existing class was found ad act upon that.
	          if (targets.length == 0) {
	            targets = document.getElementsByClassName('SL-AD-' + adInfo.AdId + '-null');
	          }

	          var _iteratorNormalCompletion = true;
	          var _didIteratorError = false;
	          var _iteratorError = undefined;

	          try {
	            for (var _iterator = (0, _getIterator3.default)(targets), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	              var iframe = _step.value;

	              iframe.height = adInfo.height + 'px';
	              iframe.width = adInfo.width;
	            }
	          } catch (err) {
	            _didIteratorError = true;
	            _iteratorError = err;
	          } finally {
	            try {
	              if (!_iteratorNormalCompletion && _iterator.return) {
	                _iterator.return();
	              }
	            } finally {
	              if (_didIteratorError) {
	                throw _iteratorError;
	              }
	            }
	          }
	        } catch (e) {}
	      });
	      _postHelper2.default.setAdScript(true);
	    }
	  }
	  return _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-ADVERTISEMENT">\n                                        <iframe src="' + url + '" width="' + ad.Size + '" id="SL-AD-' + ad.Id + '-' + ad.Size + '" class="SL-AD-' + ad.Id + '" frameborder="0">\n                                            <div>Your browser doesn\'t support iframes</div>\n                                        </iframe>\n                                    </div>');
	};

	exports.default = advertisement;

/***/ }),
/* 166 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function isMobile() {
	  return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
	  );
	}

	function renderPosts(posts) {
	  var html = [];
	  posts.forEach(function (postData) {
	    html.push(postData.fn(postData.post));
	  });
	  return html;
	}

	var List = function () {
	  function List() {
	    (0, _classCallCheck3.default)(this, List);

	    this.items = [];
	  }

	  (0, _createClass3.default)(List, [{
	    key: 'append',
	    value: function append(postFunc) {
	      this.items.push(postFunc);
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      if (this.items.length === 1) {
	        return this.items[0].fn(this.items[0].post);
	      }
	      var ids = this.items.map(function (postData) {
	        return postData.post.Id;
	      }).join('-');
	      var html = _htmlGenerator2.default.generate('<div class=\'LIST\' id=\'SL-List-' + ids + '\'></div>');
	      renderPosts(this.items).forEach(function (post) {
	        html.appendChild(post);
	      });
	      return html;
	    }
	  }, {
	    key: 'Id',
	    get: function get() {
	      if (this.items.length === 1) return this.items[0].post.Id;
	      var ids = this.items.map(function (postData) {
	        return postData.post.Id;
	      }).join('-');
	      return 'List-' + ids;
	    }
	  }]);
	  return List;
	}();

	exports.default = List;

/***/ }),
/* 167 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var audio = function audio(post) {

	  var caption = post.Caption || '';

	  var audio = _htmlGenerator2.default.generate('<div class="SL-AUDIO" id="SL-' + post.Id + '">\n      <audio src="' + post.Media.Url + '" controls>\n      </audio>\n      <div class="caption-container"><p>' + caption + '</p></div>\n    </div>');

	  return audio;
	};

	exports.default = audio;

/***/ }),
/* 168 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = untracked;

	var _untrackedSocialPostRenderer = __webpack_require__(169);

	var _untrackedSocialPostRenderer2 = _interopRequireDefault(_untrackedSocialPostRenderer);

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function untracked(post) {
	  return _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-HTML">\n    ' + _untrackedSocialPostRenderer2.default.render(post) + '</div>');
	}

/***/ }),
/* 169 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _normalize = __webpack_require__(170);

	var _normalize2 = _interopRequireDefault(_normalize);

	var _htmlStringGenerator = __webpack_require__(179);

	var _htmlStringGenerator2 = _interopRequireDefault(_htmlStringGenerator);

	var _parseTypes = __webpack_require__(171);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var UntrackedSocialPostRenderer = function () {
	  function UntrackedSocialPostRenderer() {
	    (0, _classCallCheck3.default)(this, UntrackedSocialPostRenderer);
	  }

	  (0, _createClass3.default)(UntrackedSocialPostRenderer, null, [{
	    key: 'render',
	    value: function render(post) {
	      if (post.PostMeta) {
	        return (0, _htmlStringGenerator2.default)((0, _normalize2.default)(post));
	      }
	      // Invalid post
	      return null;
	    }
	  }, {
	    key: 'parsers',
	    get: function get() {
	      return { parseTweet: _parseTypes.parseTweet, parseInstagram: _parseTypes.parseInstagram, parseFacebook: _parseTypes.parseFacebook, parseHtml: _parseTypes.parseHtml, proxifyAssets: _parseTypes.proxifyAssets, proxifyUrl: _parseTypes.proxifyUrl };
	    }
	  }]);
	  return UntrackedSocialPostRenderer;
	}();

	module.exports = UntrackedSocialPostRenderer;

/***/ }),
/* 170 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _keys = __webpack_require__(30);

	var _keys2 = _interopRequireDefault(_keys);

	var _getIterator2 = __webpack_require__(68);

	var _getIterator3 = _interopRequireDefault(_getIterator2);

	var _stringify = __webpack_require__(26);

	var _stringify2 = _interopRequireDefault(_stringify);

	var _defineProperty2 = __webpack_require__(141);

	var _defineProperty3 = _interopRequireDefault(_defineProperty2);

	var _TYPE_MAP;

	exports.default = normalize;

	var _parseTypes = __webpack_require__(171);

	var _helperFunctions = __webpack_require__(173);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var PARSE_MAP = {
	  TWEET: _parseTypes.parseTweet,
	  INSTAGRAM: _parseTypes.parseInstagram,
	  FACEBOOK: _parseTypes.parseFacebook,
	  HTML: _parseTypes.parseHtml
	};

	var TYPE_MAP = (_TYPE_MAP = {
	  1: 'TEXT',
	  2: 'IMAGE',
	  3: 'VIDEO',
	  4: 'INVITE'
	}, (0, _defineProperty3.default)(_TYPE_MAP, '4', 'SYSTEM'), (0, _defineProperty3.default)(_TYPE_MAP, 6, 'AUDIO'), (0, _defineProperty3.default)(_TYPE_MAP, 7, 'EMBED'), (0, _defineProperty3.default)(_TYPE_MAP, 8, 'JUSTINTVSTREAM'), (0, _defineProperty3.default)(_TYPE_MAP, 9, 'POLL'), (0, _defineProperty3.default)(_TYPE_MAP, 10, 'HTML'), (0, _defineProperty3.default)(_TYPE_MAP, 11, 'TWEET'), (0, _defineProperty3.default)(_TYPE_MAP, 12, 'LEADCAP'), (0, _defineProperty3.default)(_TYPE_MAP, 13, 'ADVERTISEMENT'), _TYPE_MAP);

	function getType(obj) {
	  if (obj.Type === 'HTML' && !(0, _helperFunctions.checkIfHtmlIsFacebook)(obj)) {
	    if (!obj.PostMeta || ['twitter:tweet', 'facebook:post', 'instagram:post'].indexOf(obj.PostMeta.Type) < 0) {
	      return 'HTML';
	    }
	  }

	  if ((0, _helperFunctions.isHtmlWithSocial)(obj)) {
	    return 'HTML';
	  }

	  var type = obj.type || obj.PostMeta.Type;

	  switch (type) {
	    case 'twitter:tweet':
	      return 'TWEET';
	    case 'facebook:post':
	      return 'FACEBOOK';
	    case 'instagram:post':
	    case 'Instagram:post':
	      return 'INSTAGRAM';
	  }

	  if (obj.Type) {
	    type = obj.Type;
	  }

	  var num = parseInt(type, 0);
	  if (!isNaN(num)) {
	    return TYPE_MAP['' + num];
	  }

	  return type;
	}

	function normalize(obj) {
	  var isHtml = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	  var objCopy = JSON.parse((0, _stringify2.default)(obj));
	  if (!objCopy.Type && !objCopy.type) return null;

	  objCopy.Type = getType(objCopy);

	  var _iteratorNormalCompletion = true;
	  var _didIteratorError = false;
	  var _iteratorError = undefined;

	  try {
	    for (var _iterator = (0, _getIterator3.default)((0, _keys2.default)(PARSE_MAP)), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	      var type = _step.value;

	      if (objCopy.Type === type) {
	        return PARSE_MAP[type](objCopy, isHtml);
	      }
	    }
	  } catch (err) {
	    _didIteratorError = true;
	    _iteratorError = err;
	  } finally {
	    try {
	      if (!_iteratorNormalCompletion && _iterator.return) {
	        _iterator.return();
	      }
	    } finally {
	      if (_didIteratorError) {
	        throw _iteratorError;
	      }
	    }
	  }

	  return objCopy;
	}

/***/ }),
/* 171 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.proxifyUrl = exports.proxifyAssets = exports.parseHtml = exports.parseFacebook = exports.parseInstagram = exports.parseTweet = undefined;

	var _stringify = __webpack_require__(26);

	var _stringify2 = _interopRequireDefault(_stringify);

	var _assign = __webpack_require__(50);

	var _assign2 = _interopRequireDefault(_assign);

	var _normalize = __webpack_require__(170);

	var _normalize2 = _interopRequireDefault(_normalize);

	var _untrackedFacebook = __webpack_require__(172);

	var _untrackedFacebook2 = _interopRequireDefault(_untrackedFacebook);

	var _untrackedTwitter = __webpack_require__(177);

	var _untrackedTwitter2 = _interopRequireDefault(_untrackedTwitter);

	var _untrackedInstagram = __webpack_require__(178);

	var _untrackedInstagram2 = _interopRequireDefault(_untrackedInstagram);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	/* eslint-disable max-len */
	var requestProxy = "https://octopus.scribblelive.com/data/get?url=";
	var fbURL = "//www.facebook.com/";

	var STRING_MAP = {
	  FACEBOOK: _untrackedFacebook2.default,
	  TWEET: _untrackedTwitter2.default,
	  INSTAGRAM: _untrackedInstagram2.default
	};

	function proxifyUrl(url) {
	  if (!url) return url;

	  return requestProxy + encodeURIComponent(url);
	}

	function getDefaultObj(post) {
	  return {
	    Id: post.Id,
	    Type: post.Type,
	    Content: post.Content,
	    Media: null,
	    LastModifiedDate: new Date(post.LastModifiedDate),
	    CreationDate: new Date(post.CreationDate),
	    IsDeleted: post.IsDeleted
	  };
	}

	var socialMediaId = {
	  FACEBOOK: function FACEBOOK(data) {
	    return data.id;
	  },
	  TWEET: function TWEET(data) {
	    return data.id_str;
	  },
	  INSTAGRAM: function INSTAGRAM(data) {
	    return data.id;
	  }
	};

	var socialMediaName = {
	  FACEBOOK: function FACEBOOK(data) {
	    return data.from.name;
	  },
	  TWEET: function TWEET(data) {
	    return data.user.name;
	  },
	  INSTAGRAM: function INSTAGRAM(data) {
	    return '';
	  }
	};

	var socialMediaCaption = {
	  FACEBOOK: function FACEBOOK() {
	    return '';
	  },
	  TWEET: function TWEET() {
	    return '';
	  },
	  INSTAGRAM: function INSTAGRAM(data) {
	    return data ? data.caption : '';
	  }
	};

	var socialMediaHandle = {
	  FACEBOOK: function FACEBOOK(data) {
	    var match = /http[s]{0,1}:\/\/www\.facebook\.com\/([a-zA-Z0-9\.]+)*/g.exec(data.link);
	    if (!match && data.from && data.from.name) return data.from.name;
	    if (!match && data.from && data.from.id) return data.from.id;
	    return match[1];
	  },
	  TWEET: function TWEET(data) {
	    return data.user.screen_name;
	  },
	  INSTAGRAM: function INSTAGRAM(data) {
	    return '';
	  }
	};

	var socialMediaAvatar = {
	  FACEBOOK: function FACEBOOK(data) {
	    var fromId = typeof data.from !== 'undefined' && data.from.id !== 'undefined' ? data.from.id : null;
	    return proxifyUrl('https://graph.facebook.com/' + fromId + '/picture');
	  },
	  TWEET: function TWEET(data) {
	    return proxifyUrl(data.user.profile_image_url_https);
	  },
	  INSTAGRAM: function INSTAGRAM(data) {
	    return '';
	  }
	};

	var socialMediaCreationDate = {
	  FACEBOOK: function FACEBOOK(data) {
	    return data.created_time;
	  },
	  TWEET: function TWEET(data) {
	    return data.created_at;
	  },
	  INSTAGRAM: function INSTAGRAM(data) {
	    return '';
	  }
	};

	var socialMediaLike = {
	  FACEBOOK: function FACEBOOK(data) {
	    if (data && data.actions && data.actions.length > 1) return data.actions[1].link;
	    return socialMediaSource.FACEBOOK(data);;
	  },
	  TWEET: function TWEET(data) {
	    return 'https://twitter.com/intent/like?tweet_id=' + data.id_str;
	  },
	  INSTAGRAM: function INSTAGRAM(data) {
	    return data.like_count;
	  }
	};

	var socialMediaShare = {
	  FACEBOOK: function FACEBOOK(data) {
	    if (typeof data !== "undefined" && typeof data.actions !== "undefined" && typeof data.actions.length !== "undefined" && data.actions.length > 2) {
	      return data.actions[2].link;
	    } else {
	      return socialMediaSource.FACEBOOK(data);
	    }

	    return '';
	  },
	  TWEET: function TWEET(data) {
	    return 'https://twitter.com/intent/retweet?tweet_id=' + data.id_str;
	  },
	  INSTAGRAM: function INSTAGRAM() {
	    return '';
	  }
	};

	var socialMediaReply = {
	  FACEBOOK: function FACEBOOK(data) {
	    return typeof data !== "undefined" && typeof data.actions !== "undefined" && typeof data.actions.length !== "undefined" && data.actions.length > 1 ? data.actions[0].link : socialMediaSource.FACEBOOK(data);
	  },
	  TWEET: function TWEET(data) {
	    return 'https://twitter.com/intent/tweet?in_reply_to=' + data.id_str;
	  },
	  INSTAGRAM: function INSTAGRAM() {
	    return '';
	  }
	};

	var socialMediaSource = {
	  FACEBOOK: function FACEBOOK(data) {
	    var link = data.link && data.link.indexOf(fbURL) > -1 ? data.link : data.permalink_url;
	    if (link) {
	      return link;
	    }
	    var handle = socialMediaHandle.FACEBOOK(data);
	    var id = socialMediaId.FACEBOOK(data);

	    return 'https:' + fbURL + handle + '/posts/' + id;
	  },
	  TWEET: function TWEET(data) {
	    return 'https://twitter.com/' + data.user.screen_name + '/status/' + data.id_str;
	  },
	  INSTAGRAM: function INSTAGRAM(data) {
	    return data.permalink;
	  }
	};

	var socialMediaAuthorUrl = {
	  FACEBOOK: function FACEBOOK(data) {
	    var fromId = typeof data.from !== 'undefined' && data.from.id !== 'undefined' ? data.from.id : null;
	    return data.authorUrl && data.authorUrl.indexOf(fbURL) > -1 ? data.authorUrl : fromId ? '' + fbURL + fromId : null;
	  },
	  TWEET: function TWEET(data) {
	    return null;
	  },
	  INSTAGRAM: function INSTAGRAM(data) {
	    return null;
	  }
	};

	var socialMediaUrls = {
	  FACEBOOK: function FACEBOOK(data) {
	    return null;
	  },
	  TWEET: function TWEET(data) {
	    return null;
	  },
	  INSTAGRAM: function INSTAGRAM(data) {
	    return null;
	  }
	};

	var getSocialMediaData = function getSocialMediaData(data, type) {
	  return {
	    id: socialMediaId[type](data),
	    name: socialMediaName[type](data),
	    caption: socialMediaCaption[type](data),
	    handle: socialMediaHandle[type](data),
	    avatar: socialMediaAvatar[type](data),
	    creationDate: socialMediaCreationDate[type](data),
	    like: socialMediaLike[type](data),
	    share: socialMediaShare[type](data),
	    reply: socialMediaReply[type](data),
	    source: socialMediaSource[type](data),
	    media: socialMediaUrls[type](data),
	    author: socialMediaAuthorUrl[type](data)
	  };
	};

	function findMediaTypes(obj) {
	  if (!obj.Media) return;
	  // YouTube check
	  var found = obj.Media.filter(function (a) {
	    return a.expanded_url.indexOf('//youtu') > -1 || a.expanded_url.indexOf('//www.youtu') > -1 || a.expanded_url.indexOf('//youtube.com') > -1;
	  });
	  if (found.length) {
	    // YouTube url.
	    obj.Media = found[0];
	    var indexOfV = found[0].expanded_url.indexOf('?v=');
	    var indexOfSlash = found[0].expanded_url.lastIndexOf('/');

	    obj.Media.expanded_url = 'https://www.youtube.com/embed/' + found[0].expanded_url.substr(indexOfV > -1 ? indexOfV + 3 : indexOfSlash + 1);

	    obj.MediaType = 'EMBED';
	    return;
	  }
	  // Vimeo check
	  found = obj.Media.filter(function (a) {
	    return a.expanded_url.indexOf('vimeo') > -1;
	  });
	  if (found.length) {
	    obj.Media = found[0];
	    obj.Media.expanded_url = 'https://player.vimeo.com/video/' + found[0].expanded_url.substr(found[0].expanded_url.lastIndexOf('/') + 1);
	    obj.MediaType = 'EMBED';
	  }
	}

	function parseTweet(post) {
	  var isHtml = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	  var newObj = (0, _assign2.default)({}, getDefaultObj(post));
	  var meta = isHtml ? post.data : post.PostMeta.Tweet;

	  // Ensure meta is in json format.
	  if (typeof meta === 'string') {
	    try {
	      meta = JSON.parse(meta);
	    } catch (e) {/* Do nothing */}
	  }

	  newObj.Content = meta.text;

	  if (meta.extended_entities && meta.extended_entities.media) {
	    newObj.Media = meta.extended_entities.media;
	    switch (newObj.Media[0].type) {
	      case 'video':
	        newObj.MediaType = 'VIDEO';
	        if (newObj.Media[0].media_url_https) {
	          newObj.Media[0].media_url_https = proxifyUrl(newObj.Media[0].media_url_https);
	        }
	        break;
	      default:
	        newObj.MediaType = 'IMAGE';
	        newObj.Media[0].media_url_https = proxifyUrl(newObj.Media[0].media_url_https);
	    }
	  } else if (meta.retweeted_status && meta.retweeted_status.extended_entities && meta.retweeted_status.extended_entities.media && meta.retweeted_status.extended_entities.media.length > 0) {
	    newObj.Media = meta.retweeted_status.extended_entities.media;
	  } else if (meta.retweeted_status && meta.retweeted_status.extended_tweet && meta.retweeted_status.extended_tweet.entities.media && meta.retweeted_status.extended_tweet.entities.media.length > 0) {
	    newObj.Media = meta.retweeted_status.extended_tweet.entities.media;
	  } else {
	    newObj.Media = meta.entities.urls;
	    // Check for valid media types.
	    findMediaTypes(newObj);
	  }

	  newObj.TweetId = meta.id_str;

	  // Setting social types
	  newObj.SocialData = getSocialMediaData(meta, post.Type);

	  return newObj;
	}

	function parseInstagram(post) {
	  var isHtml = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	  var newObj = (0, _assign2.default)({}, getDefaultObj(post));
	  var meta = isHtml ? post.data : post.PostMeta.Instagram;

	  // Ensure meta is in json format.
	  if (typeof meta === 'string') {
	    try {
	      meta = JSON.parse(meta);
	    } catch (e) {/* Do nothing */}
	  }

	  newObj.Content = meta.caption ? meta.caption : '';

	  var contentType = meta.media_type || (typeof meta.images !== 'undefined' ? 'IMAGE' : null);
	  switch (contentType) {
	    case 'VIDEO':
	      newObj.Media = {
	        Videos: {
	          standard_resolution: {
	            url: meta.media_url
	          }
	        },
	        Images: {
	          standard_resolution: {
	            url: meta.media_url
	          }
	        }
	      };
	      newObj.MediaType = 'VIDEO';
	      newObj.Media.Videos.standard_resolution.url = proxifyUrl(meta.media_url);
	      break;

	    case 'IMAGE':
	    default:
	      var mediaUrl = meta.media_url || meta.images && meta.images.standard_resolution && meta.images.standard_resolution.url;

	      newObj.Media = {
	        standard_resolution: {
	          url: mediaUrl
	        }
	      };
	      newObj.MediaType = 'IMAGE';
	      newObj.Media.standard_resolution.url = proxifyUrl(mediaUrl);
	      break;
	  }

	  // Setting social types
	  newObj.SocialData = getSocialMediaData(meta, post.Type);

	  return newObj;
	}

	function parseFacebook(post) {
	  var isHtml = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	  var newObj = (0, _assign2.default)({}, getDefaultObj(post));
	  var meta = isHtml ? post.data : post.PostMeta.Facebook;
	  // Ensure meta is in json format.
	  if (typeof meta === 'string') {
	    try {
	      meta = JSON.parse(meta);
	    } catch (e) {/* Do nothing */}
	  }

	  newObj.Content = meta.message || meta.description || meta.name || '';
	  if (meta.type) {
	    // We have a non-pasted post.  We don't have type if it's pasted.
	    if (meta.type === 'photo' || meta.type === 'link') {
	      newObj.Media = { Image: proxifyUrl(meta.full_picture || meta.picture) };
	      newObj.MediaType = 'IMAGE';
	    } else if (meta.type === 'video') {
	      newObj.Media = {
	        Images: proxifyUrl(meta.full_picture || meta.picture),
	        Videos: meta.source
	      };
	      newObj.MediaType = 'VIDEO';
	    }
	  } else {
	    if (meta.images) {
	      newObj.Media = { Image: proxifyUrl(meta.images[0].source) };
	      newObj.MediaType = 'IMAGE';
	    } else if (meta.source) {
	      newObj.Media = {
	        Videos: meta.source
	      };
	    }
	  }

	  newObj.SocialData = getSocialMediaData(meta, post.Type);

	  return newObj;
	}

	function parseHtml(post) {
	  if (!post.PostMeta.Data) {

	    var postCopy = JSON.parse((0, _stringify2.default)(post));
	    postCopy.Content = proxifyAssets(post.Content);
	    return postCopy;
	  }

	  var postMeta = post.PostMeta.Data;

	  try {
	    postMeta = JSON.parse(post.PostMeta.Data);
	  } catch (ex) {}

	  var socialPosts = postMeta.map(function (socialPost) {
	    return { type: socialPost.type, data: (0, _normalize2.default)(socialPost, true) };
	  });

	  var regexString = /<div class="(fb-post|fb-video)(.|\n)*?<\/div>|<div class="scrbbl-post-embed(.|\n)*?<\/div>|<blockquote[^>]*?class="twitter-tweet(.|\n)*?<\/blockquote>/;

	  var contentString = post.Content;
	  while (contentString.search(regexString) > -1) {
	    contentString = contentString.replace(regexString, STRING_MAP[socialPosts[0].type](socialPosts[0].data));
	    socialPosts.shift();
	  }

	  var newObj = post;
	  newObj.Content = contentString;

	  return newObj;
	}

	function proxifyAssets(content) {

	  var regex_str = [];
	  proxifyAssetsList().forEach(function (matchUrl) {
	    regex_str.push(matchUrl.replace(".", "\."));
	  });

	  var regex_str_replace = regex_str.join("|");
	  var regex = new RegExp("(" + regex_str_replace + ")", "i");

	  var div = document.createElement('div');
	  div.innerHTML = content;

	  var nodes = div.getElementsByTagName("img");
	  for (var i = 0; i < nodes.length; i++) {

	    if (regex.test(nodes[i].src)) {
	      var newUrl = proxifyUrl(nodes[i].src);
	      nodes[i].src = newUrl;
	    }
	  }

	  return div.innerHTML;
	}

	function proxifyAssetsList() {
	  return ["pbs.twimg.com", "fbcdn.net", "cdninstagram.com"];
	}

	exports.parseTweet = parseTweet;
	exports.parseInstagram = parseInstagram;
	exports.parseFacebook = parseFacebook;
	exports.parseHtml = parseHtml;
	exports.proxifyAssets = proxifyAssets;
	exports.proxifyUrl = proxifyUrl;

/***/ }),
/* 172 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = generateUntrackedFBString;

	var _helperFunctions = __webpack_require__(173);

	var _prepareHtmlString = __webpack_require__(174);

	var _prepareHtmlString2 = _interopRequireDefault(_prepareHtmlString);

	var _videoScripts = __webpack_require__(175);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var twttrText = __webpack_require__(176);


	function generateAnchorForLinksAndHashtags(content) {
	  return twttrText.autoLinkEntities(content, twttrText.extractEntitiesWithIndices(content, { extractUrlsWithoutProtocol: true }), {
	    target: "_blank",
	    suppressNoFollow: true,
	    hashtagUrlBase: "https://www.facebook.com/hashtag/",
	    usernameUrlBase: 'https://www.facebook.com/search/top/?q='
	  });
	}

	function createUntrackedFB(post) {
	  switch (post.MediaType) {
	    case 'IMAGE':
	      return '<div class="sl-untracked-social">\n          <img class="sl-untracked-social-picture" src="' + post.Media.Image + '" onerror="this.src=\'https://bucket-files.scribblelive.com/DefaultThumbnails/FileNotFound.png\'"/>\n          <div class="sl-untracked-social-content sl-untracked-social-media">\n            <div class="sl-untracked-social-header">\n              <a target="_blank" href="' + post.SocialData.author + '">\n                <img class="sl-untracked-social-avatar" src="' + post.SocialData.avatar + '">\n              </a>\n              <div class="sl-untracked-social-name-and-handle">\n                <div class="sl-untracked-social-name"><a href="' + post.SocialData.author + '">' + post.SocialData.name + '</a></div>\n              </div>\n              <div class="sl-icon sl-icon-facebook"><a href="' + post.SocialData.source + '"></a></div>\n            </div>\n            <div class="sl-untracked-social-message">' + generateAnchorForLinksAndHashtags(post.Content) + '</div>\n            <div class="sl-untracked-social-date">' + (0, _helperFunctions.formatDate)(post.SocialData.creationDate) + '</div>\n            <div class="sl-untracked-social-actions">\n              <div class="sl-untracked-social-action sl-like-facebook"><a href="' + post.SocialData.like + '"></a></div>\n              <div class="sl-untracked-social-action sl-comment-facebook"><a href="' + post.SocialData.reply + '"></a></div>\n              <div class="sl-untracked-social-action sl-share-facebook"><a href="' + post.SocialData.share + '"></a></div>\n            </div>\n          </div>\n        </div>';
	    case 'VIDEO':
	      return '<div class="sl-untracked-social">\n          <div class="sl-untracked-social-picture">\n            <img class="sl-untracked-social-picture" src="' + post.Media.Images + '" onerror="' + _videoScripts.fbVideoError + '"/>\n            <div class="sl-untracked-facebook-play">\n              <a href="' + post.SocialData.source + '"></a>\n            </div>\n          </div>\n          <div class="sl-untracked-social-content sl-untracked-social-media">\n            <div class="sl-untracked-social-header">\n              <a target="_blank" href="' + post.SocialData.author + '">\n                <img class="sl-untracked-social-avatar" src="' + post.SocialData.avatar + '">\n              </a>\n              <div class="sl-untracked-social-name-and-handle">\n                <div class="sl-untracked-social-name"><a href="' + post.SocialData.author + '">' + post.SocialData.name + '</a></div>\n              </div>\n              <div class="sl-icon sl-icon-facebook"><a href="' + post.SocialData.source + '"></a></div>\n            </div>\n            <div class="sl-untracked-social-message">' + generateAnchorForLinksAndHashtags(post.Content) + '</div>\n            <div class="sl-untracked-social-date">' + (0, _helperFunctions.formatDate)(post.SocialData.creationDate) + '</div>\n            <div class="sl-untracked-social-actions">\n              <div class="sl-untracked-social-action sl-like-facebook"><a href="' + post.SocialData.like + '"></a></div>\n              <div class="sl-untracked-social-action sl-comment-facebook"><a href="' + post.SocialData.reply + '"></a></div>\n              <div class="sl-untracked-social-action sl-share-facebook"><a href="' + post.SocialData.share + '"></a></div>\n            </div>\n          </div>\n        </div>';
	    default:
	      return '<div class="sl-untracked-social">\n          <div class="sl-untracked-social-content">\n            <div class="sl-untracked-social-header">\n              <a target="_blank" href="' + post.SocialData.author + '">\n                <img class="sl-untracked-social-avatar" src="' + post.SocialData.avatar + '">\n              </a>\n              <div class="sl-untracked-social-name-and-handle">\n                <div class="sl-untracked-social-name"><a href="' + post.SocialData.author + '">' + post.SocialData.name + '</a></div>\n              </div>\n              <div class="sl-icon sl-icon-facebook"><a href="' + post.SocialData.source + '"></a></div>\n            </div>\n            <div class="sl-untracked-social-message">' + generateAnchorForLinksAndHashtags(post.Content) + '</div>\n            <div class="sl-untracked-social-date">' + (0, _helperFunctions.formatDate)(post.SocialData.creationDate) + '</div>\n            <div class="sl-untracked-social-actions">\n              <div class="sl-untracked-social-action sl-like-facebook"><a href="' + post.SocialData.like + '"></a></div>\n              <div class="sl-untracked-social-action sl-comment-facebook"><a href="' + post.SocialData.reply + '"></a></div>\n              <div class="sl-untracked-social-action sl-share-facebook"><a href="' + post.SocialData.share + '"></a></div>\n            </div>\n          </div>\n        </div>';
	  }
	}

	function generateUntrackedFBString(post) {
	  return (0, _prepareHtmlString2.default)('<div ' + (post.Id ? 'id="SL-' + post.Id + '"' : '') + ' class="SL-FACEBOOK sl-untracked">' + createUntrackedFB(post) + '</div>');
	}

/***/ }),
/* 173 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.checkIfHtmlIsFacebook = checkIfHtmlIsFacebook;
	exports.isHtmlWithSocial = isHtmlWithSocial;
	exports.getHtml = getHtml;
	exports.formatDate = formatDate;
	function checkIfHtmlIsFacebook(post) {
	  return post.Content.substring(0, 19) === '<div class="fb-post' || post.Content.substring(0, 20) === '<div class="fb-video';
	}

	function isHtmlWithSocial(post) {
	  if (post.PostMeta && post.PostMeta.Data && Array.isArray(post.PostMeta.Data)) {
	    for (var i in post.PostMeta.Data) {
	      if (post.PostMeta.Data[i].type) {
	        var socialType = post.PostMeta.Data[i].type;
	        if (socialType == 'TWEET' || socialType == 'FACEBOOK' || socialType == 'INSTAGRAM') {
	          return true;
	        }
	      }
	    }
	  }
	  return false;
	}

	function getHtml(str) {
	  var div = document.createElement('div');
	  div.innerHTML = str;
	  return div.firstChild;
	}

	function formatDate(dateVal) {
	  var date = dateVal;
	  if (typeof date.getMonth !== 'function') {
	    // assume string provided
	    try {
	      date = new Date(dateVal);
	      if (date.getFullYear() < 2000 && dateVal.toString().length === 10 && typeof parseInt(dateVal, 10) === 'number') {
	        date = new Date(dateVal * 1000);
	      }
	      if (isNaN(date.getMonth())) {
	        date = new Date(dateVal.replace(/\+/g, '.'));
	      }
	      // For IE
	      if (isNaN(date.getMonth())) {
	        var dateArr = dateVal.split(' ');
	        date = new Date(Date.parse(dateArr[1] + ' ' + dateArr[2] + ', ' + dateArr[5] + ' ' + dateArr[3] + ' UTC'));
	      }
	    } catch (e) {
	      return '';
	    }
	  }

	  var lang = window.navigator.userLanguage || window.navigator.language;

	  var hour = date.getHours();
	  var displayHours = hour === 0 ? 12 : hour;
	  var mins = date.getMinutes();
	  var displayMins = mins.toString().length > 1 ? mins : '0' + mins;
	  var displayTime = displayHours > 12 ? displayHours - 12 + ':' + displayMins + ' PM' : displayHours + ':' + displayMins + ' AM';
	  var day = date.getDate();
	  var month = date.toLocaleString(lang, { month: 'long' }).substr(0, 3);
	  var year = date.getFullYear();

	  return displayTime + ' - ' + day + ' ' + month + ' ' + year;
	}

/***/ }),
/* 174 */
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = prepareHtmlString;
	function removeWhiteSpace(str) {
	  return str.replace(/>\s\s+</g, '><');
	}

	function addTargetBlankToAnchorTags(htmlString) {
	  return htmlString.replace(/<a /g, '<a target="_blank" ');
	}

	function prepareHtmlString(htmlString) {
	  return removeWhiteSpace(addTargetBlankToAnchorTags(htmlString));
	}

/***/ }),
/* 175 */
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var clickPlay = exports.clickPlay = "var postElement = event.target.parentElement.parentElement;\n  var videoElement = postElement.querySelector('video');\n  var videoContainer = postElement.querySelector('.sl-untracked-social-video');\n  var playElement = postElement.querySelector('.sl-untracked-social-video-play');\n  var pauseElement = postElement.querySelector('.sl-untracked-social-video-pause');\n\n  videoElement.play();\n  playElement.style.display = 'none';\n  videoContainer.onmouseover = function () {\n    return pauseElement.style.display = 'block';\n  };\n  videoContainer.onmouseleave = function () {\n    return pauseElement.style.display = 'none';\n  };\n\n  videoElement.addEventListener('ended', function () {\n    videoContainer.onmouseover = '';\n    videoContainer.onmouseleave = '';\n    pauseElement.style.display = 'none';\n    playElement.style.display = 'block';\n  });";

	var clickPause = exports.clickPause = "var postElement = event.target.parentElement.parentElement;\n  var videoElement = postElement.querySelector('video');\n  var videoContainer = postElement.querySelector('.sl-untracked-social-video');\n  var playElement = postElement.querySelector('.sl-untracked-social-video-play');\n  var pauseElement = postElement.querySelector('.sl-untracked-social-video-pause');\n\n  pauseElement.onclick = function () {\n    videoContainer.onmouseover = '';\n    videoContainer.onmouseleave = '';\n    videoElement.pause();\n    pauseElement.style.display = 'none';\n    playElement.style.display = 'block';\n  };";

	var igVideoError = exports.igVideoError = "this.poster='https://bucket-files.scribblelive.com/DefaultThumbnails/FileNotFound.png';\n  var parentElement = this.parentElement;\n  var playElement = parentElement.querySelector('.sl-untracked-social-video-play');\n  var pauseElement = parentElement.querySelector('.sl-untracked-social-video-pause');\n  playElement.style.display='none'\n  pauseElement.style.display='none'";

	var fbVideoError = exports.fbVideoError = "this.src='https://bucket-files.scribblelive.com/DefaultThumbnails/FileNotFound.png';\n  var parentElement = this.parentElement;\n  var playElement = parentElement.querySelector('.sl-untracked-facebook-play');\n  playElement.style.display='none'";

	var twitterVideoError = exports.twitterVideoError = "this.poster='https://bucket-files.scribblelive.com/DefaultThumbnails/FileNotFound.png';\n  var parentElement = this.parentElement;\n  var playElement = parentElement.querySelector('.sl-untracked-social-video-play');\n  var pauseElement = parentElement.querySelector('.sl-untracked-social-video-pause');\n  playElement.style.display='none'\n  pauseElement.style.display='none'";

/***/ }),
/* 176 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function() {
	  if (typeof twttr === "undefined" || twttr === null) {
	    var twttr = {};
	  }

	  twttr.txt = {};
	  twttr.txt.regexen = {};

	  var HTML_ENTITIES = {
	    '&': '&amp;',
	    '>': '&gt;',
	    '<': '&lt;',
	    '"': '&quot;',
	    "'": '&#39;'
	  };

	  // HTML escaping
	  twttr.txt.htmlEscape = function(text) {
	    return text && text.replace(/[&"'><]/g, function(character) {
	      return HTML_ENTITIES[character];
	    });
	  };

	  // Builds a RegExp
	  function regexSupplant(regex, flags) {
	    flags = flags || "";
	    if (typeof regex !== "string") {
	      if (regex.global && flags.indexOf("g") < 0) {
	        flags += "g";
	      }
	      if (regex.ignoreCase && flags.indexOf("i") < 0) {
	        flags += "i";
	      }
	      if (regex.multiline && flags.indexOf("m") < 0) {
	        flags += "m";
	      }

	      regex = regex.source;
	    }

	    return new RegExp(regex.replace(/#\{(\w+)\}/g, function(match, name) {
	      var newRegex = twttr.txt.regexen[name] || "";
	      if (typeof newRegex !== "string") {
	        newRegex = newRegex.source;
	      }
	      return newRegex;
	    }), flags);
	  }

	  twttr.txt.regexSupplant = regexSupplant;

	  // simple string interpolation
	  function stringSupplant(str, values) {
	    return str.replace(/#\{(\w+)\}/g, function(match, name) {
	      return values[name] || "";
	    });
	  }

	  twttr.txt.stringSupplant = stringSupplant;

	  twttr.txt.regexen.spaces_group = /\x09-\x0D\x20\x85\xA0\u1680\u180E\u2000-\u200A\u2028\u2029\u202F\u205F\u3000/;
	  twttr.txt.regexen.spaces = regexSupplant(/[#{spaces_group}]/);
	  twttr.txt.regexen.invalid_chars_group = /\uFFFE\uFEFF\uFFFF\u202A-\u202E/;
	  twttr.txt.regexen.invalid_chars = regexSupplant(/[#{invalid_chars_group}]/);
	  twttr.txt.regexen.punct = /\!'#%&'\(\)*\+,\\\-\.\/:;<=>\?@\[\]\^_{|}~\$/;
	  twttr.txt.regexen.rtl_chars = /[\u0600-\u06FF]|[\u0750-\u077F]|[\u0590-\u05FF]|[\uFE70-\uFEFF]/mg;
	  twttr.txt.regexen.non_bmp_code_pairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/mg;

	  twttr.txt.regexen.latinAccentChars = /\xC0-\xD6\xD8-\xF6\xF8-\xFF\u0100-\u024F\u0253\u0254\u0256\u0257\u0259\u025B\u0263\u0268\u026F\u0272\u0289\u028B\u02BB\u0300-\u036F\u1E00-\u1EFF/;

	  // Generated from unicode_regex/unicode_regex_groups.scala, same as objective c's \p{L}\p{M}
	  twttr.txt.regexen.bmpLetterAndMarks = /A-Za-z\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0300-\u0374\u0376\u0377\u037a-\u037d\u037f\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u0483-\u052f\u0531-\u0556\u0559\u0561-\u0587\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u05d0-\u05ea\u05f0-\u05f2\u0610-\u061a\u0620-\u065f\u066e-\u06d3\u06d5-\u06dc\u06df-\u06e8\u06ea-\u06ef\u06fa-\u06fc\u06ff\u0710-\u074a\u074d-\u07b1\u07ca-\u07f5\u07fa\u0800-\u082d\u0840-\u085b\u08a0-\u08b2\u08e4-\u0963\u0971-\u0983\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bc-\u09c4\u09c7\u09c8\u09cb-\u09ce\u09d7\u09dc\u09dd\u09df-\u09e3\u09f0\u09f1\u0a01-\u0a03\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a3c\u0a3e-\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a59-\u0a5c\u0a5e\u0a70-\u0a75\u0a81-\u0a83\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abc-\u0ac5\u0ac7-\u0ac9\u0acb-\u0acd\u0ad0\u0ae0-\u0ae3\u0b01-\u0b03\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3c-\u0b44\u0b47\u0b48\u0b4b-\u0b4d\u0b56\u0b57\u0b5c\u0b5d\u0b5f-\u0b63\u0b71\u0b82\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bbe-\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcd\u0bd0\u0bd7\u0c00-\u0c03\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c39\u0c3d-\u0c44\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c58\u0c59\u0c60-\u0c63\u0c81-\u0c83\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbc-\u0cc4\u0cc6-\u0cc8\u0cca-\u0ccd\u0cd5\u0cd6\u0cde\u0ce0-\u0ce3\u0cf1\u0cf2\u0d01-\u0d03\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d-\u0d44\u0d46-\u0d48\u0d4a-\u0d4e\u0d57\u0d60-\u0d63\u0d7a-\u0d7f\u0d82\u0d83\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0dca\u0dcf-\u0dd4\u0dd6\u0dd8-\u0ddf\u0df2\u0df3\u0e01-\u0e3a\u0e40-\u0e4e\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb9\u0ebb-\u0ebd\u0ec0-\u0ec4\u0ec6\u0ec8-\u0ecd\u0edc-\u0edf\u0f00\u0f18\u0f19\u0f35\u0f37\u0f39\u0f3e-\u0f47\u0f49-\u0f6c\u0f71-\u0f84\u0f86-\u0f97\u0f99-\u0fbc\u0fc6\u1000-\u103f\u1050-\u108f\u109a-\u109d\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u135d-\u135f\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16f1-\u16f8\u1700-\u170c\u170e-\u1714\u1720-\u1734\u1740-\u1753\u1760-\u176c\u176e-\u1770\u1772\u1773\u1780-\u17d3\u17d7\u17dc\u17dd\u180b-\u180d\u1820-\u1877\u1880-\u18aa\u18b0-\u18f5\u1900-\u191e\u1920-\u192b\u1930-\u193b\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19b0-\u19c9\u1a00-\u1a1b\u1a20-\u1a5e\u1a60-\u1a7c\u1a7f\u1aa7\u1ab0-\u1abe\u1b00-\u1b4b\u1b6b-\u1b73\u1b80-\u1baf\u1bba-\u1bf3\u1c00-\u1c37\u1c4d-\u1c4f\u1c5a-\u1c7d\u1cd0-\u1cd2\u1cd4-\u1cf6\u1cf8\u1cf9\u1d00-\u1df5\u1dfc-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u20d0-\u20f0\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2183\u2184\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d7f-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2de0-\u2dff\u2e2f\u3005\u3006\u302a-\u302f\u3031-\u3035\u303b\u303c\u3041-\u3096\u3099\u309a\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua672\ua674-\ua67d\ua67f-\ua69d\ua69f-\ua6e5\ua6f0\ua6f1\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua7ad\ua7b0\ua7b1\ua7f7-\ua827\ua840-\ua873\ua880-\ua8c4\ua8e0-\ua8f7\ua8fb\ua90a-\ua92d\ua930-\ua953\ua960-\ua97c\ua980-\ua9c0\ua9cf\ua9e0-\ua9ef\ua9fa-\ua9fe\uaa00-\uaa36\uaa40-\uaa4d\uaa60-\uaa76\uaa7a-\uaac2\uaadb-\uaadd\uaae0-\uaaef\uaaf2-\uaaf6\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uab30-\uab5a\uab5c-\uab5f\uab64\uab65\uabc0-\uabea\uabec\uabed\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf870-\uf87f\uf882\uf884-\uf89f\uf8b8\uf8c1-\uf8d6\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe00-\ufe0f\ufe20-\ufe2d\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc/;
	  twttr.txt.regexen.astralLetterAndMarks = /\ud800[\udc00-\udc0b\udc0d-\udc26\udc28-\udc3a\udc3c\udc3d\udc3f-\udc4d\udc50-\udc5d\udc80-\udcfa\uddfd\ude80-\ude9c\udea0-\uded0\udee0\udf00-\udf1f\udf30-\udf40\udf42-\udf49\udf50-\udf7a\udf80-\udf9d\udfa0-\udfc3\udfc8-\udfcf]|\ud801[\udc00-\udc9d\udd00-\udd27\udd30-\udd63\ude00-\udf36\udf40-\udf55\udf60-\udf67]|\ud802[\udc00-\udc05\udc08\udc0a-\udc35\udc37\udc38\udc3c\udc3f-\udc55\udc60-\udc76\udc80-\udc9e\udd00-\udd15\udd20-\udd39\udd80-\uddb7\uddbe\uddbf\ude00-\ude03\ude05\ude06\ude0c-\ude13\ude15-\ude17\ude19-\ude33\ude38-\ude3a\ude3f\ude60-\ude7c\ude80-\ude9c\udec0-\udec7\udec9-\udee6\udf00-\udf35\udf40-\udf55\udf60-\udf72\udf80-\udf91]|\ud803[\udc00-\udc48]|\ud804[\udc00-\udc46\udc7f-\udcba\udcd0-\udce8\udd00-\udd34\udd50-\udd73\udd76\udd80-\uddc4\uddda\ude00-\ude11\ude13-\ude37\udeb0-\udeea\udf01-\udf03\udf05-\udf0c\udf0f\udf10\udf13-\udf28\udf2a-\udf30\udf32\udf33\udf35-\udf39\udf3c-\udf44\udf47\udf48\udf4b-\udf4d\udf57\udf5d-\udf63\udf66-\udf6c\udf70-\udf74]|\ud805[\udc80-\udcc5\udcc7\udd80-\uddb5\uddb8-\uddc0\ude00-\ude40\ude44\ude80-\udeb7]|\ud806[\udca0-\udcdf\udcff\udec0-\udef8]|\ud808[\udc00-\udf98]|\ud80c[\udc00-\udfff]|\ud80d[\udc00-\udc2e]|\ud81a[\udc00-\ude38\ude40-\ude5e\uded0-\udeed\udef0-\udef4\udf00-\udf36\udf40-\udf43\udf63-\udf77\udf7d-\udf8f]|\ud81b[\udf00-\udf44\udf50-\udf7e\udf8f-\udf9f]|\ud82c[\udc00\udc01]|\ud82f[\udc00-\udc6a\udc70-\udc7c\udc80-\udc88\udc90-\udc99\udc9d\udc9e]|\ud834[\udd65-\udd69\udd6d-\udd72\udd7b-\udd82\udd85-\udd8b\uddaa-\uddad\ude42-\ude44]|\ud835[\udc00-\udc54\udc56-\udc9c\udc9e\udc9f\udca2\udca5\udca6\udca9-\udcac\udcae-\udcb9\udcbb\udcbd-\udcc3\udcc5-\udd05\udd07-\udd0a\udd0d-\udd14\udd16-\udd1c\udd1e-\udd39\udd3b-\udd3e\udd40-\udd44\udd46\udd4a-\udd50\udd52-\udea5\udea8-\udec0\udec2-\udeda\udedc-\udefa\udefc-\udf14\udf16-\udf34\udf36-\udf4e\udf50-\udf6e\udf70-\udf88\udf8a-\udfa8\udfaa-\udfc2\udfc4-\udfcb]|\ud83a[\udc00-\udcc4\udcd0-\udcd6]|\ud83b[\ude00-\ude03\ude05-\ude1f\ude21\ude22\ude24\ude27\ude29-\ude32\ude34-\ude37\ude39\ude3b\ude42\ude47\ude49\ude4b\ude4d-\ude4f\ude51\ude52\ude54\ude57\ude59\ude5b\ude5d\ude5f\ude61\ude62\ude64\ude67-\ude6a\ude6c-\ude72\ude74-\ude77\ude79-\ude7c\ude7e\ude80-\ude89\ude8b-\ude9b\udea1-\udea3\udea5-\udea9\udeab-\udebb]|\ud840[\udc00-\udfff]|\ud841[\udc00-\udfff]|\ud842[\udc00-\udfff]|\ud843[\udc00-\udfff]|\ud844[\udc00-\udfff]|\ud845[\udc00-\udfff]|\ud846[\udc00-\udfff]|\ud847[\udc00-\udfff]|\ud848[\udc00-\udfff]|\ud849[\udc00-\udfff]|\ud84a[\udc00-\udfff]|\ud84b[\udc00-\udfff]|\ud84c[\udc00-\udfff]|\ud84d[\udc00-\udfff]|\ud84e[\udc00-\udfff]|\ud84f[\udc00-\udfff]|\ud850[\udc00-\udfff]|\ud851[\udc00-\udfff]|\ud852[\udc00-\udfff]|\ud853[\udc00-\udfff]|\ud854[\udc00-\udfff]|\ud855[\udc00-\udfff]|\ud856[\udc00-\udfff]|\ud857[\udc00-\udfff]|\ud858[\udc00-\udfff]|\ud859[\udc00-\udfff]|\ud85a[\udc00-\udfff]|\ud85b[\udc00-\udfff]|\ud85c[\udc00-\udfff]|\ud85d[\udc00-\udfff]|\ud85e[\udc00-\udfff]|\ud85f[\udc00-\udfff]|\ud860[\udc00-\udfff]|\ud861[\udc00-\udfff]|\ud862[\udc00-\udfff]|\ud863[\udc00-\udfff]|\ud864[\udc00-\udfff]|\ud865[\udc00-\udfff]|\ud866[\udc00-\udfff]|\ud867[\udc00-\udfff]|\ud868[\udc00-\udfff]|\ud869[\udc00-\uded6\udf00-\udfff]|\ud86a[\udc00-\udfff]|\ud86b[\udc00-\udfff]|\ud86c[\udc00-\udfff]|\ud86d[\udc00-\udf34\udf40-\udfff]|\ud86e[\udc00-\udc1d]|\ud87e[\udc00-\ude1d]|\udb40[\udd00-\uddef]/;

	  // Generated from unicode_regex/unicode_regex_groups.scala, same as objective c's \p{Nd}
	  twttr.txt.regexen.bmpNumerals = /0-9\u0660-\u0669\u06f0-\u06f9\u07c0-\u07c9\u0966-\u096f\u09e6-\u09ef\u0a66-\u0a6f\u0ae6-\u0aef\u0b66-\u0b6f\u0be6-\u0bef\u0c66-\u0c6f\u0ce6-\u0cef\u0d66-\u0d6f\u0de6-\u0def\u0e50-\u0e59\u0ed0-\u0ed9\u0f20-\u0f29\u1040-\u1049\u1090-\u1099\u17e0-\u17e9\u1810-\u1819\u1946-\u194f\u19d0-\u19d9\u1a80-\u1a89\u1a90-\u1a99\u1b50-\u1b59\u1bb0-\u1bb9\u1c40-\u1c49\u1c50-\u1c59\ua620-\ua629\ua8d0-\ua8d9\ua900-\ua909\ua9d0-\ua9d9\ua9f0-\ua9f9\uaa50-\uaa59\uabf0-\uabf9\uff10-\uff19/;
	  twttr.txt.regexen.astralNumerals = /\ud801[\udca0-\udca9]|\ud804[\udc66-\udc6f\udcf0-\udcf9\udd36-\udd3f\uddd0-\uddd9\udef0-\udef9]|\ud805[\udcd0-\udcd9\ude50-\ude59\udec0-\udec9]|\ud806[\udce0-\udce9]|\ud81a[\ude60-\ude69\udf50-\udf59]|\ud835[\udfce-\udfff]/;

	  twttr.txt.regexen.hashtagSpecialChars = /_\u200c\u200d\ua67e\u05be\u05f3\u05f4\uff5e\u301c\u309b\u309c\u30a0\u30fb\u3003\u0f0b\u0f0c\xb7/;

	  // A hashtag must contain at least one unicode letter or mark, as well as numbers, underscores, and select special characters.
	  twttr.txt.regexen.hashSigns = /[#＃]/;
	  twttr.txt.regexen.hashtagAlpha = regexSupplant(/(?:[#{bmpLetterAndMarks}]|(?=#{non_bmp_code_pairs})(?:#{astralLetterAndMarks}))/);
	  twttr.txt.regexen.hashtagAlphaNumeric = regexSupplant(/(?:[#{bmpLetterAndMarks}#{bmpNumerals}#{hashtagSpecialChars}]|(?=#{non_bmp_code_pairs})(?:#{astralLetterAndMarks}|#{astralNumerals}))/);
	  twttr.txt.regexen.endHashtagMatch = regexSupplant(/^(?:#{hashSigns}|:\/\/)/);
	  twttr.txt.regexen.codePoint = /(?:[^\uD800-\uDFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF])/;
	  twttr.txt.regexen.hashtagBoundary = regexSupplant(/(?:^|\uFE0E|\uFE0F|$|(?!#{hashtagAlphaNumeric}|&)#{codePoint})/);
	  twttr.txt.regexen.validHashtag = regexSupplant(/(#{hashtagBoundary})(#{hashSigns})(?!\uFE0F|\u20E3)(#{hashtagAlphaNumeric}*#{hashtagAlpha}#{hashtagAlphaNumeric}*)/gi);

	  // Mention related regex collection
	  twttr.txt.regexen.validMentionPrecedingChars = /(?:^|[^a-zA-Z0-9_!#$%&*@＠]|(?:^|[^a-zA-Z0-9_+~.-])(?:rt|RT|rT|Rt):?)/;
	  twttr.txt.regexen.atSigns = /[@＠]/;
	  twttr.txt.regexen.validMentionOrList = regexSupplant(
	    '(#{validMentionPrecedingChars})' +  // $1: Preceding character
	    '(#{atSigns})' +                     // $2: At mark
	    '([a-zA-Z0-9_]{1,20})' +             // $3: Screen name
	    '(\/[a-zA-Z][a-zA-Z0-9_\-]{0,24})?'  // $4: List (optional)
	  , 'g');
	  twttr.txt.regexen.validReply = regexSupplant(/^(?:#{spaces})*#{atSigns}([a-zA-Z0-9_]{1,20})/);
	  twttr.txt.regexen.endMentionMatch = regexSupplant(/^(?:#{atSigns}|[#{latinAccentChars}]|:\/\/)/);

	  // URL related regex collection
	  twttr.txt.regexen.validUrlPrecedingChars = regexSupplant(/(?:[^A-Za-z0-9@＠$#＃#{invalid_chars_group}]|^)/);
	  twttr.txt.regexen.invalidUrlWithoutProtocolPrecedingChars = /[-_.\/]$/;
	  twttr.txt.regexen.invalidDomainChars = stringSupplant("#{punct}#{spaces_group}#{invalid_chars_group}", twttr.txt.regexen);
	  twttr.txt.regexen.validDomainChars = regexSupplant(/[^#{invalidDomainChars}]/);
	  twttr.txt.regexen.validSubdomain = regexSupplant(/(?:(?:#{validDomainChars}(?:[_-]|#{validDomainChars})*)?#{validDomainChars}\.)/);
	  twttr.txt.regexen.validDomainName = regexSupplant(/(?:(?:#{validDomainChars}(?:-|#{validDomainChars})*)?#{validDomainChars}\.)/);
	  twttr.txt.regexen.validGTLD = regexSupplant(RegExp(
	'(?:(?:' +
	    '삼성|닷컴|닷넷|香格里拉|餐厅|食品|飞利浦|電訊盈科|集团|通販|购物|谷歌|诺基亚|联通|网络|网站|网店|网址|组织机构|移动|珠宝|点看|游戏|淡马锡|机构|書籍|时尚|新闻|政府|' +
	    '政务|手表|手机|我爱你|慈善|微博|广东|工行|家電|娱乐|天主教|大拿|大众汽车|在线|嘉里大酒店|嘉里|商标|商店|商城|公益|公司|八卦|健康|信息|佛山|企业|中文网|中信|世界|' +
	    'ポイント|ファッション|セール|ストア|コム|グーグル|クラウド|みんな|คอม|संगठन|नेट|कॉम|همراه|موقع|موبايلي|كوم|كاثوليك|عرب|شبكة|' +
	    'بيتك|بازار|العليان|ارامكو|اتصالات|ابوظبي|קום|сайт|рус|орг|онлайн|москва|ком|католик|дети|' +
	    'zuerich|zone|zippo|zip|zero|zara|zappos|yun|youtube|you|yokohama|yoga|yodobashi|yandex|yamaxun|' +
	    'yahoo|yachts|xyz|xxx|xperia|xin|xihuan|xfinity|xerox|xbox|wtf|wtc|wow|world|works|work|woodside|' +
	    'wolterskluwer|wme|winners|wine|windows|win|williamhill|wiki|wien|whoswho|weir|weibo|wedding|wed|' +
	    'website|weber|webcam|weatherchannel|weather|watches|watch|warman|wanggou|wang|walter|walmart|' +
	    'wales|vuelos|voyage|voto|voting|vote|volvo|volkswagen|vodka|vlaanderen|vivo|viva|vistaprint|' +
	    'vista|vision|visa|virgin|vip|vin|villas|viking|vig|video|viajes|vet|versicherung|' +
	    'vermögensberatung|vermögensberater|verisign|ventures|vegas|vanguard|vana|vacations|ups|uol|uno|' +
	    'university|unicom|uconnect|ubs|ubank|tvs|tushu|tunes|tui|tube|trv|trust|travelersinsurance|' +
	    'travelers|travelchannel|travel|training|trading|trade|toys|toyota|town|tours|total|toshiba|' +
	    'toray|top|tools|tokyo|today|tmall|tkmaxx|tjx|tjmaxx|tirol|tires|tips|tiffany|tienda|tickets|' +
	    'tiaa|theatre|theater|thd|teva|tennis|temasek|telefonica|telecity|tel|technology|tech|team|tdk|' +
	    'tci|taxi|tax|tattoo|tatar|tatamotors|target|taobao|talk|taipei|tab|systems|symantec|sydney|' +
	    'swiss|swiftcover|swatch|suzuki|surgery|surf|support|supply|supplies|sucks|style|study|studio|' +
	    'stream|store|storage|stockholm|stcgroup|stc|statoil|statefarm|statebank|starhub|star|staples|' +
	    'stada|srt|srl|spreadbetting|spot|spiegel|space|soy|sony|song|solutions|solar|sohu|software|' +
	    'softbank|social|soccer|sncf|smile|smart|sling|skype|sky|skin|ski|site|singles|sina|silk|shriram|' +
	    'showtime|show|shouji|shopping|shop|shoes|shiksha|shia|shell|shaw|sharp|shangrila|sfr|sexy|sex|' +
	    'sew|seven|ses|services|sener|select|seek|security|secure|seat|search|scot|scor|scjohnson|' +
	    'science|schwarz|schule|school|scholarships|schmidt|schaeffler|scb|sca|sbs|sbi|saxo|save|sas|' +
	    'sarl|sapo|sap|sanofi|sandvikcoromant|sandvik|samsung|samsclub|salon|sale|sakura|safety|safe|' +
	    'saarland|ryukyu|rwe|run|ruhr|rugby|rsvp|room|rogers|rodeo|rocks|rocher|rmit|rip|rio|ril|' +
	    'rightathome|ricoh|richardli|rich|rexroth|reviews|review|restaurant|rest|republican|report|' +
	    'repair|rentals|rent|ren|reliance|reit|reisen|reise|rehab|redumbrella|redstone|red|recipes|' +
	    'realty|realtor|realestate|read|raid|radio|racing|qvc|quest|quebec|qpon|pwc|pub|prudential|pru|' +
	    'protection|property|properties|promo|progressive|prof|productions|prod|pro|prime|press|praxi|' +
	    'pramerica|post|porn|politie|poker|pohl|pnc|plus|plumbing|playstation|play|place|pizza|pioneer|' +
	    'pink|ping|pin|pid|pictures|pictet|pics|piaget|physio|photos|photography|photo|phone|philips|phd|' +
	    'pharmacy|pfizer|pet|pccw|pay|passagens|party|parts|partners|pars|paris|panerai|panasonic|' +
	    'pamperedchef|page|ovh|ott|otsuka|osaka|origins|orientexpress|organic|org|orange|oracle|open|ooo|' +
	    'onyourside|online|onl|ong|one|omega|ollo|oldnavy|olayangroup|olayan|okinawa|office|off|observer|' +
	    'obi|nyc|ntt|nrw|nra|nowtv|nowruz|now|norton|northwesternmutual|nokia|nissay|nissan|ninja|nikon|' +
	    'nike|nico|nhk|ngo|nfl|nexus|nextdirect|next|news|newholland|new|neustar|network|netflix|netbank|' +
	    'net|nec|nba|navy|natura|nationwide|name|nagoya|nadex|nab|mutuelle|mutual|museum|mtr|mtpc|mtn|' +
	    'msd|movistar|movie|mov|motorcycles|moto|moscow|mortgage|mormon|mopar|montblanc|monster|money|' +
	    'monash|mom|moi|moe|moda|mobily|mobile|mobi|mma|mls|mlb|mitsubishi|mit|mint|mini|mil|microsoft|' +
	    'miami|metlife|merckmsd|meo|menu|men|memorial|meme|melbourne|meet|media|med|mckinsey|mcdonalds|' +
	    'mcd|mba|mattel|maserati|marshalls|marriott|markets|marketing|market|map|mango|management|man|' +
	    'makeup|maison|maif|madrid|macys|luxury|luxe|lupin|lundbeck|ltda|ltd|lplfinancial|lpl|love|lotto|' +
	    'lotte|london|lol|loft|locus|locker|loans|loan|lixil|living|live|lipsy|link|linde|lincoln|limo|' +
	    'limited|lilly|like|lighting|lifestyle|lifeinsurance|life|lidl|liaison|lgbt|lexus|lego|legal|' +
	    'lefrak|leclerc|lease|lds|lawyer|law|latrobe|latino|lat|lasalle|lanxess|landrover|land|lancome|' +
	    'lancia|lancaster|lamer|lamborghini|ladbrokes|lacaixa|kyoto|kuokgroup|kred|krd|kpn|kpmg|kosher|' +
	    'komatsu|koeln|kiwi|kitchen|kindle|kinder|kim|kia|kfh|kerryproperties|kerrylogistics|kerryhotels|' +
	    'kddi|kaufen|juniper|juegos|jprs|jpmorgan|joy|jot|joburg|jobs|jnj|jmp|jll|jlc|jio|jewelry|jetzt|' +
	    'jeep|jcp|jcb|java|jaguar|iwc|iveco|itv|itau|istanbul|ist|ismaili|iselect|irish|ipiranga|' +
	    'investments|intuit|international|intel|int|insure|insurance|institute|ink|ing|info|infiniti|' +
	    'industries|immobilien|immo|imdb|imamat|ikano|iinet|ifm|ieee|icu|ice|icbc|ibm|hyundai|hyatt|' +
	    'hughes|htc|hsbc|how|house|hotmail|hotels|hoteles|hot|hosting|host|hospital|horse|honeywell|' +
	    'honda|homesense|homes|homegoods|homedepot|holiday|holdings|hockey|hkt|hiv|hitachi|hisamitsu|' +
	    'hiphop|hgtv|hermes|here|helsinki|help|healthcare|health|hdfcbank|hdfc|hbo|haus|hangout|hamburg|' +
	    'hair|guru|guitars|guide|guge|gucci|guardian|group|grocery|gripe|green|gratis|graphics|grainger|' +
	    'gov|got|gop|google|goog|goodyear|goodhands|goo|golf|goldpoint|gold|godaddy|gmx|gmo|gmbh|gmail|' +
	    'globo|global|gle|glass|glade|giving|gives|gifts|gift|ggee|george|genting|gent|gea|gdn|gbiz|' +
	    'garden|gap|games|game|gallup|gallo|gallery|gal|fyi|futbol|furniture|fund|fun|fujixerox|fujitsu|' +
	    'ftr|frontier|frontdoor|frogans|frl|fresenius|free|fox|foundation|forum|forsale|forex|ford|' +
	    'football|foodnetwork|food|foo|fly|flsmidth|flowers|florist|flir|flights|flickr|fitness|fit|' +
	    'fishing|fish|firmdale|firestone|fire|financial|finance|final|film|fido|fidelity|fiat|ferrero|' +
	    'ferrari|feedback|fedex|fast|fashion|farmers|farm|fans|fan|family|faith|fairwinds|fail|fage|' +
	    'extraspace|express|exposed|expert|exchange|everbank|events|eus|eurovision|etisalat|esurance|' +
	    'estate|esq|erni|ericsson|equipment|epson|epost|enterprises|engineering|engineer|energy|emerck|' +
	    'email|education|edu|edeka|eco|eat|earth|dvr|dvag|durban|dupont|duns|dunlop|duck|dubai|dtv|drive|' +
	    'download|dot|doosan|domains|doha|dog|dodge|doctor|docs|dnp|diy|dish|discover|discount|directory|' +
	    'direct|digital|diet|diamonds|dhl|dev|design|desi|dentist|dental|democrat|delta|deloitte|dell|' +
	    'delivery|degree|deals|dealer|deal|dds|dclk|day|datsun|dating|date|data|dance|dad|dabur|cyou|' +
	    'cymru|cuisinella|csc|cruises|cruise|crs|crown|cricket|creditunion|creditcard|credit|courses|' +
	    'coupons|coupon|country|corsica|coop|cool|cookingchannel|cooking|contractors|contact|consulting|' +
	    'construction|condos|comsec|computer|compare|company|community|commbank|comcast|com|cologne|' +
	    'college|coffee|codes|coach|clubmed|club|cloud|clothing|clinique|clinic|click|cleaning|claims|' +
	    'cityeats|city|citic|citi|citadel|cisco|circle|cipriani|church|chrysler|chrome|christmas|chloe|' +
	    'chintai|cheap|chat|chase|channel|chanel|cfd|cfa|cern|ceo|center|ceb|cbs|cbre|cbn|cba|catholic|' +
	    'catering|cat|casino|cash|caseih|case|casa|cartier|cars|careers|career|care|cards|caravan|car|' +
	    'capitalone|capital|capetown|canon|cancerresearch|camp|camera|cam|calvinklein|call|cal|cafe|cab|' +
	    'bzh|buzz|buy|business|builders|build|bugatti|budapest|brussels|brother|broker|broadway|' +
	    'bridgestone|bradesco|box|boutique|bot|boston|bostik|bosch|boots|booking|book|boo|bond|bom|bofa|' +
	    'boehringer|boats|bnpparibas|bnl|bmw|bms|blue|bloomberg|blog|blockbuster|blanco|blackfriday|' +
	    'black|biz|bio|bingo|bing|bike|bid|bible|bharti|bet|bestbuy|best|berlin|bentley|beer|beauty|' +
	    'beats|bcn|bcg|bbva|bbt|bbc|bayern|bauhaus|basketball|baseball|bargains|barefoot|barclays|' +
	    'barclaycard|barcelona|bar|bank|band|bananarepublic|banamex|baidu|baby|azure|axa|aws|avianca|' +
	    'autos|auto|author|auspost|audio|audible|audi|auction|attorney|athleta|associates|asia|asda|arte|' +
	    'art|arpa|army|archi|aramco|arab|aquarelle|apple|app|apartments|aol|anz|anquan|android|analytics|' +
	    'amsterdam|amica|amfam|amex|americanfamily|americanexpress|alstom|alsace|ally|allstate|allfinanz|' +
	    'alipay|alibaba|alfaromeo|akdn|airtel|airforce|airbus|aigo|aig|agency|agakhan|africa|afl|' +
	    'afamilycompany|aetna|aero|aeg|adult|ads|adac|actor|active|aco|accountants|accountant|accenture|' +
	    'academy|abudhabi|abogado|able|abc|abbvie|abbott|abb|abarth|aarp|aaa|onion' +
	')(?=[^0-9a-zA-Z@]|$))'));
	  twttr.txt.regexen.validCCTLD = regexSupplant(RegExp(
	'(?:(?:' +
	    '한국|香港|澳門|新加坡|台灣|台湾|中國|中国|გე|ไทย|ලංකා|ഭാരതം|ಭಾರತ|భారత్|சிங்கப்பூர்|இலங்கை|இந்தியா|ଭାରତ|ભારત|ਭਾਰਤ|' +
	    'ভাৰত|ভারত|বাংলা|भारोत|भारतम्|भारत|ڀارت|پاکستان|مليسيا|مصر|قطر|فلسطين|عمان|عراق|سورية|سودان|تونس|' +
	    'بھارت|بارت|ایران|امارات|المغرب|السعودية|الجزائر|الاردن|հայ|қаз|укр|срб|рф|мон|мкд|ею|бел|бг|ελ|' +
	    'zw|zm|za|yt|ye|ws|wf|vu|vn|vi|vg|ve|vc|va|uz|uy|us|um|uk|ug|ua|tz|tw|tv|tt|tr|tp|to|tn|tm|tl|tk|' +
	    'tj|th|tg|tf|td|tc|sz|sy|sx|sv|su|st|ss|sr|so|sn|sm|sl|sk|sj|si|sh|sg|se|sd|sc|sb|sa|rw|ru|rs|ro|' +
	    're|qa|py|pw|pt|ps|pr|pn|pm|pl|pk|ph|pg|pf|pe|pa|om|nz|nu|nr|np|no|nl|ni|ng|nf|ne|nc|na|mz|my|mx|' +
	    'mw|mv|mu|mt|ms|mr|mq|mp|mo|mn|mm|ml|mk|mh|mg|mf|me|md|mc|ma|ly|lv|lu|lt|ls|lr|lk|li|lc|lb|la|kz|' +
	    'ky|kw|kr|kp|kn|km|ki|kh|kg|ke|jp|jo|jm|je|it|is|ir|iq|io|in|im|il|ie|id|hu|ht|hr|hn|hm|hk|gy|gw|' +
	    'gu|gt|gs|gr|gq|gp|gn|gm|gl|gi|gh|gg|gf|ge|gd|gb|ga|fr|fo|fm|fk|fj|fi|eu|et|es|er|eh|eg|ee|ec|dz|' +
	    'do|dm|dk|dj|de|cz|cy|cx|cw|cv|cu|cr|co|cn|cm|cl|ck|ci|ch|cg|cf|cd|cc|ca|bz|by|bw|bv|bt|bs|br|bq|' +
	    'bo|bn|bm|bl|bj|bi|bh|bg|bf|be|bd|bb|ba|az|ax|aw|au|at|as|ar|aq|ao|an|am|al|ai|ag|af|ae|ad|ac' +
	')(?=[^0-9a-zA-Z@]|$))'));
	  twttr.txt.regexen.validPunycode = /(?:xn--[0-9a-z]+)/;
	  twttr.txt.regexen.validSpecialCCTLD = /(?:(?:co|tv)(?=[^0-9a-zA-Z@]|$))/;
	  twttr.txt.regexen.validDomain = regexSupplant(/(?:#{validSubdomain}*#{validDomainName}(?:#{validGTLD}|#{validCCTLD}|#{validPunycode}))/);
	  twttr.txt.regexen.validAsciiDomain = regexSupplant(/(?:(?:[\-a-z0-9#{latinAccentChars}]+)\.)+(?:#{validGTLD}|#{validCCTLD}|#{validPunycode})/gi);
	  twttr.txt.regexen.invalidShortDomain = regexSupplant(/^#{validDomainName}#{validCCTLD}$/i);
	  twttr.txt.regexen.validSpecialShortDomain = regexSupplant(/^#{validDomainName}#{validSpecialCCTLD}$/i);
	  twttr.txt.regexen.validPortNumber = /[0-9]+/;
	  twttr.txt.regexen.cyrillicLettersAndMarks = /\u0400-\u04FF/;
	  twttr.txt.regexen.validGeneralUrlPathChars = regexSupplant(/[a-z#{cyrillicLettersAndMarks}0-9!\*';:=\+,\.\$\/%#\[\]\-_~@\|&#{latinAccentChars}]/i);
	  // Allow URL paths to contain up to two nested levels of balanced parens
	  //  1. Used in Wikipedia URLs like /Primer_(film)
	  //  2. Used in IIS sessions like /S(dfd346)/
	  //  3. Used in Rdio URLs like /track/We_Up_(Album_Version_(Edited))/
	  twttr.txt.regexen.validUrlBalancedParens = regexSupplant(
	    '\\('                                   +
	      '(?:'                                 +
	        '#{validGeneralUrlPathChars}+'      +
	        '|'                                 +
	        // allow one nested level of balanced parentheses
	        '(?:'                               +
	          '#{validGeneralUrlPathChars}*'    +
	          '\\('                             +
	            '#{validGeneralUrlPathChars}+'  +
	          '\\)'                             +
	          '#{validGeneralUrlPathChars}*'    +
	        ')'                                 +
	      ')'                                   +
	    '\\)'
	  , 'i');
	  // Valid end-of-path chracters (so /foo. does not gobble the period).
	  // 1. Allow =&# for empty URL parameters and other URL-join artifacts
	  twttr.txt.regexen.validUrlPathEndingChars = regexSupplant(/[\+\-a-z#{cyrillicLettersAndMarks}0-9=_#\/#{latinAccentChars}]|(?:#{validUrlBalancedParens})/i);
	  // Allow @ in a url, but only in the middle. Catch things like http://example.com/@user/
	  twttr.txt.regexen.validUrlPath = regexSupplant('(?:' +
	    '(?:' +
	      '#{validGeneralUrlPathChars}*' +
	        '(?:#{validUrlBalancedParens}#{validGeneralUrlPathChars}*)*' +
	        '#{validUrlPathEndingChars}'+
	      ')|(?:@#{validGeneralUrlPathChars}+\/)'+
	    ')', 'i');

	  twttr.txt.regexen.validUrlQueryChars = /[a-z0-9!?\*'@\(\);:&=\+\$\/%#\[\]\-_\.,~|]/i;
	  twttr.txt.regexen.validUrlQueryEndingChars = /[a-z0-9_&=#\/]/i;
	  twttr.txt.regexen.extractUrl = regexSupplant(
	    '('                                                            + // $1 total match
	      '(#{validUrlPrecedingChars})'                                + // $2 Preceeding chracter
	      '('                                                          + // $3 URL
	        '(https?:\\/\\/)?'                                         + // $4 Protocol (optional)
	        '(#{validDomain})'                                         + // $5 Domain(s)
	        '(?::(#{validPortNumber}))?'                               + // $6 Port number (optional)
	        '(\\/#{validUrlPath}*)?'                                   + // $7 URL Path
	        '(\\?#{validUrlQueryChars}*#{validUrlQueryEndingChars})?'  + // $8 Query String
	      ')'                                                          +
	    ')'
	  , 'gi');

	  twttr.txt.regexen.validTcoUrl = /^https?:\/\/t\.co\/[a-z0-9]+/i;
	  twttr.txt.regexen.urlHasProtocol = /^https?:\/\//i;
	  twttr.txt.regexen.urlHasHttps = /^https:\/\//i;

	  // cashtag related regex
	  twttr.txt.regexen.cashtag = /[a-z]{1,6}(?:[._][a-z]{1,2})?/i;
	  twttr.txt.regexen.validCashtag = regexSupplant('(^|#{spaces})(\\$)(#{cashtag})(?=$|\\s|[#{punct}])', 'gi');

	  // These URL validation pattern strings are based on the ABNF from RFC 3986
	  twttr.txt.regexen.validateUrlUnreserved = /[a-z\u0400-\u04FF0-9\-._~]/i;
	  twttr.txt.regexen.validateUrlPctEncoded = /(?:%[0-9a-f]{2})/i;
	  twttr.txt.regexen.validateUrlSubDelims = /[!$&'()*+,;=]/i;
	  twttr.txt.regexen.validateUrlPchar = regexSupplant('(?:' +
	    '#{validateUrlUnreserved}|' +
	    '#{validateUrlPctEncoded}|' +
	    '#{validateUrlSubDelims}|' +
	    '[:|@]' +
	  ')', 'i');

	  twttr.txt.regexen.validateUrlScheme = /(?:[a-z][a-z0-9+\-.]*)/i;
	  twttr.txt.regexen.validateUrlUserinfo = regexSupplant('(?:' +
	    '#{validateUrlUnreserved}|' +
	    '#{validateUrlPctEncoded}|' +
	    '#{validateUrlSubDelims}|' +
	    ':' +
	  ')*', 'i');

	  twttr.txt.regexen.validateUrlDecOctet = /(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9]{2})|(?:2[0-4][0-9])|(?:25[0-5]))/i;
	  twttr.txt.regexen.validateUrlIpv4 = regexSupplant(/(?:#{validateUrlDecOctet}(?:\.#{validateUrlDecOctet}){3})/i);

	  // Punting on real IPv6 validation for now
	  twttr.txt.regexen.validateUrlIpv6 = /(?:\[[a-f0-9:\.]+\])/i;

	  // Also punting on IPvFuture for now
	  twttr.txt.regexen.validateUrlIp = regexSupplant('(?:' +
	    '#{validateUrlIpv4}|' +
	    '#{validateUrlIpv6}' +
	  ')', 'i');

	  // This is more strict than the rfc specifies
	  twttr.txt.regexen.validateUrlSubDomainSegment = /(?:[a-z0-9](?:[a-z0-9_\-]*[a-z0-9])?)/i;
	  twttr.txt.regexen.validateUrlDomainSegment = /(?:[a-z0-9](?:[a-z0-9\-]*[a-z0-9])?)/i;
	  twttr.txt.regexen.validateUrlDomainTld = /(?:[a-z](?:[a-z0-9\-]*[a-z0-9])?)/i;
	  twttr.txt.regexen.validateUrlDomain = regexSupplant(/(?:(?:#{validateUrlSubDomainSegment}\.)*(?:#{validateUrlDomainSegment}\.)#{validateUrlDomainTld})/i);

	  twttr.txt.regexen.validateUrlHost = regexSupplant('(?:' +
	    '#{validateUrlIp}|' +
	    '#{validateUrlDomain}' +
	  ')', 'i');

	  // Unencoded internationalized domains - this doesn't check for invalid UTF-8 sequences
	  twttr.txt.regexen.validateUrlUnicodeSubDomainSegment = /(?:(?:[a-z0-9]|[^\u0000-\u007f])(?:(?:[a-z0-9_\-]|[^\u0000-\u007f])*(?:[a-z0-9]|[^\u0000-\u007f]))?)/i;
	  twttr.txt.regexen.validateUrlUnicodeDomainSegment = /(?:(?:[a-z0-9]|[^\u0000-\u007f])(?:(?:[a-z0-9\-]|[^\u0000-\u007f])*(?:[a-z0-9]|[^\u0000-\u007f]))?)/i;
	  twttr.txt.regexen.validateUrlUnicodeDomainTld = /(?:(?:[a-z]|[^\u0000-\u007f])(?:(?:[a-z0-9\-]|[^\u0000-\u007f])*(?:[a-z0-9]|[^\u0000-\u007f]))?)/i;
	  twttr.txt.regexen.validateUrlUnicodeDomain = regexSupplant(/(?:(?:#{validateUrlUnicodeSubDomainSegment}\.)*(?:#{validateUrlUnicodeDomainSegment}\.)#{validateUrlUnicodeDomainTld})/i);

	  twttr.txt.regexen.validateUrlUnicodeHost = regexSupplant('(?:' +
	    '#{validateUrlIp}|' +
	    '#{validateUrlUnicodeDomain}' +
	  ')', 'i');

	  twttr.txt.regexen.validateUrlPort = /[0-9]{1,5}/;

	  twttr.txt.regexen.validateUrlUnicodeAuthority = regexSupplant(
	    '(?:(#{validateUrlUserinfo})@)?'  + // $1 userinfo
	    '(#{validateUrlUnicodeHost})'     + // $2 host
	    '(?::(#{validateUrlPort}))?'        //$3 port
	  , "i");

	  twttr.txt.regexen.validateUrlAuthority = regexSupplant(
	    '(?:(#{validateUrlUserinfo})@)?' + // $1 userinfo
	    '(#{validateUrlHost})'           + // $2 host
	    '(?::(#{validateUrlPort}))?'       // $3 port
	  , "i");

	  twttr.txt.regexen.validateUrlPath = regexSupplant(/(\/#{validateUrlPchar}*)*/i);
	  twttr.txt.regexen.validateUrlQuery = regexSupplant(/(#{validateUrlPchar}|\/|\?)*/i);
	  twttr.txt.regexen.validateUrlFragment = regexSupplant(/(#{validateUrlPchar}|\/|\?)*/i);

	  // Modified version of RFC 3986 Appendix B
	  twttr.txt.regexen.validateUrlUnencoded = regexSupplant(
	    '^'                               + // Full URL
	    '(?:'                             +
	      '([^:/?#]+):\\/\\/'             + // $1 Scheme
	    ')?'                              +
	    '([^/?#]*)'                       + // $2 Authority
	    '([^?#]*)'                        + // $3 Path
	    '(?:'                             +
	      '\\?([^#]*)'                    + // $4 Query
	    ')?'                              +
	    '(?:'                             +
	      '#(.*)'                         + // $5 Fragment
	    ')?$'
	  , "i");


	  // Default CSS class for auto-linked lists (along with the url class)
	  var DEFAULT_LIST_CLASS = "tweet-url list-slug";
	  // Default CSS class for auto-linked usernames (along with the url class)
	  var DEFAULT_USERNAME_CLASS = "tweet-url username";
	  // Default CSS class for auto-linked hashtags (along with the url class)
	  var DEFAULT_HASHTAG_CLASS = "tweet-url hashtag";
	  // Default CSS class for auto-linked cashtags (along with the url class)
	  var DEFAULT_CASHTAG_CLASS = "tweet-url cashtag";
	  // Options which should not be passed as HTML attributes
	  var OPTIONS_NOT_ATTRIBUTES = {'urlClass':true, 'listClass':true, 'usernameClass':true, 'hashtagClass':true, 'cashtagClass':true,
	                            'usernameUrlBase':true, 'listUrlBase':true, 'hashtagUrlBase':true, 'cashtagUrlBase':true,
	                            'usernameUrlBlock':true, 'listUrlBlock':true, 'hashtagUrlBlock':true, 'linkUrlBlock':true,
	                            'usernameIncludeSymbol':true, 'suppressLists':true, 'suppressNoFollow':true, 'targetBlank':true,
	                            'suppressDataScreenName':true, 'urlEntities':true, 'symbolTag':true, 'textWithSymbolTag':true, 'urlTarget':true,
	                            'invisibleTagAttrs':true, 'linkAttributeBlock':true, 'linkTextBlock': true, 'htmlEscapeNonEntities': true
	                            };

	  var BOOLEAN_ATTRIBUTES = {'disabled':true, 'readonly':true, 'multiple':true, 'checked':true};

	  // Simple object cloning function for simple objects
	  function clone(o) {
	    var r = {};
	    for (var k in o) {
	      if (o.hasOwnProperty(k)) {
	        r[k] = o[k];
	      }
	    }

	    return r;
	  }

	  twttr.txt.tagAttrs = function(attributes) {
	    var htmlAttrs = "";
	    for (var k in attributes) {
	      var v = attributes[k];
	      if (BOOLEAN_ATTRIBUTES[k]) {
	        v = v ? k : null;
	      }
	      if (v == null) continue;
	      htmlAttrs += " " + twttr.txt.htmlEscape(k) + "=\"" + twttr.txt.htmlEscape(v.toString()) + "\"";
	    }
	    return htmlAttrs;
	  };

	  twttr.txt.linkToText = function(entity, text, attributes, options) {
	    if (!options.suppressNoFollow) {
	      attributes.rel = "nofollow";
	    }
	    // if linkAttributeBlock is specified, call it to modify the attributes
	    if (options.linkAttributeBlock) {
	      options.linkAttributeBlock(entity, attributes);
	    }
	    // if linkTextBlock is specified, call it to get a new/modified link text
	    if (options.linkTextBlock) {
	      text = options.linkTextBlock(entity, text);
	    }
	    var d = {
	      text: text,
	      attr: twttr.txt.tagAttrs(attributes)
	    };
	    return stringSupplant("<a#{attr}>#{text}</a>", d);
	  };

	  twttr.txt.linkToTextWithSymbol = function(entity, symbol, text, attributes, options) {
	    var taggedSymbol = options.symbolTag ? "<" + options.symbolTag + ">" + symbol + "</"+ options.symbolTag + ">" : symbol;
	    text = twttr.txt.htmlEscape(text);
	    var taggedText = options.textWithSymbolTag ? "<" + options.textWithSymbolTag + ">" + text + "</"+ options.textWithSymbolTag + ">" : text;

	    if (options.usernameIncludeSymbol || !symbol.match(twttr.txt.regexen.atSigns)) {
	      return twttr.txt.linkToText(entity, taggedSymbol + taggedText, attributes, options);
	    } else {
	      return taggedSymbol + twttr.txt.linkToText(entity, taggedText, attributes, options);
	    }
	  };

	  twttr.txt.linkToHashtag = function(entity, text, options) {
	    var hash = text.substring(entity.indices[0], entity.indices[0] + 1);
	    var hashtag = twttr.txt.htmlEscape(entity.hashtag);
	    var attrs = clone(options.htmlAttrs || {});
	    attrs.href = options.hashtagUrlBase + hashtag;
	    attrs.title = "#" + hashtag;
	    attrs["class"] = options.hashtagClass;
	    if (hashtag.charAt(0).match(twttr.txt.regexen.rtl_chars)){
	      attrs["class"] += " rtl";
	    }
	    if (options.targetBlank) {
	      attrs.target = '_blank';
	    }

	    return twttr.txt.linkToTextWithSymbol(entity, hash, hashtag, attrs, options);
	  };

	  twttr.txt.linkToCashtag = function(entity, text, options) {
	    var cashtag = twttr.txt.htmlEscape(entity.cashtag);
	    var attrs = clone(options.htmlAttrs || {});
	    attrs.href = options.cashtagUrlBase + cashtag;
	    attrs.title = "$" + cashtag;
	    attrs["class"] =  options.cashtagClass;
	    if (options.targetBlank) {
	      attrs.target = '_blank';
	    }

	    return twttr.txt.linkToTextWithSymbol(entity, "$", cashtag, attrs, options);
	  };

	  twttr.txt.linkToMentionAndList = function(entity, text, options) {
	    var at = text.substring(entity.indices[0], entity.indices[0] + 1);
	    var user = twttr.txt.htmlEscape(entity.screenName);
	    var slashListname = twttr.txt.htmlEscape(entity.listSlug);
	    var isList = entity.listSlug && !options.suppressLists;
	    var attrs = clone(options.htmlAttrs || {});
	    attrs["class"] = (isList ? options.listClass : options.usernameClass);
	    attrs.href = isList ? options.listUrlBase + user + slashListname : options.usernameUrlBase + user;
	    if (!isList && !options.suppressDataScreenName) {
	      attrs['data-screen-name'] = user;
	    }
	    if (options.targetBlank) {
	      attrs.target = '_blank';
	    }

	    return twttr.txt.linkToTextWithSymbol(entity, at, isList ? user + slashListname : user, attrs, options);
	  };

	  twttr.txt.linkToUrl = function(entity, text, options) {
	    var url = entity.url;
	    var displayUrl = url;
	    var linkText = twttr.txt.htmlEscape(displayUrl);

	    // If the caller passed a urlEntities object (provided by a Twitter API
	    // response with include_entities=true), we use that to render the display_url
	    // for each URL instead of it's underlying t.co URL.
	    var urlEntity = (options.urlEntities && options.urlEntities[url]) || entity;
	    if (urlEntity.display_url) {
	      linkText = twttr.txt.linkTextWithEntity(urlEntity, options);
	    }

	    var attrs = clone(options.htmlAttrs || {});

	    if (!url.match(twttr.txt.regexen.urlHasProtocol)) {
	      url = "http://" + url;
	    }
	    attrs.href = url;

	    if (options.targetBlank) {
	      attrs.target = '_blank';
	    }

	    // set class only if urlClass is specified.
	    if (options.urlClass) {
	      attrs["class"] = options.urlClass;
	    }

	    // set target only if urlTarget is specified.
	    if (options.urlTarget) {
	      attrs.target = options.urlTarget;
	    }

	    if (!options.title && urlEntity.display_url) {
	      attrs.title = urlEntity.expanded_url;
	    }

	    return twttr.txt.linkToText(entity, linkText, attrs, options);
	  };

	  twttr.txt.linkTextWithEntity = function (entity, options) {
	    var displayUrl = entity.display_url;
	    var expandedUrl = entity.expanded_url;

	    // Goal: If a user copies and pastes a tweet containing t.co'ed link, the resulting paste
	    // should contain the full original URL (expanded_url), not the display URL.
	    //
	    // Method: Whenever possible, we actually emit HTML that contains expanded_url, and use
	    // font-size:0 to hide those parts that should not be displayed (because they are not part of display_url).
	    // Elements with font-size:0 get copied even though they are not visible.
	    // Note that display:none doesn't work here. Elements with display:none don't get copied.
	    //
	    // Additionally, we want to *display* ellipses, but we don't want them copied.  To make this happen we
	    // wrap the ellipses in a tco-ellipsis class and provide an onCopy handler that sets display:none on
	    // everything with the tco-ellipsis class.
	    //
	    // Exception: pic.twitter.com images, for which expandedUrl = "https://twitter.com/#!/username/status/1234/photo/1
	    // For those URLs, display_url is not a substring of expanded_url, so we don't do anything special to render the elided parts.
	    // For a pic.twitter.com URL, the only elided part will be the "https://", so this is fine.

	    var displayUrlSansEllipses = displayUrl.replace(/…/g, ""); // We have to disregard ellipses for matching
	    // Note: we currently only support eliding parts of the URL at the beginning or the end.
	    // Eventually we may want to elide parts of the URL in the *middle*.  If so, this code will
	    // become more complicated.  We will probably want to create a regexp out of display URL,
	    // replacing every ellipsis with a ".*".
	    if (expandedUrl.indexOf(displayUrlSansEllipses) != -1) {
	      var displayUrlIndex = expandedUrl.indexOf(displayUrlSansEllipses);
	      var v = {
	        displayUrlSansEllipses: displayUrlSansEllipses,
	        // Portion of expandedUrl that precedes the displayUrl substring
	        beforeDisplayUrl: expandedUrl.substr(0, displayUrlIndex),
	        // Portion of expandedUrl that comes after displayUrl
	        afterDisplayUrl: expandedUrl.substr(displayUrlIndex + displayUrlSansEllipses.length),
	        precedingEllipsis: displayUrl.match(/^…/) ? "…" : "",
	        followingEllipsis: displayUrl.match(/…$/) ? "…" : ""
	      };
	      for (var k in v) {
	        if (v.hasOwnProperty(k)) {
	          v[k] = twttr.txt.htmlEscape(v[k]);
	        }
	      }
	      // As an example: The user tweets "hi http://longdomainname.com/foo"
	      // This gets shortened to "hi http://t.co/xyzabc", with display_url = "…nname.com/foo"
	      // This will get rendered as:
	      // <span class='tco-ellipsis'> <!-- This stuff should get displayed but not copied -->
	      //   …
	      //   <!-- There's a chance the onCopy event handler might not fire. In case that happens,
	      //        we include an &nbsp; here so that the … doesn't bump up against the URL and ruin it.
	      //        The &nbsp; is inside the tco-ellipsis span so that when the onCopy handler *does*
	      //        fire, it doesn't get copied.  Otherwise the copied text would have two spaces in a row,
	      //        e.g. "hi  http://longdomainname.com/foo".
	      //   <span style='font-size:0'>&nbsp;</span>
	      // </span>
	      // <span style='font-size:0'>  <!-- This stuff should get copied but not displayed -->
	      //   http://longdomai
	      // </span>
	      // <span class='js-display-url'> <!-- This stuff should get displayed *and* copied -->
	      //   nname.com/foo
	      // </span>
	      // <span class='tco-ellipsis'> <!-- This stuff should get displayed but not copied -->
	      //   <span style='font-size:0'>&nbsp;</span>
	      //   …
	      // </span>
	      v['invisible'] = options.invisibleTagAttrs;
	      return stringSupplant("<span class='tco-ellipsis'>#{precedingEllipsis}<span #{invisible}>&nbsp;</span></span><span #{invisible}>#{beforeDisplayUrl}</span><span class='js-display-url'>#{displayUrlSansEllipses}</span><span #{invisible}>#{afterDisplayUrl}</span><span class='tco-ellipsis'><span #{invisible}>&nbsp;</span>#{followingEllipsis}</span>", v);
	    }
	    return displayUrl;
	  };

	  twttr.txt.autoLinkEntities = function(text, entities, options) {
	    options = clone(options || {});

	    options.hashtagClass = options.hashtagClass || DEFAULT_HASHTAG_CLASS;
	    options.hashtagUrlBase = options.hashtagUrlBase || "https://twitter.com/#!/search?q=%23";
	    options.cashtagClass = options.cashtagClass || DEFAULT_CASHTAG_CLASS;
	    options.cashtagUrlBase = options.cashtagUrlBase || "https://twitter.com/#!/search?q=%24";
	    options.listClass = options.listClass || DEFAULT_LIST_CLASS;
	    options.usernameClass = options.usernameClass || DEFAULT_USERNAME_CLASS;
	    options.usernameUrlBase = options.usernameUrlBase || "https://twitter.com/";
	    options.listUrlBase = options.listUrlBase || "https://twitter.com/";
	    options.htmlAttrs = twttr.txt.extractHtmlAttrsFromOptions(options);
	    options.invisibleTagAttrs = options.invisibleTagAttrs || "style='position:absolute;left:-9999px;'";

	    // remap url entities to hash
	    var urlEntities, i, len;
	    if(options.urlEntities) {
	      urlEntities = {};
	      for(i = 0, len = options.urlEntities.length; i < len; i++) {
	        urlEntities[options.urlEntities[i].url] = options.urlEntities[i];
	      }
	      options.urlEntities = urlEntities;
	    }

	    var result = "";
	    var beginIndex = 0;

	    // sort entities by start index
	    entities.sort(function(a,b){ return a.indices[0] - b.indices[0]; });

	    var nonEntity = options.htmlEscapeNonEntities ? twttr.txt.htmlEscape : function(text) {
	      return text;
	    };

	    for (var i = 0; i < entities.length; i++) {
	      var entity = entities[i];
	      result += nonEntity(text.substring(beginIndex, entity.indices[0]));

	      if (entity.url) {
	        result += twttr.txt.linkToUrl(entity, text, options);
	      } else if (entity.hashtag) {
	        result += twttr.txt.linkToHashtag(entity, text, options);
	      } else if (entity.screenName) {
	        result += twttr.txt.linkToMentionAndList(entity, text, options);
	      } else if (entity.cashtag) {
	        result += twttr.txt.linkToCashtag(entity, text, options);
	      }
	      beginIndex = entity.indices[1];
	    }
	    result += nonEntity(text.substring(beginIndex, text.length));
	    return result;
	  };

	  twttr.txt.autoLinkWithJSON = function(text, json, options) {
	    // map JSON entity to twitter-text entity
	    if (json.user_mentions) {
	      for (var i = 0; i < json.user_mentions.length; i++) {
	        // this is a @mention
	        json.user_mentions[i].screenName = json.user_mentions[i].screen_name;
	      }
	    }

	    if (json.hashtags) {
	      for (var i = 0; i < json.hashtags.length; i++) {
	        // this is a #hashtag
	        json.hashtags[i].hashtag = json.hashtags[i].text;
	      }
	    }

	    if (json.symbols) {
	      for (var i = 0; i < json.symbols.length; i++) {
	        // this is a $CASH tag
	        json.symbols[i].cashtag = json.symbols[i].text;
	      }
	    }

	    // concatenate all entities
	    var entities = [];
	    for (var key in json) {
	      entities = entities.concat(json[key]);
	    }

	    // modify indices to UTF-16
	    twttr.txt.modifyIndicesFromUnicodeToUTF16(text, entities);

	    return twttr.txt.autoLinkEntities(text, entities, options);
	  };

	  twttr.txt.extractHtmlAttrsFromOptions = function(options) {
	    var htmlAttrs = {};
	    for (var k in options) {
	      var v = options[k];
	      if (OPTIONS_NOT_ATTRIBUTES[k]) continue;
	      if (BOOLEAN_ATTRIBUTES[k]) {
	        v = v ? k : null;
	      }
	      if (v == null) continue;
	      htmlAttrs[k] = v;
	    }
	    return htmlAttrs;
	  };

	  twttr.txt.autoLink = function(text, options) {
	    var entities = twttr.txt.extractEntitiesWithIndices(text, {extractUrlsWithoutProtocol: false});
	    return twttr.txt.autoLinkEntities(text, entities, options);
	  };

	  twttr.txt.autoLinkUsernamesOrLists = function(text, options) {
	    var entities = twttr.txt.extractMentionsOrListsWithIndices(text);
	    return twttr.txt.autoLinkEntities(text, entities, options);
	  };

	  twttr.txt.autoLinkHashtags = function(text, options) {
	    var entities = twttr.txt.extractHashtagsWithIndices(text);
	    return twttr.txt.autoLinkEntities(text, entities, options);
	  };

	  twttr.txt.autoLinkCashtags = function(text, options) {
	    var entities = twttr.txt.extractCashtagsWithIndices(text);
	    return twttr.txt.autoLinkEntities(text, entities, options);
	  };

	  twttr.txt.autoLinkUrlsCustom = function(text, options) {
	    var entities = twttr.txt.extractUrlsWithIndices(text, {extractUrlsWithoutProtocol: false});
	    return twttr.txt.autoLinkEntities(text, entities, options);
	  };

	  twttr.txt.removeOverlappingEntities = function(entities) {
	    entities.sort(function(a,b){ return a.indices[0] - b.indices[0]; });

	    var prev = entities[0];
	    for (var i = 1; i < entities.length; i++) {
	      if (prev.indices[1] > entities[i].indices[0]) {
	        entities.splice(i, 1);
	        i--;
	      } else {
	        prev = entities[i];
	      }
	    }
	  };

	  twttr.txt.extractEntitiesWithIndices = function(text, options) {
	    var entities = twttr.txt.extractUrlsWithIndices(text, options)
	                    .concat(twttr.txt.extractMentionsOrListsWithIndices(text))
	                    .concat(twttr.txt.extractHashtagsWithIndices(text, {checkUrlOverlap: false}))
	                    .concat(twttr.txt.extractCashtagsWithIndices(text));

	    if (entities.length == 0) {
	      return [];
	    }

	    twttr.txt.removeOverlappingEntities(entities);
	    return entities;
	  };

	  twttr.txt.extractMentions = function(text) {
	    var screenNamesOnly = [],
	        screenNamesWithIndices = twttr.txt.extractMentionsWithIndices(text);

	    for (var i = 0; i < screenNamesWithIndices.length; i++) {
	      var screenName = screenNamesWithIndices[i].screenName;
	      screenNamesOnly.push(screenName);
	    }

	    return screenNamesOnly;
	  };

	  twttr.txt.extractMentionsWithIndices = function(text) {
	    var mentions = [],
	        mentionOrList,
	        mentionsOrLists = twttr.txt.extractMentionsOrListsWithIndices(text);

	    for (var i = 0 ; i < mentionsOrLists.length; i++) {
	      mentionOrList = mentionsOrLists[i];
	      if (mentionOrList.listSlug == '') {
	        mentions.push({
	          screenName: mentionOrList.screenName,
	          indices: mentionOrList.indices
	        });
	      }
	    }

	    return mentions;
	  };

	  /**
	   * Extract list or user mentions.
	   * (Presence of listSlug indicates a list)
	   */
	  twttr.txt.extractMentionsOrListsWithIndices = function(text) {
	    if (!text || !text.match(twttr.txt.regexen.atSigns)) {
	      return [];
	    }

	    var possibleNames = [],
	        slashListname;

	    text.replace(twttr.txt.regexen.validMentionOrList, function(match, before, atSign, screenName, slashListname, offset, chunk) {
	      var after = chunk.slice(offset + match.length);
	      if (!after.match(twttr.txt.regexen.endMentionMatch)) {
	        slashListname = slashListname || '';
	        var startPosition = offset + before.length;
	        var endPosition = startPosition + screenName.length + slashListname.length + 1;
	        possibleNames.push({
	          screenName: screenName,
	          listSlug: slashListname,
	          indices: [startPosition, endPosition]
	        });
	      }
	    });

	    return possibleNames;
	  };


	  twttr.txt.extractReplies = function(text) {
	    if (!text) {
	      return null;
	    }

	    var possibleScreenName = text.match(twttr.txt.regexen.validReply);
	    if (!possibleScreenName ||
	        RegExp.rightContext.match(twttr.txt.regexen.endMentionMatch)) {
	      return null;
	    }

	    return possibleScreenName[1];
	  };

	  twttr.txt.extractUrls = function(text, options) {
	    var urlsOnly = [],
	        urlsWithIndices = twttr.txt.extractUrlsWithIndices(text, options);

	    for (var i = 0; i < urlsWithIndices.length; i++) {
	      urlsOnly.push(urlsWithIndices[i].url);
	    }

	    return urlsOnly;
	  };

	  twttr.txt.extractUrlsWithIndices = function(text, options) {
	    if (!options) {
	      options = {extractUrlsWithoutProtocol: true};
	    }
	    if (!text || (options.extractUrlsWithoutProtocol ? !text.match(/\./) : !text.match(/:/))) {
	      return [];
	    }

	    var urls = [];

	    while (twttr.txt.regexen.extractUrl.exec(text)) {
	      var before = RegExp.$2, url = RegExp.$3, protocol = RegExp.$4, domain = RegExp.$5, path = RegExp.$7;
	      var endPosition = twttr.txt.regexen.extractUrl.lastIndex,
	          startPosition = endPosition - url.length;

	      // if protocol is missing and domain contains non-ASCII characters,
	      // extract ASCII-only domains.
	      if (!protocol) {
	        if (!options.extractUrlsWithoutProtocol
	            || before.match(twttr.txt.regexen.invalidUrlWithoutProtocolPrecedingChars)) {
	          continue;
	        }
	        var lastUrl = null,
	            asciiEndPosition = 0;
	        domain.replace(twttr.txt.regexen.validAsciiDomain, function(asciiDomain) {
	          var asciiStartPosition = domain.indexOf(asciiDomain, asciiEndPosition);
	          asciiEndPosition = asciiStartPosition + asciiDomain.length;
	          lastUrl = {
	            url: asciiDomain,
	            indices: [startPosition + asciiStartPosition, startPosition + asciiEndPosition]
	          };
	          if (path
	              || asciiDomain.match(twttr.txt.regexen.validSpecialShortDomain)
	              || !asciiDomain.match(twttr.txt.regexen.invalidShortDomain)) {
	            urls.push(lastUrl);
	          }
	        });

	        // no ASCII-only domain found. Skip the entire URL.
	        if (lastUrl == null) {
	          continue;
	        }

	        // lastUrl only contains domain. Need to add path and query if they exist.
	        if (path) {
	          lastUrl.url = url.replace(domain, lastUrl.url);
	          lastUrl.indices[1] = endPosition;
	        }
	      } else {
	        // In the case of t.co URLs, don't allow additional path characters.
	        if (url.match(twttr.txt.regexen.validTcoUrl)) {
	          url = RegExp.lastMatch;
	          endPosition = startPosition + url.length;
	        }
	        urls.push({
	          url: url,
	          indices: [startPosition, endPosition]
	        });
	      }
	    }

	    return urls;
	  };

	  twttr.txt.extractHashtags = function(text) {
	    var hashtagsOnly = [],
	        hashtagsWithIndices = twttr.txt.extractHashtagsWithIndices(text);

	    for (var i = 0; i < hashtagsWithIndices.length; i++) {
	      hashtagsOnly.push(hashtagsWithIndices[i].hashtag);
	    }

	    return hashtagsOnly;
	  };

	  twttr.txt.extractHashtagsWithIndices = function(text, options) {
	    if (!options) {
	      options = {checkUrlOverlap: true};
	    }

	    if (!text || !text.match(twttr.txt.regexen.hashSigns)) {
	      return [];
	    }

	    var tags = [];

	    text.replace(twttr.txt.regexen.validHashtag, function(match, before, hash, hashText, offset, chunk) {
	      var after = chunk.slice(offset + match.length);
	      if (after.match(twttr.txt.regexen.endHashtagMatch))
	        return;
	      var startPosition = offset + before.length;
	      var endPosition = startPosition + hashText.length + 1;
	      tags.push({
	        hashtag: hashText,
	        indices: [startPosition, endPosition]
	      });
	    });

	    if (options.checkUrlOverlap) {
	      // also extract URL entities
	      var urls = twttr.txt.extractUrlsWithIndices(text);
	      if (urls.length > 0) {
	        var entities = tags.concat(urls);
	        // remove overlap
	        twttr.txt.removeOverlappingEntities(entities);
	        // only push back hashtags
	        tags = [];
	        for (var i = 0; i < entities.length; i++) {
	          if (entities[i].hashtag) {
	            tags.push(entities[i]);
	          }
	        }
	      }
	    }

	    return tags;
	  };

	  twttr.txt.extractCashtags = function(text) {
	    var cashtagsOnly = [],
	        cashtagsWithIndices = twttr.txt.extractCashtagsWithIndices(text);

	    for (var i = 0; i < cashtagsWithIndices.length; i++) {
	      cashtagsOnly.push(cashtagsWithIndices[i].cashtag);
	    }

	    return cashtagsOnly;
	  };

	  twttr.txt.extractCashtagsWithIndices = function(text) {
	    if (!text || text.indexOf("$") == -1) {
	      return [];
	    }

	    var tags = [];

	    text.replace(twttr.txt.regexen.validCashtag, function(match, before, dollar, cashtag, offset, chunk) {
	      var startPosition = offset + before.length;
	      var endPosition = startPosition + cashtag.length + 1;
	      tags.push({
	        cashtag: cashtag,
	        indices: [startPosition, endPosition]
	      });
	    });

	    return tags;
	  };

	  twttr.txt.modifyIndicesFromUnicodeToUTF16 = function(text, entities) {
	    twttr.txt.convertUnicodeIndices(text, entities, false);
	  };

	  twttr.txt.modifyIndicesFromUTF16ToUnicode = function(text, entities) {
	    twttr.txt.convertUnicodeIndices(text, entities, true);
	  };

	  twttr.txt.getUnicodeTextLength = function(text) {
	    return text.replace(twttr.txt.regexen.non_bmp_code_pairs, ' ').length;
	  };

	  twttr.txt.convertUnicodeIndices = function(text, entities, indicesInUTF16) {
	    if (entities.length == 0) {
	      return;
	    }

	    var charIndex = 0;
	    var codePointIndex = 0;

	    // sort entities by start index
	    entities.sort(function(a,b){ return a.indices[0] - b.indices[0]; });
	    var entityIndex = 0;
	    var entity = entities[0];

	    while (charIndex < text.length) {
	      if (entity.indices[0] == (indicesInUTF16 ? charIndex : codePointIndex)) {
	        var len = entity.indices[1] - entity.indices[0];
	        entity.indices[0] = indicesInUTF16 ? codePointIndex : charIndex;
	        entity.indices[1] = entity.indices[0] + len;

	        entityIndex++;
	        if (entityIndex == entities.length) {
	          // no more entity
	          break;
	        }
	        entity = entities[entityIndex];
	      }

	      var c = text.charCodeAt(charIndex);
	      if (0xD800 <= c && c <= 0xDBFF && charIndex < text.length - 1) {
	        // Found high surrogate char
	        c = text.charCodeAt(charIndex + 1);
	        if (0xDC00 <= c && c <= 0xDFFF) {
	          // Found surrogate pair
	          charIndex++;
	        }
	      }
	      codePointIndex++;
	      charIndex++;
	    }
	  };

	  // this essentially does text.split(/<|>/)
	  // except that won't work in IE, where empty strings are ommitted
	  // so "<>".split(/<|>/) => [] in IE, but is ["", "", ""] in all others
	  // but "<<".split("<") => ["", "", ""]
	  twttr.txt.splitTags = function(text) {
	    var firstSplits = text.split("<"),
	        secondSplits,
	        allSplits = [],
	        split;

	    for (var i = 0; i < firstSplits.length; i += 1) {
	      split = firstSplits[i];
	      if (!split) {
	        allSplits.push("");
	      } else {
	        secondSplits = split.split(">");
	        for (var j = 0; j < secondSplits.length; j += 1) {
	          allSplits.push(secondSplits[j]);
	        }
	      }
	    }

	    return allSplits;
	  };

	  twttr.txt.hitHighlight = function(text, hits, options) {
	    var defaultHighlightTag = "em";

	    hits = hits || [];
	    options = options || {};

	    if (hits.length === 0) {
	      return text;
	    }

	    var tagName = options.tag || defaultHighlightTag,
	        tags = ["<" + tagName + ">", "</" + tagName + ">"],
	        chunks = twttr.txt.splitTags(text),
	        i,
	        j,
	        result = "",
	        chunkIndex = 0,
	        chunk = chunks[0],
	        prevChunksLen = 0,
	        chunkCursor = 0,
	        startInChunk = false,
	        chunkChars = chunk,
	        flatHits = [],
	        index,
	        hit,
	        tag,
	        placed,
	        hitSpot;

	    for (i = 0; i < hits.length; i += 1) {
	      for (j = 0; j < hits[i].length; j += 1) {
	        flatHits.push(hits[i][j]);
	      }
	    }

	    for (index = 0; index < flatHits.length; index += 1) {
	      hit = flatHits[index];
	      tag = tags[index % 2];
	      placed = false;

	      while (chunk != null && hit >= prevChunksLen + chunk.length) {
	        result += chunkChars.slice(chunkCursor);
	        if (startInChunk && hit === prevChunksLen + chunkChars.length) {
	          result += tag;
	          placed = true;
	        }

	        if (chunks[chunkIndex + 1]) {
	          result += "<" + chunks[chunkIndex + 1] + ">";
	        }

	        prevChunksLen += chunkChars.length;
	        chunkCursor = 0;
	        chunkIndex += 2;
	        chunk = chunks[chunkIndex];
	        chunkChars = chunk;
	        startInChunk = false;
	      }

	      if (!placed && chunk != null) {
	        hitSpot = hit - prevChunksLen;
	        result += chunkChars.slice(chunkCursor, hitSpot) + tag;
	        chunkCursor = hitSpot;
	        if (index % 2 === 0) {
	          startInChunk = true;
	        } else {
	          startInChunk = false;
	        }
	      } else if(!placed) {
	        placed = true;
	        result += tag;
	      }
	    }

	    if (chunk != null) {
	      if (chunkCursor < chunkChars.length) {
	        result += chunkChars.slice(chunkCursor);
	      }
	      for (index = chunkIndex + 1; index < chunks.length; index += 1) {
	        result += (index % 2 === 0 ? chunks[index] : "<" + chunks[index] + ">");
	      }
	    }

	    return result;
	  };

	  var MAX_LENGTH = 140;

	  // Returns the length of Tweet text with consideration to t.co URL replacement
	  // and chars outside the basic multilingual plane that use 2 UTF16 code points
	  twttr.txt.getTweetLength = function(text, options) {
	    if (!options) {
	      options = {
	          // These come from https://api.twitter.com/1.1/help/configuration.json
	          // described by https://dev.twitter.com/rest/reference/get/help/configuration
	          short_url_length: 23,
	          short_url_length_https: 23
	      };
	    }
	    var textLength = twttr.txt.getUnicodeTextLength(text),
	        urlsWithIndices = twttr.txt.extractUrlsWithIndices(text);
	    twttr.txt.modifyIndicesFromUTF16ToUnicode(text, urlsWithIndices);

	    for (var i = 0; i < urlsWithIndices.length; i++) {
	      // Subtract the length of the original URL
	      textLength += urlsWithIndices[i].indices[0] - urlsWithIndices[i].indices[1];

	      // Add 23 characters for URL starting with https://
	      // http:// URLs still use https://t.co so they are 23 characters as well
	      if (urlsWithIndices[i].url.toLowerCase().match(twttr.txt.regexen.urlHasHttps)) {
	         textLength += options.short_url_length_https;
	      } else {
	        textLength += options.short_url_length;
	      }
	    }

	    return textLength;
	  };

	  // Check the text for any reason that it may not be valid as a Tweet. This is meant as a pre-validation
	  // before posting to api.twitter.com. There are several server-side reasons for Tweets to fail but this pre-validation
	  // will allow quicker feedback.
	  //
	  // Returns false if this text is valid. Otherwise one of the following strings will be returned:
	  //
	  //   "too_long": if the text is too long
	  //   "empty": if the text is nil or empty
	  //   "invalid_characters": if the text contains non-Unicode or any of the disallowed Unicode characters
	  twttr.txt.isInvalidTweet = function(text) {
	    if (!text) {
	      return "empty";
	    }

	    // Determine max length independent of URL length
	    if (twttr.txt.getTweetLength(text) > MAX_LENGTH) {
	      return "too_long";
	    }

	    if (twttr.txt.hasInvalidCharacters(text)) {
	      return "invalid_characters";
	    }

	    return false;
	  };

	  twttr.txt.hasInvalidCharacters = function(text) {
	    return twttr.txt.regexen.invalid_chars.test(text);
	  };

	  twttr.txt.isValidTweetText = function(text) {
	    return !twttr.txt.isInvalidTweet(text);
	  };

	  twttr.txt.isValidUsername = function(username) {
	    if (!username) {
	      return false;
	    }

	    var extracted = twttr.txt.extractMentions(username);

	    // Should extract the username minus the @ sign, hence the .slice(1)
	    return extracted.length === 1 && extracted[0] === username.slice(1);
	  };

	  var VALID_LIST_RE = regexSupplant(/^#{validMentionOrList}$/);

	  twttr.txt.isValidList = function(usernameList) {
	    var match = usernameList.match(VALID_LIST_RE);

	    // Must have matched and had nothing before or after
	    return !!(match && match[1] == "" && match[4]);
	  };

	  twttr.txt.isValidHashtag = function(hashtag) {
	    if (!hashtag) {
	      return false;
	    }

	    var extracted = twttr.txt.extractHashtags(hashtag);

	    // Should extract the hashtag minus the # sign, hence the .slice(1)
	    return extracted.length === 1 && extracted[0] === hashtag.slice(1);
	  };

	  twttr.txt.isValidUrl = function(url, unicodeDomains, requireProtocol) {
	    if (unicodeDomains == null) {
	      unicodeDomains = true;
	    }

	    if (requireProtocol == null) {
	      requireProtocol = true;
	    }

	    if (!url) {
	      return false;
	    }

	    var urlParts = url.match(twttr.txt.regexen.validateUrlUnencoded);

	    if (!urlParts || urlParts[0] !== url) {
	      return false;
	    }

	    var scheme = urlParts[1],
	        authority = urlParts[2],
	        path = urlParts[3],
	        query = urlParts[4],
	        fragment = urlParts[5];

	    if (!(
	      (!requireProtocol || (isValidMatch(scheme, twttr.txt.regexen.validateUrlScheme) && scheme.match(/^https?$/i))) &&
	      isValidMatch(path, twttr.txt.regexen.validateUrlPath) &&
	      isValidMatch(query, twttr.txt.regexen.validateUrlQuery, true) &&
	      isValidMatch(fragment, twttr.txt.regexen.validateUrlFragment, true)
	    )) {
	      return false;
	    }

	    return (unicodeDomains && isValidMatch(authority, twttr.txt.regexen.validateUrlUnicodeAuthority)) ||
	           (!unicodeDomains && isValidMatch(authority, twttr.txt.regexen.validateUrlAuthority));
	  };

	  function isValidMatch(string, regex, optional) {
	    if (!optional) {
	      // RegExp["$&"] is the text of the last match
	      // blank strings are ok, but are falsy, so we check stringiness instead of truthiness
	      return ((typeof string === "string") && string.match(regex) && RegExp["$&"] === string);
	    }

	    // RegExp["$&"] is the text of the last match
	    return (!string || (string.match(regex) && RegExp["$&"] === string));
	  }

	  if (typeof module != 'undefined' && module.exports) {
	    module.exports = twttr.txt;
	  }

	  if (true) {
	    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (twttr.txt), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  }

	  if (typeof window != 'undefined') {
	    if (window.twttr) {
	      for (var prop in twttr) {
	        window.twttr[prop] = twttr[prop];
	      }
	    } else {
	      window.twttr = twttr;
	    }
	  }
	})();


/***/ }),
/* 177 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _getIterator2 = __webpack_require__(68);

	var _getIterator3 = _interopRequireDefault(_getIterator2);

	exports.default = generateUntrackedTwitterString;

	var _helperFunctions = __webpack_require__(173);

	var _prepareHtmlString = __webpack_require__(174);

	var _prepareHtmlString2 = _interopRequireDefault(_prepareHtmlString);

	var _videoScripts = __webpack_require__(175);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var twttrText = __webpack_require__(176);

	function generateAnchorForTweetLink(content) {

	  return twttrText.autoLinkEntities(content, twttrText.extractEntitiesWithIndices(content, { extractUrlsWithoutProtocol: true }), {
	    target: "_blank",
	    suppressNoFollow: true,
	    hashtagUrlBase: "https://twitter.com/hashtag/",
	    usernameUrlBase: 'https://twitter.com/'
	  });
	}

	function getVideoSource(post) {
	  var mp4s = post.Media[0].video_info.variants.filter(function (variant) {
	    return variant.hasOwnProperty('bitrate');
	  });
	  var bestQuality = void 0;
	  var _iteratorNormalCompletion = true;
	  var _didIteratorError = false;
	  var _iteratorError = undefined;

	  try {
	    for (var _iterator = (0, _getIterator3.default)(mp4s), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	      var mp4 = _step.value;

	      if (!bestQuality || mp4.bitrate > bestQuality.bitrate) {
	        bestQuality = mp4;
	      }
	    }
	  } catch (err) {
	    _didIteratorError = true;
	    _iteratorError = err;
	  } finally {
	    try {
	      if (!_iteratorNormalCompletion && _iterator.return) {
	        _iterator.return();
	      }
	    } finally {
	      if (_didIteratorError) {
	        throw _iteratorError;
	      }
	    }
	  }

	  return bestQuality.url;
	}

	function getIframeSource(post) {
	  if (post.Media) {
	    if (Array.isArray(post.Media) && post.Media.length) {
	      return post.Media[0].expanded_url;
	    }
	    return post.Media.expanded_url;
	  }
	  return '';
	}

	function createUntrackedTwitter(post) {
	  switch (post.MediaType) {
	    case 'IMAGE':
	      return '<div class="sl-untracked-social">\n          <img class="sl-untracked-social-picture" src="' + post.Media[0].media_url_https + '" onerror="this.src=\'https://bucket-files.scribblelive.com/DefaultThumbnails/FileNotFound.png\'"/>\n          <div class="sl-untracked-social-content sl-untracked-social-media">\n            <div class="sl-untracked-social-header">\n              <img class="sl-untracked-social-avatar" src="' + post.SocialData.avatar + '"/>\n              <div class="sl-untracked-social-name-and-handle">\n                <div class="sl-untracked-social-name">' + post.SocialData.name + '</div>\n                <div class="sl-untracked-social-handle"><a href="https://twitter.com/' + post.SocialData.handle + '">@' + post.SocialData.handle + '</a></div>\n              </div>\n              <div class="sl-icon sl-icon-twitter"><a href="' + post.SocialData.source + '"></a></div>\n            </div>\n            <div class="sl-untracked-social-message">' + generateAnchorForTweetLink(post.Content) + '</div>\n            <div class="sl-untracked-social-date">' + (0, _helperFunctions.formatDate)(post.SocialData.creationDate) + '</div>\n            <div class="sl-untracked-social-actions">\n              <div class="sl-untracked-social-action sl-comment-twitter"><a href="' + post.SocialData.reply + '"></a></div>\n              <div class="sl-untracked-social-action sl-share-twitter"><a href="' + post.SocialData.share + '"></a></div>\n              <div class="sl-untracked-social-action sl-like-twitter"><a href="' + post.SocialData.like + '"></a></div>\n            </div>\n          </div>\n        </div>';
	    case 'VIDEO':
	      return '<div class="sl-untracked-social">\n          <div class="sl-untracked-social-video">\n            <video src="' + getVideoSource(post) + '" onerror="' + _videoScripts.twitterVideoError + '"></video>\n            <div class="sl-untracked-social-video-play" onclick="' + _videoScripts.clickPlay + '"></div>\n            <div class="sl-untracked-social-video-pause" onclick="' + _videoScripts.clickPause + '" style="display: none;"></div>\n          </div>\n          <div class="sl-untracked-social-content sl-untracked-social-media">\n            <div class="sl-untracked-social-header">\n              <img class="sl-untracked-social-avatar" src="' + post.SocialData.avatar + '">\n              <div class="sl-untracked-social-name-and-handle">\n                <div class="sl-untracked-social-name">' + post.SocialData.name + '</div>\n                <div class="sl-untracked-social-handle"><a href="https://twitter.com/' + post.SocialData.handle + '">@' + post.SocialData.handle + '</a></div>\n              </div>\n              <div class="sl-icon sl-icon-twitter"><a href="' + post.SocialData.source + '"></a></div>\n            </div>\n            <div class="sl-untracked-social-message">' + generateAnchorForTweetLink(post.Content) + '</div>\n            <div class="sl-untracked-social-date">' + (0, _helperFunctions.formatDate)(post.SocialData.creationDate) + '</div>\n            <div class="sl-untracked-social-actions">\n              <div class="sl-untracked-social-action sl-comment-twitter"><a href="' + post.SocialData.reply + '"></a></div>\n              <div class="sl-untracked-social-action sl-share-twitter"><a href="' + post.SocialData.share + '"></a></div>\n              <div class="sl-untracked-social-action sl-like-twitter"><a href="' + post.SocialData.like + '"></a></div>\n            </div>\n          </div>\n        </div>';
	    case 'EMBED':
	      return '<div class="sl-untracked-social">\n          <div class="sl-untracked-social-iframe">\n            <iframe src="' + getIframeSource(post) + '"></iframe>\n          </div>\n          <div class="sl-untracked-social-content sl-untracked-social-media">\n            <div class="sl-untracked-social-header">\n              <img class="sl-untracked-social-avatar" src="' + post.SocialData.avatar + '">\n              <div class="sl-untracked-social-name-and-handle">\n                <div class="sl-untracked-social-name">' + post.SocialData.name + '</div>\n                <div class="sl-untracked-social-handle"><a href="https://twitter.com/' + post.SocialData.handle + '">@' + post.SocialData.handle + '</a></div>\n              </div>\n              <div class="sl-icon sl-icon-twitter"><a href="' + post.SocialData.source + '"></a></div>\n            </div>\n            <div class="sl-untracked-social-message">' + generateAnchorForTweetLink(post.Content) + '</div>\n            <div class="sl-untracked-social-date">' + (0, _helperFunctions.formatDate)(post.SocialData.creationDate) + '</div>\n            <div class="sl-untracked-social-actions">\n              <div class="sl-untracked-social-action sl-comment-twitter"><a href="' + post.SocialData.reply + '"></a></div>\n              <div class="sl-untracked-social-action sl-share-twitter"><a href="' + post.SocialData.share + '"></a></div>\n              <div class="sl-untracked-social-action sl-like-twitter"><a href="' + post.SocialData.like + '"></a></div>\n            </div>\n          </div>\n        </div>';
	    default:
	      return '<div class="sl-untracked-social no-thumbnail-image">\n          <div class="sl-untracked-social-content">\n            <div class="sl-untracked-social-header">\n              <img class="sl-untracked-social-avatar" src="' + post.SocialData.avatar + '">\n              <div class="sl-untracked-social-name-and-handle">\n                <div class="sl-untracked-social-name">' + post.SocialData.name + '</div>\n                <div class="sl-untracked-social-handle"><a href="https://twitter.com/' + post.SocialData.handle + '">@' + post.SocialData.handle + '</a></div>\n              </div>\n              <div class="sl-icon sl-icon-twitter"><a href="' + post.SocialData.source + '"></a></div>\n            </div>\n            <div class="sl-untracked-social-message">' + generateAnchorForTweetLink(post.Content) + '</div>\n            <div class="sl-untracked-social-date">' + (0, _helperFunctions.formatDate)(post.SocialData.creationDate) + '</div>\n            <div class="sl-untracked-social-actions">\n              <div class="sl-untracked-social-action sl-comment-twitter"><a href="' + post.SocialData.reply + '"></a></div>\n              <div class="sl-untracked-social-action sl-share-twitter"><a href="' + post.SocialData.share + '"></a></div>\n              <div class="sl-untracked-social-action sl-like-twitter"><a href="' + post.SocialData.like + '"></a></div>\n            </div>\n          </div>\n        </div>';
	  }
	}

	function generateUntrackedTwitterString(post) {
	  return (0, _prepareHtmlString2.default)('<div ' + (post.Id ? 'id="SL-' + post.Id + '"' : '') + ' class="SL-TWITTER sl-untracked">' + createUntrackedTwitter(post) + '</div>');
	}

/***/ }),
/* 178 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = generateUntrackedInstagramString;

	var _helperFunctions = __webpack_require__(173);

	var _prepareHtmlString = __webpack_require__(174);

	var _prepareHtmlString2 = _interopRequireDefault(_prepareHtmlString);

	var _videoScripts = __webpack_require__(175);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var twttrText = __webpack_require__(176);

	function generateAnchorForHashtag(content) {

	  var text = typeof content !== "undefined" && typeof content.text !== "undefined" ? content.text : content;

	  return twttrText.autoLinkEntities(text, twttrText.extractEntitiesWithIndices(text, {
	    extractUrlsWithoutProtocol: true
	  }), {
	    target: "_blank",
	    suppressNoFollow: true,
	    hashtagUrlBase: 'https://instagram.com/explore/tags/',
	    usernameUrlBase: 'https://instagram.com/'
	  });
	}

	function createUntrackedInstagram(post) {
	  switch (post.MediaType) {
	    case 'IMAGE':
	      return '<div class="sl-untracked-social">\n          <img class="sl-untracked-social-picture" src="' + post.Media.standard_resolution.url + '" onerror="this.src=\'https://bucket-files.scribblelive.com/DefaultThumbnails/FileNotFound.png\'"/>\n          <div class="sl-untracked-social-content sl-untracked-social-media">\n            <div class="sl-untracked-social-header">\n              ' + (post.SocialData.avatar ? '<a href="https://instagram.com/' + post.SocialData.handle + '">\n                <img class="sl-untracked-social-avatar" src="' + post.SocialData.avatar + '">\n              </a>' : '') + '\n              ' + (post.SocialData.handle ? '<div class="sl-untracked-social-name-and-handle">\n                <div class="sl-untracked-social-name"><a href="https://instagram.com/' + post.SocialData.handle + '">' + post.SocialData.name + '</a></div>\n                <div class="sl-untracked-social-handle">@' + post.SocialData.handle + '</div>\n              </div>' : '') + '\n              ' + (post.SocialData.source ? '<div class="sl-icon sl-icon-instagram"><a href="' + post.SocialData.source + '"></a></div>' : '') + '\n            </div>\n             <div class="sl-untracked-social-message">' + generateAnchorForHashtag(post.Content) + '</div>\n            ' + (post.SocialData.creationDate ? '<div class="sl-untracked-social-date">' + (0, _helperFunctions.formatDate)(post.SocialData.creationDate) + '</div>' : '') + '\n          </div>\n        </div>';
	    case 'VIDEO':
	    default:
	      return '<div class="sl-untracked-social">\n          <div class="sl-untracked-social-video">\n            <video src="' + post.Media.Videos.standard_resolution.url + '" onerror="' + _videoScripts.igVideoError + '"></video>\n            <div class="sl-untracked-social-video-play" onclick="' + _videoScripts.clickPlay + '"></div>\n            <div class="sl-untracked-social-video-pause" onclick="' + _videoScripts.clickPause + '" style="display: none;"></div>\n          </div>\n          <div class="sl-untracked-social-content sl-untracked-social-media">\n            <div class="sl-untracked-social-header">\n              ' + (post.SocialData.avatar ? '<a href="https://instagram.com/' + post.SocialData.handle + '">\n                <img class="sl-untracked-social-avatar" src="' + post.SocialData.avatar + '">\n              </a>' : '') + '\n              ' + (post.SocialData.handle ? '<div class="sl-untracked-social-name-and-handle">\n                <div class="sl-untracked-social-name"><a href="https://instagram.com/' + post.SocialData.handle + '">' + post.SocialData.name + '</a></div>\n                <div class="sl-untracked-social-handle">@' + post.SocialData.handle + '</div>\n              </div>' : '') + '\n              ' + (post.SocialData.source ? '<div class="sl-icon sl-icon-instagram"><a href="' + post.SocialData.source + '"></a></div>' : '') + '\n            </div>\n             <div class="sl-untracked-social-message">' + generateAnchorForHashtag(post.Content) + '</div>\n            ' + (post.SocialData.creationDate ? '<div class="sl-untracked-social-date">' + (0, _helperFunctions.formatDate)(post.SocialData.creationDate) + '</div>' : '') + '\n          </div>\n        </div>';
	  }
	}

	function generateUntrackedInstagramString(post) {
	  return (0, _prepareHtmlString2.default)('<div ' + (post.Id ? 'id="SL-' + post.Id + '"' : '') + ' class="SL-INSTAGRAM sl-untracked">' + createUntrackedInstagram(post) + '</div>');
	}

/***/ }),
/* 179 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = generateHtmlString;

	var _untrackedFacebook = __webpack_require__(172);

	var _untrackedFacebook2 = _interopRequireDefault(_untrackedFacebook);

	var _untrackedTwitter = __webpack_require__(177);

	var _untrackedTwitter2 = _interopRequireDefault(_untrackedTwitter);

	var _untrackedInstagram = __webpack_require__(178);

	var _untrackedInstagram2 = _interopRequireDefault(_untrackedInstagram);

	var _untrackedHtml = __webpack_require__(180);

	var _untrackedHtml2 = _interopRequireDefault(_untrackedHtml);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var RENDER_MAP = {
	  FACEBOOK: _untrackedFacebook2.default,
	  TWEET: _untrackedTwitter2.default,
	  INSTAGRAM: _untrackedInstagram2.default,
	  HTML: _untrackedHtml2.default
	};

	function generateHtmlString(post) {
	  if (RENDER_MAP.hasOwnProperty(post.Type)) {
	    return RENDER_MAP[post.Type](post);
	  }

	  return post.Content || '';
	}

/***/ }),
/* 180 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = generateUntrackedHtmlString;

	var _helperFunctions = __webpack_require__(173);

	function imageOnRight(html) {
	    var cols = html.getElementsByClassName("col-md-6");
	    if (!cols || cols.length !== 2) return false;
	    // There is an image on the right, but not on the left.
	    if (cols[1].getElementsByTagName("img").length && cols[0].getElementsByTagName("img").length === 0) {
	        return true;
	    }
	    return false;
	}

	function generateUntrackedHtmlString(post) {
	    var htmlPost = (0, _helperFunctions.getHtml)(post.Content);
	    var imgOnRight = imageOnRight(htmlPost);
	    return "<div id=\"SL-" + post.Id + "\" class=\"SL-HTML" + (imgOnRight ? " img-right" : "") + " sl-untracked\">" + post.Content + "</div>";
	};

/***/ }),
/* 181 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _htmlGenerator = __webpack_require__(57);

	var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function footer() {
	  return _htmlGenerator2.default.generate('<div class="SL-footer">\n      <a href="http://www.scribblelive.com/products/content-curation-live-blog/">\n        <img src="https://s3.amazonaws.com/scribblelive-visualizations/stencils/27c31655-1058-4fe0-a29e-3ea4277de1b2/poweredby.png" />\n      </a>\n    </div>');
	}

	exports.default = footer;

/***/ }),
/* 182 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	/**
	 * UIConfigService keeps track of the page state and configurations
	 */
	var UIConfigService = function () {
	    function UIConfigService() {
	        (0, _classCallCheck3.default)(this, UIConfigService);
	    }

	    (0, _createClass3.default)(UIConfigService, null, [{
	        key: "init",
	        value: function init(options) {
	            this._options = options;
	        }

	        /**
	         * currentPage returns the last page number loaded for posts
	         */

	    }, {
	        key: "incrementCurrentPage",


	        /**
	         * To be called when a page of posts is loaded
	         * Useful to see how many pages of posts are loaded
	         */
	        value: function incrementCurrentPage() {

	            if (this._options.currentPage == null) {
	                this._options.currentPage = 0;
	            }
	            this._options.currentPage = this._options.currentPage + 1;
	        }

	        /**
	         * Controls whether newest posts should be at bottom of list or not
	         */

	    }, {
	        key: "incrementLastRenderedIndex",


	        /**
	         * Increment the index to point to the recently rendered post
	         */
	        value: function incrementLastRenderedIndex() {
	            if (this._options.lastRenderedIndex == null) {
	                this._options.lastRenderedIndex = -1;
	            }

	            this._options.lastRenderedIndex = this._options.lastRenderedIndex + 1;
	        }
	    }, {
	        key: "currentPage",
	        get: function get() {

	            if (this._options.currentPage == null) {
	                this._options.currentPage = 0;
	            }

	            return this._options.currentPage;
	        }

	        /**
	         * update the pageSize (number of records retrieved)
	         */

	    }, {
	        key: "pageSize",
	        set: function set(size) {
	            this._options.pageSize = size;
	        }

	        /**
	         * Returns the pageSize configured
	         */
	        ,
	        get: function get() {
	            var value = this._options.pageSize ? this._options.pageSize : 10;

	            if (value <= 0 || value > 100) {
	                return 100;
	            }

	            return value;
	        }
	    }, {
	        key: "newestAtBottom",
	        get: function get() {
	            return this._options.newestAtBottom ? this._options.newestAtBottom : false;
	        }

	        /**
	         * Updates the flag to control whether new posts should be first or last
	         */
	        ,
	        set: function set(value) {
	            this._options.newestAtBottom = value ? true : false;
	        }

	        /**
	         * Get the index of the last rendered post
	         */

	    }, {
	        key: "lastRenderedIndex",
	        get: function get() {
	            if (this._options.lastRenderedIndex == null) {
	                this._options.lastRenderedIndex = -1;
	            }

	            return this._options.lastRenderedIndex;
	        }
	    }]);
	    return UIConfigService;
	}();

	exports.default = UIConfigService;

/***/ }),
/* 183 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _extends2 = __webpack_require__(184);

	var _extends3 = _interopRequireDefault(_extends2);

	exports.addStreamIds = addStreamIds;
	exports.formatDate = formatDate;

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function addStreamIds(posts, streamId) {
	  return posts.map(function (post) {
	    return (0, _extends3.default)({}, post, { StreamId: post.StreamId || streamId });
	  });
	}

	function formatDate(dateVal) {
	  var date = dateVal;
	  if (typeof date.getMonth !== 'function') {
	    // assume string provided
	    try {
	      date = new Date(dateVal);
	      if (isNaN(date.getMonth())) {
	        date = new Date(dateVal.replace(/\+/g, '.'));
	      }
	      // For IE
	      if (isNaN(date.getMonth())) {
	        var dateArr = dateVal.split(' ');
	        date = new Date(Date.parse(dateArr[1] + ' ' + dateArr[2] + ', ' + dateArr[5] + ' ' + dateArr[3] + ' UTC'));
	      }
	    } catch (e) {
	      return '';
	    }
	  }

	  var lang = window.navigator.userLanguage || window.navigator.language;

	  var hour = date.getHours();
	  var displayHours = hour === 0 ? 12 : hour;
	  var mins = date.getMinutes();
	  var displayMins = mins.toString().length > 1 ? mins : '0' + mins;
	  var displayTime = displayHours > 12 ? displayHours - 12 + ':' + displayMins + ' PM' : displayHours + ':' + displayMins + ' AM';
	  var day = date.getDate();
	  var month = date.toLocaleString(lang, { month: 'long' }).substr(0, 3);
	  var year = date.getFullYear();

	  return displayTime + ' - ' + day + ' ' + month + ' ' + year;
	}

/***/ }),
/* 184 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	exports.__esModule = true;

	var _assign = __webpack_require__(50);

	var _assign2 = _interopRequireDefault(_assign);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = _assign2.default || function (target) {
	  for (var i = 1; i < arguments.length; i++) {
	    var source = arguments[i];

	    for (var key in source) {
	      if (Object.prototype.hasOwnProperty.call(source, key)) {
	        target[key] = source[key];
	      }
	    }
	  }

	  return target;
	};

/***/ }),
/* 185 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _classCallCheck2 = __webpack_require__(1);

	var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

	var _createClass2 = __webpack_require__(2);

	var _createClass3 = _interopRequireDefault(_createClass2);

	var _postStore = __webpack_require__(28);

	var _postStore2 = _interopRequireDefault(_postStore);

	var _jsCookie = __webpack_require__(186);

	var _jsCookie2 = _interopRequireDefault(_jsCookie);

	var _consentForm2 = __webpack_require__(187);

	var _consentForm3 = _interopRequireDefault(_consentForm2);

	var _renderingOptions = __webpack_require__(29);

	var _renderingOptions2 = _interopRequireDefault(_renderingOptions);

	var _axios = __webpack_require__(188);

	var _axios2 = _interopRequireDefault(_axios);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var ConsentFormService = function () {
	    function ConsentFormService() {
	        (0, _classCallCheck3.default)(this, ConsentFormService);
	    }

	    (0, _createClass3.default)(ConsentFormService, null, [{
	        key: 'init',
	        value: function init(_ref) {
	            var token = _ref.token,
	                baseUrl = _ref.baseUrl;

	            this.token = token;
	            this.baseUrl = baseUrl || 'https://api.scribblelive.com/v1';
	        }
	    }, {
	        key: 'getClientData',
	        value: function getClientData() {
	            var _this = this;

	            _postStore2.default.consentFormData.loading = true;
	            return (0, _axios2.default)(this.baseUrl + '/client?token=' + this.token + '&includePrivacySettings=true').then(function (res) {
	                var data = res.data;
	                if (data) _postStore2.default.clientData = data;
	                _this.checkCookies();
	                _postStore2.default.consentFormData.loading = false;
	                _this.renderConsentForm();
	            }).catch(function (error) {
	                _postStore2.default.consentFormData.loading = false;
	                _this.renderConsentForm();
	                throw error;
	            });
	        }
	    }, {
	        key: 'checkCookies',
	        value: function checkCookies() {
	            var clientData = _postStore2.default.clientData ? _postStore2.default.clientData : null;
	            if (clientData && clientData.Id && clientData.ConsentForm === '1' && clientData.ConsentFormLastModified) {
	                var clientId = clientData.Id;
	                var cookieData = _jsCookie2.default.get();
	                var filteredPosts = cookieData[this.getFilteredPostField(clientId)];
	                var lastModified = cookieData[this.getFormLastModifiedField(clientId)];
	                if (!filteredPosts || !lastModified) {
	                    _postStore2.default.consentFormData.showModal = true;
	                    _postStore2.default.consentFormData.filterPost = false;
	                } else if (new Date(clientData.ConsentFormLastModified) > new Date(lastModified)) {
	                    _postStore2.default.consentFormData.showModal = true;
	                    _postStore2.default.consentFormData.filterPost = filteredPosts === 'true';
	                } else {
	                    _postStore2.default.consentFormData.showModal = false;
	                    _postStore2.default.consentFormData.filterPost = filteredPosts === 'true';
	                }
	            }
	        }
	    }, {
	        key: 'handleAccept',
	        value: function handleAccept() {
	            var clientId = _postStore2.default.clientData.Id;
	            _jsCookie2.default.set(this.getFilteredPostField(clientId), false);
	            _jsCookie2.default.set(this.getFormLastModifiedField(clientId), new Date().toUTCString());
	            _postStore2.default.consentFormData.filterPost = false;
	            location.reload();
	        }
	    }, {
	        key: 'handleReject',
	        value: function handleReject() {
	            var clientId = _postStore2.default.clientData.Id;
	            _jsCookie2.default.set(this.getFilteredPostField(clientId), true);
	            _jsCookie2.default.set(this.getFormLastModifiedField(clientId), new Date().toUTCString());
	            _postStore2.default.consentFormData.filterPost = true;
	            location.reload();
	        }
	    }, {
	        key: 'renderConsentForm',
	        value: function renderConsentForm() {
	            if (_postStore2.default.consentFormData.showModal) {
	                var consentForm = document.getElementById("consentForm");
	                if (!consentForm) {
	                    consentForm = document.createElement('div');
	                    consentForm.setAttribute("id", "consentForm");
	                    _renderingOptions2.default.rootEl.appendChild(consentForm);
	                }
	                consentForm.innerHTML = (0, _consentForm3.default)(_postStore2.default.clientData, this.handleAccept.bind(this), this.handleReject.bind(this));
	            } else {
	                var _consentForm = document.getElementById("consentForm");
	                if (_consentForm) _consentForm.innerHTML = '';
	                document.body.style.background = 'none';
	            }
	        }
	    }, {
	        key: 'getFilteredPostField',
	        value: function getFilteredPostField(clientId) {
	            return 'FilteredTrackingPost_' + clientId;
	        }
	    }, {
	        key: 'getFormLastModifiedField',
	        value: function getFormLastModifiedField(clientId) {
	            return 'ConsentFormLastModified_' + clientId;
	        }
	    }]);
	    return ConsentFormService;
	}();

	exports.default = ConsentFormService;

/***/ }),
/* 186 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
	 * JavaScript Cookie v2.2.1
	 * https://github.com/js-cookie/js-cookie
	 *
	 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
	 * Released under the MIT license
	 */
	;(function (factory) {
		var registeredInModuleLoader;
		if (true) {
			!(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
			registeredInModuleLoader = true;
		}
		if (true) {
			module.exports = factory();
			registeredInModuleLoader = true;
		}
		if (!registeredInModuleLoader) {
			var OldCookies = window.Cookies;
			var api = window.Cookies = factory();
			api.noConflict = function () {
				window.Cookies = OldCookies;
				return api;
			};
		}
	}(function () {
		function extend () {
			var i = 0;
			var result = {};
			for (; i < arguments.length; i++) {
				var attributes = arguments[ i ];
				for (var key in attributes) {
					result[key] = attributes[key];
				}
			}
			return result;
		}

		function decode (s) {
			return s.replace(/(%[0-9A-Z]{2})+/g, decodeURIComponent);
		}

		function init (converter) {
			function api() {}

			function set (key, value, attributes) {
				if (typeof document === 'undefined') {
					return;
				}

				attributes = extend({
					path: '/'
				}, api.defaults, attributes);

				if (typeof attributes.expires === 'number') {
					attributes.expires = new Date(new Date() * 1 + attributes.expires * 864e+5);
				}

				// We're using "expires" because "max-age" is not supported by IE
				attributes.expires = attributes.expires ? attributes.expires.toUTCString() : '';

				try {
					var result = JSON.stringify(value);
					if (/^[\{\[]/.test(result)) {
						value = result;
					}
				} catch (e) {}

				value = converter.write ?
					converter.write(value, key) :
					encodeURIComponent(String(value))
						.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

				key = encodeURIComponent(String(key))
					.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent)
					.replace(/[\(\)]/g, escape);

				var stringifiedAttributes = '';
				for (var attributeName in attributes) {
					if (!attributes[attributeName]) {
						continue;
					}
					stringifiedAttributes += '; ' + attributeName;
					if (attributes[attributeName] === true) {
						continue;
					}

					// Considers RFC 6265 section 5.2:
					// ...
					// 3.  If the remaining unparsed-attributes contains a %x3B (";")
					//     character:
					// Consume the characters of the unparsed-attributes up to,
					// not including, the first %x3B (";") character.
					// ...
					stringifiedAttributes += '=' + attributes[attributeName].split(';')[0];
				}

				return (document.cookie = key + '=' + value + stringifiedAttributes);
			}

			function get (key, json) {
				if (typeof document === 'undefined') {
					return;
				}

				var jar = {};
				// To prevent the for loop in the first place assign an empty array
				// in case there are no cookies at all.
				var cookies = document.cookie ? document.cookie.split('; ') : [];
				var i = 0;

				for (; i < cookies.length; i++) {
					var parts = cookies[i].split('=');
					var cookie = parts.slice(1).join('=');

					if (!json && cookie.charAt(0) === '"') {
						cookie = cookie.slice(1, -1);
					}

					try {
						var name = decode(parts[0]);
						cookie = (converter.read || converter)(cookie, name) ||
							decode(cookie);

						if (json) {
							try {
								cookie = JSON.parse(cookie);
							} catch (e) {}
						}

						jar[name] = cookie;

						if (key === name) {
							break;
						}
					} catch (e) {}
				}

				return key ? jar[key] : jar;
			}

			api.set = set;
			api.get = function (key) {
				return get(key, false /* read as raw */);
			};
			api.getJSON = function (key) {
				return get(key, true /* read as json */);
			};
			api.remove = function (key, attributes) {
				set(key, '', extend(attributes, {
					expires: -1
				}));
			};

			api.defaults = {};

			api.withConverter = init;

			return api;
		}

		return init(function () {});
	}));


/***/ }),
/* 187 */
/***/ (function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var ConsentForm = function ConsentForm(data, accept, reject) {
	    setTimeout(function () {
	        document.getElementById("accept_button").addEventListener('click', accept);
	        document.getElementById("reject_button").addEventListener('click', reject);
	    });

	    return "\n    <div id=\"consent-form-pinboard\">\n        <div class=\"consent-form\">\n            <div>\n                <h1>" + data.ConsentFormTitle + "</h1>\n            </div>\n            <div>\n                " + data.ConsentFormText + "\n            </div>\n            <br />\n            <div class=\"consent-form-buttons\">\n                <button id=\"accept_button\" class=\"consent-form-button consent-form-button__text\">" + data.ConsentFormConfirmText + "</button>\n                <button id=\"reject_button\" class=\"consent-form-button consent-form-button__text\">" + data.ConsentFormCancelText + "</button>\n            </div>\n        </div>\n    </div>";
	};

	exports.default = ConsentForm;

/***/ }),
/* 188 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(189);

/***/ }),
/* 189 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);
	var bind = __webpack_require__(191);
	var Axios = __webpack_require__(193);
	var mergeConfig = __webpack_require__(210);
	var defaults = __webpack_require__(199);

	/**
	 * Create an instance of Axios
	 *
	 * @param {Object} defaultConfig The default config for the instance
	 * @return {Axios} A new instance of Axios
	 */
	function createInstance(defaultConfig) {
	  var context = new Axios(defaultConfig);
	  var instance = bind(Axios.prototype.request, context);

	  // Copy axios.prototype to instance
	  utils.extend(instance, Axios.prototype, context);

	  // Copy context to instance
	  utils.extend(instance, context);

	  return instance;
	}

	// Create the default instance to be exported
	var axios = createInstance(defaults);

	// Expose Axios class to allow class inheritance
	axios.Axios = Axios;

	// Factory for creating new instances
	axios.create = function create(instanceConfig) {
	  return createInstance(mergeConfig(axios.defaults, instanceConfig));
	};

	// Expose Cancel & CancelToken
	axios.Cancel = __webpack_require__(211);
	axios.CancelToken = __webpack_require__(212);
	axios.isCancel = __webpack_require__(198);

	// Expose all/spread
	axios.all = function all(promises) {
	  return Promise.all(promises);
	};
	axios.spread = __webpack_require__(213);

	module.exports = axios;

	// Allow use of default import syntax in TypeScript
	module.exports.default = axios;


/***/ }),
/* 190 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var bind = __webpack_require__(191);
	var isBuffer = __webpack_require__(192);

	/*global toString:true*/

	// utils is a library of generic helper functions non-specific to axios

	var toString = Object.prototype.toString;

	/**
	 * Determine if a value is an Array
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an Array, otherwise false
	 */
	function isArray(val) {
	  return toString.call(val) === '[object Array]';
	}

	/**
	 * Determine if a value is an ArrayBuffer
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
	 */
	function isArrayBuffer(val) {
	  return toString.call(val) === '[object ArrayBuffer]';
	}

	/**
	 * Determine if a value is a FormData
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an FormData, otherwise false
	 */
	function isFormData(val) {
	  return (typeof FormData !== 'undefined') && (val instanceof FormData);
	}

	/**
	 * Determine if a value is a view on an ArrayBuffer
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
	 */
	function isArrayBufferView(val) {
	  var result;
	  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
	    result = ArrayBuffer.isView(val);
	  } else {
	    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
	  }
	  return result;
	}

	/**
	 * Determine if a value is a String
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a String, otherwise false
	 */
	function isString(val) {
	  return typeof val === 'string';
	}

	/**
	 * Determine if a value is a Number
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Number, otherwise false
	 */
	function isNumber(val) {
	  return typeof val === 'number';
	}

	/**
	 * Determine if a value is undefined
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if the value is undefined, otherwise false
	 */
	function isUndefined(val) {
	  return typeof val === 'undefined';
	}

	/**
	 * Determine if a value is an Object
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is an Object, otherwise false
	 */
	function isObject(val) {
	  return val !== null && typeof val === 'object';
	}

	/**
	 * Determine if a value is a Date
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Date, otherwise false
	 */
	function isDate(val) {
	  return toString.call(val) === '[object Date]';
	}

	/**
	 * Determine if a value is a File
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a File, otherwise false
	 */
	function isFile(val) {
	  return toString.call(val) === '[object File]';
	}

	/**
	 * Determine if a value is a Blob
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Blob, otherwise false
	 */
	function isBlob(val) {
	  return toString.call(val) === '[object Blob]';
	}

	/**
	 * Determine if a value is a Function
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Function, otherwise false
	 */
	function isFunction(val) {
	  return toString.call(val) === '[object Function]';
	}

	/**
	 * Determine if a value is a Stream
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a Stream, otherwise false
	 */
	function isStream(val) {
	  return isObject(val) && isFunction(val.pipe);
	}

	/**
	 * Determine if a value is a URLSearchParams object
	 *
	 * @param {Object} val The value to test
	 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
	 */
	function isURLSearchParams(val) {
	  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
	}

	/**
	 * Trim excess whitespace off the beginning and end of a string
	 *
	 * @param {String} str The String to trim
	 * @returns {String} The String freed of excess whitespace
	 */
	function trim(str) {
	  return str.replace(/^\s*/, '').replace(/\s*$/, '');
	}

	/**
	 * Determine if we're running in a standard browser environment
	 *
	 * This allows axios to run in a web worker, and react-native.
	 * Both environments support XMLHttpRequest, but not fully standard globals.
	 *
	 * web workers:
	 *  typeof window -> undefined
	 *  typeof document -> undefined
	 *
	 * react-native:
	 *  navigator.product -> 'ReactNative'
	 * nativescript
	 *  navigator.product -> 'NativeScript' or 'NS'
	 */
	function isStandardBrowserEnv() {
	  if (typeof navigator !== 'undefined' && (navigator.product === 'ReactNative' ||
	                                           navigator.product === 'NativeScript' ||
	                                           navigator.product === 'NS')) {
	    return false;
	  }
	  return (
	    typeof window !== 'undefined' &&
	    typeof document !== 'undefined'
	  );
	}

	/**
	 * Iterate over an Array or an Object invoking a function for each item.
	 *
	 * If `obj` is an Array callback will be called passing
	 * the value, index, and complete array for each item.
	 *
	 * If 'obj' is an Object callback will be called passing
	 * the value, key, and complete object for each property.
	 *
	 * @param {Object|Array} obj The object to iterate
	 * @param {Function} fn The callback to invoke for each item
	 */
	function forEach(obj, fn) {
	  // Don't bother if no value provided
	  if (obj === null || typeof obj === 'undefined') {
	    return;
	  }

	  // Force an array if not already something iterable
	  if (typeof obj !== 'object') {
	    /*eslint no-param-reassign:0*/
	    obj = [obj];
	  }

	  if (isArray(obj)) {
	    // Iterate over array values
	    for (var i = 0, l = obj.length; i < l; i++) {
	      fn.call(null, obj[i], i, obj);
	    }
	  } else {
	    // Iterate over object keys
	    for (var key in obj) {
	      if (Object.prototype.hasOwnProperty.call(obj, key)) {
	        fn.call(null, obj[key], key, obj);
	      }
	    }
	  }
	}

	/**
	 * Accepts varargs expecting each argument to be an object, then
	 * immutably merges the properties of each object and returns result.
	 *
	 * When multiple objects contain the same key the later object in
	 * the arguments list will take precedence.
	 *
	 * Example:
	 *
	 * ```js
	 * var result = merge({foo: 123}, {foo: 456});
	 * console.log(result.foo); // outputs 456
	 * ```
	 *
	 * @param {Object} obj1 Object to merge
	 * @returns {Object} Result of all merge properties
	 */
	function merge(/* obj1, obj2, obj3, ... */) {
	  var result = {};
	  function assignValue(val, key) {
	    if (typeof result[key] === 'object' && typeof val === 'object') {
	      result[key] = merge(result[key], val);
	    } else {
	      result[key] = val;
	    }
	  }

	  for (var i = 0, l = arguments.length; i < l; i++) {
	    forEach(arguments[i], assignValue);
	  }
	  return result;
	}

	/**
	 * Function equal to merge with the difference being that no reference
	 * to original objects is kept.
	 *
	 * @see merge
	 * @param {Object} obj1 Object to merge
	 * @returns {Object} Result of all merge properties
	 */
	function deepMerge(/* obj1, obj2, obj3, ... */) {
	  var result = {};
	  function assignValue(val, key) {
	    if (typeof result[key] === 'object' && typeof val === 'object') {
	      result[key] = deepMerge(result[key], val);
	    } else if (typeof val === 'object') {
	      result[key] = deepMerge({}, val);
	    } else {
	      result[key] = val;
	    }
	  }

	  for (var i = 0, l = arguments.length; i < l; i++) {
	    forEach(arguments[i], assignValue);
	  }
	  return result;
	}

	/**
	 * Extends object a by mutably adding to it the properties of object b.
	 *
	 * @param {Object} a The object to be extended
	 * @param {Object} b The object to copy properties from
	 * @param {Object} thisArg The object to bind function to
	 * @return {Object} The resulting value of object a
	 */
	function extend(a, b, thisArg) {
	  forEach(b, function assignValue(val, key) {
	    if (thisArg && typeof val === 'function') {
	      a[key] = bind(val, thisArg);
	    } else {
	      a[key] = val;
	    }
	  });
	  return a;
	}

	module.exports = {
	  isArray: isArray,
	  isArrayBuffer: isArrayBuffer,
	  isBuffer: isBuffer,
	  isFormData: isFormData,
	  isArrayBufferView: isArrayBufferView,
	  isString: isString,
	  isNumber: isNumber,
	  isObject: isObject,
	  isUndefined: isUndefined,
	  isDate: isDate,
	  isFile: isFile,
	  isBlob: isBlob,
	  isFunction: isFunction,
	  isStream: isStream,
	  isURLSearchParams: isURLSearchParams,
	  isStandardBrowserEnv: isStandardBrowserEnv,
	  forEach: forEach,
	  merge: merge,
	  deepMerge: deepMerge,
	  extend: extend,
	  trim: trim
	};


/***/ }),
/* 191 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function bind(fn, thisArg) {
	  return function wrap() {
	    var args = new Array(arguments.length);
	    for (var i = 0; i < args.length; i++) {
	      args[i] = arguments[i];
	    }
	    return fn.apply(thisArg, args);
	  };
	};


/***/ }),
/* 192 */
/***/ (function(module, exports) {

	/*!
	 * Determine if an object is a Buffer
	 *
	 * @author   Feross Aboukhadijeh <https://feross.org>
	 * @license  MIT
	 */

	module.exports = function isBuffer (obj) {
	  return obj != null && obj.constructor != null &&
	    typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
	}


/***/ }),
/* 193 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);
	var buildURL = __webpack_require__(194);
	var InterceptorManager = __webpack_require__(195);
	var dispatchRequest = __webpack_require__(196);
	var mergeConfig = __webpack_require__(210);

	/**
	 * Create a new instance of Axios
	 *
	 * @param {Object} instanceConfig The default config for the instance
	 */
	function Axios(instanceConfig) {
	  this.defaults = instanceConfig;
	  this.interceptors = {
	    request: new InterceptorManager(),
	    response: new InterceptorManager()
	  };
	}

	/**
	 * Dispatch a request
	 *
	 * @param {Object} config The config specific for this request (merged with this.defaults)
	 */
	Axios.prototype.request = function request(config) {
	  /*eslint no-param-reassign:0*/
	  // Allow for axios('example/url'[, config]) a la fetch API
	  if (typeof config === 'string') {
	    config = arguments[1] || {};
	    config.url = arguments[0];
	  } else {
	    config = config || {};
	  }

	  config = mergeConfig(this.defaults, config);
	  config.method = config.method ? config.method.toLowerCase() : 'get';

	  // Hook up interceptors middleware
	  var chain = [dispatchRequest, undefined];
	  var promise = Promise.resolve(config);

	  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
	    chain.unshift(interceptor.fulfilled, interceptor.rejected);
	  });

	  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
	    chain.push(interceptor.fulfilled, interceptor.rejected);
	  });

	  while (chain.length) {
	    promise = promise.then(chain.shift(), chain.shift());
	  }

	  return promise;
	};

	Axios.prototype.getUri = function getUri(config) {
	  config = mergeConfig(this.defaults, config);
	  return buildURL(config.url, config.params, config.paramsSerializer).replace(/^\?/, '');
	};

	// Provide aliases for supported request methods
	utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
	  /*eslint func-names:0*/
	  Axios.prototype[method] = function(url, config) {
	    return this.request(utils.merge(config || {}, {
	      method: method,
	      url: url
	    }));
	  };
	});

	utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
	  /*eslint func-names:0*/
	  Axios.prototype[method] = function(url, data, config) {
	    return this.request(utils.merge(config || {}, {
	      method: method,
	      url: url,
	      data: data
	    }));
	  };
	});

	module.exports = Axios;


/***/ }),
/* 194 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);

	function encode(val) {
	  return encodeURIComponent(val).
	    replace(/%40/gi, '@').
	    replace(/%3A/gi, ':').
	    replace(/%24/g, '$').
	    replace(/%2C/gi, ',').
	    replace(/%20/g, '+').
	    replace(/%5B/gi, '[').
	    replace(/%5D/gi, ']');
	}

	/**
	 * Build a URL by appending params to the end
	 *
	 * @param {string} url The base of the url (e.g., http://www.google.com)
	 * @param {object} [params] The params to be appended
	 * @returns {string} The formatted url
	 */
	module.exports = function buildURL(url, params, paramsSerializer) {
	  /*eslint no-param-reassign:0*/
	  if (!params) {
	    return url;
	  }

	  var serializedParams;
	  if (paramsSerializer) {
	    serializedParams = paramsSerializer(params);
	  } else if (utils.isURLSearchParams(params)) {
	    serializedParams = params.toString();
	  } else {
	    var parts = [];

	    utils.forEach(params, function serialize(val, key) {
	      if (val === null || typeof val === 'undefined') {
	        return;
	      }

	      if (utils.isArray(val)) {
	        key = key + '[]';
	      } else {
	        val = [val];
	      }

	      utils.forEach(val, function parseValue(v) {
	        if (utils.isDate(v)) {
	          v = v.toISOString();
	        } else if (utils.isObject(v)) {
	          v = JSON.stringify(v);
	        }
	        parts.push(encode(key) + '=' + encode(v));
	      });
	    });

	    serializedParams = parts.join('&');
	  }

	  if (serializedParams) {
	    var hashmarkIndex = url.indexOf('#');
	    if (hashmarkIndex !== -1) {
	      url = url.slice(0, hashmarkIndex);
	    }

	    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
	  }

	  return url;
	};


/***/ }),
/* 195 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);

	function InterceptorManager() {
	  this.handlers = [];
	}

	/**
	 * Add a new interceptor to the stack
	 *
	 * @param {Function} fulfilled The function to handle `then` for a `Promise`
	 * @param {Function} rejected The function to handle `reject` for a `Promise`
	 *
	 * @return {Number} An ID used to remove interceptor later
	 */
	InterceptorManager.prototype.use = function use(fulfilled, rejected) {
	  this.handlers.push({
	    fulfilled: fulfilled,
	    rejected: rejected
	  });
	  return this.handlers.length - 1;
	};

	/**
	 * Remove an interceptor from the stack
	 *
	 * @param {Number} id The ID that was returned by `use`
	 */
	InterceptorManager.prototype.eject = function eject(id) {
	  if (this.handlers[id]) {
	    this.handlers[id] = null;
	  }
	};

	/**
	 * Iterate over all the registered interceptors
	 *
	 * This method is particularly useful for skipping over any
	 * interceptors that may have become `null` calling `eject`.
	 *
	 * @param {Function} fn The function to call for each interceptor
	 */
	InterceptorManager.prototype.forEach = function forEach(fn) {
	  utils.forEach(this.handlers, function forEachHandler(h) {
	    if (h !== null) {
	      fn(h);
	    }
	  });
	};

	module.exports = InterceptorManager;


/***/ }),
/* 196 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);
	var transformData = __webpack_require__(197);
	var isCancel = __webpack_require__(198);
	var defaults = __webpack_require__(199);
	var isAbsoluteURL = __webpack_require__(208);
	var combineURLs = __webpack_require__(209);

	/**
	 * Throws a `Cancel` if cancellation has been requested.
	 */
	function throwIfCancellationRequested(config) {
	  if (config.cancelToken) {
	    config.cancelToken.throwIfRequested();
	  }
	}

	/**
	 * Dispatch a request to the server using the configured adapter.
	 *
	 * @param {object} config The config that is to be used for the request
	 * @returns {Promise} The Promise to be fulfilled
	 */
	module.exports = function dispatchRequest(config) {
	  throwIfCancellationRequested(config);

	  // Support baseURL config
	  if (config.baseURL && !isAbsoluteURL(config.url)) {
	    config.url = combineURLs(config.baseURL, config.url);
	  }

	  // Ensure headers exist
	  config.headers = config.headers || {};

	  // Transform request data
	  config.data = transformData(
	    config.data,
	    config.headers,
	    config.transformRequest
	  );

	  // Flatten headers
	  config.headers = utils.merge(
	    config.headers.common || {},
	    config.headers[config.method] || {},
	    config.headers || {}
	  );

	  utils.forEach(
	    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
	    function cleanHeaderConfig(method) {
	      delete config.headers[method];
	    }
	  );

	  var adapter = config.adapter || defaults.adapter;

	  return adapter(config).then(function onAdapterResolution(response) {
	    throwIfCancellationRequested(config);

	    // Transform response data
	    response.data = transformData(
	      response.data,
	      response.headers,
	      config.transformResponse
	    );

	    return response;
	  }, function onAdapterRejection(reason) {
	    if (!isCancel(reason)) {
	      throwIfCancellationRequested(config);

	      // Transform response data
	      if (reason && reason.response) {
	        reason.response.data = transformData(
	          reason.response.data,
	          reason.response.headers,
	          config.transformResponse
	        );
	      }
	    }

	    return Promise.reject(reason);
	  });
	};


/***/ }),
/* 197 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);

	/**
	 * Transform the data for a request or a response
	 *
	 * @param {Object|String} data The data to be transformed
	 * @param {Array} headers The headers for the request or response
	 * @param {Array|Function} fns A single function or Array of functions
	 * @returns {*} The resulting transformed data
	 */
	module.exports = function transformData(data, headers, fns) {
	  /*eslint no-param-reassign:0*/
	  utils.forEach(fns, function transform(fn) {
	    data = fn(data, headers);
	  });

	  return data;
	};


/***/ }),
/* 198 */
/***/ (function(module, exports) {

	'use strict';

	module.exports = function isCancel(value) {
	  return !!(value && value.__CANCEL__);
	};


/***/ }),
/* 199 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {'use strict';

	var utils = __webpack_require__(190);
	var normalizeHeaderName = __webpack_require__(200);

	var DEFAULT_CONTENT_TYPE = {
	  'Content-Type': 'application/x-www-form-urlencoded'
	};

	function setContentTypeIfUnset(headers, value) {
	  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
	    headers['Content-Type'] = value;
	  }
	}

	function getDefaultAdapter() {
	  var adapter;
	  // Only Node.JS has a process variable that is of [[Class]] process
	  if (typeof process !== 'undefined' && Object.prototype.toString.call(process) === '[object process]') {
	    // For node use HTTP adapter
	    adapter = __webpack_require__(201);
	  } else if (typeof XMLHttpRequest !== 'undefined') {
	    // For browsers use XHR adapter
	    adapter = __webpack_require__(201);
	  }
	  return adapter;
	}

	var defaults = {
	  adapter: getDefaultAdapter(),

	  transformRequest: [function transformRequest(data, headers) {
	    normalizeHeaderName(headers, 'Accept');
	    normalizeHeaderName(headers, 'Content-Type');
	    if (utils.isFormData(data) ||
	      utils.isArrayBuffer(data) ||
	      utils.isBuffer(data) ||
	      utils.isStream(data) ||
	      utils.isFile(data) ||
	      utils.isBlob(data)
	    ) {
	      return data;
	    }
	    if (utils.isArrayBufferView(data)) {
	      return data.buffer;
	    }
	    if (utils.isURLSearchParams(data)) {
	      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
	      return data.toString();
	    }
	    if (utils.isObject(data)) {
	      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
	      return JSON.stringify(data);
	    }
	    return data;
	  }],

	  transformResponse: [function transformResponse(data) {
	    /*eslint no-param-reassign:0*/
	    if (typeof data === 'string') {
	      try {
	        data = JSON.parse(data);
	      } catch (e) { /* Ignore */ }
	    }
	    return data;
	  }],

	  /**
	   * A timeout in milliseconds to abort a request. If set to 0 (default) a
	   * timeout is not created.
	   */
	  timeout: 0,

	  xsrfCookieName: 'XSRF-TOKEN',
	  xsrfHeaderName: 'X-XSRF-TOKEN',

	  maxContentLength: -1,

	  validateStatus: function validateStatus(status) {
	    return status >= 200 && status < 300;
	  }
	};

	defaults.headers = {
	  common: {
	    'Accept': 'application/json, text/plain, */*'
	  }
	};

	utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
	  defaults.headers[method] = {};
	});

	utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
	  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
	});

	module.exports = defaults;

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(23)))

/***/ }),
/* 200 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);

	module.exports = function normalizeHeaderName(headers, normalizedName) {
	  utils.forEach(headers, function processHeader(value, name) {
	    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
	      headers[normalizedName] = value;
	      delete headers[name];
	    }
	  });
	};


/***/ }),
/* 201 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);
	var settle = __webpack_require__(202);
	var buildURL = __webpack_require__(194);
	var parseHeaders = __webpack_require__(205);
	var isURLSameOrigin = __webpack_require__(206);
	var createError = __webpack_require__(203);

	module.exports = function xhrAdapter(config) {
	  return new Promise(function dispatchXhrRequest(resolve, reject) {
	    var requestData = config.data;
	    var requestHeaders = config.headers;

	    if (utils.isFormData(requestData)) {
	      delete requestHeaders['Content-Type']; // Let the browser set it
	    }

	    var request = new XMLHttpRequest();

	    // HTTP basic authentication
	    if (config.auth) {
	      var username = config.auth.username || '';
	      var password = config.auth.password || '';
	      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
	    }

	    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);

	    // Set the request timeout in MS
	    request.timeout = config.timeout;

	    // Listen for ready state
	    request.onreadystatechange = function handleLoad() {
	      if (!request || request.readyState !== 4) {
	        return;
	      }

	      // The request errored out and we didn't get a response, this will be
	      // handled by onerror instead
	      // With one exception: request that using file: protocol, most browsers
	      // will return status as 0 even though it's a successful request
	      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
	        return;
	      }

	      // Prepare the response
	      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
	      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
	      var response = {
	        data: responseData,
	        status: request.status,
	        statusText: request.statusText,
	        headers: responseHeaders,
	        config: config,
	        request: request
	      };

	      settle(resolve, reject, response);

	      // Clean up request
	      request = null;
	    };

	    // Handle browser request cancellation (as opposed to a manual cancellation)
	    request.onabort = function handleAbort() {
	      if (!request) {
	        return;
	      }

	      reject(createError('Request aborted', config, 'ECONNABORTED', request));

	      // Clean up request
	      request = null;
	    };

	    // Handle low level network errors
	    request.onerror = function handleError() {
	      // Real errors are hidden from us by the browser
	      // onerror should only fire if it's a network error
	      reject(createError('Network Error', config, null, request));

	      // Clean up request
	      request = null;
	    };

	    // Handle timeout
	    request.ontimeout = function handleTimeout() {
	      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED',
	        request));

	      // Clean up request
	      request = null;
	    };

	    // Add xsrf header
	    // This is only done if running in a standard browser environment.
	    // Specifically not if we're in a web worker, or react-native.
	    if (utils.isStandardBrowserEnv()) {
	      var cookies = __webpack_require__(207);

	      // Add xsrf header
	      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ?
	        cookies.read(config.xsrfCookieName) :
	        undefined;

	      if (xsrfValue) {
	        requestHeaders[config.xsrfHeaderName] = xsrfValue;
	      }
	    }

	    // Add headers to the request
	    if ('setRequestHeader' in request) {
	      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
	        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
	          // Remove Content-Type if data is undefined
	          delete requestHeaders[key];
	        } else {
	          // Otherwise add header to the request
	          request.setRequestHeader(key, val);
	        }
	      });
	    }

	    // Add withCredentials to request if needed
	    if (config.withCredentials) {
	      request.withCredentials = true;
	    }

	    // Add responseType to request if needed
	    if (config.responseType) {
	      try {
	        request.responseType = config.responseType;
	      } catch (e) {
	        // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
	        // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
	        if (config.responseType !== 'json') {
	          throw e;
	        }
	      }
	    }

	    // Handle progress if needed
	    if (typeof config.onDownloadProgress === 'function') {
	      request.addEventListener('progress', config.onDownloadProgress);
	    }

	    // Not all browsers support upload events
	    if (typeof config.onUploadProgress === 'function' && request.upload) {
	      request.upload.addEventListener('progress', config.onUploadProgress);
	    }

	    if (config.cancelToken) {
	      // Handle cancellation
	      config.cancelToken.promise.then(function onCanceled(cancel) {
	        if (!request) {
	          return;
	        }

	        request.abort();
	        reject(cancel);
	        // Clean up request
	        request = null;
	      });
	    }

	    if (requestData === undefined) {
	      requestData = null;
	    }

	    // Send the request
	    request.send(requestData);
	  });
	};


/***/ }),
/* 202 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var createError = __webpack_require__(203);

	/**
	 * Resolve or reject a Promise based on response status.
	 *
	 * @param {Function} resolve A function that resolves the promise.
	 * @param {Function} reject A function that rejects the promise.
	 * @param {object} response The response.
	 */
	module.exports = function settle(resolve, reject, response) {
	  var validateStatus = response.config.validateStatus;
	  if (!validateStatus || validateStatus(response.status)) {
	    resolve(response);
	  } else {
	    reject(createError(
	      'Request failed with status code ' + response.status,
	      response.config,
	      null,
	      response.request,
	      response
	    ));
	  }
	};


/***/ }),
/* 203 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var enhanceError = __webpack_require__(204);

	/**
	 * Create an Error with the specified message, config, error code, request and response.
	 *
	 * @param {string} message The error message.
	 * @param {Object} config The config.
	 * @param {string} [code] The error code (for example, 'ECONNABORTED').
	 * @param {Object} [request] The request.
	 * @param {Object} [response] The response.
	 * @returns {Error} The created error.
	 */
	module.exports = function createError(message, config, code, request, response) {
	  var error = new Error(message);
	  return enhanceError(error, config, code, request, response);
	};


/***/ }),
/* 204 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Update an Error with the specified config, error code, and response.
	 *
	 * @param {Error} error The error to update.
	 * @param {Object} config The config.
	 * @param {string} [code] The error code (for example, 'ECONNABORTED').
	 * @param {Object} [request] The request.
	 * @param {Object} [response] The response.
	 * @returns {Error} The error.
	 */
	module.exports = function enhanceError(error, config, code, request, response) {
	  error.config = config;
	  if (code) {
	    error.code = code;
	  }

	  error.request = request;
	  error.response = response;
	  error.isAxiosError = true;

	  error.toJSON = function() {
	    return {
	      // Standard
	      message: this.message,
	      name: this.name,
	      // Microsoft
	      description: this.description,
	      number: this.number,
	      // Mozilla
	      fileName: this.fileName,
	      lineNumber: this.lineNumber,
	      columnNumber: this.columnNumber,
	      stack: this.stack,
	      // Axios
	      config: this.config,
	      code: this.code
	    };
	  };
	  return error;
	};


/***/ }),
/* 205 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);

	// Headers whose duplicates are ignored by node
	// c.f. https://nodejs.org/api/http.html#http_message_headers
	var ignoreDuplicateOf = [
	  'age', 'authorization', 'content-length', 'content-type', 'etag',
	  'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since',
	  'last-modified', 'location', 'max-forwards', 'proxy-authorization',
	  'referer', 'retry-after', 'user-agent'
	];

	/**
	 * Parse headers into an object
	 *
	 * ```
	 * Date: Wed, 27 Aug 2014 08:58:49 GMT
	 * Content-Type: application/json
	 * Connection: keep-alive
	 * Transfer-Encoding: chunked
	 * ```
	 *
	 * @param {String} headers Headers needing to be parsed
	 * @returns {Object} Headers parsed into an object
	 */
	module.exports = function parseHeaders(headers) {
	  var parsed = {};
	  var key;
	  var val;
	  var i;

	  if (!headers) { return parsed; }

	  utils.forEach(headers.split('\n'), function parser(line) {
	    i = line.indexOf(':');
	    key = utils.trim(line.substr(0, i)).toLowerCase();
	    val = utils.trim(line.substr(i + 1));

	    if (key) {
	      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
	        return;
	      }
	      if (key === 'set-cookie') {
	        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
	      } else {
	        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
	      }
	    }
	  });

	  return parsed;
	};


/***/ }),
/* 206 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);

	module.exports = (
	  utils.isStandardBrowserEnv() ?

	  // Standard browser envs have full support of the APIs needed to test
	  // whether the request URL is of the same origin as current location.
	    (function standardBrowserEnv() {
	      var msie = /(msie|trident)/i.test(navigator.userAgent);
	      var urlParsingNode = document.createElement('a');
	      var originURL;

	      /**
	    * Parse a URL to discover it's components
	    *
	    * @param {String} url The URL to be parsed
	    * @returns {Object}
	    */
	      function resolveURL(url) {
	        var href = url;

	        if (msie) {
	        // IE needs attribute set twice to normalize properties
	          urlParsingNode.setAttribute('href', href);
	          href = urlParsingNode.href;
	        }

	        urlParsingNode.setAttribute('href', href);

	        // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
	        return {
	          href: urlParsingNode.href,
	          protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
	          host: urlParsingNode.host,
	          search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
	          hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
	          hostname: urlParsingNode.hostname,
	          port: urlParsingNode.port,
	          pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
	            urlParsingNode.pathname :
	            '/' + urlParsingNode.pathname
	        };
	      }

	      originURL = resolveURL(window.location.href);

	      /**
	    * Determine if a URL shares the same origin as the current location
	    *
	    * @param {String} requestURL The URL to test
	    * @returns {boolean} True if URL shares the same origin, otherwise false
	    */
	      return function isURLSameOrigin(requestURL) {
	        var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
	        return (parsed.protocol === originURL.protocol &&
	            parsed.host === originURL.host);
	      };
	    })() :

	  // Non standard browser envs (web workers, react-native) lack needed support.
	    (function nonStandardBrowserEnv() {
	      return function isURLSameOrigin() {
	        return true;
	      };
	    })()
	);


/***/ }),
/* 207 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);

	module.exports = (
	  utils.isStandardBrowserEnv() ?

	  // Standard browser envs support document.cookie
	    (function standardBrowserEnv() {
	      return {
	        write: function write(name, value, expires, path, domain, secure) {
	          var cookie = [];
	          cookie.push(name + '=' + encodeURIComponent(value));

	          if (utils.isNumber(expires)) {
	            cookie.push('expires=' + new Date(expires).toGMTString());
	          }

	          if (utils.isString(path)) {
	            cookie.push('path=' + path);
	          }

	          if (utils.isString(domain)) {
	            cookie.push('domain=' + domain);
	          }

	          if (secure === true) {
	            cookie.push('secure');
	          }

	          document.cookie = cookie.join('; ');
	        },

	        read: function read(name) {
	          var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
	          return (match ? decodeURIComponent(match[3]) : null);
	        },

	        remove: function remove(name) {
	          this.write(name, '', Date.now() - 86400000);
	        }
	      };
	    })() :

	  // Non standard browser env (web workers, react-native) lack needed support.
	    (function nonStandardBrowserEnv() {
	      return {
	        write: function write() {},
	        read: function read() { return null; },
	        remove: function remove() {}
	      };
	    })()
	);


/***/ }),
/* 208 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Determines whether the specified URL is absolute
	 *
	 * @param {string} url The URL to test
	 * @returns {boolean} True if the specified URL is absolute, otherwise false
	 */
	module.exports = function isAbsoluteURL(url) {
	  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
	  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
	  // by any combination of letters, digits, plus, period, or hyphen.
	  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
	};


/***/ }),
/* 209 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Creates a new URL by combining the specified URLs
	 *
	 * @param {string} baseURL The base URL
	 * @param {string} relativeURL The relative URL
	 * @returns {string} The combined URL
	 */
	module.exports = function combineURLs(baseURL, relativeURL) {
	  return relativeURL
	    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
	    : baseURL;
	};


/***/ }),
/* 210 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var utils = __webpack_require__(190);

	/**
	 * Config-specific merge-function which creates a new config-object
	 * by merging two configuration objects together.
	 *
	 * @param {Object} config1
	 * @param {Object} config2
	 * @returns {Object} New object resulting from merging config2 to config1
	 */
	module.exports = function mergeConfig(config1, config2) {
	  // eslint-disable-next-line no-param-reassign
	  config2 = config2 || {};
	  var config = {};

	  utils.forEach(['url', 'method', 'params', 'data'], function valueFromConfig2(prop) {
	    if (typeof config2[prop] !== 'undefined') {
	      config[prop] = config2[prop];
	    }
	  });

	  utils.forEach(['headers', 'auth', 'proxy'], function mergeDeepProperties(prop) {
	    if (utils.isObject(config2[prop])) {
	      config[prop] = utils.deepMerge(config1[prop], config2[prop]);
	    } else if (typeof config2[prop] !== 'undefined') {
	      config[prop] = config2[prop];
	    } else if (utils.isObject(config1[prop])) {
	      config[prop] = utils.deepMerge(config1[prop]);
	    } else if (typeof config1[prop] !== 'undefined') {
	      config[prop] = config1[prop];
	    }
	  });

	  utils.forEach([
	    'baseURL', 'transformRequest', 'transformResponse', 'paramsSerializer',
	    'timeout', 'withCredentials', 'adapter', 'responseType', 'xsrfCookieName',
	    'xsrfHeaderName', 'onUploadProgress', 'onDownloadProgress', 'maxContentLength',
	    'validateStatus', 'maxRedirects', 'httpAgent', 'httpsAgent', 'cancelToken',
	    'socketPath'
	  ], function defaultToConfig2(prop) {
	    if (typeof config2[prop] !== 'undefined') {
	      config[prop] = config2[prop];
	    } else if (typeof config1[prop] !== 'undefined') {
	      config[prop] = config1[prop];
	    }
	  });

	  return config;
	};


/***/ }),
/* 211 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * A `Cancel` is an object that is thrown when an operation is canceled.
	 *
	 * @class
	 * @param {string=} message The message.
	 */
	function Cancel(message) {
	  this.message = message;
	}

	Cancel.prototype.toString = function toString() {
	  return 'Cancel' + (this.message ? ': ' + this.message : '');
	};

	Cancel.prototype.__CANCEL__ = true;

	module.exports = Cancel;


/***/ }),
/* 212 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var Cancel = __webpack_require__(211);

	/**
	 * A `CancelToken` is an object that can be used to request cancellation of an operation.
	 *
	 * @class
	 * @param {Function} executor The executor function.
	 */
	function CancelToken(executor) {
	  if (typeof executor !== 'function') {
	    throw new TypeError('executor must be a function.');
	  }

	  var resolvePromise;
	  this.promise = new Promise(function promiseExecutor(resolve) {
	    resolvePromise = resolve;
	  });

	  var token = this;
	  executor(function cancel(message) {
	    if (token.reason) {
	      // Cancellation has already been requested
	      return;
	    }

	    token.reason = new Cancel(message);
	    resolvePromise(token.reason);
	  });
	}

	/**
	 * Throws a `Cancel` if cancellation has been requested.
	 */
	CancelToken.prototype.throwIfRequested = function throwIfRequested() {
	  if (this.reason) {
	    throw this.reason;
	  }
	};

	/**
	 * Returns an object that contains a new `CancelToken` and a function that, when called,
	 * cancels the `CancelToken`.
	 */
	CancelToken.source = function source() {
	  var cancel;
	  var token = new CancelToken(function executor(c) {
	    cancel = c;
	  });
	  return {
	    token: token,
	    cancel: cancel
	  };
	};

	module.exports = CancelToken;


/***/ }),
/* 213 */
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Syntactic sugar for invoking a function and expanding an array for arguments.
	 *
	 * Common use case would be to use `Function.prototype.apply`.
	 *
	 *  ```js
	 *  function f(x, y, z) {}
	 *  var args = [1, 2, 3];
	 *  f.apply(null, args);
	 *  ```
	 *
	 * With `spread` this example can be re-written.
	 *
	 *  ```js
	 *  spread(function(x, y, z) {})([1, 2, 3]);
	 *  ```
	 *
	 * @param {Function} callback
	 * @returns {Function}
	 */
	module.exports = function spread(callback) {
	  return function wrap(arr) {
	    return callback.apply(null, arr);
	  };
	};


/***/ })
/******/ ]);