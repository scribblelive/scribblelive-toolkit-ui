import HtmlGenerator from "../services/html-generator";

const twitter = (post) => {
    return HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-TWITTER">
                                        ${post.Content}
                                    </div>`)
};

export default twitter;