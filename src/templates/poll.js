import HtmlGenerator from '../services/html-generator';
import Core from '../services/core-wrapper';

// holds references to answer elements
const answerElements = {};

const calculatePercentage = (answerVotes, totalVotes) => {
  if (answerVotes === 0 || totalVotes === 0) {
    return 0;
  }
  return Math.floor(100 * (answerVotes / totalVotes));
};

const generateWrapper = (post, pollClass) => `<div id="SL-${post.Id}" class="SL-POLL ${pollClass}">
            <p class="poll-question">${post.Content}</p>
          </div>`;

const generateVoteText = votes => `(${votes})`;

/**
 * Sets the reference data for later use
 * @param answer
 * @param element
 */
const setAnswerElements = ({ answer, element }) => {
  if (!answer || !answer.Id || !element) return;
  answerElements[answer.Id] = Object.assign({}, answerElements[answer.Id], {
    element,
    data: answer,
  });
};

const pollingCallback = (err, data) => {
  for (const answer of data.Answers) {
    const prevAnswer = answerElements[answer.Id];

    if (!prevAnswer) {
      continue;
    }

    const { element } = prevAnswer;
    const percentageElement = element.querySelector('.poll-result-item-percentage');
    const percentage = calculatePercentage(answer.Votes, data.TotalVotes);
    const prevPercentage = parseInt(percentageElement.innerHTML, 10);

    element.querySelector('.poll-result-item-description__votes').innerHTML = generateVoteText(answer.Votes);
    element.querySelector('.poll-result-item-bar__color-bar').style.width = `${percentage}%`;

    let start = prevPercentage;
    const interval = setInterval(() => {
      if (percentage > prevPercentage) {
        start++;
      } else if (percentage < prevPercentage) {
        start--;
      }

      percentageElement.innerHTML = `${start}%`;
      if (start === percentage) {
        clearInterval(interval);
        setAnswerElements({ answer, element });
      }
    }, 100);
  }
};


const renderResultView = (post) => {
  const { Answers: answers, TotalVotes: totalVotes, Id: pollId } = post.Entities;
  const answerContainer = HtmlGenerator.generate('<div class="poll-results"></div>');

  answers.forEach((answer) => {
    const percentage = calculatePercentage(answer.Votes, totalVotes);
    const voteText = generateVoteText(answer.Votes);
    const element = HtmlGenerator.generate(`<div class="poll-result-item">
                                                    <div class="poll-result-item-row">
                                                      <div class="poll-result-item-description">
                                                          <span class="poll-result-item-description__text">${answer.Text}</span> 
                                                          <span class="poll-result-item-description__votes">${voteText}</span>
                                                      </div>
                                                      <div class="poll-result-item-percentage">
                                                          ${percentage}%
                                                      </div>
                                                    </div>
                                                    <div class="poll-result-item-bar">
                                                        <span class="poll-result-item-bar__color-bar" 
                                                                style="width:${percentage}%"></span>
                                                    </div>
                                                </div>`);
    answerContainer.appendChild(element);
    setAnswerElements({ answer, element });
  });

  Core.instance.Poll.listen({ pollId, cb: pollingCallback });
  return answerContainer;
};

const renderVoteView = (post) => {
  const { Answers: answers, Id: pollId } = post.Entities;
  const streamId = post.StreamId;
  const answerContainer = HtmlGenerator.generate('<div class="poll-answers"></div>');
  const wrapper = HtmlGenerator.generate(generateWrapper(post, 'voting'));

  answers.forEach((answer, index) => {
    const answerDom = HtmlGenerator.generate(`<div class="poll-answer">${answer.Text}</div>`);
    const hoverClass = 'poll-answer--hover';
    const vote = () => {
      answer.Votes++;
      post.Entities.TotalVotes++;
      Core.instance.Poll.vote({
        streamId,
        pollId,
        selectionId: answer.Id,
      })
        .catch(() => {
        });

      answerContainer.classList.add('poll-answers--hidden');
      answerContainer.addEventListener('transitionend', () => {
        const resultsDom = renderResultView(post);
        answerContainer.parentNode.removeChild(answerContainer);
        resultsDom.classList.add('poll-results--hidden');
        wrapper.appendChild(resultsDom);
        resultsDom.classList.remove('poll-results--hidden');
      });
    };

    const hover = (e) => {
      e.target.classList.add(hoverClass);
    };

    const mouseout = (e) => {
      e.target.classList.remove(hoverClass);
    };

    if (index === (answers.length - 1)) {
      answerDom.classList.add('poll-answer--last');
    }

    answerDom.addEventListener('click', vote);
    answerDom.addEventListener('mouseover', hover);
    answerDom.addEventListener('mouseout', mouseout);
    answerContainer.appendChild(answerDom);
  });

  wrapper.appendChild(answerContainer);
  return wrapper;
};

const poll = (post) => {
  const streamId = post.StreamId;
  const pollId = post.Entities.Id;

  if (Core.instance.Poll.hasVoted({ streamId, pollId })) {
    const wrapper = HtmlGenerator.generate(generateWrapper(post, 'result'));
    const pollContent = renderResultView(post);
    wrapper.appendChild(pollContent);
    return wrapper;
  }
  return renderVoteView(post);
};

export default poll;