import HtmlGenerator from '../services/html-generator';

function isMobile() {
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

function renderPosts(posts) {
  const html = [];
  posts.forEach((postData) => {
    html.push(postData.fn(postData.post));
  });
  return html;
}

class List {
  constructor() {
    this.items = [];
  }

  append(postFunc) {
    this.items.push(postFunc);
  }

  get Id() {
    if (this.items.length === 1) return this.items[0].post.Id;
    const ids = this.items.map((postData) => postData.post.Id).join('-');
    return `List-${ids}`;
  }

  render() {
    if (this.items.length === 1) {
      return this.items[0].fn(this.items[0].post);
    }
    const ids = this.items.map((postData) => postData.post.Id).join('-');
    const html = HtmlGenerator.generate(`<div class='LIST' id='SL-List-${ids}'></div>`);
    renderPosts(this.items).forEach((post) => {
      html.appendChild(post);
    });
    return html;
  }
}

export default List;