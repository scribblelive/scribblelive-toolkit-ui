import HtmlGenerator from '../services/html-generator';
import { isMobile } from '../services/helper-functions';

const video = (post) => {
    const _isMobile = isMobile();
    let url = post.Media.Url;
    let thumbnail = post.Media.Thumbnail || '';
    let duration = post.Media.Duration || 0;
    let isHero = false;
    if (post.PostMeta && post.PostMeta.Tags && post.PostMeta.Tags.indexOf("hero") > -1) {
        isHero = true;
    }

    let caption = '';
    if (post.Caption !== undefined) {
        caption = `<div class="caption-container">
                        ${isHero ? `<div>${post.Caption}</div>` : `<p>${post.Caption}</p>`}
                    </div>`;
    }

    let _isFF = navigator.userAgent.match(/firefox/i) != null;
    var _isEdge = navigator.userAgent.indexOf("Edge") > -1;

    // don't include the objectTag in FF to stop the autoplay
    let objectTag = `<object data="${url}"><embed src="${thumbnail}"></object>`;

    const videoPost = HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-VIDEO${isHero ? " hero" : ""}">
                                        ${isHero && _isMobile ? '<div class="fa fa-video-camera"></div>' : ''}
                                        <video ${!isHero ? "controls" : (_isMobile ? "" : "loop muted autoplay")} poster="${thumbnail}" preload="${(duration > 15 && !isHero) ? 'none' : 'auto'}">
                                            <source src="${url}" type="video/mp4">
                                            Your browser does not support the video tag.
                                            ${_isFF || _isEdge ? "":objectTag}
                                        </video>
                                        ${caption}
                                    </div>`);

    if (_isMobile) {
        const videoElement = videoPost.querySelector('video');
        videoPost.onclick = () => {
            videoElement.play();
            if (typeof videoElement.webkitEnterFullscreen !== 'undefined') {
                // Android Stock
                videoElement.webkitEnterFullscreen();
            } else if (typeof videoElement.webkitRequestFullscreen !== 'undefined') {
                // Chrome
                videoElement.webkitRequestFullscreen();
            }
        };
        videoElement.addEventListener('webkitfullscreenchange', () => {
            if (!document.webkitFullscreenElement) {
                videoElement.pause();
            }
        });
    }

    return videoPost;
};

export default video;