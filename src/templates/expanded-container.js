import HtmlGenerator from '../services/html-generator';
import { closeExpandedImage } from '../services/expand-functions';

const expandedContainer = (post) => {
  const container = HtmlGenerator.generate(
    `<div class="SL-EXPANDED">
      <div class="fa fa-times SL-expanded-close"></div>
      <div class="SL-expanded-inner">
        <img class="SL-expanded-image" src="${post.Media.Url}"/>
        ${post.Caption ? `<div class="SL-expanded-caption">${post.Caption}</div>` : ''}
      </div>
    </div>`
  );

  container.getElementsByClassName('SL-expanded-close')[0].onclick = closeExpandedImage;

  return container;
};

export default expandedContainer;