const ConsentForm = (data, accept, reject) => {
    setTimeout(() => {
        document.getElementById("accept_button").addEventListener('click', accept);
        document.getElementById("reject_button").addEventListener('click', reject);
    });

    return `
    <div id="consent-form-pinboard">
        <div class="consent-form">
            <div>
                <h1>${data.ConsentFormTitle}</h1>
            </div>
            <div>
                ${data.ConsentFormText}
            </div>
            <br />
            <div class="consent-form-buttons">
                <button id="accept_button" class="consent-form-button consent-form-button__text">${data.ConsentFormConfirmText}</button>
                <button id="reject_button" class="consent-form-button consent-form-button__text">${data.ConsentFormCancelText}</button>
            </div>
        </div>
    </div>`
};

export default ConsentForm;