import HtmlGenerator from '../services/html-generator';
import PostHelper from '../services/post-helper';

const adUrl = 'https://client-ads.scribblelive.com';

function getAd(clientId, ad) {
  const Id = ad.Id;
  const Size = ad.Size || ad.EventId;
  const clientDir = `${clientId}`.split('').join('/');
  return `${adUrl}/${clientDir}/${Id}_${clientId}_${Size}.html?calculateWidth`;
}

function getValidAd(ads) {
  const width = document.documentElement.clientWidth;
  let target = null;
  for (let i = 0; i < ads.length; i++) {
    if (!ads[i].Id || (!ads[i].Size && !ads[i].EventId)) {
      continue;
    } else if (!target && (ads[i].Size < width || ads[i].EventId)) {
      target = ads[i];
      if (ads[i].EventId) break;
    }
  }
  if (!target) {
    return ads[0];
  }
  return target;
}

function sortBySize(a, b) {
  if (a.Size < b.Size) return 1;
  else if (a.Size > b.Size) return -1;
  return 0;
}


const advertisement = (post) => {
  const adData = PostHelper.getAdData();
  const adScript = PostHelper.getAdScript();

  if (adData) {
    adData.ads.sort(sortBySize);
  }

  const ad = getValidAd(adData.ads);
  const url = getAd(adData.clientId, ad);
  if (url) {
    if (!adScript) {
            // Set window.eventListener
      window.addEventListener('message', (data) => {
        try {
          if (typeof data.data !== 'string') return;
          const adInfo = JSON.parse(data.data.replace('scrbbl-resizableiframe: ', ''));

          //switch to classes because all the iframes are loaded with same id
          //when we lookup iframe for resizing, we only get one. the class
          //allows us to resize all.
          let targets = document.getElementsByClassName(`SL-AD-${adInfo.AdId}`);

          //the width of the advertisement can be null if the ad code was placed
          //during creation of the story. So we'll  check for null if no element
          //with existing class was found ad act upon that.
          if (targets.length == 0) {
            targets = document.getElementsByClassName(`SL-AD-${adInfo.AdId}-null`);
          }

          for (let iframe of targets) {
            iframe.height = `${adInfo.height}px`;
            iframe.width = adInfo.width;
          }
        } catch (e) {}
      });
      PostHelper.setAdScript(true);
    }
  }
  return HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-ADVERTISEMENT">
                                        <iframe src="${url}" width="${ad.Size}" id="SL-AD-${ad.Id}-${ad.Size}" class="SL-AD-${ad.Id}" frameborder="0">
                                            <div>Your browser doesn't support iframes</div>
                                        </iframe>
                                    </div>`);
};


export default advertisement;