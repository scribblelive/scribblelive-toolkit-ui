import UntrackedSocialPostRenderer from '@scrbbl/untracked-social-post-renderer';
import HtmlGenerator from '../services/html-generator';

export default function untracked(post) {
  return HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-HTML">
    ${UntrackedSocialPostRenderer.render(post)}</div>`);
}
