import HtmlGenerator from "../services/html-generator";

const instagram = (post) => {
    return HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-INSTAGRAM">
                                        ${post.Content}
                                    </div>`);
}

export default instagram;