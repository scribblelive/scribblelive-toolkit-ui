import HtmlGenerator from '../services/html-generator';

function renderHTMLScriptElements(post) {
  try {
    const postHTML = unescape(post.Content);

    let isScript = false;
    if (postHTML.indexOf('&lt;script') > -1 || postHTML.indexOf('<script') > -1) {
      isScript = true;
    }

    if (isScript) {
      // will render the scribble embed for this post id
      const scriptEmbed = `<div class="scrbbl-post-embed" data-post-id="${post.Id}"></div>`;
      return scriptEmbed;
    }
  } catch (err) {

  }

  return post.Content;
}

function imageOnRight(html) {
  const cols = html.getElementsByClassName('col-md-6');
  if (!cols || cols.length !== 2) return false;
    // There is an image on the right, but not on the left.
  if (cols[1].getElementsByTagName('img').length && cols[0].getElementsByTagName('img').length === 0) {
    return true;
  }
  return false;
}

const html = (post) => {
  const htmlPost = HtmlGenerator.getHtml(post.Content);
  const imgOnRight = imageOnRight(htmlPost);
    // console.log(htmlPost);
  return HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-HTML${imgOnRight ? ' img-right' : ''}">
                                    ${renderHTMLScriptElements(post)}
                                    </div>`);
};


export default html;