import HtmlGenerator from '../services/html-generator';

function footer() {
  return HtmlGenerator.generate(
    `<div class="SL-footer">
      <a href="http://www.scribblelive.com/products/content-curation-live-blog/">
        <img src="https://s3.amazonaws.com/scribblelive-visualizations/stencils/27c31655-1058-4fe0-a29e-3ea4277de1b2/poweredby.png" />
      </a>
    </div>`
  );
}

export default footer;
