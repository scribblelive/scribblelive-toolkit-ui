import HtmlGenerator from '../services/html-generator';

const audio = (post) => {

  let caption = post.Caption || '';

  let audio = HtmlGenerator.generate(
    `<div class="SL-AUDIO" id="SL-${post.Id}">
      <audio src="${post.Media.Url}" controls>
      </audio>
      <div class="caption-container"><p>${caption}</p></div>
    </div>`
  );

  return audio;
}

export default audio;
