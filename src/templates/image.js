import HtmlGenerator from '../services/html-generator';
import { expandImage } from '../services/expand-functions';
import { isMobile } from '../services/helper-functions';

function getAppropriateImageSize(media) {
  if (!media.Sizes) return media.Url;
  const width = document.documentElement.clientWidth;
  let validUrl = media.Url;
  for (const size in media.Sizes) {
    if (size > width) {
      return validUrl;
    }
    validUrl = media.Sizes[size];
  }
  return validUrl;
}

function getSizes(media) {
  if (!media.Sizes) return media.Url;
  const list = [];
  const keys = Object.keys(media.Sizes);
  for (let i = 0; i < keys.length; i++) {
    if (keys[i] === 'orig') {
      list.push(`${media.Sizes[keys[i]]} ${parseInt(keys[i - 1], 10) + 1}w`);
    } else {
      list.push(`${media.Sizes[keys[i]]} ${keys[i]}w`);
    }
  }
  return list.join(', ');
}

function mobileCheck() {
  return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
}

function isIE() {
    const ua = window.navigator.userAgent;
    if (ua.indexOf('MSIE ') > -1 || ua.indexOf('Trident') > -1) {
      return true;
    }
    return false;
}

function inIframe () {
  try {
    return window.self !== window.top;
  } catch (e) {
    return true;
  }
}

function getImageSet(post) {
  if (isIE()) {
    const src = (post.Media.Sizes && post.Media.Sizes['orig']) ? post.Media.Sizes['orig'] : post.Media.Url;
    return `<picture><img src="${src}" size="300w" /></picture>`;
  }
  return `<picture class="section-background-picture">
      <source srcset="${getSizes(post.Media)}">
      <img src="${post.Media.Url}" size="300w;"">
  </picture>`;
}

const image = (post, index) => {
  let caption = `<div class="caption-container"></div>`;
  let hero = '';
  let arrow = '';
  const transitionImage = !post.imageClose ? null : ' TRANSITION';
  
  if (post.PostMeta && post.PostMeta.Tags && post.PostMeta.Tags.indexOf('hero') > -1) {
    hero = ' hero-image';

    if (!(inIframe() && mobileCheck())) {
      hero = hero + ' desktop';
    }
  }
  if (hero && index !== undefined && index === 0) {
    arrow = `<div class="hero-arrow-button">
        <div class="arrow"></div>
      </div>`;
  }
  if (post.Caption && post.Caption.length > 0) {
    if (hero.length > 0) {
      caption = `<div class="caption-container"><div>${post.Caption}</div></div>`;
    } else {
      caption = `<div class="caption-container"><p>${post.Caption}</p></div>`;
    }
  }
  let image = null;
  if (hero.length > 0) {
    image = HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-IMAGE${hero}" style="background-image:url('${getAppropriateImageSize(post.Media)}')">
            ${caption}
            ${arrow}
        </div>`);
  } else if (!transitionImage) {
    image = HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-IMAGE">
        <img src="${getAppropriateImageSize(post.Media)}">
            ${caption}
        </div>`);
  } else {
    image = HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-IMAGE${transitionImage}" >
          ${caption}
          <div class="section-background fixed">
              ${getImageSet(post)}
          </div>
      </div>`);
  }

  if (post.Media && post.Media.Sizes && post.Media.Sizes['800'] && mobileCheck()) {
    const largerImage = HtmlGenerator.generate(`<img style="display:none; width:0; height:0;" src="${post.Media.Sizes['800']}" />`);
    largerImage.onload = ({ target }) => {
      if (image.style.backgroundImage !== '') {
        image.style.backgroundImage = `url(${target.src})`;
      } else {
        image.getElementsByTagName('img')[0].src = target.src;
        image.getElementsByTagName('img')[0].size = '800w';
      }
    };
  }

  function scrollToY(scrollTargetY = 0, speed = 2000, easing = 'easeOutSine') {
      // scrollTargetY: the target scrollY property of the window
      // speed: time in pixels per second
      // easing: easing equation to use

    let scrollY = window.scrollY,
      currentTime = 0;

      // min time .1, max time .8 seconds
    const time = Math.max(0.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, 0.8));

      // easing equations from https://github.com/danro/easing-js/blob/master/easing.js
    let PI_D2 = Math.PI / 2,
      easingEquations = {
        easeOutSine(pos) {
          return Math.sin(pos * (Math.PI / 2));
        },
        easeInOutSine(pos) {
          return (-0.5 * (Math.cos(Math.PI * pos) - 1));
        },
        easeInOutQuint(pos) {
          if ((pos /= 0.5) < 1) {
            return 0.5 * Math.pow(pos, 5);
          }
          return 0.5 * (Math.pow((pos - 2), 5) + 2);
        },
      };

      // add animation loop
    function tick() {
      currentTime += 1 / 60;

      const p = currentTime / time;
      const t = easingEquations[easing](p);

      if (p < 1) {
        requestAnimFrame(tick);
        window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
      } else {
        window.scrollTo(0, scrollTargetY);
      }
    }

      // call it once to get started
    tick();
  }

  if (arrow.length > 0) {
    window.requestAnimFrame = (function () {
      return window.requestAnimationFrame ||
              window.webkitRequestAnimationFrame ||
              window.mozRequestAnimationFrame ||
              function (callback) {
                window.setTimeout(callback, 1000 / 60);
              };
    }());

    const arrow = image.querySelector('.hero-arrow-button .arrow');
    if (arrow) {
      arrow.onclick = ({ target }) => {
        if (!target) return;
        const imageBottom = window.scrollY + target.parentNode.getBoundingClientRect().bottom;
        scrollToY(imageBottom, 3000);
      };
    }
  }

  if (isMobile()) {
    if (arrow.length > 0) {
      image.querySelector('.caption-container').onclick = () => expandImage(post);
    } else {
      image.onclick = () => expandImage(post);
    }
  }

  return image;
};

export default image;
