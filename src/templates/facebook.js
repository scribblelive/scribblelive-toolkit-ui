import HtmlGenerator from "../services/html-generator";

const facebook = (post) => {
    return HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-FACEBOOK">
                                ${post.Content}
                            </div>`);
};

export default facebook;