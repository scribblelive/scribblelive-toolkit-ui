import HtmlGenerator from "../services/html-generator";

const embed = (post) => {
    return HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-EMBED">
                                ${post.Content}
                            </div>`);
};

export default embed;