import HtmlGenerator from "../services/html-generator";

const text = (post) => {
    return HtmlGenerator.generate(`<div id="SL-${post.Id}" class="SL-TEXT">
                            <p>${post.Content}</p>
                        </div>`);
};

export default text;