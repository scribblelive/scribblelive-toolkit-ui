import STORE from './post-store';
import RenderingOptions from './rendering-options';
import List from '../templates/list';
import footer from '../templates/footer';
import { checkIfHtmlIsFacebook } from './helper-functions';
import UiConfig from './ui-config-service';

/**
 * PostHelper controls the updating and placement of posts on the page.
 */
export default class PostHelper {

    /*
        GETTERS
     */

    /**
     * This is a getter which returns the posts in the STORE.
     * @returns {Array}
     */
  static get posts() {
    return STORE.posts;
  }

  static getAdData() {
    return STORE.adData;
  }

  static getAdScript() {
    return STORE.adScriptEnabled;
  }

    /*
        SETTERS
     */
  static setAdScript(val) {
    STORE.adScriptEnabled = val;
  }

    /*
        STATIC FUNCTIONS
     */

    /**
     * This function will mark, add, update and/or remove any new posts.
     * @param post
     */
  static update(post) {

    post.HasUpdated = true;

    if (post.IsDeleted === 1) {
            // Delete post from DOM and STORE.
      if (getPostIndex(post) !== -1) {
        STORE.posts.splice(getPostIndex(post), 1);
        return removePost(post);
      }
      return;
    }

    const postIndex = getPostIndex(post);
    if (postIndex === -1) {
      STORE.posts.push(post);
      return;
    }

    let postDate = null;
    let oldDate = null;
    try {
      postDate = post.LastModifiedDate ? new Date(post.LastModifiedDate) : new Date(`${post.Date} GMT`);
      oldDate = STORE.posts[postIndex].LastModifiedDate ? new Date(STORE.posts[postIndex].LastModifiedDate) : new Date(`${STORE.posts[postIndex].Date} GMT`);
    } catch(e) {
      return;
    }
    if (postDate > oldDate) {
      STORE.posts[postIndex] = post;
    }
  }

    /**
     * This function will sort posts by creationDate by default although allow
     * a user to pass in a custom sorting method.
     * @param sortFunc
     */
  static sort(sortFunc = sortPostsByCreationDate) {
    STORE.posts.sort(sortFunc);
  }

  static organizeTypes() {
    const target = 'IMAGE';
    let lastFound = -2;
    // Copying posts to renderList
    
    if (!UiConfig.newestAtBottom) {
      STORE.renderList = STORE.posts.reverse().map((post) => post);
    }
    else {
      STORE.renderList = STORE.posts.map((post) => post);
    }

    // Loop over renderList and find connected posts.
    for (let i = 0; i < STORE.renderList.length; i++) {
      if (STORE.renderList[i].Type === target) {
        if (lastFound === i - 1) {
          STORE.renderList[i - 1].imageClose = true;
          STORE.renderList[i].imageClose = true;
          lastFound = i;
        } else {
          lastFound = i;
        }
      }
    }
  }

    /**
     * This function will remove the HasUpdated flag, remove the post from the page
     * and redraw the post on the page.
     * @param post
     */
  static sync(post) {
    STORE.posts[getPostIndex(post)].HasUpdated = false;
    removePost(post);
  }

  static isFilterNeeded() {
    return STORE.consentFormData.filterPost;
  }

  static filterPosts(posts) {
    return posts.filter(post => {
      if (post.PostMeta
          && post.PostMeta.Type
          && (post.PostMeta.Type.indexOf('facebook:post') > -1
              || post.PostMeta.Type.indexOf('instagram:post') > -1)) {
        return false;
      } else if (post.PostMeta
          && post.PostMeta.Data
          && (JSON.stringify(post.PostMeta.Data).indexOf('\"type\":\"FACEBOOK\"') > -1
              || JSON.stringify(post.PostMeta.Data).indexOf('\"type\":\"INSTAGRAM\"') > -1)) {
        return false
      } else if (post.Content
          && (post.Content.indexOf('data-url=\"https://www.instagram.com/') > -1
              || post.Content.indexOf('href=\\"https://www.facebook.com/') > -1
              || post.Content.indexOf(encodeURIComponent('data-url=\"https://www.instagram.com/')) > -1
              || post.Content.indexOf(encodeURIComponent('href=\\"https://www.facebook.com/')) > -1
              || post.Content.indexOf('data-url%3D%22https%3A//www.instagram.com/') > -1
              || post.Content.indexOf('href%3D%22https%3A//www.facebook.com/') > -1)) {
        return false;
      }
      return true;
    });
  }

  static renderPosts() {
    if (STORE.consentFormData.showModal || STORE.consentFormData.loading) return;
    STORE.renderList = this.isFilterNeeded() ? this.filterPosts(STORE.renderList) : STORE.renderList;
    for (let i = 0; i < STORE.renderList.length; i++) {
      let newPost = STORE.renderList[i];
      putPost(newPost, i);
    }
    if (!STORE.footerRendered) {
      this.renderFooter();
    }
  }

  static renderFooter() {
    RenderingOptions.rootEl.appendChild(footer());
    STORE.footerRendered = true;
  }

  static setAdData(data) {
    STORE.adData = data;
  }

}

/**
 * This function will find the passed in post within the store and return it's index.
 * @param post
 * @returns {number}
 */
function getPostIndex(post) {
  for (let i = 0; i < STORE.posts.length; i++) {
    if (STORE.posts[i].Id === post.Id) {
      return i;
    }
  }
  return -1;
}

/**
 * This function will check if a post is currently on the page.
 * @param  {[type]} post [description]
 * @return {[type]}      [description]
 */
function currentlyExistsOnPage(post) {
  const target = document.getElementById(`SL-${post.Id}`);
  return target !== undefined && target !== null;
}

/**
 * This function will put a post on the page in the order it is listed.
 * @param post
 */
function putPost(post, index) {
  if (!post.HasUpdated && currentlyExistsOnPage(post)) return;
  let htmlEle = null;
  if (post instanceof List) {
    htmlEle = post.render();
  } else {
    const renderFunc = RenderingOptions.getRenderer(post.Type, post);
    if (renderFunc === null || typeof renderFunc !== 'function') return;
    htmlEle = renderFunc(post, index);
  }

  //check the current page number. then based on it, either prepend or append
  //if newestAtBottom, append
  //if newestNotAtBottom, prepend

  if (UiConfig.currentPage == 1) {
    if (index === 0) { 
      RenderingOptions.rootEl.insertBefore(htmlEle, RenderingOptions.rootEl.childNodes[0]);
      UiConfig.incrementLastRenderedIndex();
      return;
    } else if (currentlyExistsOnPage(post)) {
      let target = document.getElementById(`SL-${post.Id}`);
      target = htmlEle;
      return;
    }
    const targetEle = document.getElementById(`SL-${STORE.renderList[index - 1].Id}`);
    
    insertAfter(targetEle, htmlEle);
    UiConfig.incrementLastRenderedIndex();
    return;
  }




  const targetEl = RenderingOptions.rootEl.childNodes[UiConfig.lastRenderedIndex ];
  insertAfter(targetEl, htmlEle);
  //targetEl.appendChild(htmlEle);
  UiConfig.incrementLastRenderedIndex(); 
  return;
  

}

/**
 * This function will add a post after a reference to another post.
 * @param referenceNode
 * @param newNode
 */
function insertAfter(referenceNode, newNode) {
  if (referenceNode && referenceNode.parentNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  }
}

/**
 * This function will remove a post from the page.
 * @param post
 * @returns {boolean} based on whether the post was removed or not.
 */
function removePost(post) {
  const target = document.getElementById(`SL-${post.Id}`);
  if (!target) return false;
  const parent = target.parentNode;
  parent.removeChild(target);
  return true;
}

/**
 * This is the default sorting function which sorts by a posts CreationDate.
 * @param a
 * @param b
 * @returns {number}
 */
function sortPostsByCreationDate(a, b) {
  if (new Date(a.CreationDate) < new Date(b.CreationDate)) return RenderingOptions.newestAtBottom(1);
  if (new Date(a.CreationDate) > new Date(b.CreationDate)) return RenderingOptions.newestAtBottom(-1);
  return 0;
}
