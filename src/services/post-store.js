/**
 * Store containing static information regarding post information.
 * @type {{posts: Array}}
 */
export default {
  posts: [],
  renderList: [],
  footerRendered: false,
  videoScrollCreated: false,
  adData: {},
  adScriptEnabled: false,
  expandedContainer: null,
  clientData: null,
  consentFormData: {
    showModal: false,
    filterPost: false,
    loading: false,
  }
};
