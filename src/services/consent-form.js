import STORE from './post-store';
import Cookies from 'js-cookie';
import ConsentForm from '../templates/consent-form';
import RenderingOptions from "./rendering-options";
import axios from 'axios';

class ConsentFormService {

    static init({ token, baseUrl }) {
        this.token = token;
        this.baseUrl = baseUrl ||'https://api.scribblelive.com/v1';
    }

    static getClientData() {
        STORE.consentFormData.loading = true;
        return axios(`${this.baseUrl}/client?token=${this.token}&includePrivacySettings=true`)
            .then(res => {
                const data = res.data;
                if (data) STORE.clientData = data;
                this.checkCookies();
                STORE.consentFormData.loading = false;
                this.renderConsentForm();
            })
            .catch(error => {
                STORE.consentFormData.loading = false;
                this.renderConsentForm();
                throw error;
            });
    }

    static checkCookies() {
        const clientData = STORE.clientData ? STORE.clientData : null;
        if (clientData && clientData.Id && clientData.ConsentForm === '1' && clientData.ConsentFormLastModified) {
            const clientId = clientData.Id;
            const cookieData = Cookies.get();
            const filteredPosts = cookieData[this.getFilteredPostField(clientId)];
            const lastModified = cookieData[this.getFormLastModifiedField(clientId)];
            if (!filteredPosts || !lastModified) {
                STORE.consentFormData.showModal = true;
                STORE.consentFormData.filterPost = false;
            } else if (new Date(clientData.ConsentFormLastModified) > new Date(lastModified)) {
                STORE.consentFormData.showModal = true;
                STORE.consentFormData.filterPost = filteredPosts === 'true';
            } else {
                STORE.consentFormData.showModal = false;
                STORE.consentFormData.filterPost = filteredPosts === 'true';
            }
        }
    }

    static handleAccept() {
        const clientId = STORE.clientData.Id;
        Cookies.set(this.getFilteredPostField(clientId), false);
        Cookies.set(this.getFormLastModifiedField(clientId), new Date().toUTCString());
        STORE.consentFormData.filterPost = false;
        location.reload();
    }

    static handleReject() {
        const clientId = STORE.clientData.Id;
        Cookies.set(this.getFilteredPostField(clientId), true);
        Cookies.set(this.getFormLastModifiedField(clientId), new Date().toUTCString());
        STORE.consentFormData.filterPost = true;
        location.reload();
    }

    static renderConsentForm() {
        if (STORE.consentFormData.showModal) {
            let consentForm = document.getElementById("consentForm");
            if (!consentForm) {
                consentForm = document.createElement('div');
                consentForm.setAttribute("id", "consentForm");
                RenderingOptions.rootEl.appendChild(consentForm);
            }
            consentForm.innerHTML = ConsentForm(STORE.clientData, this.handleAccept.bind(this), this.handleReject.bind(this));
        } else {
            const consentForm = document.getElementById("consentForm");
            if (consentForm) consentForm.innerHTML = '';
            document.body.style.background = 'none';
        }
    }


    static getFilteredPostField(clientId){
        return `FilteredTrackingPost_${clientId}`;
    }
    static getFormLastModifiedField(clientId) {
        return `ConsentFormLastModified_${clientId}`;
    }
}

export default ConsentFormService;
