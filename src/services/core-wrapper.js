import ScribbleToolkitCore from '@scrbbl/scribblelive-toolkit-core';

let _core = null;

export const initialize = ({ token, options }) => {
  _core = new ScribbleToolkitCore({ token, options });
  return _core;
};

export default {
  get instance(){
    if (_core === null){
      throw new Error('Core has not been initialized');
    }

    return _core;
  }
}