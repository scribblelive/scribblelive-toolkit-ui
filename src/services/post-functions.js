export function addStreamIds(posts, streamId) {
  return posts.map(post => ({ ...post, StreamId: post.StreamId || streamId }));
}

export function formatDate(dateVal) {
  let date = dateVal;
  if (typeof date.getMonth !== 'function') { // assume string provided
    try {
      date = new Date(dateVal);
      if (isNaN(date.getMonth())) {
        date = new Date(dateVal.replace(/\+/g, '.'));
      }
      // For IE
      if (isNaN(date.getMonth())) {
        const dateArr = dateVal.split(' ');
        date = new Date(Date.parse(`${dateArr[1]} ${dateArr[2]}, ${dateArr[5]} ${dateArr[3]} UTC`));
      }
    } catch (e) {
      return '';
    }
  }

  const lang = window.navigator.userLanguage || window.navigator.language;

  const hour = date.getHours();
  const displayHours = hour === 0 ? 12 : hour;
  const mins = date.getMinutes();
  const displayMins = mins.toString().length > 1 ? mins : `0${mins}`;
  const displayTime = displayHours > 12 ? `${displayHours - 12}:${displayMins} PM` : `${displayHours}:${displayMins} AM`;
  const day = date.getDate();
  const month = date.toLocaleString(lang, { month: 'long' }).substr(0, 3);
  const year = date.getFullYear();

  return `${displayTime} - ${day} ${month} ${year}`;
}