
/**
 * UIConfigService keeps track of the page state and configurations
 */
export default class UIConfigService {
    static init(options) {
        this._options = options;
    }

/**
 * currentPage returns the last page number loaded for posts
 */
  static get currentPage() {
    
    if (this._options.currentPage == null) {
      this._options.currentPage = 0;
    }

    return this._options.currentPage;
  }

/**
 * update the pageSize (number of records retrieved)
 */
  static set pageSize(size) {
      this._options.pageSize = size;
  }

  /**
   * Returns the pageSize configured
   */
  static get pageSize() {
      const value = this._options.pageSize ?  this._options.pageSize : 10;

      if (value <= 0 || value > 100) {
          return 100;
      }

      return value;
  }

  /**
   * To be called when a page of posts is loaded
   * Useful to see how many pages of posts are loaded
   */
  static  incrementCurrentPage() {

      if (this._options.currentPage == null) {
      this._options.currentPage = 0;
    }
    this._options.currentPage = this._options.currentPage + 1;
  }

  /**
   * Controls whether newest posts should be at bottom of list or not
   */
  static get newestAtBottom() {
    return this._options.newestAtBottom ? this._options.newestAtBottom : false;
  }

  /**
   * Updates the flag to control whether new posts should be first or last
   */
  static set newestAtBottom(value) {
      this._options.newestAtBottom = value ? true : false;
  }

  /**
   * Get the index of the last rendered post
   */
  static get lastRenderedIndex() {
      if (this._options.lastRenderedIndex == null) {
          this._options.lastRenderedIndex = -1;
      }

      return this._options.lastRenderedIndex;
  }

  /**
   * Increment the index to point to the recently rendered post
   */
  static incrementLastRenderedIndex() {
      if (this._options.lastRenderedIndex == null) {
          this._options.lastRenderedIndex = -1;
      }

      this._options.lastRenderedIndex = this._options.lastRenderedIndex + 1;
  }

}