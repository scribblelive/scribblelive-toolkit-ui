import Image from '../templates/image';
import Text from '../templates/text';
import Html from '../templates/html';
import Instagram from '../templates/instagram';
import Facebook from '../templates/facebook';
import Twitter from '../templates/twitter';
import Embed from '../templates/embed';
import Poll from '../templates/poll';
import Video from '../templates/video';
import Advertisement from '../templates/advertisement';
import List from '../templates/list';
import Audio from '../templates/audio';
import UntrackedSocial from '../templates/untracked-social';
import { checkIfHtmlIsFacebook } from './helper-functions';

const DEFAULT_OPTIONS = {
  isNonTracking: null,
  renderingTypes: {
    IMAGE: null,
    TEXT: null,
    HTML: null,
    INSTAGRAM: null,
    FACEBOOK: null,
    TWITTER: null,
  },
  rootElement: null,
  posts: [],
};

export default class RenderingOptions {

  static init(options) {
    this._options = Object.assign({}, DEFAULT_OPTIONS, options);
    validate(this._options);
  }

  static getRenderer(postType, post) {
    const { Content: content } = post;
    switch (postType) {
      case 'IMAGE':
        return this.image;
      case 'TEXT':
        return this.text;
      case 'TWEET':
        return this.twitter;
      case 'FACEBOOK':
        return this.facebook;
      case 'INSTAGRAM':
        return this.instagram;
      case 'HTML':
        if (checkIfHtmlIsFacebook(post)) {
          return this.facebook;
        }
        return this.html;
      case 'EMBED':
        return this.embed;
      case 'POLL':
        return this.poll;
      case 'VIDEO':
        return this.video;
      case 'ADVERTISEMENT':
        return this.advertisement;
      case 'AUDIO':
        return this.audio;
      case 'SLIDESHOW':
        return this.html;
      case 'YOUTUBE':
        return this.html;
      default:
        return null;
    }
  }

  static get rootEl() {
    let rootEl = null;
    const rootClassName = "scrbbl-storytelling";

    if (this._options && this._options.rootElement) {
        if (typeof this._options.rootElement === 'string') { 
          // iframe
          rootEl = document.querySelector(this._options.rootElement);

          if ( !document.body.classList.contains(rootClassName) ) {
            document.body.classList.add(rootClassName);
          }

          return rootEl;
        }
        else { 
          // script embed

          let existingContainer = document.getElementsByClassName(rootClassName);

          if ( existingContainer.length > 0 ) {
            rootEl = existingContainer[0];
          } else {
            rootEl = document.createElement("div");
            this._options.rootElement.appendChild(rootEl);
          }
        }
    }
  
    if ( !rootEl ) {
      rootEl = document.body;
    }

    if ( rootEl && !rootEl.classList.contains(rootClassName) ) {
      rootEl.classList.add(rootClassName);
    }

    return rootEl;
  }

  static get isNonTracking() {
    if (this._options.isNonTracking) return this._options.isNonTracking;
    return false;
  }


  static get image() {
    if (this._options.renderingTypes.IMAGE) return this._options.renderingTypes.IMAGE;
    return Image;
  }

  static get text() {
    if (this._options.renderingTypes.TEXT) return this._options.renderingTypes.TEXT;
    return Text;
  }

  static get html() {
    if (this._options.renderingTypes.HTML) return this._options.renderingTypes.HTML;
    return this.isNonTracking ? UntrackedSocial : Html;
  }

  static get instagram() {
    if (this._options.renderingTypes.INSTAGRAM) return this._options.renderingTypes.INSTAGRAM;
    return this.isNonTracking ? UntrackedSocial : Instagram;
  }

  static get facebook() {
    if (this._options.renderingTypes.FACEBOOK) return this._options.renderingTypes.FACEBOOK;
    return this.isNonTracking ? UntrackedSocial : Facebook;
  }

  static get twitter() {
    if (this._options.renderingTypes.TWITTER) return this._options.renderingTypes.TWITTER;
    return this.isNonTracking ? UntrackedSocial : Twitter;
  }

  static get poll() {
    if (this._options.renderingTypes.POLL) return this._options.renderingTypes.POLL;
    return Poll;
  }

  static get embed() {
    if (this._options.renderingTypes.EMBED) return this._options.renderingTypes.EMBED;
    return Embed;
  }

  static get video() {
    if (this._options.renderingTypes.VIDEO) return this._options.renderingTypes.VIDEO;
    return Video;
  }

  static get advertisement() {
    if (this._options.renderingTypes.ADVERTISEMENT) return this._options.renderingTypes.ADVERTISEMENT;
    return Advertisement;
  }

  static get audio() {
    if (this._options.renderingTypes.AUDIO) return this._options.renderingTypes.AUDIO;
    return Audio;
  }

  static get posts() {
    return this._options.posts;
  }

  static newestAtBottom(before) {
    let newestAtBottom = true;
    if (this._options && this._options.newestAtBottom) {
      try {
        newestAtBottom = JSON.parse(this._options.newestAtBottom);
        if (typeof newestAtBottom !== "boolean") {
          newestAtBottom = true;
        }
      } catch (e) {
        newestAtBottom = false;
      }
    }

    if (before === 1 && newestAtBottom) {
      if (newestAtBottom) {
        return -1;
      } 
        return 1;
      
    }
    else if (before === -1) {
      if (newestAtBottom) {
        return 1;
      } 
        return -1;
      
    }
    return before;
  }

  static getList() {
    return List;
  }

}

function validate(options) {
  const keys = Object.keys(options.renderingTypes);
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    if (typeof options[key] !== 'function') {
      delete options[key];
      keys.splice(i, 1);
      i--;
    }
  }
  if (!options.rootElement) {
    throw 'No root element specified.';
  }
}