
export default class HtmlGenerator {
  static generate(str) {
    return postProcess(this.getHtml(str));
  }
  static getHtml(str) {
    const div = document.createElement('div');
    div.innerHTML = str;
    return div.firstChild;
  }
}

function postProcess(ele) {
  addTargetBlank(ele);
  return ele;
}

function addTargetBlank(ele) {
  const as = ele.getElementsByTagName('a');
  for (let i = 0; i < as.length; i++) {
    as[i].setAttribute('target', '_blank');
  }
}