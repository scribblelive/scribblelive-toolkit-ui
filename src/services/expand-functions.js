import STORE from './post-store';
import RenderingOptions from './rendering-options';
import expandedContainer from '../templates/expanded-container';

export function expandImage(post) {
  if (!STORE.expandedContainer) {
    STORE.expandedContainer = RenderingOptions.rootEl.appendChild(expandedContainer(post));
    document.body.classList.add('expanded');
  }
}

export function closeExpandedImage() {
  RenderingOptions.rootEl.removeChild(STORE.expandedContainer);
  STORE.expandedContainer = null;
  document.body.classList.remove('expanded');
}
