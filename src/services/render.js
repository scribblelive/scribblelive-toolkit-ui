import PostHelper from './post-helper';

/**
 * RenderService contains wrapper functions to call PostHelper.
 */
export default class RenderService {

  static list(posts, options) {
    posts.forEach(PostHelper.update);
    PostHelper.sort();
    PostHelper.posts.forEach(post => post.HasUpdated ? PostHelper.sync(post) : '');
    PostHelper.organizeTypes();
    PostHelper.renderPosts();
  }

  static adData(data) {
    PostHelper.setAdData(data);
  }
}
