import Promise from 'es6-promise';
import RenderService from './services/render';
import RenderingOptions from './services/rendering-options';
import Core, { initialize } from './services/core-wrapper';
import { addStreamIds } from './services/post-functions';
import UiConfig from './services/ui-config-service';
import ConsentFormService from './services/consent-form';
import STORE from './services/post-store';

if (typeof window !== 'undefined' && !window.Promise) {
  window.Promise = Promise;
}

class ScribbleToolkitUi {
  constructor(options) {
    RenderingOptions.init(options);
    this.Render = RenderService;
    const token = options.token;
    initialize({ token, options });
    this.isNonTracking = options.isNonTracking;
    UiConfig.init(options.uiOptions || {});
    UiConfig.pageSize = options.paging ? options.paging.pageSize : 10;
    UiConfig.newestAtBottom = options.newestAtBottom;
    const baseUrl = options.baseUrl ? options.baseUrl : 'https://api.scribblelive.com/v1';
    ConsentFormService.init({ token, baseUrl });
  }


  init(streamId) {
    ConsentFormService.getClientData()
        .then(() => {
          renderPosts();
        })
        .catch((e) => {
          console.log(e);
          renderPosts();
        });

    const renderPosts = () => {
      if (RenderingOptions.posts && RenderingOptions.posts.length > 0) {
        this.Render.list(RenderingOptions.posts);
        return;
      }
      Core.instance.Stream.loadWithOptions({streamId: streamId,
        pageSize: UiConfig.pageSize,
        newestAtBottom: UiConfig.newestAtBottom}, postCallback);

      window.addEventListener('message', (message) => {
        if (message.origin.indexOf('scribblelive.com') === -1) return;
        if (!message.data.action) return;
        if (!message.data.iframeName) return;

        if (message.data.action === 'REMOVE_TRACKING_POST_SLIDESHOW') {
            var child = document.querySelector('iframe[name="' + message.data.iframeName + '"').closest('.SL-HTML');
            if (child) {
                child.parentNode.removeChild(child);
            }
            
        }
        if (message.data.action === 'GET_FILTERTRACKING_COOKIE') {
            var payload = {
                action: 'POST_FILTERTRACKING_COOKIE',
                value: STORE.consentFormData.filterPost ? true : false,
            }
            var frm = window.frames[message.data.iframeName]
            frm.postMessage(payload, '*');
        }
      });
    };
   
    const postCallback = (err, data) => {
      if (err) {
        console.log(err);
        return;
      }

      
      if (data.adData) {
       
        this.Render.adData(data.adData);
      }
      
      const postsWithStreamIds = addStreamIds(data.posts, streamId);

      if (data.pagination && UiConfig.currentPage <= (data.pagination.TotalPages -1)) {
        
        UiConfig.incrementCurrentPage();

        Core.instance.Stream.loadWithOptions({streamId: streamId, 
          pageNum: UiConfig.currentPage, 
          pageSize: UiConfig.pageSize,
         newestAtBottom: UiConfig.newestAtBottom}, postCallback);
      } else if (data.pagination && UiConfig.currentPage > (data.pagination.TotalPages - 1)) {
        UiConfig.incrementCurrentPage(); //increment so we don't come into this func again
        Core.instance.Stream.poll(streamId, postCallback); //start polling when everything is done
      }

    
        this.Render.list(postsWithStreamIds);

      if (!this.isNonTracking) {
        FB.XFBML.parse();
        if (twttr && twttr.widgets && twttr.widgets) {
          twttr.widgets.load();
        }
      }
      
      SCRBBL.post.go();
    };
  }
}

module.exports = ScribbleToolkitUi;
