## Scribblelive Toolkit Storytelling
This package allows developers to have a template for a storytelling experience.

### Installations
To install the package run `npm i @scrbbl/scribblelive-toolkit-storytelling`

### Prerequisites
- Scribblelive authentication token

### Example
This example assumes the use of npm and a module bundler such as webpack.

```html
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="./index.css" />
    <script id="facebook-jssdk" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.2"></script>
    <script id="twitter-wjs" src="https://platform.twitter.com/widgets.js"></script>
    <script id="scrbbl-post-embed" src="https://embed.scribblelive.com/widgets/embed/post.js"></script>
</head>
<body>
<div id="SL-Content"></div>
<script src="./index.js"></script> <script type="text/javascript">
    const streamId = 123456;
    const STRenderInstance = new ScribbleToolkit({"rootElement":"SL-Content","newestAtBottom":true,"token":"my-token"});
    STRenderInstance.init(streamId);
</script>

</body>
</html>
```

### Storytelling Options

These options can be provided when instantiating the ScribbleliveToolkit object.
Example: 
```javascript
const STRenderInstance = new ScribbleliveToolkit({"rootElement":"body"});
```

*Required*
#### token: *string*
This is the token for the stream.  This is required as it is used by the [Scribblelive Toolkit Core](https://www.npmjs.com/package/@scrbbl/scribblelive-toolkit-core).

*Optional*
#### rootElement: *string*
This option will specify where to put posts on the page.
Default is *SL-Content*.

#### newestAtBottom: *Boolean*
This option will specify if you want posts to be as they are displayed by creation date or reversed.
Default is *false*.


Made with :heart: at [ScribbleLive](https://scribblelive.com)