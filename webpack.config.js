var path = require('path');

module.exports = {
    stats: {
        children: false
    },
    entry: __dirname + '/src/index.js',
    output: {
        path: __dirname + '/sample-project',
        filename: 'index.js',
        library: 'ScribbleToolkit'
    },
    module: {
        loaders: [{
            test: /\.(js|jsx)$/,
            loader: 'babel-loader',
            exclude: /node_modules/,
            query: {
                presets: ['es2015']
            }
        }]
    },
    plugins: []
};