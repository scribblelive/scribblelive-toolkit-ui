'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _htmlGenerator = require('../services/html-generator');

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

var _helperFunctions = require('../services/helper-functions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var video = function video(post) {
    var _isMobile = (0, _helperFunctions.isMobile)();
    var url = post.Media.Url;
    var thumbnail = post.Media.Thumbnail || '';
    var duration = post.Media.Duration || 0;
    var isHero = false;
    if (post.PostMeta && post.PostMeta.Tags && post.PostMeta.Tags.indexOf("hero") > -1) {
        isHero = true;
    }

    var caption = '';
    if (post.Caption !== undefined) {
        caption = '<div class="caption-container">\n                        ' + (isHero ? '<div>' + post.Caption + '</div>' : '<p>' + post.Caption + '</p>') + '\n                    </div>';
    }

    var _isFF = navigator.userAgent.match(/firefox/i) != null;
    var _isEdge = navigator.userAgent.indexOf("Edge") > -1;

    // don't include the objectTag in FF to stop the autoplay
    var objectTag = '<object data="' + url + '"><embed src="' + thumbnail + '"></object>';

    var videoPost = _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-VIDEO' + (isHero ? " hero" : "") + '">\n                                        ' + (isHero && _isMobile ? '<div class="fa fa-video-camera"></div>' : '') + '\n                                        <video ' + (!isHero ? "controls" : _isMobile ? "" : "loop muted autoplay") + ' poster="' + thumbnail + '" preload="' + (duration > 15 && !isHero ? 'none' : 'auto') + '">\n                                            <source src="' + url + '" type="video/mp4">\n                                            Your browser does not support the video tag.\n                                            ' + (_isFF || _isEdge ? "" : objectTag) + '\n                                        </video>\n                                        ' + caption + '\n                                    </div>');

    if (_isMobile) {
        var videoElement = videoPost.querySelector('video');
        videoPost.onclick = function () {
            videoElement.play();
            if (typeof videoElement.webkitEnterFullscreen !== 'undefined') {
                // Android Stock
                videoElement.webkitEnterFullscreen();
            } else if (typeof videoElement.webkitRequestFullscreen !== 'undefined') {
                // Chrome
                videoElement.webkitRequestFullscreen();
            }
        };
        videoElement.addEventListener('webkitfullscreenchange', function () {
            if (!document.webkitFullscreenElement) {
                videoElement.pause();
            }
        });
    }

    return videoPost;
};

exports.default = video;