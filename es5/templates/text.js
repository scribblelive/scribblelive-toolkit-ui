"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _htmlGenerator = require("../services/html-generator");

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var text = function text(post) {
    return _htmlGenerator2.default.generate("<div id=\"SL-" + post.Id + "\" class=\"SL-TEXT\">\n                            <p>" + post.Content + "</p>\n                        </div>");
};

exports.default = text;