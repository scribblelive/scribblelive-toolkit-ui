"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var ConsentForm = function ConsentForm(data, accept, reject) {
    setTimeout(function () {
        document.getElementById("accept_button").addEventListener('click', accept);
        document.getElementById("reject_button").addEventListener('click', reject);
    });

    return "\n    <div id=\"consent-form-pinboard\">\n        <div class=\"consent-form\">\n            <div>\n                <h1>" + data.ConsentFormTitle + "</h1>\n            </div>\n            <div>\n                " + data.ConsentFormText + "\n            </div>\n            <br />\n            <div class=\"consent-form-buttons\">\n                <button id=\"accept_button\" class=\"consent-form-button consent-form-button__text\">" + data.ConsentFormConfirmText + "</button>\n                <button id=\"reject_button\" class=\"consent-form-button consent-form-button__text\">" + data.ConsentFormCancelText + "</button>\n            </div>\n        </div>\n    </div>";
};

exports.default = ConsentForm;