'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _htmlGenerator = require('../services/html-generator');

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

var _coreWrapper = require('../services/core-wrapper');

var _coreWrapper2 = _interopRequireDefault(_coreWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// holds references to answer elements
var answerElements = {};

var calculatePercentage = function calculatePercentage(answerVotes, totalVotes) {
  if (answerVotes === 0 || totalVotes === 0) {
    return 0;
  }
  return Math.floor(100 * (answerVotes / totalVotes));
};

var generateWrapper = function generateWrapper(post, pollClass) {
  return '<div id="SL-' + post.Id + '" class="SL-POLL ' + pollClass + '">\n            <p class="poll-question">' + post.Content + '</p>\n          </div>';
};

var generateVoteText = function generateVoteText(votes) {
  return '(' + votes + ')';
};

/**
 * Sets the reference data for later use
 * @param answer
 * @param element
 */
var setAnswerElements = function setAnswerElements(_ref) {
  var answer = _ref.answer,
      element = _ref.element;

  if (!answer || !answer.Id || !element) return;
  answerElements[answer.Id] = (0, _assign2.default)({}, answerElements[answer.Id], {
    element: element,
    data: answer
  });
};

var pollingCallback = function pollingCallback(err, data) {
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    var _loop = function _loop() {
      var answer = _step.value;

      var prevAnswer = answerElements[answer.Id];

      if (!prevAnswer) {
        return 'continue';
      }

      var element = prevAnswer.element;

      var percentageElement = element.querySelector('.poll-result-item-percentage');
      var percentage = calculatePercentage(answer.Votes, data.TotalVotes);
      var prevPercentage = parseInt(percentageElement.innerHTML, 10);

      element.querySelector('.poll-result-item-description__votes').innerHTML = generateVoteText(answer.Votes);
      element.querySelector('.poll-result-item-bar__color-bar').style.width = percentage + '%';

      var start = prevPercentage;
      var interval = setInterval(function () {
        if (percentage > prevPercentage) {
          start++;
        } else if (percentage < prevPercentage) {
          start--;
        }

        percentageElement.innerHTML = start + '%';
        if (start === percentage) {
          clearInterval(interval);
          setAnswerElements({ answer: answer, element: element });
        }
      }, 100);
    };

    for (var _iterator = (0, _getIterator3.default)(data.Answers), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _ret = _loop();

      if (_ret === 'continue') continue;
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }
};

var renderResultView = function renderResultView(post) {
  var _post$Entities = post.Entities,
      answers = _post$Entities.Answers,
      totalVotes = _post$Entities.TotalVotes,
      pollId = _post$Entities.Id;

  var answerContainer = _htmlGenerator2.default.generate('<div class="poll-results"></div>');

  answers.forEach(function (answer) {
    var percentage = calculatePercentage(answer.Votes, totalVotes);
    var voteText = generateVoteText(answer.Votes);
    var element = _htmlGenerator2.default.generate('<div class="poll-result-item">\n                                                    <div class="poll-result-item-row">\n                                                      <div class="poll-result-item-description">\n                                                          <span class="poll-result-item-description__text">' + answer.Text + '</span> \n                                                          <span class="poll-result-item-description__votes">' + voteText + '</span>\n                                                      </div>\n                                                      <div class="poll-result-item-percentage">\n                                                          ' + percentage + '%\n                                                      </div>\n                                                    </div>\n                                                    <div class="poll-result-item-bar">\n                                                        <span class="poll-result-item-bar__color-bar" \n                                                                style="width:' + percentage + '%"></span>\n                                                    </div>\n                                                </div>');
    answerContainer.appendChild(element);
    setAnswerElements({ answer: answer, element: element });
  });

  _coreWrapper2.default.instance.Poll.listen({ pollId: pollId, cb: pollingCallback });
  return answerContainer;
};

var renderVoteView = function renderVoteView(post) {
  var _post$Entities2 = post.Entities,
      answers = _post$Entities2.Answers,
      pollId = _post$Entities2.Id;

  var streamId = post.StreamId;
  var answerContainer = _htmlGenerator2.default.generate('<div class="poll-answers"></div>');
  var wrapper = _htmlGenerator2.default.generate(generateWrapper(post, 'voting'));

  answers.forEach(function (answer, index) {
    var answerDom = _htmlGenerator2.default.generate('<div class="poll-answer">' + answer.Text + '</div>');
    var hoverClass = 'poll-answer--hover';
    var vote = function vote() {
      answer.Votes++;
      post.Entities.TotalVotes++;
      _coreWrapper2.default.instance.Poll.vote({
        streamId: streamId,
        pollId: pollId,
        selectionId: answer.Id
      }).catch(function () {});

      answerContainer.classList.add('poll-answers--hidden');
      answerContainer.addEventListener('transitionend', function () {
        var resultsDom = renderResultView(post);
        answerContainer.parentNode.removeChild(answerContainer);
        resultsDom.classList.add('poll-results--hidden');
        wrapper.appendChild(resultsDom);
        resultsDom.classList.remove('poll-results--hidden');
      });
    };

    var hover = function hover(e) {
      e.target.classList.add(hoverClass);
    };

    var mouseout = function mouseout(e) {
      e.target.classList.remove(hoverClass);
    };

    if (index === answers.length - 1) {
      answerDom.classList.add('poll-answer--last');
    }

    answerDom.addEventListener('click', vote);
    answerDom.addEventListener('mouseover', hover);
    answerDom.addEventListener('mouseout', mouseout);
    answerContainer.appendChild(answerDom);
  });

  wrapper.appendChild(answerContainer);
  return wrapper;
};

var poll = function poll(post) {
  var streamId = post.StreamId;
  var pollId = post.Entities.Id;

  if (_coreWrapper2.default.instance.Poll.hasVoted({ streamId: streamId, pollId: pollId })) {
    var wrapper = _htmlGenerator2.default.generate(generateWrapper(post, 'result'));
    var pollContent = renderResultView(post);
    wrapper.appendChild(pollContent);
    return wrapper;
  }
  return renderVoteView(post);
};

exports.default = poll;