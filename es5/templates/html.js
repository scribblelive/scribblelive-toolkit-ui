'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _htmlGenerator = require('../services/html-generator');

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function renderHTMLScriptElements(post) {
  try {
    var postHTML = unescape(post.Content);

    var isScript = false;
    if (postHTML.indexOf('&lt;script') > -1 || postHTML.indexOf('<script') > -1) {
      isScript = true;
    }

    if (isScript) {
      // will render the scribble embed for this post id
      var scriptEmbed = '<div class="scrbbl-post-embed" data-post-id="' + post.Id + '"></div>';
      return scriptEmbed;
    }
  } catch (err) {}

  return post.Content;
}

function imageOnRight(html) {
  var cols = html.getElementsByClassName('col-md-6');
  if (!cols || cols.length !== 2) return false;
  // There is an image on the right, but not on the left.
  if (cols[1].getElementsByTagName('img').length && cols[0].getElementsByTagName('img').length === 0) {
    return true;
  }
  return false;
}

var html = function html(post) {
  var htmlPost = _htmlGenerator2.default.getHtml(post.Content);
  var imgOnRight = imageOnRight(htmlPost);
  // console.log(htmlPost);
  return _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-HTML' + (imgOnRight ? ' img-right' : '') + '">\n                                    ' + renderHTMLScriptElements(post) + '\n                                    </div>');
};

exports.default = html;