'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _htmlGenerator = require('../services/html-generator');

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

var _postHelper = require('../services/post-helper');

var _postHelper2 = _interopRequireDefault(_postHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var adUrl = 'https://client-ads.scribblelive.com';

function getAd(clientId, ad) {
  var Id = ad.Id;
  var Size = ad.Size || ad.EventId;
  var clientDir = ('' + clientId).split('').join('/');
  return adUrl + '/' + clientDir + '/' + Id + '_' + clientId + '_' + Size + '.html?calculateWidth';
}

function getValidAd(ads) {
  var width = document.documentElement.clientWidth;
  var target = null;
  for (var i = 0; i < ads.length; i++) {
    if (!ads[i].Id || !ads[i].Size && !ads[i].EventId) {
      continue;
    } else if (!target && (ads[i].Size < width || ads[i].EventId)) {
      target = ads[i];
      if (ads[i].EventId) break;
    }
  }
  if (!target) {
    return ads[0];
  }
  return target;
}

function sortBySize(a, b) {
  if (a.Size < b.Size) return 1;else if (a.Size > b.Size) return -1;
  return 0;
}

var advertisement = function advertisement(post) {
  var adData = _postHelper2.default.getAdData();
  var adScript = _postHelper2.default.getAdScript();

  if (adData) {
    adData.ads.sort(sortBySize);
  }

  var ad = getValidAd(adData.ads);
  var url = getAd(adData.clientId, ad);
  if (url) {
    if (!adScript) {
      // Set window.eventListener
      window.addEventListener('message', function (data) {
        try {
          if (typeof data.data !== 'string') return;
          var adInfo = JSON.parse(data.data.replace('scrbbl-resizableiframe: ', ''));

          //switch to classes because all the iframes are loaded with same id
          //when we lookup iframe for resizing, we only get one. the class
          //allows us to resize all.
          var targets = document.getElementsByClassName('SL-AD-' + adInfo.AdId);

          //the width of the advertisement can be null if the ad code was placed
          //during creation of the story. So we'll  check for null if no element
          //with existing class was found ad act upon that.
          if (targets.length == 0) {
            targets = document.getElementsByClassName('SL-AD-' + adInfo.AdId + '-null');
          }

          var _iteratorNormalCompletion = true;
          var _didIteratorError = false;
          var _iteratorError = undefined;

          try {
            for (var _iterator = (0, _getIterator3.default)(targets), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              var iframe = _step.value;

              iframe.height = adInfo.height + 'px';
              iframe.width = adInfo.width;
            }
          } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
              }
            } finally {
              if (_didIteratorError) {
                throw _iteratorError;
              }
            }
          }
        } catch (e) {}
      });
      _postHelper2.default.setAdScript(true);
    }
  }
  return _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-ADVERTISEMENT">\n                                        <iframe src="' + url + '" width="' + ad.Size + '" id="SL-AD-' + ad.Id + '-' + ad.Size + '" class="SL-AD-' + ad.Id + '" frameborder="0">\n                                            <div>Your browser doesn\'t support iframes</div>\n                                        </iframe>\n                                    </div>');
};

exports.default = advertisement;