'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _htmlGenerator = require('../services/html-generator');

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var audio = function audio(post) {

  var caption = post.Caption || '';

  var audio = _htmlGenerator2.default.generate('<div class="SL-AUDIO" id="SL-' + post.Id + '">\n      <audio src="' + post.Media.Url + '" controls>\n      </audio>\n      <div class="caption-container"><p>' + caption + '</p></div>\n    </div>');

  return audio;
};

exports.default = audio;