"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _htmlGenerator = require("../services/html-generator");

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var facebook = function facebook(post) {
    return _htmlGenerator2.default.generate("<div id=\"SL-" + post.Id + "\" class=\"SL-FACEBOOK\">\n                                " + post.Content + "\n                            </div>");
};

exports.default = facebook;