'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = untracked;

var _untrackedSocialPostRenderer = require('@scrbbl/untracked-social-post-renderer');

var _untrackedSocialPostRenderer2 = _interopRequireDefault(_untrackedSocialPostRenderer);

var _htmlGenerator = require('../services/html-generator');

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function untracked(post) {
  return _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-HTML">\n    ' + _untrackedSocialPostRenderer2.default.render(post) + '</div>');
}