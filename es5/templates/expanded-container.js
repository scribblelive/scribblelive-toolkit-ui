'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _htmlGenerator = require('../services/html-generator');

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

var _expandFunctions = require('../services/expand-functions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var expandedContainer = function expandedContainer(post) {
  var container = _htmlGenerator2.default.generate('<div class="SL-EXPANDED">\n      <div class="fa fa-times SL-expanded-close"></div>\n      <div class="SL-expanded-inner">\n        <img class="SL-expanded-image" src="' + post.Media.Url + '"/>\n        ' + (post.Caption ? '<div class="SL-expanded-caption">' + post.Caption + '</div>' : '') + '\n      </div>\n    </div>');

  container.getElementsByClassName('SL-expanded-close')[0].onclick = _expandFunctions.closeExpandedImage;

  return container;
};

exports.default = expandedContainer;