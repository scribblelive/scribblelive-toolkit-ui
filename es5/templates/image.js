'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _htmlGenerator = require('../services/html-generator');

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

var _expandFunctions = require('../services/expand-functions');

var _helperFunctions = require('../services/helper-functions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getAppropriateImageSize(media) {
  if (!media.Sizes) return media.Url;
  var width = document.documentElement.clientWidth;
  var validUrl = media.Url;
  for (var size in media.Sizes) {
    if (size > width) {
      return validUrl;
    }
    validUrl = media.Sizes[size];
  }
  return validUrl;
}

function getSizes(media) {
  if (!media.Sizes) return media.Url;
  var list = [];
  var keys = (0, _keys2.default)(media.Sizes);
  for (var i = 0; i < keys.length; i++) {
    if (keys[i] === 'orig') {
      list.push(media.Sizes[keys[i]] + ' ' + (parseInt(keys[i - 1], 10) + 1) + 'w');
    } else {
      list.push(media.Sizes[keys[i]] + ' ' + keys[i] + 'w');
    }
  }
  return list.join(', ');
}

function mobileCheck() {
  return typeof window.orientation !== "undefined" || navigator.userAgent.indexOf('IEMobile') !== -1;
}

function isIE() {
  var ua = window.navigator.userAgent;
  if (ua.indexOf('MSIE ') > -1 || ua.indexOf('Trident') > -1) {
    return true;
  }
  return false;
}

function inIframe() {
  try {
    return window.self !== window.top;
  } catch (e) {
    return true;
  }
}

function getImageSet(post) {
  if (isIE()) {
    var src = post.Media.Sizes && post.Media.Sizes['orig'] ? post.Media.Sizes['orig'] : post.Media.Url;
    return '<picture><img src="' + src + '" size="300w" /></picture>';
  }
  return '<picture class="section-background-picture">\n      <source srcset="' + getSizes(post.Media) + '">\n      <img src="' + post.Media.Url + '" size="300w;"">\n  </picture>';
}

var image = function image(post, index) {
  var caption = '<div class="caption-container"></div>';
  var hero = '';
  var arrow = '';
  var transitionImage = !post.imageClose ? null : ' TRANSITION';

  if (post.PostMeta && post.PostMeta.Tags && post.PostMeta.Tags.indexOf('hero') > -1) {
    hero = ' hero-image';

    if (!(inIframe() && mobileCheck())) {
      hero = hero + ' desktop';
    }
  }
  if (hero && index !== undefined && index === 0) {
    arrow = '<div class="hero-arrow-button">\n        <div class="arrow"></div>\n      </div>';
  }
  if (post.Caption && post.Caption.length > 0) {
    if (hero.length > 0) {
      caption = '<div class="caption-container"><div>' + post.Caption + '</div></div>';
    } else {
      caption = '<div class="caption-container"><p>' + post.Caption + '</p></div>';
    }
  }
  var image = null;
  if (hero.length > 0) {
    image = _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-IMAGE' + hero + '" style="background-image:url(\'' + getAppropriateImageSize(post.Media) + '\')">\n            ' + caption + '\n            ' + arrow + '\n        </div>');
  } else if (!transitionImage) {
    image = _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-IMAGE">\n        <img src="' + getAppropriateImageSize(post.Media) + '">\n            ' + caption + '\n        </div>');
  } else {
    image = _htmlGenerator2.default.generate('<div id="SL-' + post.Id + '" class="SL-IMAGE' + transitionImage + '" >\n          ' + caption + '\n          <div class="section-background fixed">\n              ' + getImageSet(post) + '\n          </div>\n      </div>');
  }

  if (post.Media && post.Media.Sizes && post.Media.Sizes['800'] && mobileCheck()) {
    var largerImage = _htmlGenerator2.default.generate('<img style="display:none; width:0; height:0;" src="' + post.Media.Sizes['800'] + '" />');
    largerImage.onload = function (_ref) {
      var target = _ref.target;

      if (image.style.backgroundImage !== '') {
        image.style.backgroundImage = 'url(' + target.src + ')';
      } else {
        image.getElementsByTagName('img')[0].src = target.src;
        image.getElementsByTagName('img')[0].size = '800w';
      }
    };
  }

  function scrollToY() {
    var scrollTargetY = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
    var speed = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2000;
    var easing = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'easeOutSine';

    // scrollTargetY: the target scrollY property of the window
    // speed: time in pixels per second
    // easing: easing equation to use

    var scrollY = window.scrollY,
        currentTime = 0;

    // min time .1, max time .8 seconds
    var time = Math.max(0.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, 0.8));

    // easing equations from https://github.com/danro/easing-js/blob/master/easing.js
    var PI_D2 = Math.PI / 2,
        easingEquations = {
      easeOutSine: function easeOutSine(pos) {
        return Math.sin(pos * (Math.PI / 2));
      },
      easeInOutSine: function easeInOutSine(pos) {
        return -0.5 * (Math.cos(Math.PI * pos) - 1);
      },
      easeInOutQuint: function easeInOutQuint(pos) {
        if ((pos /= 0.5) < 1) {
          return 0.5 * Math.pow(pos, 5);
        }
        return 0.5 * (Math.pow(pos - 2, 5) + 2);
      }
    };

    // add animation loop
    function tick() {
      currentTime += 1 / 60;

      var p = currentTime / time;
      var t = easingEquations[easing](p);

      if (p < 1) {
        requestAnimFrame(tick);
        window.scrollTo(0, scrollY + (scrollTargetY - scrollY) * t);
      } else {
        window.scrollTo(0, scrollTargetY);
      }
    }

    // call it once to get started
    tick();
  }

  if (arrow.length > 0) {
    window.requestAnimFrame = function () {
      return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (callback) {
        window.setTimeout(callback, 1000 / 60);
      };
    }();

    var _arrow = image.querySelector('.hero-arrow-button .arrow');
    if (_arrow) {
      _arrow.onclick = function (_ref2) {
        var target = _ref2.target;

        if (!target) return;
        var imageBottom = window.scrollY + target.parentNode.getBoundingClientRect().bottom;
        scrollToY(imageBottom, 3000);
      };
    }
  }

  if ((0, _helperFunctions.isMobile)()) {
    if (arrow.length > 0) {
      image.querySelector('.caption-container').onclick = function () {
        return (0, _expandFunctions.expandImage)(post);
      };
    } else {
      image.onclick = function () {
        return (0, _expandFunctions.expandImage)(post);
      };
    }
  }

  return image;
};

exports.default = image;