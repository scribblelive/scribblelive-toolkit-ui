'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _htmlGenerator = require('../services/html-generator');

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function footer() {
  return _htmlGenerator2.default.generate('<div class="SL-footer">\n      <a href="http://www.scribblelive.com/products/content-curation-live-blog/">\n        <img src="https://s3.amazonaws.com/scribblelive-visualizations/stencils/27c31655-1058-4fe0-a29e-3ea4277de1b2/poweredby.png" />\n      </a>\n    </div>');
}

exports.default = footer;