'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _htmlGenerator = require('../services/html-generator');

var _htmlGenerator2 = _interopRequireDefault(_htmlGenerator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function isMobile() {
  return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
  );
}

function renderPosts(posts) {
  var html = [];
  posts.forEach(function (postData) {
    html.push(postData.fn(postData.post));
  });
  return html;
}

var List = function () {
  function List() {
    (0, _classCallCheck3.default)(this, List);

    this.items = [];
  }

  (0, _createClass3.default)(List, [{
    key: 'append',
    value: function append(postFunc) {
      this.items.push(postFunc);
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.items.length === 1) {
        return this.items[0].fn(this.items[0].post);
      }
      var ids = this.items.map(function (postData) {
        return postData.post.Id;
      }).join('-');
      var html = _htmlGenerator2.default.generate('<div class=\'LIST\' id=\'SL-List-' + ids + '\'></div>');
      renderPosts(this.items).forEach(function (post) {
        html.appendChild(post);
      });
      return html;
    }
  }, {
    key: 'Id',
    get: function get() {
      if (this.items.length === 1) return this.items[0].post.Id;
      var ids = this.items.map(function (postData) {
        return postData.post.Id;
      }).join('-');
      return 'List-' + ids;
    }
  }]);
  return List;
}();

exports.default = List;