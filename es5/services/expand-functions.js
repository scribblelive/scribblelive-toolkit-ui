'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.expandImage = expandImage;
exports.closeExpandedImage = closeExpandedImage;

var _postStore = require('./post-store');

var _postStore2 = _interopRequireDefault(_postStore);

var _renderingOptions = require('./rendering-options');

var _renderingOptions2 = _interopRequireDefault(_renderingOptions);

var _expandedContainer = require('../templates/expanded-container');

var _expandedContainer2 = _interopRequireDefault(_expandedContainer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function expandImage(post) {
  if (!_postStore2.default.expandedContainer) {
    _postStore2.default.expandedContainer = _renderingOptions2.default.rootEl.appendChild((0, _expandedContainer2.default)(post));
    document.body.classList.add('expanded');
  }
}

function closeExpandedImage() {
  _renderingOptions2.default.rootEl.removeChild(_postStore2.default.expandedContainer);
  _postStore2.default.expandedContainer = null;
  document.body.classList.remove('expanded');
}