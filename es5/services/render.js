'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _postHelper = require('./post-helper');

var _postHelper2 = _interopRequireDefault(_postHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * RenderService contains wrapper functions to call PostHelper.
 */
var RenderService = function () {
  function RenderService() {
    (0, _classCallCheck3.default)(this, RenderService);
  }

  (0, _createClass3.default)(RenderService, null, [{
    key: 'list',
    value: function list(posts, options) {
      posts.forEach(_postHelper2.default.update);
      _postHelper2.default.sort();
      _postHelper2.default.posts.forEach(function (post) {
        return post.HasUpdated ? _postHelper2.default.sync(post) : '';
      });
      _postHelper2.default.organizeTypes();
      _postHelper2.default.renderPosts();
    }
  }, {
    key: 'adData',
    value: function adData(data) {
      _postHelper2.default.setAdData(data);
    }
  }]);
  return RenderService;
}();

exports.default = RenderService;