'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HtmlGenerator = function () {
  function HtmlGenerator() {
    (0, _classCallCheck3.default)(this, HtmlGenerator);
  }

  (0, _createClass3.default)(HtmlGenerator, null, [{
    key: 'generate',
    value: function generate(str) {
      return postProcess(this.getHtml(str));
    }
  }, {
    key: 'getHtml',
    value: function getHtml(str) {
      var div = document.createElement('div');
      div.innerHTML = str;
      return div.firstChild;
    }
  }]);
  return HtmlGenerator;
}();

exports.default = HtmlGenerator;


function postProcess(ele) {
  addTargetBlank(ele);
  return ele;
}

function addTargetBlank(ele) {
  var as = ele.getElementsByTagName('a');
  for (var i = 0; i < as.length; i++) {
    as[i].setAttribute('target', '_blank');
  }
}