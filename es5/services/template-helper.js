'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.removeWhiteSpace = removeWhiteSpace;
function removeWhiteSpace(str) {
  return str.replace(/>\s\s+</g, '><');
}