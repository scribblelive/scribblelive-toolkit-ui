'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

exports.addStreamIds = addStreamIds;
exports.formatDate = formatDate;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function addStreamIds(posts, streamId) {
  return posts.map(function (post) {
    return (0, _extends3.default)({}, post, { StreamId: post.StreamId || streamId });
  });
}

function formatDate(dateVal) {
  var date = dateVal;
  if (typeof date.getMonth !== 'function') {
    // assume string provided
    try {
      date = new Date(dateVal);
      if (isNaN(date.getMonth())) {
        date = new Date(dateVal.replace(/\+/g, '.'));
      }
      // For IE
      if (isNaN(date.getMonth())) {
        var dateArr = dateVal.split(' ');
        date = new Date(Date.parse(dateArr[1] + ' ' + dateArr[2] + ', ' + dateArr[5] + ' ' + dateArr[3] + ' UTC'));
      }
    } catch (e) {
      return '';
    }
  }

  var lang = window.navigator.userLanguage || window.navigator.language;

  var hour = date.getHours();
  var displayHours = hour === 0 ? 12 : hour;
  var mins = date.getMinutes();
  var displayMins = mins.toString().length > 1 ? mins : '0' + mins;
  var displayTime = displayHours > 12 ? displayHours - 12 + ':' + displayMins + ' PM' : displayHours + ':' + displayMins + ' AM';
  var day = date.getDate();
  var month = date.toLocaleString(lang, { month: 'long' }).substr(0, 3);
  var year = date.getFullYear();

  return displayTime + ' - ' + day + ' ' + month + ' ' + year;
}