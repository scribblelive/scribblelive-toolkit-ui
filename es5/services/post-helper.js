'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _postStore = require('./post-store');

var _postStore2 = _interopRequireDefault(_postStore);

var _renderingOptions = require('./rendering-options');

var _renderingOptions2 = _interopRequireDefault(_renderingOptions);

var _list = require('../templates/list');

var _list2 = _interopRequireDefault(_list);

var _footer = require('../templates/footer');

var _footer2 = _interopRequireDefault(_footer);

var _helperFunctions = require('./helper-functions');

var _uiConfigService = require('./ui-config-service');

var _uiConfigService2 = _interopRequireDefault(_uiConfigService);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * PostHelper controls the updating and placement of posts on the page.
 */
var PostHelper = function () {
  function PostHelper() {
    (0, _classCallCheck3.default)(this, PostHelper);
  }

  (0, _createClass3.default)(PostHelper, null, [{
    key: 'getAdData',
    value: function getAdData() {
      return _postStore2.default.adData;
    }
  }, {
    key: 'getAdScript',
    value: function getAdScript() {
      return _postStore2.default.adScriptEnabled;
    }

    /*
        SETTERS
     */

  }, {
    key: 'setAdScript',
    value: function setAdScript(val) {
      _postStore2.default.adScriptEnabled = val;
    }

    /*
        STATIC FUNCTIONS
     */

    /**
     * This function will mark, add, update and/or remove any new posts.
     * @param post
     */

  }, {
    key: 'update',
    value: function update(post) {

      post.HasUpdated = true;

      if (post.IsDeleted === 1) {
        // Delete post from DOM and STORE.
        if (getPostIndex(post) !== -1) {
          _postStore2.default.posts.splice(getPostIndex(post), 1);
          return removePost(post);
        }
        return;
      }

      var postIndex = getPostIndex(post);
      if (postIndex === -1) {
        _postStore2.default.posts.push(post);
        return;
      }

      var postDate = null;
      var oldDate = null;
      try {
        postDate = post.LastModifiedDate ? new Date(post.LastModifiedDate) : new Date(post.Date + ' GMT');
        oldDate = _postStore2.default.posts[postIndex].LastModifiedDate ? new Date(_postStore2.default.posts[postIndex].LastModifiedDate) : new Date(_postStore2.default.posts[postIndex].Date + ' GMT');
      } catch (e) {
        return;
      }
      if (postDate > oldDate) {
        _postStore2.default.posts[postIndex] = post;
      }
    }

    /**
     * This function will sort posts by creationDate by default although allow
     * a user to pass in a custom sorting method.
     * @param sortFunc
     */

  }, {
    key: 'sort',
    value: function sort() {
      var sortFunc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : sortPostsByCreationDate;

      _postStore2.default.posts.sort(sortFunc);
    }
  }, {
    key: 'organizeTypes',
    value: function organizeTypes() {
      var target = 'IMAGE';
      var lastFound = -2;
      // Copying posts to renderList

      if (!_uiConfigService2.default.newestAtBottom) {
        _postStore2.default.renderList = _postStore2.default.posts.reverse().map(function (post) {
          return post;
        });
      } else {
        _postStore2.default.renderList = _postStore2.default.posts.map(function (post) {
          return post;
        });
      }

      // Loop over renderList and find connected posts.
      for (var i = 0; i < _postStore2.default.renderList.length; i++) {
        if (_postStore2.default.renderList[i].Type === target) {
          if (lastFound === i - 1) {
            _postStore2.default.renderList[i - 1].imageClose = true;
            _postStore2.default.renderList[i].imageClose = true;
            lastFound = i;
          } else {
            lastFound = i;
          }
        }
      }
    }

    /**
     * This function will remove the HasUpdated flag, remove the post from the page
     * and redraw the post on the page.
     * @param post
     */

  }, {
    key: 'sync',
    value: function sync(post) {
      _postStore2.default.posts[getPostIndex(post)].HasUpdated = false;
      removePost(post);
    }
  }, {
    key: 'isFilterNeeded',
    value: function isFilterNeeded() {
      return _postStore2.default.consentFormData.filterPost;
    }
  }, {
    key: 'filterPosts',
    value: function filterPosts(posts) {
      return posts.filter(function (post) {
        if (post.PostMeta && post.PostMeta.Type && (post.PostMeta.Type.indexOf('facebook:post') > -1 || post.PostMeta.Type.indexOf('instagram:post') > -1)) {
          return false;
        } else if (post.PostMeta && post.PostMeta.Data && ((0, _stringify2.default)(post.PostMeta.Data).indexOf('\"type\":\"FACEBOOK\"') > -1 || (0, _stringify2.default)(post.PostMeta.Data).indexOf('\"type\":\"INSTAGRAM\"') > -1)) {
          return false;
        } else if (post.Content && (post.Content.indexOf('data-url=\"https://www.instagram.com/') > -1 || post.Content.indexOf('href=\\"https://www.facebook.com/') > -1 || post.Content.indexOf(encodeURIComponent('data-url=\"https://www.instagram.com/')) > -1 || post.Content.indexOf(encodeURIComponent('href=\\"https://www.facebook.com/')) > -1 || post.Content.indexOf('data-url%3D%22https%3A//www.instagram.com/') > -1 || post.Content.indexOf('href%3D%22https%3A//www.facebook.com/') > -1)) {
          return false;
        }
        return true;
      });
    }
  }, {
    key: 'renderPosts',
    value: function renderPosts() {
      if (_postStore2.default.consentFormData.showModal || _postStore2.default.consentFormData.loading) return;
      _postStore2.default.renderList = this.isFilterNeeded() ? this.filterPosts(_postStore2.default.renderList) : _postStore2.default.renderList;
      for (var i = 0; i < _postStore2.default.renderList.length; i++) {
        var newPost = _postStore2.default.renderList[i];
        putPost(newPost, i);
      }
      if (!_postStore2.default.footerRendered) {
        this.renderFooter();
      }
    }
  }, {
    key: 'renderFooter',
    value: function renderFooter() {
      _renderingOptions2.default.rootEl.appendChild((0, _footer2.default)());
      _postStore2.default.footerRendered = true;
    }
  }, {
    key: 'setAdData',
    value: function setAdData(data) {
      _postStore2.default.adData = data;
    }
  }, {
    key: 'posts',


    /*
        GETTERS
     */

    /**
     * This is a getter which returns the posts in the STORE.
     * @returns {Array}
     */
    get: function get() {
      return _postStore2.default.posts;
    }
  }]);
  return PostHelper;
}();

/**
 * This function will find the passed in post within the store and return it's index.
 * @param post
 * @returns {number}
 */


exports.default = PostHelper;
function getPostIndex(post) {
  for (var i = 0; i < _postStore2.default.posts.length; i++) {
    if (_postStore2.default.posts[i].Id === post.Id) {
      return i;
    }
  }
  return -1;
}

/**
 * This function will check if a post is currently on the page.
 * @param  {[type]} post [description]
 * @return {[type]}      [description]
 */
function currentlyExistsOnPage(post) {
  var target = document.getElementById('SL-' + post.Id);
  return target !== undefined && target !== null;
}

/**
 * This function will put a post on the page in the order it is listed.
 * @param post
 */
function putPost(post, index) {
  if (!post.HasUpdated && currentlyExistsOnPage(post)) return;
  var htmlEle = null;
  if (post instanceof _list2.default) {
    htmlEle = post.render();
  } else {
    var renderFunc = _renderingOptions2.default.getRenderer(post.Type, post);
    if (renderFunc === null || typeof renderFunc !== 'function') return;
    htmlEle = renderFunc(post, index);
  }

  //check the current page number. then based on it, either prepend or append
  //if newestAtBottom, append
  //if newestNotAtBottom, prepend

  if (_uiConfigService2.default.currentPage == 1) {
    if (index === 0) {
      _renderingOptions2.default.rootEl.insertBefore(htmlEle, _renderingOptions2.default.rootEl.childNodes[0]);
      _uiConfigService2.default.incrementLastRenderedIndex();
      return;
    } else if (currentlyExistsOnPage(post)) {
      var target = document.getElementById('SL-' + post.Id);
      target = htmlEle;
      return;
    }
    var targetEle = document.getElementById('SL-' + _postStore2.default.renderList[index - 1].Id);

    insertAfter(targetEle, htmlEle);
    _uiConfigService2.default.incrementLastRenderedIndex();
    return;
  }

  var targetEl = _renderingOptions2.default.rootEl.childNodes[_uiConfigService2.default.lastRenderedIndex];
  insertAfter(targetEl, htmlEle);
  //targetEl.appendChild(htmlEle);
  _uiConfigService2.default.incrementLastRenderedIndex();
  return;
}

/**
 * This function will add a post after a reference to another post.
 * @param referenceNode
 * @param newNode
 */
function insertAfter(referenceNode, newNode) {
  if (referenceNode && referenceNode.parentNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  }
}

/**
 * This function will remove a post from the page.
 * @param post
 * @returns {boolean} based on whether the post was removed or not.
 */
function removePost(post) {
  var target = document.getElementById('SL-' + post.Id);
  if (!target) return false;
  var parent = target.parentNode;
  parent.removeChild(target);
  return true;
}

/**
 * This is the default sorting function which sorts by a posts CreationDate.
 * @param a
 * @param b
 * @returns {number}
 */
function sortPostsByCreationDate(a, b) {
  if (new Date(a.CreationDate) < new Date(b.CreationDate)) return _renderingOptions2.default.newestAtBottom(1);
  if (new Date(a.CreationDate) > new Date(b.CreationDate)) return _renderingOptions2.default.newestAtBottom(-1);
  return 0;
}