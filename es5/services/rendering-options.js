'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _image = require('../templates/image');

var _image2 = _interopRequireDefault(_image);

var _text = require('../templates/text');

var _text2 = _interopRequireDefault(_text);

var _html = require('../templates/html');

var _html2 = _interopRequireDefault(_html);

var _instagram = require('../templates/instagram');

var _instagram2 = _interopRequireDefault(_instagram);

var _facebook = require('../templates/facebook');

var _facebook2 = _interopRequireDefault(_facebook);

var _twitter = require('../templates/twitter');

var _twitter2 = _interopRequireDefault(_twitter);

var _embed = require('../templates/embed');

var _embed2 = _interopRequireDefault(_embed);

var _poll = require('../templates/poll');

var _poll2 = _interopRequireDefault(_poll);

var _video = require('../templates/video');

var _video2 = _interopRequireDefault(_video);

var _advertisement = require('../templates/advertisement');

var _advertisement2 = _interopRequireDefault(_advertisement);

var _list = require('../templates/list');

var _list2 = _interopRequireDefault(_list);

var _audio = require('../templates/audio');

var _audio2 = _interopRequireDefault(_audio);

var _untrackedSocial = require('../templates/untracked-social');

var _untrackedSocial2 = _interopRequireDefault(_untrackedSocial);

var _helperFunctions = require('./helper-functions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DEFAULT_OPTIONS = {
  isNonTracking: null,
  renderingTypes: {
    IMAGE: null,
    TEXT: null,
    HTML: null,
    INSTAGRAM: null,
    FACEBOOK: null,
    TWITTER: null
  },
  rootElement: null,
  posts: []
};

var RenderingOptions = function () {
  function RenderingOptions() {
    (0, _classCallCheck3.default)(this, RenderingOptions);
  }

  (0, _createClass3.default)(RenderingOptions, null, [{
    key: 'init',
    value: function init(options) {
      this._options = (0, _assign2.default)({}, DEFAULT_OPTIONS, options);
      validate(this._options);
    }
  }, {
    key: 'getRenderer',
    value: function getRenderer(postType, post) {
      var content = post.Content;

      switch (postType) {
        case 'IMAGE':
          return this.image;
        case 'TEXT':
          return this.text;
        case 'TWEET':
          return this.twitter;
        case 'FACEBOOK':
          return this.facebook;
        case 'INSTAGRAM':
          return this.instagram;
        case 'HTML':
          if ((0, _helperFunctions.checkIfHtmlIsFacebook)(post)) {
            return this.facebook;
          }
          return this.html;
        case 'EMBED':
          return this.embed;
        case 'POLL':
          return this.poll;
        case 'VIDEO':
          return this.video;
        case 'ADVERTISEMENT':
          return this.advertisement;
        case 'AUDIO':
          return this.audio;
        case 'SLIDESHOW':
          return this.html;
        case 'YOUTUBE':
          return this.html;
        default:
          return null;
      }
    }
  }, {
    key: 'newestAtBottom',
    value: function newestAtBottom(before) {
      var newestAtBottom = true;
      if (this._options && this._options.newestAtBottom) {
        try {
          newestAtBottom = JSON.parse(this._options.newestAtBottom);
          if (typeof newestAtBottom !== "boolean") {
            newestAtBottom = true;
          }
        } catch (e) {
          newestAtBottom = false;
        }
      }

      if (before === 1 && newestAtBottom) {
        if (newestAtBottom) {
          return -1;
        }
        return 1;
      } else if (before === -1) {
        if (newestAtBottom) {
          return 1;
        }
        return -1;
      }
      return before;
    }
  }, {
    key: 'getList',
    value: function getList() {
      return _list2.default;
    }
  }, {
    key: 'rootEl',
    get: function get() {
      var rootEl = null;
      var rootClassName = "scrbbl-storytelling";

      if (this._options && this._options.rootElement) {
        if (typeof this._options.rootElement === 'string') {
          // iframe
          rootEl = document.querySelector(this._options.rootElement);

          if (!document.body.classList.contains(rootClassName)) {
            document.body.classList.add(rootClassName);
          }

          return rootEl;
        } else {
          // script embed

          var existingContainer = document.getElementsByClassName(rootClassName);

          if (existingContainer.length > 0) {
            rootEl = existingContainer[0];
          } else {
            rootEl = document.createElement("div");
            this._options.rootElement.appendChild(rootEl);
          }
        }
      }

      if (!rootEl) {
        rootEl = document.body;
      }

      if (rootEl && !rootEl.classList.contains(rootClassName)) {
        rootEl.classList.add(rootClassName);
      }

      return rootEl;
    }
  }, {
    key: 'isNonTracking',
    get: function get() {
      if (this._options.isNonTracking) return this._options.isNonTracking;
      return false;
    }
  }, {
    key: 'image',
    get: function get() {
      if (this._options.renderingTypes.IMAGE) return this._options.renderingTypes.IMAGE;
      return _image2.default;
    }
  }, {
    key: 'text',
    get: function get() {
      if (this._options.renderingTypes.TEXT) return this._options.renderingTypes.TEXT;
      return _text2.default;
    }
  }, {
    key: 'html',
    get: function get() {
      if (this._options.renderingTypes.HTML) return this._options.renderingTypes.HTML;
      return this.isNonTracking ? _untrackedSocial2.default : _html2.default;
    }
  }, {
    key: 'instagram',
    get: function get() {
      if (this._options.renderingTypes.INSTAGRAM) return this._options.renderingTypes.INSTAGRAM;
      return this.isNonTracking ? _untrackedSocial2.default : _instagram2.default;
    }
  }, {
    key: 'facebook',
    get: function get() {
      if (this._options.renderingTypes.FACEBOOK) return this._options.renderingTypes.FACEBOOK;
      return this.isNonTracking ? _untrackedSocial2.default : _facebook2.default;
    }
  }, {
    key: 'twitter',
    get: function get() {
      if (this._options.renderingTypes.TWITTER) return this._options.renderingTypes.TWITTER;
      return this.isNonTracking ? _untrackedSocial2.default : _twitter2.default;
    }
  }, {
    key: 'poll',
    get: function get() {
      if (this._options.renderingTypes.POLL) return this._options.renderingTypes.POLL;
      return _poll2.default;
    }
  }, {
    key: 'embed',
    get: function get() {
      if (this._options.renderingTypes.EMBED) return this._options.renderingTypes.EMBED;
      return _embed2.default;
    }
  }, {
    key: 'video',
    get: function get() {
      if (this._options.renderingTypes.VIDEO) return this._options.renderingTypes.VIDEO;
      return _video2.default;
    }
  }, {
    key: 'advertisement',
    get: function get() {
      if (this._options.renderingTypes.ADVERTISEMENT) return this._options.renderingTypes.ADVERTISEMENT;
      return _advertisement2.default;
    }
  }, {
    key: 'audio',
    get: function get() {
      if (this._options.renderingTypes.AUDIO) return this._options.renderingTypes.AUDIO;
      return _audio2.default;
    }
  }, {
    key: 'posts',
    get: function get() {
      return this._options.posts;
    }
  }]);
  return RenderingOptions;
}();

exports.default = RenderingOptions;


function validate(options) {
  var keys = (0, _keys2.default)(options.renderingTypes);
  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    if (typeof options[key] !== 'function') {
      delete options[key];
      keys.splice(i, 1);
      i--;
    }
  }
  if (!options.rootElement) {
    throw 'No root element specified.';
  }
}