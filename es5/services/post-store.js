"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Store containing static information regarding post information.
 * @type {{posts: Array}}
 */
exports.default = {
  posts: [],
  renderList: [],
  footerRendered: false,
  videoScrollCreated: false,
  adData: {},
  adScriptEnabled: false,
  expandedContainer: null,
  clientData: null,
  consentFormData: {
    showModal: false,
    filterPost: false,
    loading: false
  }
};