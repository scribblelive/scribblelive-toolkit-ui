"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * UIConfigService keeps track of the page state and configurations
 */
var UIConfigService = function () {
    function UIConfigService() {
        (0, _classCallCheck3.default)(this, UIConfigService);
    }

    (0, _createClass3.default)(UIConfigService, null, [{
        key: "init",
        value: function init(options) {
            this._options = options;
        }

        /**
         * currentPage returns the last page number loaded for posts
         */

    }, {
        key: "incrementCurrentPage",


        /**
         * To be called when a page of posts is loaded
         * Useful to see how many pages of posts are loaded
         */
        value: function incrementCurrentPage() {

            if (this._options.currentPage == null) {
                this._options.currentPage = 0;
            }
            this._options.currentPage = this._options.currentPage + 1;
        }

        /**
         * Controls whether newest posts should be at bottom of list or not
         */

    }, {
        key: "incrementLastRenderedIndex",


        /**
         * Increment the index to point to the recently rendered post
         */
        value: function incrementLastRenderedIndex() {
            if (this._options.lastRenderedIndex == null) {
                this._options.lastRenderedIndex = -1;
            }

            this._options.lastRenderedIndex = this._options.lastRenderedIndex + 1;
        }
    }, {
        key: "currentPage",
        get: function get() {

            if (this._options.currentPage == null) {
                this._options.currentPage = 0;
            }

            return this._options.currentPage;
        }

        /**
         * update the pageSize (number of records retrieved)
         */

    }, {
        key: "pageSize",
        set: function set(size) {
            this._options.pageSize = size;
        }

        /**
         * Returns the pageSize configured
         */
        ,
        get: function get() {
            var value = this._options.pageSize ? this._options.pageSize : 10;

            if (value <= 0 || value > 100) {
                return 100;
            }

            return value;
        }
    }, {
        key: "newestAtBottom",
        get: function get() {
            return this._options.newestAtBottom ? this._options.newestAtBottom : false;
        }

        /**
         * Updates the flag to control whether new posts should be first or last
         */
        ,
        set: function set(value) {
            this._options.newestAtBottom = value ? true : false;
        }

        /**
         * Get the index of the last rendered post
         */

    }, {
        key: "lastRenderedIndex",
        get: function get() {
            if (this._options.lastRenderedIndex == null) {
                this._options.lastRenderedIndex = -1;
            }

            return this._options.lastRenderedIndex;
        }
    }]);
    return UIConfigService;
}();

exports.default = UIConfigService;