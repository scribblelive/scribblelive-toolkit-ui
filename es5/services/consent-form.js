'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _postStore = require('./post-store');

var _postStore2 = _interopRequireDefault(_postStore);

var _jsCookie = require('js-cookie');

var _jsCookie2 = _interopRequireDefault(_jsCookie);

var _consentForm2 = require('../templates/consent-form');

var _consentForm3 = _interopRequireDefault(_consentForm2);

var _renderingOptions = require('./rendering-options');

var _renderingOptions2 = _interopRequireDefault(_renderingOptions);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ConsentFormService = function () {
    function ConsentFormService() {
        (0, _classCallCheck3.default)(this, ConsentFormService);
    }

    (0, _createClass3.default)(ConsentFormService, null, [{
        key: 'init',
        value: function init(_ref) {
            var token = _ref.token,
                baseUrl = _ref.baseUrl;

            this.token = token;
            this.baseUrl = baseUrl || 'https://api.scribblelive.com/v1';
        }
    }, {
        key: 'getClientData',
        value: function getClientData() {
            var _this = this;

            _postStore2.default.consentFormData.loading = true;
            return (0, _axios2.default)(this.baseUrl + '/client?token=' + this.token + '&includePrivacySettings=true').then(function (res) {
                var data = res.data;
                if (data) _postStore2.default.clientData = data;
                _this.checkCookies();
                _postStore2.default.consentFormData.loading = false;
                _this.renderConsentForm();
            }).catch(function (error) {
                _postStore2.default.consentFormData.loading = false;
                _this.renderConsentForm();
                throw error;
            });
        }
    }, {
        key: 'checkCookies',
        value: function checkCookies() {
            var clientData = _postStore2.default.clientData ? _postStore2.default.clientData : null;
            if (clientData && clientData.Id && clientData.ConsentForm === '1' && clientData.ConsentFormLastModified) {
                var clientId = clientData.Id;
                var cookieData = _jsCookie2.default.get();
                var filteredPosts = cookieData[this.getFilteredPostField(clientId)];
                var lastModified = cookieData[this.getFormLastModifiedField(clientId)];
                if (!filteredPosts || !lastModified) {
                    _postStore2.default.consentFormData.showModal = true;
                    _postStore2.default.consentFormData.filterPost = false;
                } else if (new Date(clientData.ConsentFormLastModified) > new Date(lastModified)) {
                    _postStore2.default.consentFormData.showModal = true;
                    _postStore2.default.consentFormData.filterPost = filteredPosts === 'true';
                } else {
                    _postStore2.default.consentFormData.showModal = false;
                    _postStore2.default.consentFormData.filterPost = filteredPosts === 'true';
                }
            }
        }
    }, {
        key: 'handleAccept',
        value: function handleAccept() {
            var clientId = _postStore2.default.clientData.Id;
            _jsCookie2.default.set(this.getFilteredPostField(clientId), false);
            _jsCookie2.default.set(this.getFormLastModifiedField(clientId), new Date().toUTCString());
            _postStore2.default.consentFormData.filterPost = false;
            location.reload();
        }
    }, {
        key: 'handleReject',
        value: function handleReject() {
            var clientId = _postStore2.default.clientData.Id;
            _jsCookie2.default.set(this.getFilteredPostField(clientId), true);
            _jsCookie2.default.set(this.getFormLastModifiedField(clientId), new Date().toUTCString());
            _postStore2.default.consentFormData.filterPost = true;
            location.reload();
        }
    }, {
        key: 'renderConsentForm',
        value: function renderConsentForm() {
            if (_postStore2.default.consentFormData.showModal) {
                var consentForm = document.getElementById("consentForm");
                if (!consentForm) {
                    consentForm = document.createElement('div');
                    consentForm.setAttribute("id", "consentForm");
                    _renderingOptions2.default.rootEl.appendChild(consentForm);
                }
                consentForm.innerHTML = (0, _consentForm3.default)(_postStore2.default.clientData, this.handleAccept.bind(this), this.handleReject.bind(this));
            } else {
                var _consentForm = document.getElementById("consentForm");
                if (_consentForm) _consentForm.innerHTML = '';
                document.body.style.background = 'none';
            }
        }
    }, {
        key: 'getFilteredPostField',
        value: function getFilteredPostField(clientId) {
            return 'FilteredTrackingPost_' + clientId;
        }
    }, {
        key: 'getFormLastModifiedField',
        value: function getFormLastModifiedField(clientId) {
            return 'ConsentFormLastModified_' + clientId;
        }
    }]);
    return ConsentFormService;
}();

exports.default = ConsentFormService;