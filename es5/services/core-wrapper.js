'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initialize = undefined;

var _scribbleliveToolkitCore = require('@scrbbl/scribblelive-toolkit-core');

var _scribbleliveToolkitCore2 = _interopRequireDefault(_scribbleliveToolkitCore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _core = null;

var initialize = exports.initialize = function initialize(_ref) {
  var token = _ref.token,
      options = _ref.options;

  _core = new _scribbleliveToolkitCore2.default({ token: token, options: options });
  return _core;
};

exports.default = {
  get instance() {
    if (_core === null) {
      throw new Error('Core has not been initialized');
    }

    return _core;
  }
};