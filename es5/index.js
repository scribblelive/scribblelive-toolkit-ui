'use strict';

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _es6Promise = require('es6-promise');

var _es6Promise2 = _interopRequireDefault(_es6Promise);

var _render = require('./services/render');

var _render2 = _interopRequireDefault(_render);

var _renderingOptions = require('./services/rendering-options');

var _renderingOptions2 = _interopRequireDefault(_renderingOptions);

var _coreWrapper = require('./services/core-wrapper');

var _coreWrapper2 = _interopRequireDefault(_coreWrapper);

var _postFunctions = require('./services/post-functions');

var _uiConfigService = require('./services/ui-config-service');

var _uiConfigService2 = _interopRequireDefault(_uiConfigService);

var _consentForm = require('./services/consent-form');

var _consentForm2 = _interopRequireDefault(_consentForm);

var _postStore = require('./services/post-store');

var _postStore2 = _interopRequireDefault(_postStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (typeof window !== 'undefined' && !window.Promise) {
  window.Promise = _es6Promise2.default;
}

var ScribbleToolkitUi = function () {
  function ScribbleToolkitUi(options) {
    (0, _classCallCheck3.default)(this, ScribbleToolkitUi);

    _renderingOptions2.default.init(options);
    this.Render = _render2.default;
    var token = options.token;
    (0, _coreWrapper.initialize)({ token: token, options: options });
    this.isNonTracking = options.isNonTracking;
    _uiConfigService2.default.init(options.uiOptions || {});
    _uiConfigService2.default.pageSize = options.paging ? options.paging.pageSize : 10;
    _uiConfigService2.default.newestAtBottom = options.newestAtBottom;
    var baseUrl = options.baseUrl ? options.baseUrl : 'https://api.scribblelive.com/v1';
    _consentForm2.default.init({ token: token, baseUrl: baseUrl });
  }

  (0, _createClass3.default)(ScribbleToolkitUi, [{
    key: 'init',
    value: function init(streamId) {
      var _this = this;

      _consentForm2.default.getClientData().then(function () {
        renderPosts();
      }).catch(function (e) {
        console.log(e);
        renderPosts();
      });

      var renderPosts = function renderPosts() {
        if (_renderingOptions2.default.posts && _renderingOptions2.default.posts.length > 0) {
          _this.Render.list(_renderingOptions2.default.posts);
          return;
        }
        _coreWrapper2.default.instance.Stream.loadWithOptions({ streamId: streamId,
          pageSize: _uiConfigService2.default.pageSize,
          newestAtBottom: _uiConfigService2.default.newestAtBottom }, postCallback);

        window.addEventListener('message', function (message) {
          if (message.origin.indexOf('scribblelive.com') === -1) return;
          if (!message.data.action) return;
          if (!message.data.iframeName) return;

          if (message.data.action === 'REMOVE_TRACKING_POST_SLIDESHOW') {
            var child = document.querySelector('iframe[name="' + message.data.iframeName + '"').closest('.SL-HTML');
            if (child) {
              child.parentNode.removeChild(child);
            }
          }
          if (message.data.action === 'GET_FILTERTRACKING_COOKIE') {
            var payload = {
              action: 'POST_FILTERTRACKING_COOKIE',
              value: _postStore2.default.consentFormData.filterPost ? true : false
            };
            var frm = window.frames[message.data.iframeName];
            frm.postMessage(payload, '*');
          }
        });
      };

      var postCallback = function postCallback(err, data) {
        if (err) {
          console.log(err);
          return;
        }

        if (data.adData) {

          _this.Render.adData(data.adData);
        }

        var postsWithStreamIds = (0, _postFunctions.addStreamIds)(data.posts, streamId);

        if (data.pagination && _uiConfigService2.default.currentPage <= data.pagination.TotalPages - 1) {

          _uiConfigService2.default.incrementCurrentPage();

          _coreWrapper2.default.instance.Stream.loadWithOptions({ streamId: streamId,
            pageNum: _uiConfigService2.default.currentPage,
            pageSize: _uiConfigService2.default.pageSize,
            newestAtBottom: _uiConfigService2.default.newestAtBottom }, postCallback);
        } else if (data.pagination && _uiConfigService2.default.currentPage > data.pagination.TotalPages - 1) {
          _uiConfigService2.default.incrementCurrentPage(); //increment so we don't come into this func again
          _coreWrapper2.default.instance.Stream.poll(streamId, postCallback); //start polling when everything is done
        }

        _this.Render.list(postsWithStreamIds);

        if (!_this.isNonTracking) {
          FB.XFBML.parse();
          if (twttr && twttr.widgets && twttr.widgets) {
            twttr.widgets.load();
          }
        }

        SCRBBL.post.go();
      };
    }
  }]);
  return ScribbleToolkitUi;
}();

module.exports = ScribbleToolkitUi;